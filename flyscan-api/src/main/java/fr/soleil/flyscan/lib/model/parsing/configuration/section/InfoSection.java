package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import java.util.ArrayList;
import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Info section
 * 
 * @author guerre-giordano
 * 
 */
public class InfoSection extends Section {

    /**
     * Constructor
     */
    public InfoSection() {
        super(ParsingUtil.INFO);

    }

    /**
     * Return configuration name
     * 
     * @return String
     */
    public String getConfigName() {
        return getStringValue(ParsingUtil.NAME);
    }

    /**
     * Set configuration name
     * 
     * @param configName String
     */
    public void setConfigName(String configName) {
        setStringValue(ParsingUtil.NAME, configName);
    }

    /**
     * Get author name
     * 
     * @return String
     */
    public String getAuthor() {
        return getStringValue(ParsingUtil.AUTHORS);
    }

    /**
     * Set author name
     * 
     * @param author String
     */
    public void setAuthor(String author) {
        setStringValue(ParsingUtil.AUTHORS, author);
    }

    /**
     * Return keywords
     * 
     * @return Collection<String>
     */
    public Collection<String> getKeywords() {
        Collection<String> keywords = new ArrayList<>();
        Entry entry = getEntry(ParsingUtil.KEYWORDS);
        if (entry != null) {
            String[] str = entry.getValue().split(ParsingUtil.COMMA);
            for (String s : str) {
                keywords.add(s);
            }
        }
        return keywords;
    }

    /**
     * Set keywords
     * 
     * @param keywords Collection<String>
     */
    public void setKeywords(Collection<String> keywords) {
        StringBuilder val = new StringBuilder();
        for (String str : keywords) {
            val.append(ParsingUtil.COMMA).append(str);
        }
        if (val.length() > 0) {
            // remove first ","
            val.deleteCharAt(0);
        }
        setStringValue(ParsingUtil.KEYWORDS, val.toString());
    }

    /**
     * Return comment
     * 
     * @return String
     */
    public String getComment() {
        return getStringValue(ParsingUtil.COMMENT);
    }

    /**
     * Set comment
     * 
     * @param comment String
     */
    public void setComment(String comment) {
        setStringValue(ParsingUtil.COMMENT, comment);
    }

    /**
     * Return revision
     * 
     * @return String
     */
    public String getRevision() {
        String value = getStringValue(ParsingUtil.REVISION);
        value = value.replace('{', ' ').replace('}', ' ').trim();
        return value;
    }

    /**
     * Set revision
     * 
     * @param revision String
     */
    public void setRevision(String revision) {
        setStringValue(ParsingUtil.REVISION,
                ParsingUtil.CURLY_BRACE_OPENNING + revision + ParsingUtil.CURLY_BRACE_CLOSING);
    }

    /**
     * Return config version
     * 
     * @return String
     */
    public String getConfigVersion() {
        String value = getStringValue(ParsingUtil.CONFIG_VERSION);
        value = value.replace('{', ' ').replace('}', ' ').trim();
        return value;
    }

    /**
     * Set config version
     * 
     * @param configVersion String
     */
    public void setConfigVersion(String configVersion) {
        setStringValue(ParsingUtil.CONFIG_VERSION,
                ParsingUtil.CURLY_BRACE_OPENNING + configVersion + ParsingUtil.CURLY_BRACE_CLOSING);
    }

    /**
     * Return fly scan version
     * 
     * @return String
     */
    public String getFlyScanVersion() {
        String value = getStringValue(ParsingUtil.FLYSCAN_VERSION);
        value = value.replace('{', ' ').replace('}', ' ').trim();
        return value;
    }

    /**
     * Set fly scan version
     * 
     * @param flyScanVersion String
     */
    public void setFlyScanVersion(String flyScanVersion) {
        setStringValue(ParsingUtil.FLYSCAN_VERSION,
                ParsingUtil.CURLY_BRACE_OPENNING + flyScanVersion + ParsingUtil.CURLY_BRACE_OPENNING);

    }

}
