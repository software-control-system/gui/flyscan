package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

public enum TrajectoryMode {

    ABSOLUTE("absolute"), RELATIVE("relative");

    private final String name;

    private TrajectoryMode(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
