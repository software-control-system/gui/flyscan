package fr.soleil.flyscan.lib.model.parsing.util;

import java.util.Collection;
import java.util.List;

public class ParsingTool implements ParsingUtil {

    protected static final String NAN = "nan";
    protected static final String INFINITY = "inf";
    protected static final String POSITIVE_INFINITY = "+inf";
    protected static final String NEGATIVE_INFINITY = "-inf";

    public static boolean contains(Collection<? extends Number> numberList, Number value) {
        boolean contains;
        if (numberList == null) {
            contains = false;
        } else if (numberList.isEmpty()) {
            contains = false;
        } else if (numberList.contains(value)) {
            contains = true;
        } else if (value == null) {
            contains = false;
        } else {
            contains = false;
            for (Number val : numberList) {
                if (sameValue(val, value)) {
                    contains = true;
                    break;
                }
            }
        }
        return contains;
    }

    public static int indexOf(List<? extends Number> numberList, Number value) {
        int index;
        if (numberList == null) {
            index = -1;
        } else if (numberList.isEmpty()) {
            index = -1;
        } else {
            index = numberList.indexOf(value);
            if ((index < 0) && (value != null)) {
                int i = 0;
                for (Number val : numberList) {
                    if (sameValue(val, value)) {
                        index = i;
                        break;
                    }
                    i++;
                }
            }
        }
        return index;
    }

    public static boolean sameValue(Number value1, Number value2) {
        boolean same;
        if (value1 == null) {
            same = (value2 == null);
        } else if (value2 == null) {
            same = false;
        } else if (value1.equals(value2)) {
            same = true;
        } else if (mustBeFloating(value1)) {
            if (mustBeFloating(value2)) {
                same = (value1.doubleValue() == value2.doubleValue());
            } else {
                same = false;
            }
        } else if (mustBeFloating(value2)) {
            same = false;
        } else {
            same = (value1.longValue() == value2.longValue());
        }
        return same;
    }

    public static boolean mustBeFloating(Number value) {
        boolean floating;
        if (value == null) {
            floating = false;
        } else if ((value instanceof Float) || (value instanceof Double)) {
            double val = value.doubleValue();
            floating = ((Double.isNaN(val)) || Double.isInfinite(val) || (val != Math.rint(val)));
        } else {
            floating = false;
        }
        return floating;
    }

    public static boolean isDouble(String str) {
        boolean isDouble;
        if (str == null) {
            isDouble = false;
        } else {
            str = str.trim().toLowerCase();
            isDouble = (str.indexOf('.') > 0) || (str.indexOf('e') > 0) || (str.indexOf(INFINITY) > 0)
                    || (str.indexOf(NAN) > 0);
        }
        return isDouble;
    }

    public static final Number parseNumber(String str) {
        Number result;
        if (str == null) {
            result = null;
        } else if (isDouble(str)) {
            result = Double.valueOf(parseDouble(str));
        } else {
            result = Long.valueOf(str.trim());
        }
        return result;
    }

    public static long parseLong(String str) {
        long l;
        try {
            l = Long.parseLong(str.trim());
        } catch (Exception e) {
            l = (long) Double.parseDouble(str);
        }
        return l;
    }

    public static final double parseDouble(String str) {
        double value;
        if (str == null) {
            value = Double.NaN;
        } else {
            str = str.trim();
            if (NAN.equalsIgnoreCase(str) || str.isEmpty()) {
                value = Double.NaN;
            } else if (INFINITY.equalsIgnoreCase(str) || POSITIVE_INFINITY.equalsIgnoreCase(str)) {
                value = Double.POSITIVE_INFINITY;
            } else if (NEGATIVE_INFINITY.equalsIgnoreCase(str)) {
                value = Double.NEGATIVE_INFINITY;
            } else {
                value = Double.parseDouble(str);
            }
        }
        return value;
    }

    public static final float parseFloat(String str) {
        float value;
        if ((str == null) || NAN.equalsIgnoreCase(str) || str.trim().isEmpty()) {
            value = Float.NaN;
        } else if (INFINITY.equalsIgnoreCase(str) || POSITIVE_INFINITY.equalsIgnoreCase(str)) {
            value = Float.POSITIVE_INFINITY;
        } else if (NEGATIVE_INFINITY.equalsIgnoreCase(str)) {
            value = Float.NEGATIVE_INFINITY;
        } else {
            value = Float.parseFloat(str);
        }
        return value;
    }

    public static final String toString(double value) {
        String toString;
        if (Double.isInfinite(value)) {
            if (value > 0) {
                toString = INFINITY;
            } else {
                toString = NEGATIVE_INFINITY;
            }
        } else if (Double.isNaN(value)) {
            toString = NAN;
        } else {
            toString = Double.toString(value);
        }
        return toString;
    }

    public static final String toString(float value) {
        String toString;
        if (Float.isInfinite(value)) {
            if (value > 0) {
                toString = INFINITY;
            } else {
                toString = NEGATIVE_INFINITY;
            }
        } else if (Float.isNaN(value)) {
            toString = NAN;
        } else {
            toString = Float.toString(value);
        }
        return toString;
    }

    public static final String toString(Object value) {
        String toString;
        if (value instanceof Double) {
            toString = toString(((Double) value).doubleValue());
        } else if (value instanceof Float) {
            toString = toString(((Float) value).floatValue());
        } else {
            toString = String.valueOf(value);
        }
        return toString;
    }
}
