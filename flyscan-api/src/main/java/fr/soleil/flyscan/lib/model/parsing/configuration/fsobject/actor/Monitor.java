package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class Monitor extends Actor {

    public Monitor(EntryContainer parent) {
        super(ParsingUtil.MONITOR, parent);
    }

}
