package fr.soleil.flyscan.lib.model.parsing.plugin;

/**
 * Entry type
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public enum EntryType {
    NUMBER, STRING, JSON, ENUM, DEV, DEV_ATTR, BOOL, DEV_STATE, DEV_CLASS, INT_ARRAY, INT, FLOAT, TRAJECTORY;

    private static final String BOOLEAN = "BOOLEAN";
    private static final String INTEGER = "INTEGER";
    private static final String TANGO_DEVICE = "TANGO-DEVICE";
    private static final String TANGO_DEVICE_CLASS = "TANGO-DEVICE-CLASS";
    private static final String TANGO_DEVICE_STATE = "TANGO-DEVICE-STATE";
    private static final String TANGO_DEVICE_ATTRIBUTE = "TANGO-DEVICE-ATTRIBUTE";

    public static EntryType parse(String text) {
        EntryType result = STRING;
        if (text != null) {
            String toCheck = text.trim().toUpperCase();
            if (!toCheck.isEmpty()) {
                switch (toCheck) {
                    case BOOLEAN:
                        result = BOOL;
                        break;
                    case INTEGER:
                        result = INT;
                        break;
                    case TANGO_DEVICE:
                        result = DEV;
                        break;
                    case TANGO_DEVICE_CLASS:
                        result = DEV_CLASS;
                        break;
                    case TANGO_DEVICE_STATE:
                        result = DEV_STATE;
                        break;
                    case TANGO_DEVICE_ATTRIBUTE:
                        result = DEV_ATTR;
                        break;
                    default:
                        for (EntryType type : values()) {
                            if (toCheck.equals(type.toString())) {
                                result = type;
                                break;
                            }
                        }
                        break;
                } // end switch (toCheck)
            } // end if (!toCheck.isEmpty())
        } // end if (text != null)
        return result;
    }
}
