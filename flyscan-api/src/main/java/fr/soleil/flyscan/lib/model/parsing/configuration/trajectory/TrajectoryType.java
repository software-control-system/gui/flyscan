package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

public enum TrajectoryType {
    CONTINUOUS, STEP_BY_STEP, CUSTOM
}
