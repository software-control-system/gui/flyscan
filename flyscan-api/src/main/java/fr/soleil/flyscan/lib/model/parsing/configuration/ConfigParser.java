package fr.soleil.flyscan.lib.model.parsing.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

import fr.soleil.flyscan.lib.model.ActorType;
import fr.soleil.flyscan.lib.model.constraints.ConstantConstraint;
import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.constraints.CountConstraint;
import fr.soleil.flyscan.lib.model.constraints.DeviceClassConstraint;
import fr.soleil.flyscan.lib.model.constraints.DeviceConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraintStr;
import fr.soleil.flyscan.lib.model.constraints.MinMaxConstraint;
import fr.soleil.flyscan.lib.model.constraints.hierarchical.HierarchicalConstraint;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.ContextMonitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Hook;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Monitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Sensor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.TimeBase;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.ActuatorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.BehaviorObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.ContextMonitorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.HookBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.MonitorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.SensorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.TimeBaseBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.AcquisitionSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.ActorSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.BehaviorSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.EasyConfigPropSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.EasyConfigSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.InfoSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.RecordingSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TrajectoryParser;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.plugin.PluginParser;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Singleton used to parse a config file
 * 
 * @author guerre-giordano
 * 
 */
public class ConfigParser {

    public static final int SECTION_BUFFER_LENGTH = 50;

    private ConfigParser() {
    }

    protected static String[] extractStringArray(String value) {
        return value.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).split(ParsingUtil.COMMA);
    }

    protected static List<String> extractStringList(String value) {
        String[] lStr = extractStringArray(value);
        List<String> listStr = new ArrayList<>(lStr.length);
        for (String str : lStr) {
            listStr.add(str);
        }
        return listStr;
    }

    /**
     * Parse a config file
     * 
     * @param configName
     * @param content
     * @return
     * @throws FSParsingException
     */
    public static Configuration parse(String configName, String... content) throws FSParsingException {
        configName = configName.replace(ParsingUtil.UTF8_BOM, ObjectUtils.EMPTY_STRING);
        Configuration configuration = new Configuration(configName);
        Section currentSection = null;
        FSObject currentObject = null;
        String comment = null;

        Map<Actuator, FSParsingException> trajectoryparsingExceptionBuffer = new HashMap<>();

        Section[] sections = new Section[SECTION_BUFFER_LENGTH];
        int index = 0;

        StringBuilder lineBuilder = null;
        EasyConfigSection easyConfigSection = null;
        EasyConfigPropSection easyConfigPropSection = null;
        Collection<Plugin> easyConfigPlugins = null;
        String easyProp = null;
        StringBuilder easyPropBuilder = null;
        int startIndex = -1, endIndex = -1;
        int lineIndex = -1;

        if (content != null) {
            for (String sCurrentLine : content) {
                lineIndex++;
                sCurrentLine = (sCurrentLine == null ? ObjectUtils.EMPTY_STRING : sCurrentLine.trim());
                if ((sCurrentLine.startsWith(ParsingUtil.COMMENT_START)
                        || sCurrentLine.startsWith(ParsingUtil.COMMENT_START2))) {
                    if (lineBuilder != null) {
                        lineBuilder.append(ParsingUtil.CRLF).append(sCurrentLine);
                    }
                    continue;
                }
                if (sCurrentLine.endsWith(ParsingUtil.LINE_ESCAPE)) {
                    if (lineBuilder == null) {
                        lineBuilder = new StringBuilder(sCurrentLine);
                    } else {
                        lineBuilder.append(ParsingUtil.CRLF).append(sCurrentLine);
                    }
                    continue;
                } else if (lineBuilder != null) {
                    lineBuilder.append(ParsingUtil.CRLF).append(sCurrentLine);
                    sCurrentLine = lineBuilder.toString();
                    lineBuilder = null;
                }
                if (!sCurrentLine.isEmpty()) {
                    // Handle new entry
                    sCurrentLine = sCurrentLine.replace(ParsingUtil.UTF8_BOM, ObjectUtils.EMPTY_STRING).trim();
                    if (sCurrentLine.endsWith(ParsingUtil.BRACKET_CLOSING)
                            && sCurrentLine.startsWith(ParsingUtil.BRACKET_OPENNING)) {
                        String name = sCurrentLine.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                        // Register formerly processed object and section
                        if (currentSection != null) {
                            if (currentObject != null) {
                                currentSection.addFSObject(currentObject);
                                currentObject = null;
                            }
                            sections[index++] = currentSection;
                            if (index == SECTION_BUFFER_LENGTH) {
                                configuration.addSections(sections);
                                sections = new Section[SECTION_BUFFER_LENGTH];
                                index = 0;
                            }
                        } else if (currentObject != null) {
                            throw new FSParsingException(
                                    "Fly scan object " + currentObject.getName() + " must exist within a section");
                        }
                        boolean sectionOk = false;
                        if (name.equalsIgnoreCase(ParsingUtil.EASY_CONFIG_PROP)) {
                            startIndex = lineIndex;
                            easyPropBuilder = new StringBuilder(sCurrentLine);
                            easyConfigPropSection = new EasyConfigPropSection();
                            currentSection = easyConfigPropSection;
                            sectionOk = true;
                        } else if (startIndex > -1) {
                            endIndex = lineIndex;
                            easyProp = easyPropBuilder.toString();
                            easyPropBuilder = null;
                            easyConfigPlugins = PluginParser.parse(content, startIndex, endIndex);
                            startIndex = -1;
                            endIndex = -1;
                            if (easyConfigSection != null) {
                                easyConfigSection.setPlugins(easyConfigPlugins);
                            }
                            if (easyConfigPropSection != null) {
                                easyConfigPropSection.setContent(easyProp);
                                easyConfigPropSection = null;
                            }
                            easyProp = null;
                        }

                        if (!sectionOk) {
                            // Handle new section
                            if (name.equalsIgnoreCase(ParsingUtil.ACQUISITION)) {
                                currentSection = new AcquisitionSection();
                            } else if (name.equalsIgnoreCase(ParsingUtil.ACTORS)) {
                                currentSection = new ActorSection();
                            } else if (name.equalsIgnoreCase(ParsingUtil.BEHAVIOR)) {
                                currentSection = new BehaviorSection();
                            } else if (name.equalsIgnoreCase(ParsingUtil.INFO)) {
                                currentSection = new InfoSection();
                            } else if (name.equalsIgnoreCase(ParsingUtil.RECORDING)) {
                                currentSection = new RecordingSection();
                            } else if (name.equalsIgnoreCase(ParsingUtil.EASY_CONIG)) {
                                easyConfigSection = new EasyConfigSection();
                                easyConfigSection.setPlugins(easyConfigPlugins);
                                currentSection = easyConfigSection;
                            } else {
                                currentSection = new Section(name);
                            }
                        }

                    } else if (startIndex > -1) {
                        easyPropBuilder.append(ParsingUtil.CRLF).append(sCurrentLine);
                        continue;
                    } else if (sCurrentLine.endsWith(ParsingUtil.COLON)) {
                        String name = sCurrentLine.replace(ParsingUtil.COLON, ObjectUtils.EMPTY_STRING).trim();
                        if (currentSection == null) {
                            throw new FSParsingException("Fly scan object " + name + " must exist within a section");
                        } else {
                            if (currentObject != null) {
                                // hyphen is optional.
                                // Register formerly processed object
                                finalizeFSObjectTreatment(currentObject, currentSection,
                                        trajectoryparsingExceptionBuffer);
                                // Release currentObject object
                                currentObject = null;
                            }
                            // Handle new object
                            if (currentSection instanceof BehaviorSection) {
                                if (name.equalsIgnoreCase(ParsingUtil.CONTEXT_MONITOR)) {
                                    currentObject = new ContextMonitorBehavior(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.ACTUATOR)) {
                                    currentObject = new ActuatorBehavior(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.SENSOR)) {
                                    currentObject = new SensorBehavior(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.TIMEBASE)) {
                                    currentObject = new TimeBaseBehavior(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.HOOK)) {
                                    currentObject = new HookBehavior(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.MONITOR)) {
                                    currentObject = new MonitorBehavior(currentSection);
                                }
                            } else if (currentSection instanceof ActorSection) {
                                if (name.equalsIgnoreCase(ParsingUtil.CONTEXT_MONITOR)) {
                                    currentObject = new ContextMonitor(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.ACTUATOR)
                                        || name.equalsIgnoreCase(ParsingUtil.CONTINUOUS_ACTUATOR)) {
                                    currentObject = new Actuator(name, currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.SENSOR)) {
                                    currentObject = new Sensor(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.TIMEBASE)) {
                                    currentObject = new TimeBase(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.HOOK)) {
                                    currentObject = new Hook(currentSection);
                                } else if (name.equalsIgnoreCase(ParsingUtil.MONITOR)) {
                                    currentObject = new Monitor(currentSection);
                                } else if (name.startsWith(ParsingUtil.CONSTRAINT)) {
                                    // Hierarchical constraint case processing
                                    currentObject = new HierarchicalConstraint(currentSection);
                                    String[] parts = name.split("/");
                                    if (parts.length == 3) {
                                        String type = parts[1].trim();
                                        String actorName = parts[2].trim();
                                        ((HierarchicalConstraint) currentObject).setActorName(actorName);
                                        ((HierarchicalConstraint) currentObject).setActorType(ActorType.valueOf(type));

                                    }

                                }
                            } else {
                                currentObject = new FSObject(name, currentSection);
                            }
                        }

                    } else if (sCurrentLine.contains(ParsingUtil.EQUALS)) {
                        String[] entryPair = sCurrentLine.split(ParsingUtil.EQUALS);
                        String key = entryPair[0].trim();
                        // Method used to prevent bug in case the value of the entry contains '=' character
                        StringBuilder valueSB = new StringBuilder();
                        for (int i = 1; i < entryPair.length; i++) {
                            valueSB.append(entryPair[i]);
                        }
                        String value = valueSB.toString().trim();

                        if ((currentSection == null) && (currentObject == null)) {
                            throw new FSParsingException("Entry " + key + " must exist within a section or an object");
                        } else {
                            Entry entry = null;
                            if (key.startsWith(ParsingUtil.PARAMETER + ParsingUtil.COLON)) {
                                // Entry entry = null;
                                String[] keyParts = key.split(ParsingUtil.COLON);
                                if (keyParts.length > 2) {
                                    // Expected encoding: "parameter:name:keyword"
                                    String name = keyParts[1].trim();
                                    String lastPart = keyParts[2].trim();
                                    if (ParsingUtil.LEVEL.equalsIgnoreCase(lastPart)) {
                                        if (currentObject != null) {
                                            entry = getExistingEntry(currentObject, name);
                                        } else if (currentSection != null) {
                                            entry = getExistingEntry(currentSection, name);
                                        }
                                        if (entry != null) {
                                            entry.setExpert(true);
                                        }
                                    } else {
                                        if (currentObject != null) {
                                            entry = getExistingParameterEntry(currentObject, name);
                                        } else if (currentSection != null) {
                                            entry = getExistingParameterEntry(currentSection, name);
                                        }
                                        if (entry == null) {
                                            entry = new ParameterEntry();
                                        }
                                        entry.setKey(name);
                                        Constraint constraint = null;
                                        if (ParsingUtil.VALUE.equalsIgnoreCase(lastPart)) {
                                            entry.setValue(value);
                                            List<Constraint> constraints = entry.getConstraints();
                                            if (constraints != null) {
                                                for (Constraint cons : constraints) {
                                                    if (cons instanceof ConstantConstraint) {
                                                        ((ConstantConstraint) cons).setConstant(value);
                                                    }
                                                }
                                            }
                                            // SCAN-603, SCAN-442: new constraint format
                                        } else if (ParsingUtil.CONSTANT_KEYWORD.equalsIgnoreCase(lastPart)) {
                                            constraint = new ConstantConstraint(false);
                                        } else if (lastPart.equalsIgnoreCase(ParsingUtil.IN_LIST_KEYWORD)) {
                                            String[] lStr = extractStringArray(value);
                                            List<Number> numberList = new ArrayList<>(lStr.length);
                                            boolean isDouble = ParsingTool.isDouble(value);
                                            if (isDouble) {
                                                for (String s : lStr) {
                                                    try {
                                                        Double d = Double.valueOf(ParsingTool.parseDouble(s.trim()));
                                                        numberList.add(d);
                                                    } catch (NumberFormatException e) {
                                                        List<String> listStr = new ArrayList<>(lStr.length);
                                                        for (String str : lStr) {
                                                            listStr.add(str.trim());
                                                        }
                                                        constraint = new InConstraintStr(false, listStr);
                                                        numberList.clear();
                                                        break;
                                                    }
                                                }
                                            } else {
                                                for (String s : lStr) {
                                                    try {
                                                        Long l = Long.valueOf(s.trim());
                                                        numberList.add(l);
                                                    } catch (NumberFormatException e) {
                                                        List<String> listStr = new ArrayList<>(lStr.length);
                                                        for (String str : lStr) {
                                                            listStr.add(str.trim());
                                                        }
                                                        constraint = new InConstraintStr(false, listStr);
                                                        numberList.clear();
                                                        break;
                                                    }
                                                }
                                            }
                                            if (constraint == null) {
                                                constraint = new InConstraint(false, numberList);
                                            }
                                        } else if (lastPart.equalsIgnoreCase(ParsingUtil.IN_RANGE_KEYWORD)) {
                                            String[] lStr = extractStringArray(value);
                                            if (lStr.length != 2) {
                                                throw new FSParsingException(
                                                        "Can not parse in range constraint value: " + value);
                                            }
                                            try {
                                                Double min = ParsingTool.parseDouble(lStr[0].trim());
                                                Double max = ParsingTool.parseDouble(lStr[1].trim());
                                                constraint = new MinMaxConstraint(false, min, max);
                                            } catch (NumberFormatException e) {
                                                throw new FSParsingException(
                                                        "Failed to parse number value for parameter: " + key
                                                                + " (value=" + value + ")",
                                                        e);
                                            }
                                        } else if (lastPart.equalsIgnoreCase(ParsingUtil.FROM_CLASS)) {
                                            constraint = new DeviceClassConstraint(false, extractStringList(value));
                                        } else if (lastPart.equalsIgnoreCase(ParsingUtil.FROM_DEVICE)) {
                                            constraint = new DeviceConstraint(false, extractStringList(value));
                                        } else if (lastPart.equalsIgnoreCase(ParsingUtil.COUNT)) {
                                            try {
                                                int count = Integer.parseInt(value);
                                                if (count < 1) {
                                                    throw new FSParsingException(
                                                            "Invalid count constraint for parameter: " + key
                                                                    + " (count=" + value + ")");
                                                }
                                                constraint = new CountConstraint(false, count);
                                            } catch (NumberFormatException e) {
                                                throw new FSParsingException("Invalid count constraint for parameter: "
                                                        + key + " (count=" + value + ")", e);
                                            }
                                        }
                                        if ((constraint != null) && (entry instanceof ParameterEntry)) {
                                            ((ParameterEntry) entry).addConstraint(constraint);
                                        }
                                        if (entry != null) {
                                            registerEntry(entry, currentObject, currentSection);
                                        }
                                    }
                                }
                            } else if ((currentSection instanceof AcquisitionSection) && (currentObject == null)) {
                                if (key.equalsIgnoreCase(ParsingUtil.CONTINUOUS_RANGES)
                                        || key.equalsIgnoreCase(ParsingUtil.PERIOD)) {
                                    entry = new PredefinedEntry(key, value, EntryType.INT);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.INFINITE_MODE)
                                        || key.equalsIgnoreCase(ParsingUtil.MANUAL_MODE)
                                        || key.equalsIgnoreCase(ParsingUtil.ZIGZAG)) {
                                    entry = new PredefinedEntry(key, value, EntryType.BOOL);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.SCAN_MODE)) {
                                    entry = new PredefinedEntry(key, value, EntryType.STRING);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.CONTINUOUS)) {
                                    entry = new PredefinedEntry(key, value, EntryType.BOOL);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.DIMENSIONS)) {
                                    entry = new PredefinedEntry(key, value, EntryType.INT_ARRAY);
                                    registerEntry(entry, currentObject, currentSection);
                                } else {
                                    entry = new Entry(key, value, false);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if ((currentSection instanceof BehaviorSection) && (currentObject == null)) {
                                if (ParsingUtil.ERROR_STRATEGY.equalsIgnoreCase(key)
                                        || ParsingUtil.ORDER_STRATEGY.equalsIgnoreCase(key)) {
                                    entry = new PredefinedEntry(key, value, EntryType.ENUM);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if ((currentSection instanceof InfoSection) && (currentObject == null)) {
                                if (key.equalsIgnoreCase(ParsingUtil.AUTHORS)
                                        || key.equalsIgnoreCase(ParsingUtil.COMMENT)
                                        || key.equalsIgnoreCase(ParsingUtil.NAME)
                                        || key.equalsIgnoreCase(ParsingUtil.CONFIG_VERSION)
                                        || key.equalsIgnoreCase(ParsingUtil.FLYSCAN_VERSION)
                                        || key.equalsIgnoreCase(ParsingUtil.KEYWORDS)
                                        || key.equalsIgnoreCase(ParsingUtil.REVISION)
                                        || key.equalsIgnoreCase(ParsingUtil.VERSION)) {
                                    entry = new PredefinedEntry(key, value, EntryType.STRING);
                                    registerEntry(entry, currentObject, currentSection);
                                } else {
                                    entry = new Entry(key, value, false);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if ((currentSection instanceof RecordingSection) && (currentObject == null)) {
                                if (key.equalsIgnoreCase(ParsingUtil.SPLIT)
                                        || key.equalsIgnoreCase(ParsingUtil.MAX_SCAN_LINES_PER_FILE)
                                        || key.equalsIgnoreCase(ParsingUtil.MAX_STEPS_PER_FILE)) {
                                    entry = new PredefinedEntry(key, value, EntryType.INT);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if ((currentSection instanceof EasyConfigSection) && (currentObject == null)) {
                                if (ParsingUtil.ENABLE.equalsIgnoreCase(key)) {
                                    entry = new PredefinedEntry(key, value, EntryType.BOOL);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (ParsingUtil.CONFIGURATOR.equalsIgnoreCase(key)) {
                                    entry = new PredefinedEntry(key, value, EntryType.STRING);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if ((currentObject != null)
                                    && ParsingUtil.BUFFER.equalsIgnoreCase(currentObject.getName())) {
                                if (key.equalsIgnoreCase(ParsingUtil.NAME)) {
                                    entry = new PredefinedEntry(key, value, EntryType.STRING);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if (currentObject instanceof Actor) {
                                if (key.equalsIgnoreCase(ParsingUtil.IDENTIFIER)
                                        || key.equalsIgnoreCase(ParsingUtil.NAME)) {
                                    entry = new PredefinedEntry(key, value, EntryType.STRING);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.DIMENSION)) {
                                    entry = new PredefinedEntry(key, value, EntryType.INT);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.ENABLE)) {
                                    entry = new PredefinedEntry(key, value, EntryType.BOOL);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.ORDER)) {
                                    entry = new PredefinedEntry(key, value, EntryType.INT);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (key.equalsIgnoreCase(ParsingUtil.TYPE)) {
                                    entry = new PredefinedEntry(key, value, EntryType.STRING);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (currentObject instanceof Actuator) {
                                    Actuator actuator = (Actuator) currentObject;
                                    if (key.equalsIgnoreCase(ParsingUtil.TRAJECTORY)) {
                                        String className = actuator.getClass().getSimpleName();
                                        entry = new TrajectoryEntry(key, value);
                                        Trajectory trajectory = TrajectoryParser.parse(configName, value,
                                                className + " " + actuator.getName());
                                        ((TrajectoryEntry) entry).setTrajectory(trajectory);
                                        registerEntry(entry, currentObject, currentSection);
                                    } else if (key.equalsIgnoreCase(ParsingUtil.TRAJECTORY_MODE)) {
                                        entry = new PredefinedEntry(key, value, EntryType.ENUM);
                                        registerEntry(entry, currentObject, currentSection);
                                    } else if (key.equalsIgnoreCase(ParsingUtil.POST_SCAN_ACTION)) {
                                        entry = new PredefinedEntry(key, value, EntryType.ENUM);
                                        registerEntry(entry, currentObject, currentSection);
                                    } else {
                                        entry = new Entry(key, value, false);
                                        registerEntry(entry, currentObject, currentSection);
                                    }
                                } else if (currentObject instanceof Hook) {
                                    if (key.equalsIgnoreCase(ParsingUtil.ACTION)) {
                                        entry = new PredefinedEntry(key, value, EntryType.ENUM);
                                        registerEntry(entry, currentObject, currentSection);
                                    }
                                } else {
                                    entry = new Entry(key, value, false);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if (currentObject instanceof BehaviorObject) {
                                if (ParsingUtil.DELAY.equalsIgnoreCase(key)
                                        || ParsingUtil.TIMEOUT.equalsIgnoreCase(key)) {
                                    entry = new PredefinedEntry(key, value, EntryType.INT);
                                    registerEntry(entry, currentObject, currentSection);
                                } else if (ParsingUtil.ERROR_STRATEGY.equalsIgnoreCase(key)
                                        || ParsingUtil.ORDER_STRATEGY.equalsIgnoreCase(key)) {
                                    entry = new PredefinedEntry(key, value, EntryType.ENUM);
                                    registerEntry(entry, currentObject, currentSection);
                                } else {
                                    entry = new Entry(key, value, false);
                                    registerEntry(entry, currentObject, currentSection);
                                }
                            } else if (currentObject instanceof HierarchicalConstraint) {
                                if (key.endsWith("enum")) {
                                    List<String> possibleValues = Arrays
                                            .asList(value.replaceAll(" ", ObjectUtils.EMPTY_STRING).split(","));
                                    Constraint inConstraint = new InConstraintStr(false, possibleValues);
                                    ((HierarchicalConstraint) currentObject).setConstraint(inConstraint);
                                } else if (key.endsWith("min")) {
                                    Constraint constraint = ((HierarchicalConstraint) currentObject).getConstraint();
                                    if (constraint == null) {
                                        constraint = new MinMaxConstraint(false);
                                    }
                                    ((MinMaxConstraint) constraint).setMin(ParsingTool.parseDouble(value.trim()));
                                    ((HierarchicalConstraint) currentObject).setConstraint(constraint);
                                } else if (key.endsWith("max")) {
                                    Constraint constraint = ((HierarchicalConstraint) currentObject).getConstraint();
                                    if (constraint == null) {
                                        constraint = new MinMaxConstraint(false);
                                    }
                                    ((MinMaxConstraint) constraint).setMax(ParsingTool.parseDouble(value.trim()));
                                    ((HierarchicalConstraint) currentObject).setConstraint(constraint);
                                }
                            } else {
                                /*Entry */entry = new Entry(key, value, false);
                                registerEntry(entry, currentObject, currentSection);
                            }
                            if (entry != null && comment != null) {
                                entry.setComment(comment);
                                comment = null;
                            }
                        }
                    } else if (sCurrentLine.contains(ParsingUtil.COLON)
                            && sCurrentLine.endsWith(ParsingUtil.COLON + ParsingUtil.CONSTANT_KEYWORD)
                            && sCurrentLine.startsWith(ParsingUtil.PARAMETER + ParsingUtil.COLON)) {
                        String[] keyParts = sCurrentLine.split(ParsingUtil.COLON);
                        if (keyParts.length == 3) {
                            String name = keyParts[1].trim();
                            Entry entry = null;
                            if ((currentSection == null) && (currentObject == null)) {
                                throw new FSParsingException(
                                        "Entry " + sCurrentLine + " must exist within a section or an object");
                            } else if (currentObject != null) {
                                entry = getExistingEntry(currentObject, name);
                            } else {
                                entry = getExistingEntry(currentSection, name);
                            }
                            if (entry instanceof ParameterEntry) {
                                ConstantConstraint constraint = new ConstantConstraint(false);
                                String value = entry.getValue();
                                if ((value != null) && (!value.trim().isEmpty())) {
                                    constraint.setConstant(value);
                                }
                                entry.addConstraint(constraint);
                            }
                        }
                    } else if (sCurrentLine.equalsIgnoreCase(ParsingUtil.HYPHEN)) {
                        // Register formerly processed object
                        finalizeFSObjectTreatment(currentObject, currentSection, trajectoryparsingExceptionBuffer);
                        // Release currentObject object
                        currentObject = null;
                    } else if (sCurrentLine.startsWith("#")) {
                        comment = sCurrentLine.replace("#", ObjectUtils.EMPTY_STRING).trim();
                    }
                }
            } // end for (String sCurrentLine : content)
        } // end if (content != null)
        if (startIndex > -1) {
            endIndex = lineIndex + 1;
            easyProp = easyPropBuilder.toString();
            easyPropBuilder = null;
            easyConfigPlugins = PluginParser.parse(content, startIndex, endIndex);
            startIndex = -1;
            endIndex = -1;
            if (easyConfigSection != null) {
                easyConfigSection.setPlugins(easyConfigPlugins);
            }
            if (easyConfigPropSection != null) {
                easyConfigPropSection.setContent(easyProp);
                easyConfigPropSection = null;
            }
            easyProp = null;
        }
        if (currentSection != null) {
            finalizeFSObjectTreatment(currentObject, currentSection, trajectoryparsingExceptionBuffer);
            currentObject = null;
            sections[index++] = currentSection;
            if (index == SECTION_BUFFER_LENGTH) {
                configuration.addSections(sections);
                sections = new Section[SECTION_BUFFER_LENGTH];
                index = 0;
            }
        }
        if (index != 0) {
            configuration.addSections(sections);
        }
        return configuration;
    }

    protected static void finalizeFSObjectTreatment(FSObject currentObject, Section currentSection,
            Map<Actuator, FSParsingException> trajectoryparsingExceptionBuffer) throws FSParsingException {
        if (currentObject != null) {
            currentSection.addFSObject(currentObject);
            if (currentObject instanceof Actuator) {
                // If a FSParsingException occurred while trying to parse a trajectory, throw it
                FSParsingException fsParsingException = trajectoryparsingExceptionBuffer.get(currentObject);
                if (fsParsingException != null) {
                    throw fsParsingException;
                }
            }
        }
    }

    public static StringBuilder deparse(Configuration configuration, Collection<Plugin> plugins,
            StringBuilder builder) {
        StringBuilder sb = builder == null ? new StringBuilder() : builder;
        for (Section section : configuration.getSections()) {
            Collection<Plugin> pluginsToUse = plugins;
            boolean contentOk = false;
            if (section instanceof EasyConfigPropSection) {
                String content = ((EasyConfigPropSection) section).getContent();
                if ((content != null) && (!content.trim().isEmpty())) {
                    contentOk = true;
                    sb.append(content).append(ParsingUtil.CRLF);
                }
            }
            if (section instanceof EasyConfigSection) {
                pluginsToUse = ((EasyConfigSection) section).getPlugins();
            }
            if (!contentOk) {
                sb.append(ParsingUtil.BRACKET_OPENNING).append(section.getName()).append(ParsingUtil.BRACKET_CLOSING);
                sb.append(ParsingUtil.CRLF);
                for (Entry entry : section.getEntries()) {
                    writeEntry(sb, entry, null);
                }
                for (FSObject obj : section.getObjects()) {
                    if (obj != null) {
                        Plugin plugin = null;
                        if (pluginsToUse != null) {
                            String type = obj.getType().trim();
                            for (Plugin p : pluginsToUse) {
                                String pluginName = p.getName().trim();
                                if (pluginName.equalsIgnoreCase(type)) {
                                    plugin = p;
                                }
                            }
                        }
                        if (obj instanceof Actor) {
                            if (obj instanceof ContextMonitor) {
                                sb.append(ParsingUtil.CONTEXT_MONITOR).append(ParsingUtil.COLON);
                                sb.append(ParsingUtil.CRLF);
                            } else if (obj instanceof Actuator) {
                                sb.append(ParsingUtil.ACTUATOR).append(ParsingUtil.COLON);
                                sb.append(ParsingUtil.CRLF);
                            } else if (obj instanceof Sensor) {
                                sb.append(ParsingUtil.SENSOR).append(ParsingUtil.COLON);
                                sb.append(ParsingUtil.CRLF);
                            } else if (obj instanceof TimeBase) {
                                sb.append(ParsingUtil.TIMEBASE).append(ParsingUtil.COLON);
                                sb.append(ParsingUtil.CRLF);
                            } else if (obj instanceof Hook) {
                                sb.append(ParsingUtil.HOOK).append(ParsingUtil.COLON);
                                sb.append(ParsingUtil.CRLF);
                            } else if (obj instanceof Monitor) {
                                sb.append(ParsingUtil.MONITOR).append(ParsingUtil.COLON);
                                sb.append(ParsingUtil.CRLF);
                            }
                        } else {
                            sb.append(obj.getName()).append(ParsingUtil.COLON);
                            sb.append(ParsingUtil.CRLF);
                        }

                        for (Entry entry : obj.getEntries()) {
                            writeEntry(sb, entry, plugin);
                        }
                    }
                    sb.append(ParsingUtil.HYPHEN);
                    sb.append(ParsingUtil.CRLF);
                } // end for (FSObject obj : section.getObjects())

                // remove too numerous CRLF
                while (sb.lastIndexOf(ParsingUtil.CRLF) == sb.length() - ParsingUtil.CRLF.length()) {
                    sb.delete(sb.length() - ParsingUtil.CRLF.length(), sb.length());
                }
                // ensure section ends with "-" (SCAN-388)
                if (sb.lastIndexOf(ParsingUtil.HYPHEN) != sb.length() - ParsingUtil.HYPHEN.length()) {
                    sb.append(ParsingUtil.CRLF).append(ParsingUtil.HYPHEN);
                }
                sb.append(ParsingUtil.CRLF);
            }
        }
        return sb;
    }

    public static String deparse(Configuration configuration, Collection<Plugin> plugins) {
        return deparse(configuration, plugins, null).toString();
    }

    protected static void writeEntry(StringBuilder sb, Entry entry, Plugin plugin) {
        // While unparsing, do not write entry with unchanged default value
        if ((entry != null) && entry.isPresentInConfig()) {
            if (entry.getComment() != null && !entry.getComment().isEmpty()) {
                sb.append(ParsingUtil.COMMENT_START_SPACE).append(entry.getComment());
                sb.append(ParsingUtil.CRLF);
            }
            String key = entry.getKey();
            boolean parameterKey = false;
            if (entry instanceof ParameterEntry) {
                ParameterEntry parameterEntry = (ParameterEntry) entry;
                for (Constraint constraint : parameterEntry.getConstraints()) {
                    final String textConstraint = constraint.getText();
                    // JIRA SCAN-282
                    // Do not generate a constraint unless it has been created or modified within the configuration
                    if (!constraint.isFromPlugin()) {
                        appendParameterKeyWordToStringBuilder(sb, key, textConstraint);
                        sb.append(ParsingUtil.CRLF);
                    }
                }
                parameterKey = true;
            } // end if (entry instanceof ParameterEntry)
            if (parameterKey) {
                appendParameterKeyWordToStringBuilder(sb, key, ParsingUtil.VALUE);
            } else {
                sb.append(key);
            }
            appendEqualsToStringBuilder(sb, entry.getValue());
            sb.append(ParsingUtil.CRLF);
            if (entry.isExpert()) {
                appendEqualsToStringBuilder(appendParameterKeyWordToStringBuilder(sb, key, ParsingUtil.LEVEL),
                        ParsingUtil.EXPERT);
                sb.append(ParsingUtil.CRLF);
            }

        }

    }

    protected static StringBuilder appendParameterKeyWordToStringBuilder(StringBuilder sb, String parameterName,
            String keyWord) {
        if (sb == null) {
            sb = new StringBuilder();
        }
        return sb.append(ParsingUtil.PARAMETER).append(ParsingUtil.COLON).append(parameterName)
                .append(ParsingUtil.COLON).append(keyWord);
    }

    protected static StringBuilder appendEqualsToStringBuilder(StringBuilder sb, String value) {
        if (sb == null) {
            sb = new StringBuilder();
        }
        return sb.append(' ').append(ParsingUtil.EQUALS).append(' ').append(value);
    }

    /**
     * Add entry to the EntryContainer currently being edited
     * 
     * @param entry Entry
     * @param currentObject FSObject
     * @param currentSection Section
     */
    protected static void registerEntry(Entry entry, FSObject currentObject, Section currentSection) {
        EntryContainer container = currentObject == null ? currentSection : currentObject;
        container.addEntry(entry);
    }

    /**
     * Since parameter type entry can be written on several lines, check whether parameter entry was already
     * declared
     * 
     * @param entryContainer
     * @param name
     * @return
     */
    protected static ParameterEntry getExistingParameterEntry(EntryContainer entryContainer, String name) {
        ParameterEntry entry = null;
        if (entryContainer != null) {
            Entry tmp = entryContainer.getEntry(name);
            if (tmp instanceof ParameterEntry) {
                entry = (ParameterEntry) tmp;
            }
        }
        return entry;
    }

    /**
     * Retrieve existing entry using its key
     * 
     * @param entryContainer EntryContainer
     * @param name String
     * @return Entry
     */
    protected static Entry getExistingEntry(EntryContainer entryContainer, String name) {
        Entry entry = null;
        if (entryContainer != null) {
            entry = entryContainer.getEntry(name);
        }
        return entry;
    }

    protected static boolean isOldDimension(String value) {
        return value.replace(" ", ObjectUtils.EMPTY_STRING).indexOf("][") > -1;
    }

    protected static String[] extractDimensionStringArray(String value, boolean oldDimensions) {
        String[] valStr;
        if (oldDimensions) {
            valStr = value.split(ParsingUtil.BRACKET_CLOSING);
        } else {
            valStr = value.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).split(ParsingUtil.COMMA);
        }
        return valStr;
    }

    protected static String getParsableDimension(String str, boolean oldDimensions) {
        if (oldDimensions) {
            str = str.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING);
        }
        return str.trim();
    }

    /**
     * Return scan dimensions
     * 
     * @return int[]
     */
    public static int[] getScanDimensions(Entry entry, Logger logger) {
        int[] val = null;
        if ((entry != null) && ParsingUtil.DIMENSIONS.equalsIgnoreCase(entry.getKey())) {
            String value = entry.getValue();
            boolean oldDimensions = isOldDimension(value);
            String[] valStr = extractDimensionStringArray(value, oldDimensions);
            val = new int[valStr.length];
            int i = 0;
            for (String str : valStr) {
                try {
                    str = getParsableDimension(str, oldDimensions);
                    val[i] = Integer.parseInt(str);
                } catch (Exception e) {
                    try {
                        val[i] = (int) ParsingTool.parseDouble(str);
                    } catch (ExceptionInInitializerError e2) {
                        if (logger != null) {
                            logger.warn("Inappropriate value for scan dimension: " + str);
                        }
                    }
                }
                i++;
            }
        }
        return val;
    }

    public static boolean recoverScanDimensions(Entry entry, Collection<Long> dimensionList, Logger logger) {
        boolean oldDimensions = false;
        if ((entry != null) && ParsingUtil.DIMENSIONS.equalsIgnoreCase(entry.getKey())) {
            String value = entry.getValue();
            oldDimensions = isOldDimension(value);
            String[] valStr = extractDimensionStringArray(value, oldDimensions);
            Long currentDim;
            for (String str : valStr) {
                try {
                    str = getParsableDimension(str, oldDimensions);
                    currentDim = Long.valueOf(str);
                } catch (Exception e) {
                    try {
                        currentDim = Long.valueOf((long) ParsingTool.parseDouble(str));
                    } catch (ExceptionInInitializerError e2) {
                        currentDim = Long.valueOf(0);
                        if (logger != null) {
                            logger.warn("Inappropriate value for scan dimension: " + str);
                        }
                    }
                }
                dimensionList.add(currentDim);
            }
        }
        return oldDimensions;
    }

}
