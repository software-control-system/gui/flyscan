package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject;

import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.ERROR_STRATEGY;
import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.ORDER_STRATEGY;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Fly scan object
 * 
 * @author guerre-giordano
 * 
 */
public class FSObject extends EntryContainer {

    protected EntryContainer parent;

    /**
     * Constructor
     * 
     * @param name String
     */
    public FSObject(String name, EntryContainer parent) {
        super(name);
        this.parent = parent;
    }

    @Override
    public void addEntry(Entry entry) {
        if (entry.getKey().trim().equalsIgnoreCase(ParsingUtil.NAME)) {
            setName(entry.getValue().trim());
        }
        super.addEntry(entry);
    }

    public EntryContainer getParent() {
        return parent;
    }

    public void setParent(EntryContainer parent) {
        this.parent = parent;
    }

    protected void addErrorStrategy(String errorStrategy) {
        if (errorStrategy != null) {
            Entry errorStrategyEntry = new Entry(ERROR_STRATEGY, errorStrategy);
            addEntry(errorStrategyEntry);
        }
    }

    protected void addOrderStrategy(String orderStrategy) {
        if (orderStrategy != null) {
            Entry orderStrategyEntry = new Entry(ORDER_STRATEGY, orderStrategy);
            addEntry(orderStrategyEntry);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Object ").append(name);
        sb.append(ParsingUtil.CRLF);
        for (Entry entry : entries) {
            sb.append(entry.toString());

        }
        return sb.toString();
    }

}
