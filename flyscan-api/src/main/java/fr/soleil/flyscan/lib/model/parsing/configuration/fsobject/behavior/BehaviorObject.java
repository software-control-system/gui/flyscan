package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.ERROR_STRATEGY;
import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.ORDER_STRATEGY;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Generic behavior object
 * 
 * @author guerre-giordano
 * 
 */
public abstract class BehaviorObject extends FSObject {

    /**
     * Constructor
     * 
     * @param name String
     */
    public BehaviorObject(String name, EntryContainer parent) {
        super(name, parent);
    }

    public long getTimeout() {
        return getLongValue(ParsingUtil.TIMEOUT);
    }

    public void setTimeout(long timeout) {
        setStringValue(ParsingUtil.TIMEOUT, Long.toString(timeout));
    }

    public long getDelay() {
        return getLongValue(ParsingUtil.DELAY);
    }

    public void setDelay(long delay) {
        setStringValue(ParsingUtil.DELAY, Long.toString(delay));
    }

    public ErrorStrategy getErrorStrategy() {
        String val = ObjectUtils.EMPTY_STRING;
        Entry entry = getEntry(ERROR_STRATEGY);
        if (entry != null) {
            val = entry.getValue();
        }
        return ErrorStrategy.valueOf(val);
    }

    public void setErrorStrategy(ErrorStrategy errorStrategy) {
        Entry entry = getEntry(ERROR_STRATEGY);
        if (entry != null) {
            entry.setValue(errorStrategy.name().toLowerCase());
        }
    }

    public OrderStrategy getOrderStrategy() {
        String val = ObjectUtils.EMPTY_STRING;
        Entry entry = getEntry(ORDER_STRATEGY);
        if (entry != null) {
            val = entry.getValue();
        }
        return OrderStrategy.valueOf(val);
    }

    public void setOrderStrategy(OrderStrategy errorStrategy) {
        Entry entry = getEntry(ORDER_STRATEGY);
        if (entry != null) {
            entry.setValue(errorStrategy.name().toLowerCase());
        }
    }

}
