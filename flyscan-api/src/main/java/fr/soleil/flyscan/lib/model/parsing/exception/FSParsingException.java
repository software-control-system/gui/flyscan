package fr.soleil.flyscan.lib.model.parsing.exception;

/***
 * Fly scan parsing exception
 * 
 * @author guerre-giordano
 * 
 */
public class FSParsingException extends Exception {

    private static final long serialVersionUID = 5700704768185019450L;

    /**
     * Constructor
     * 
     * @param message String
     */
    public FSParsingException(String message) {
        super(message);
    }

    /**
     * Constructor
     * 
     * @param message String
     * @param t Throwable
     */
    public FSParsingException(String message, Throwable t) {
        super(message, t);
    }

}
