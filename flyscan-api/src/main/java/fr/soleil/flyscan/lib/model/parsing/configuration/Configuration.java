package fr.soleil.flyscan.lib.model.parsing.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Configuration of a scan
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public class Configuration {

    private String name;
    private final Collection<Section> sections;

    /**
     * Constructor
     */
    public Configuration() {
        this(new LinkedHashSet<>());
    }

    /**
     * Constructor
     * 
     * @param name String
     */
    public Configuration(String name) {
        this();
        this.name = name;
    }

    /**
     * Constructor
     * 
     * @param sections Collection<Section>
     */
    public Configuration(Collection<Section> sections) {
        super();
        this.sections = sections;
    }

    public Collection<Section> getSections() {
        Collection<Section> sections;
        synchronized (this.sections) {
            sections = new ArrayList<>(this.sections);
        }
        return sections;
    }

    public void addSections(Section... sections) {
        if (sections != null) {
            synchronized (this.sections) {
                for (Section section : sections) {
                    if (section != null) {
                        this.sections.add(section);
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Configuration");
        sb.append(ParsingUtil.CRLF);
        for (Section section : sections) {
            sb.append(section.toString());
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Configuration config = (Configuration) obj;
            equals = config.getSections().equals(getSections());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xCE;
        int mult = 0xCF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getSections());
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
