package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.CONTEXT_MONITOR;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;

/**
 * Context monitor object
 * 
 * @author guerre-giordano
 * 
 */
public class ContextMonitorBehavior extends BehaviorObject {

    /**
     * Constructor
     */
    public ContextMonitorBehavior(EntryContainer parent) {
        super(CONTEXT_MONITOR, parent);
    }

}
