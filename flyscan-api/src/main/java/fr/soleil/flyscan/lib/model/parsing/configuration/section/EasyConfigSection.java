package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class EasyConfigSection extends Section {

    protected Collection<Plugin> plugins;

    public EasyConfigSection() {
        super(ParsingUtil.EASY_CONIG);
    }

    public Collection<Plugin> getPlugins() {
        return plugins;
    }

    public void setPlugins(Collection<Plugin> plugins) {
        this.plugins = plugins;
    }

}
