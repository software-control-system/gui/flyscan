package fr.soleil.flyscan.lib.model.constraints;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class CountConstraint extends Constraint {

    protected int value;

    public CountConstraint(boolean fromPlugin, int value) {
        super(fromPlugin);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    protected String getType() {
        return ParsingUtil.COUNT;
    }

    @Override
    protected StringBuilder appendValueToStringBuilder(StringBuilder builder) {
        StringBuilder sb = getInitializedStringBuilder(builder);
        sb.append(value);
        return sb;
    }

}
