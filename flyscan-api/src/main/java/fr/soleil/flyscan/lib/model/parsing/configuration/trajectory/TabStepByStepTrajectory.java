package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

import java.util.List;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class TabStepByStepTrajectory implements IStepByStepTrajectory {

    private List<Double> points;

    public TabStepByStepTrajectory(List<Double> points) {
        super();
        this.points = points;
    }

    public List<Double> getPoints() {
        return points;
    }

    public void setPoints(List<Double> points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            TabStepByStepTrajectory tabStepByStepTrajectory = (TabStepByStepTrajectory) obj;
            equals = tabStepByStepTrajectory.getPoints().equals(getPoints());
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAB;
        int multi = 0xCD;
        code = code * multi + points.hashCode();
        return code;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Step by step trajectory  points = [");
        if (points != null && points.size() > 0) {
            for (int i = 0; i < points.size(); i++) {
                sb.append(ParsingTool.toString(points.get(i)));
                if (i == points.size() - 1) {
                    sb.append(ParsingUtil.BRACKET_CLOSING);
                } else {
                    sb.append(ParsingUtil.COMMA);
                }
            }
        } else {
            sb.append(ParsingUtil.BRACKET_CLOSING);
        }
        return sb.toString();
    }

    @Override
    public String getStringTrajectory() {
        // [0, 10, 20, ... 100]
        StringBuilder sb = new StringBuilder();
        sb.append(ParsingUtil.BRACKET_OPENNING);
        for (int i = 0; i < points.size(); i++) {
            sb.append(ParsingTool.toString(points.get(i)));
            if (i < points.size() - 1) {
                sb.append(ParsingUtil.COMMA);
            }
        }
        sb.append(ParsingUtil.BRACKET_CLOSING);
        return sb.toString();
    }

}
