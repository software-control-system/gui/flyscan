package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.SENSOR;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;

/**
 * Sensor object
 * 
 * @author guerre-giordano
 * 
 */
public class SensorBehavior extends BehaviorObject {

    /**
     * Constructor
     */
    public SensorBehavior(EntryContainer parent) {
        super(SENSOR, parent);

    }

}
