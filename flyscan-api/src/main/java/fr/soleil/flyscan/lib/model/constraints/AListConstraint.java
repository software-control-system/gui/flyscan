package fr.soleil.flyscan.lib.model.constraints;

import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.date.IDateConstants;

public abstract class AListConstraint<V> extends Constraint {

    private Collection<V> possibleValues;

    public AListConstraint(boolean fromPlugin, Collection<V> possibleValues) {
        super(fromPlugin);
        this.possibleValues = possibleValues;
    }

    public final Collection<V> getPossibleValues() {
        return possibleValues;
    }

    public final void setPossibleValues(Collection<V> possibleValues) {
        this.possibleValues = possibleValues;
        fromPlugin = false;
    }

    @Override
    protected final StringBuilder appendValueToStringBuilder(StringBuilder builder) {
        StringBuilder sb = getInitializedStringBuilder(builder);
        sb.append(ParsingUtil.BRACKET_OPENNING);
        Collection<V> values = getPossibleValues();
        int i = 0, length = values.size();
        for (V value : values) {
            sb.append(value);
            if (i++ < length - 1) {
                sb.append(IDateConstants.SPACE_SEPARATOR).append(ParsingUtil.COMMA);
            }
        }
        sb.append(ParsingUtil.BRACKET_CLOSING);
        return sb;
    }

}
