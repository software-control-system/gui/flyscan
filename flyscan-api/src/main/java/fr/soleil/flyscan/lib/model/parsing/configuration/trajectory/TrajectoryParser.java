package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Singleton used to parse an actuator trajectory
 * 
 * @author guerre-giordano
 * 
 */
public class TrajectoryParser {

    private static final String INTERVAL_START = "[(";

    /**
     * Parse a Trajectory string
     * 
     * @param configName The config name
     * @param content String
     * @return Trajectory
     * @throws FSParsingException
     */
    public static Trajectory parse(String configName, String content, String actuatorName) throws FSParsingException {
        Trajectory trajectory = null;
        List<Interval> intervals = null;
        if (content.startsWith(INTERVAL_START)) {
            intervals = getIntervals(configName, content, actuatorName);
            trajectory = new IntervalsTrajectory(intervals);
        } else {
            content = content.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING)
                    .replace(" ", ObjectUtils.EMPTY_STRING).trim();
            String[] pointsStr = content.split(ParsingUtil.COMMA);
            List<Double> points = new ArrayList<>();
            try {
                for (int i = 0; i < pointsStr.length; i++) {
                    points.add(Double.valueOf(ParsingTool.parseDouble(pointsStr[i].trim())));
                }
            } catch (NumberFormatException e) {
                throw new FSParsingException("Invalid format for trajectory interval" + content);
            }
            trajectory = new TabStepByStepTrajectory(points);
        }
        return trajectory;

    }

    private static List<Interval> getIntervals(String configName, String content, String actuatorName)
            throws FSParsingException {
        String adaptedContent = content.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).replace(" ", ObjectUtils.EMPTY_STRING)
                .trim();
        List<String> intervalsStr = new ArrayList<>();
        for (String s : adaptedContent.split("\\),")) {
            intervalsStr.add(s.replace("(", ObjectUtils.EMPTY_STRING).replace(")", ObjectUtils.EMPTY_STRING).trim());
        }
        List<Interval> intervals = new ArrayList<>();
        for (String s : intervalsStr) {
            String[] members = s.split(ParsingUtil.COMMA);
            if (members.length != 3) {
                throw buildTrajectoryException(configName, content, actuatorName);
            } else {
                try {
                    double from = ParsingTool.parseDouble(members[0]);
                    double to = ParsingTool.parseDouble(members[1]);
                    int nbAcqPts = Integer.parseInt(members[2]);
//                    int nbAcqPts = (int) Double.parseDouble(members[2]);
                    // XXX Do not force from being < to (SCAN-379)
//                    if (to < from) {
//                        throw new FSParsingException("Invalid format for trajectory interval" + content + " at " + s);
//                    } else {
                    Interval interval = new Interval(from, to, nbAcqPts);
                    intervals.add(interval);
//                    }
                } catch (Exception e) {
                    throw buildTrajectoryException(configName, content, actuatorName);
                }
            }
        }
        return intervals;
    }

    protected static FSParsingException buildTrajectoryException(String configName, String content,
            String actuatorName) {
        return new FSParsingException(
                configName + ": Invalid trajectory format in " + actuatorName + " (" + content + ")");
    }

}
