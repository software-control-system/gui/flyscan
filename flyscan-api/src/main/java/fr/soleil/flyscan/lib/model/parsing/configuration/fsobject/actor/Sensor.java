package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class Sensor extends Actor {

    public Sensor(EntryContainer parent) {
        super(ParsingUtil.SENSOR, parent);
    }

}
