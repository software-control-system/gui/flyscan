package fr.soleil.flyscan.lib.model.constraints;

import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class InConstraint extends AListConstraint<Number> {

    public InConstraint(boolean fromPlugin, Collection<Number> possibleValues) {
        super(fromPlugin, possibleValues);
    }

    @Override
    protected String getType() {
        return ParsingUtil.IN_LIST_KEYWORD;
    }

}
