package fr.soleil.flyscan.lib.model;

import java.util.Collection;

public class Specification {

    private Description description;

    private final Collection<Actor> actors;

    public Specification(Description description) {
        actors = description.getActors();
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public Collection<Actor> getActors() {
        return actors;
    }

}
