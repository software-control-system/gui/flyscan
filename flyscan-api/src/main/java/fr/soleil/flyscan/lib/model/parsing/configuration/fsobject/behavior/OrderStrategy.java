package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

/**
 * Possible type of order strategies
 * 
 * @author GIRARDOT
 */
public enum OrderStrategy {
    DIRECT, SYMMETRIC
}
