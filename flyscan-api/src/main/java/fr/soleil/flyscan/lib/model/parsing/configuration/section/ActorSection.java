package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import java.util.ArrayList;
import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Actor section
 * 
 * @author guerre-giordano
 * 
 */
public class ActorSection extends Section {

    /**
     * Constructor
     */
    public ActorSection() {
        super(ParsingUtil.ACTORS);
    }

    /**
     * Constructor
     * 
     * @param actors Collection<Actor>
     */
    public ActorSection(Collection<Actor> actors) {
        super(ParsingUtil.ACTORS, actors);
    }

    public Collection<Actor> getActors() {
        Collection<Actor> actors = new ArrayList<>(objects.size());
        for (FSObject obj : objects) {
            if (obj instanceof Actor) {
                actors.add((Actor) obj);
            }
        }
        return actors;
    }

}
