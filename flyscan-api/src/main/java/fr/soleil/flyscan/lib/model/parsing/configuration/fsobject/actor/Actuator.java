package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class Actuator extends Actor {

    protected Trajectory trajectory;

    public Actuator(String name, EntryContainer parent) {
        super(name, parent);
    }

    public Actuator(String timeOut, String delay, String errorStrategy, String orderStrategy, String name,
            EntryContainer parent) {
        super(name, parent);
        Entry timeOutEntry = new Entry(ParsingUtil.TIMEOUT, timeOut, false);
        addEntry(timeOutEntry);
        Entry delayEntry = new Entry(ParsingUtil.DELAY, delay, false);
        addEntry(delayEntry);
        addErrorStrategy(errorStrategy);
        addOrderStrategy(orderStrategy);
    }

    public Trajectory getActuatorTrajectory() {
        return trajectory;
    }

    public void setActuatorTrajectory(Trajectory trajectory) {
        this.trajectory = trajectory;
    }

}
