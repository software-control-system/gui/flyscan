package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.flyscan.lib.model.parsing.configuration.ConfigParser;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Acquisition section
 * 
 * @author guerre-giordano
 * 
 */
public class AcquisitionSection extends Section {

    private final static Logger LOGGER = LoggerFactory.getLogger(AcquisitionSection.class.getName());

    /**
     * Constructor
     */
    public AcquisitionSection() {
        super(ParsingUtil.ACQUISITION);
    }

    /**
     * Return scan dimensions
     * 
     * @return int[]
     */
    public int[] getScanDimensions() {
        int[] val = null;
        Entry entry = getEntry(ParsingUtil.DIMENSIONS);
        if (entry != null) {
            val = ConfigParser.getScanDimensions(entry, LOGGER);
        }
        if (val == null) {
            val = new int[0];
        }
        return val;
    }

    /**
     * Set scan dimensions
     * 
     * @param scanDimensions int[]
     */
    public void setScanDimensions(int... scanDimensions) {
        StringBuilder val = new StringBuilder(ParsingUtil.BRACKET_OPENNING);
        for (int i = 0; i < scanDimensions.length; i++) {
            val.append(scanDimensions[i]);
            if (i < scanDimensions.length - 1) {
                val.append(ParsingUtil.COMMA).append(' ');
            }
        }
        val.append(ParsingUtil.BRACKET_CLOSING);
        setStringValue(ParsingUtil.DIMENSIONS, val.toString());

    }

    /**
     * Return whether scan is continuous
     * 
     * @return boolean
     */
    public boolean isContinuousScan() {
        return getBooleanValue(ParsingUtil.CONTINUOUS);
    }

    /**
     * Set continuous scan
     * 
     * @param continuousScan boolean
     */
    public void setContinuousScan(boolean continuousScan) {
        setStringValue(ParsingUtil.CONTINUOUS, Boolean.toString(continuousScan));

    }

    /**
     * Return continuous ranges
     * 
     * @return int
     */
    public int getContinuousRanges() {
        return getIntValue(ParsingUtil.CONTINUOUS_RANGES);
    }

    /**
     * Set continuous ranges
     * 
     * @param continuousRange int
     */
    public void setContinuousRange(int continuousRange) {
        setStringValue(ParsingUtil.CONTINUOUS_RANGES, Integer.toString(continuousRange));
    }

    /**
     * Return whether manuel mode is activated
     * 
     * @return boolean
     */
    public boolean getManualMode() {
        return getBooleanValue(ParsingUtil.MANUAL_MODE);
    }

    /**
     * Activate or deactivate manual mode
     * 
     * @param manualMode boolean
     */
    public void setManualMode(boolean manualMode) {
        setStringValue(ParsingUtil.MANUAL_MODE, Boolean.toString(manualMode));

    }

    /**
     * Return whether infinite mode is activated
     * 
     * @return boolean
     */
    public boolean getInfiniteMode() {
        return getBooleanValue(ParsingUtil.INFINITE_MODE);
    }

    /**
     * Activate or deactivate infinite mode
     * 
     * @param infiniteMode boolean
     */
    public void setInfiniteMode(boolean infiniteMode) {
        setStringValue(ParsingUtil.INFINITE_MODE, Boolean.toString(infiniteMode));

    }

    /**
     * Return whether zigzag is activated
     * 
     * @return boolean
     */
    public boolean isZigZag() {
        return getBooleanValue(ParsingUtil.ZIGZAG);
    }

    /**
     * Activate or deactivate zigzag
     * 
     * @param zigzag boolean
     */
    public void setZigzag(boolean zigzag) {
        setStringValue(ParsingUtil.ZIGZAG, Boolean.toString(zigzag));
    }

    /**
     * Return scan mode
     * 
     * @return String
     */
    public String getScanMode() {
        return getStringValue(ParsingUtil.SCAN_MODE);
    }

    /**
     * Set scan mode
     * 
     * @param scanMode String
     */
    public void setScanMode(String scanMode) {
        setStringValue(ParsingUtil.SCAN_MODE, scanMode);
    }

    /**
     * Return period
     * 
     * @return long
     */
    public long getPeriod() {
        return getLongValue(ParsingUtil.PERIOD);
    }

    /**
     * Set period
     * 
     * @param period long
     */
    public void setPeriod(long period) {
        setStringValue(ParsingUtil.PERIOD, Long.toString(period));
    }

}
