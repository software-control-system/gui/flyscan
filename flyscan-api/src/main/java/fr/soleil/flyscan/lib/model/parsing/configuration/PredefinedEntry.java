package fr.soleil.flyscan.lib.model.parsing.configuration;

import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Non parameter type entry
 * 
 * @author guerre-giordano
 * 
 */
public class PredefinedEntry extends Entry {

    /**
     * Constructor
     */
    public PredefinedEntry() {
        super();
    }

    /**
     * Constructor
     * 
     * @param key String
     * @param value String
     * @param parameterType EntryType
     */
    public PredefinedEntry(String key, String value, EntryType parameterType) {
        super(key, value);
        this.type = parameterType;
    }

    @Override
    public void setValue(String value) {
        String effectiveValue = value;
        if (type == EntryType.INT) {
            // for integer values, write effective integer (no float look-a-like)
            try {
                effectiveValue = Integer.toString(Integer.parseInt(value));
            } catch (Exception e1) {
                try {
                    effectiveValue = Integer.toString((int) Double.parseDouble(value));
                } catch (Exception e2) {
                    effectiveValue = value;
                }
            }
        }
        super.setValue(effectiveValue);
    }

    @Override
    public boolean equals(Object obj) {
        PredefinedEntry predefinedEntry = (PredefinedEntry) obj;
        return super.equals(obj) && ObjectUtils.sameObject(getEntryType(), predefinedEntry.getEntryType());
    }

    @Override
    public int hashCode() {
        return super.hashCode() * 0xAD * 0xEF * ObjectUtils.getHashCode(getEntryType());
    }

    @Override
    public String toString() {
        return super.toString() + " EntryType " + getEntryType();
    }

}
