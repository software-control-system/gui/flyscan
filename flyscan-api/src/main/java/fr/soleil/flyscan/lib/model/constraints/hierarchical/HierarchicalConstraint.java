package fr.soleil.flyscan.lib.model.constraints.hierarchical;

import fr.soleil.flyscan.lib.model.ActorType;
import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class HierarchicalConstraint extends FSObject {
    private Constraint prerequisite;
    private Constraint constraint;
    private ActorType actorType;
    private String actorName;

    public HierarchicalConstraint(EntryContainer parent) {
        super(ParsingUtil.CONSTRAINT, parent);

    }

    public Constraint getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(Constraint prerequisite) {
        this.prerequisite = prerequisite;
    }

    public ActorType getActorType() {
        return actorType;
    }

    public void setActorType(ActorType actorType) {
        this.actorType = actorType;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void setConstraint(Constraint constraint) {
        this.constraint = constraint;
    }

}
