package fr.soleil.flyscan.lib.model;

import java.util.ArrayList;
import java.util.Collection;

import fr.soleil.lib.project.ObjectUtils;

public class Actor {

    private ActorType type;
    private String name;
    private String descriptor;
    private final Collection<FSParameter> parameters;

    public Actor() {
        parameters = new ArrayList<>();
    }

    public Actor(final ActorType type, final String name, final String descriptor,
            final Collection<FSParameter> parameters) {
        this.type = type;
        this.name = name;
        this.descriptor = descriptor;
        this.parameters = parameters;
    }

    public void addParameter(FSParameter parameter) {
        this.parameters.add(parameter);
    }

    public boolean removeParameter(FSParameter parameter) {
        boolean found = false;
        FSParameter toRemove = null;
        for (FSParameter p : parameters) {
            if (p.equals(parameter)) {
                toRemove = p;
            }
        }
        if (toRemove != null) {
            found = parameters.remove(toRemove);
        }
        return found;
    }

    public ActorType getType() {
        return type;
    }

    public void setType(ActorType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Actor actor = (Actor) obj;
            equals = ObjectUtils.sameObject(getName(), actor.getName())
                    && ObjectUtils.sameObject(getType(), actor.getType())
                    && ObjectUtils.sameObject(getDescriptor(), actor.getDescriptor());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAC;
        int mult = 0xEF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + ObjectUtils.getHashCode(getType());
        code = code * mult + ObjectUtils.getHashCode(getDescriptor());
        return code;
    }

}
