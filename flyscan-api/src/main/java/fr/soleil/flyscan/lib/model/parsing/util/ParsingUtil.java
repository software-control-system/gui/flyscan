/**
 * 
 */
package fr.soleil.flyscan.lib.model.parsing.util;

/**
 * Utility class used to keep all constants used
 * 
 * @author guerre-giordano
 * 
 */
public interface ParsingUtil {

    // Line separator
    public static final String CRLF = System.getProperty("line.separator");
    // Line escape
    public static final String LINE_ESCAPE = "\\";
    public static final String ESCAPED_LF = LINE_ESCAPE + CRLF;
    // UTF-8 BOM
    public static final String UTF8_BOM = "\uFEFF";

    // Punctuation
    public static final String COLON = ":";
    public static final String HYPHEN = "-";
    public static final String EQUALS = "=";
    public static final String BRACKET_CLOSING = "]";
    public static final String BRACKET_OPENNING = "[";
    public static final String CURLY_BRACE_CLOSING = "}";
    public static final String CURLY_BRACE_OPENNING = "{";
    public static final String COMMA = ",";

    // Coments
    public static final String COMMENT_START = "#";
    public static final String COMMENT_START_SPACE = "# ";
    public static final String COMMENT_START2 = ";";

    // Keywords
    public static final String HOOK = "hook";
    public static final String SENSOR = "sensor";
    public static final String CONTEXT_MONITOR = "context_monitor";
    public static final String TIMEBASE = "timebase";
    public static final String CONSTRAINTS = "constraints";
    public static final String CONSTRAINT = "constraint";
    public static final String VALUE = "value";
    public static final String PARAMETER = "parameter";
    public static final String CONSTANT_KEYWORD = "const";
    public static final String IN_RANGE_KEYWORD = "in_range";
    public static final String IN_LIST_KEYWORD = "in_list";
    public static final String DEFAULT_VALUE = "default";
    public static final String FROM_CLASS = "from_class";
    public static final String FROM_DEVICE = "from_device";
    public static final String COUNT = "count";
    public static final String VALUES = "values";
    public static final String UNIT = "unit";
    public static final String LIST = "list";
    public static final String FORMAT = "format";
    public static final String TYPE = "type";
    public static final String DESCRIPTION = "description";
    public static final String NAME = "name";
    public static final String TRAJECTORY = "trajectory";
    public static final String TRAJECTORY_MODE = "trajectory_mode";
    public static final String DIMENSION = "dimension";
    public static final String IDENTIFIER = "identifier";
    public static final String ACTUATOR = "actuator";
    public static final String CONTINUOUS_ACTUATOR = "continuous_actuator";
    public static final String CUSTOM_ACTUATOR = "custom_actuator";
    public static final String ERROR_STRATEGY = "error_strategy";
    public static final String ORDER_STRATEGY = "order_strategy";
    public static final String DELAY = "delay";
    public static final String TIMEOUT = "timeout";
    public static final String ACQUISITION = "acquisition";
    public static final String PERIOD = "period";
    public static final String ZIGZAG = "zigzag";
    public static final String SCAN_MODE = "scan_mode";
    public static final String INFINITE_MODE = "infinite_mode";
    public static final String MANUAL_MODE = "manual_mode";
    public static final String CONTINUOUS_RANGES = "continuous_ranges";
    public static final String CONTINUOUS = "continuous";
    public static final String DIMENSIONS = "dimensions";
    public static final String ACTORS = "actors";
    public static final String BEHAVIOR = "behavior";
    public static final String INFO = "info";
    public static final String FLYSCAN_VERSION = "flyscan_version";
    public static final String CONFIG_VERSION = "config_version";
    public static final String REVISION = "revision";
    public static final String COMMENT = "comment";
    public static final String KEYWORDS = "keywords";
    public static final String AUTHORS = "authors";
    public static final String RECORDING = "recording";
    public static final String PATH = "path";
    public static final String BUFFER = "buffer";
    public static final String ENABLE = "enable";
    public static final String ORDER = "order";
    public static final String LEVEL = "level";
    public static final String USER = "user";
    public static final String EXPERT = "expert";
    public static final String MONITOR = "monitor";
    public static final String VERSION = "version";
    public static final String ACTION = "action";
    public static final String SPLIT = "split";
    public static final String MAX_SCAN_LINES_PER_FILE = "max_scan_lines_per_file";
    public static final String MAX_STEPS_PER_FILE = "max_steps_per_file";
    public static final String POST_SCAN_ACTION = "post_scan_action";
    public static final String EASY_CONIG = "easy_config";
    public static final String CONFIGURATOR = "configurator";
    public static final String EASY_CONFIG_PROP = "easy_config_prop";

}
