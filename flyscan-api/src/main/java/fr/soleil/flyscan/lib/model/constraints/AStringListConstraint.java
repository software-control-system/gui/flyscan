package fr.soleil.flyscan.lib.model.constraints;

import java.util.Collection;

public abstract class AStringListConstraint extends AListConstraint<String> {

    public AStringListConstraint(boolean fromPlugin, Collection<String> possibleValues) {
        super(fromPlugin, possibleValues);
    }

}
