package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class MonitorBehavior extends BehaviorObject {

    /**
     * Constructor
     */
    public MonitorBehavior(EntryContainer parent) {
        super(ParsingUtil.MONITOR, parent);
    }

}