package fr.soleil.flyscan.lib.model.parsing.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import fr.soleil.flyscan.lib.model.constraints.ConstantConstraint;
import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.constraints.CountConstraint;
import fr.soleil.flyscan.lib.model.constraints.DeviceClassConstraint;
import fr.soleil.flyscan.lib.model.constraints.DeviceConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraintStr;
import fr.soleil.flyscan.lib.model.constraints.MinMaxConstraint;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Singleton used to parse a plugin file
 * 
 * @author guerre-giordano
 * 
 */
public class PluginParser {

    /**
     * Parse a plugin file and return a Plugin objects list
     * 
     * @param inputPath String
     * @return List<Plugin>
     * @throws FSParsingException
     */
    public static Collection<Plugin> parse(String... content) throws FSParsingException {
        Collection<Plugin> plugins = new ArrayList<>();
        if (content != null) {
            PluginBuffer buffer = new PluginBuffer();
            for (String currentLine : content) {
                parseLine(currentLine, plugins, buffer);
            }
            addPlugin(plugins, buffer);
        }
        return plugins;
    }

    public static Collection<Plugin> parse(String[] content, int start, int end) throws FSParsingException {
        Collection<Plugin> plugins = new ArrayList<>();
        if (content != null) {
            PluginBuffer buffer = new PluginBuffer();
            int from = Math.max(start, 0);
            int to = Math.min(Math.max(end, 0), content.length);
            for (int i = from; i < to; i++) {
                parseLine(content[i], plugins, buffer);
            }
            addPlugin(plugins, buffer);
        }
        return plugins;
    }

    protected static void addPlugin(Collection<Plugin> plugins, PluginBuffer buffer) {
        if (buffer.currentPlugin != null) {
            if (buffer.currentParameter != null) {
                buffer.currentPlugin.addParameter(buffer.currentParameter);
                buffer.currentParameter = null;
            }
            if ((buffer.currentPlugin.getType() == null)
                    && ParsingUtil.EASY_CONFIG_PROP.equalsIgnoreCase(buffer.currentPlugin.getName())) {
                buffer.currentPlugin.setType(PluginType.EASY_CONFIG_PROP, PluginType.EASY_CONFIG_PROP.toString());
            }
            plugins.add(buffer.currentPlugin);
        }
    }

    private static void parseLine(String currentLine, Collection<Plugin> plugins, PluginBuffer buffer)
            throws FSParsingException {
        currentLine = currentLine.replace(ParsingUtil.UTF8_BOM, ObjectUtils.EMPTY_STRING).trim();
        if ((!currentLine.startsWith(ParsingUtil.COMMENT_START))
                && (!currentLine.startsWith(ParsingUtil.COMMENT_START2))) {
            if (currentLine.endsWith(ParsingUtil.BRACKET_CLOSING)
                    && currentLine.startsWith(ParsingUtil.BRACKET_OPENNING)) {
                String name = currentLine.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                        .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();

                if ((buffer.currentPlugin == null) && (buffer.currentParameter != null)) {
                    throw new FSParsingException(
                            "FSParameter " + buffer.currentParameter.getName() + " must exist within a plugin");
                } else {
                    addPlugin(plugins, buffer);
                }
                buffer.currentPlugin = new Plugin();
                buffer.currentPlugin.setName(name);
            } else if (currentLine.trim().endsWith(ParsingUtil.COLON)) {
                String name = currentLine.replace(ParsingUtil.COLON, ObjectUtils.EMPTY_STRING).trim();
                if (buffer.currentPlugin == null) {
                    throw new FSParsingException("FSParameter " + name + " must exist within a plugin");
                } else {
                    // Only register valid parameters
                    if ((buffer.currentParameter != null) && (buffer.currentParameter.getName() != null)
                            && (buffer.currentParameter.getType() != null)
                            && (buffer.currentParameter.getFormat() != null)) {
                        buffer.currentPlugin.addParameter(buffer.currentParameter);
                    }
                    buffer.currentParameter = new Parameter();
                }
            } else if (currentLine.equalsIgnoreCase(ParsingUtil.CONSTANT_KEYWORD)
                    && (buffer.currentParameter != null)) {
                buffer.currentParameter.addConstraint(new ConstantConstraint(true));
            } else if (currentLine.contains(ParsingUtil.EQUALS)) {
                String[] entryPair = currentLine.split(ParsingUtil.EQUALS);
                String key = entryPair[0].trim();
                String value = entryPair[1].trim();
                if ((buffer.currentPlugin == null) && (buffer.currentParameter == null)) {
                    throw new FSParsingException("Entry " + key + " must exist within a plugin or a parameter");
                } else {
                    if (buffer.currentParameter == null) {
                        if (ParsingUtil.TYPE.equalsIgnoreCase(key)
                                && (!ParsingUtil.EASY_CONIG.equalsIgnoreCase(value))) {
                            PluginType type = getPluginType(value);
                            buffer.currentPlugin.setType(type, value);
                        } else if (ParsingUtil.DESCRIPTION.equalsIgnoreCase(key)) {
                            buffer.currentPlugin.setLongName(value);
                        }
                    } else {
                        // Handle new entry
                        if (ParsingUtil.NAME.equalsIgnoreCase(key)) {
                            buffer.currentParameter.setName(value);
                        } else if (ParsingUtil.DESCRIPTION.equalsIgnoreCase(key)) {
                            buffer.currentParameter.setDescription(value);
                        } else if (ParsingUtil.TYPE.equalsIgnoreCase(key)) {
                            EntryType type = EntryType.parse(value);
                            buffer.currentParameter.setType(type);
                        } else if (ParsingUtil.FORMAT.equalsIgnoreCase(key)) {
                            ParameterFormat format;
                            if (ParsingUtil.LIST.equalsIgnoreCase(value)) {
                                format = ParameterFormat.LIST;
                            } else {
                                format = ParameterFormat.SCALAR;
                            }
                            buffer.currentParameter.setFormat(format);
                        } else if (ParsingUtil.UNIT.equalsIgnoreCase(key)) {
                            buffer.currentParameter.setUnit(value);
                        } else if (ParsingUtil.VALUES.equalsIgnoreCase(key)) {
                            value = value.replace(ParsingUtil.CURLY_BRACE_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.CURLY_BRACE_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            String[] tmp = value.split(",");
                            String[] val = new String[tmp.length];
                            for (int i = 0; i < tmp.length; i++) {
                                String s = tmp[i];
                                val[i] = s.trim();
                            }
                            buffer.currentParameter.setValues(val);
                        } else if (ParsingUtil.FROM_CLASS.equalsIgnoreCase(key)) {
                            value = value.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            String[] tmp = value.split(",");
                            String[] val = new String[tmp.length];
                            for (int i = 0; i < tmp.length; i++) {
                                String s = tmp[i];
                                val[i] = s.trim();
                            }
                            buffer.currentParameter.addConstraint(new DeviceClassConstraint(true, Arrays.asList(val)));
                        } else if (ParsingUtil.FROM_DEVICE.equalsIgnoreCase(key)) {
                            value = value.replace(ParsingUtil.CURLY_BRACE_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.CURLY_BRACE_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            String[] tmp = value.split(",");
                            String[] val = new String[tmp.length];
                            for (int i = 0; i < tmp.length; i++) {
                                String s = tmp[i];
                                val[i] = s.trim();
                            }
                            buffer.currentParameter.addConstraint(new DeviceConstraint(true, Arrays.asList(val)));
                        } else if (ParsingUtil.COUNT.equalsIgnoreCase(key)) {
                            try {
                                int count = Integer.parseInt(value);
                                if (count > 0) {
                                    buffer.currentParameter.addConstraint(new CountConstraint(true, count));
                                } else {
                                    throw new FSParsingException("Invalid count constraint: " + value);
                                }
                            } catch (NumberFormatException e) {
                                throw new FSParsingException("Invalid count constraint: " + value, e);
                            }
                        } else if (ParsingUtil.DEFAULT_VALUE.equalsIgnoreCase(key)) {
                            buffer.currentParameter.setDefaultValue(value);
                        } else if (ParsingUtil.LEVEL.equalsIgnoreCase(key)) {
                            buffer.currentParameter
                                    .setExpert(Boolean.valueOf(ParsingUtil.EXPERT.equalsIgnoreCase(value)));
                        } else if (key.equalsIgnoreCase(ParsingUtil.IN_LIST_KEYWORD)) {
                            Constraint constraint = null;
                            String[] lStr = value.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING)
                                    .split(ParsingUtil.COMMA);
                            List<Number> numberList = new ArrayList<>();
                            boolean isDouble = ParsingTool.isDouble(value);
                            boolean constraintOk = false;
                            if (isDouble) {
                                try {
                                    for (String s : lStr) {
                                        Double d = Double.valueOf(ParsingTool.parseDouble(s.trim()));
                                        numberList.add(d);
                                    }
                                    constraintOk = true;
                                } catch (NumberFormatException e) {
                                    List<String> listStr = new ArrayList<>();
                                    for (String str : lStr) {
                                        listStr.add(str.trim());
                                    }
                                    constraint = new InConstraintStr(true, listStr);
                                    // throw new FSParsingException(e.getMessage(), e);
                                }
                            } else {
                                try {
                                    for (String s : lStr) {
                                        Long d = Long.valueOf(s.trim());
                                        numberList.add(d);
                                    }
                                    constraintOk = true;
                                } catch (NumberFormatException e) {
                                    List<String> listStr = new ArrayList<>();
                                    for (String str : lStr) {
                                        listStr.add(str.trim());
                                    }
                                    constraint = new InConstraintStr(true, listStr);
                                    // throw new FSParsingException(e.getMessage(), e);
                                }
                            }
                            if (constraintOk) {
                                constraint = new InConstraint(true, numberList);
                            }
                            if (constraint != null) {
                                buffer.currentParameter.addConstraint(constraint);
                            }
                        } else if (key.equalsIgnoreCase(ParsingUtil.IN_RANGE_KEYWORD)) {
                            Constraint constraint = null;
                            String[] lStr = value.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING)
                                    .split(ParsingUtil.COMMA);
                            if (lStr.length != 2) {
                                throw new FSParsingException("Can not parse in range constraint value: " + value);
                            }
                            try {
                                Double min = ParsingTool.parseDouble(lStr[0].trim());
                                Double max = ParsingTool.parseDouble(lStr[1].trim());
                                constraint = new MinMaxConstraint(true, min, max);
                            } catch (NumberFormatException e) {
                                throw new FSParsingException(e.getMessage(), e);
                            }
                            if (constraint != null) {
                                buffer.currentParameter.addConstraint(constraint);
                            }
                        }
                    }
                }
            } else if ((ParsingUtil.HYPHEN).equalsIgnoreCase(currentLine.trim())) {
                if (buffer.currentParameter != null) {
                    buffer.currentPlugin.addParameter(buffer.currentParameter);
                    buffer.currentParameter = null;
                }
            }
        }
    }

    private static PluginType getPluginType(String value) throws FSParsingException {
        PluginType type;
        if ((ParsingUtil.ACTUATOR).equalsIgnoreCase(value) || ParsingUtil.CONTINUOUS_ACTUATOR.equalsIgnoreCase(value)) {
            type = PluginType.ACTUATOR;
        } else if (ParsingUtil.HOOK.equalsIgnoreCase(value)) {
            type = PluginType.HOOK;
        } else if (ParsingUtil.MONITOR.equalsIgnoreCase(value) || ParsingUtil.CONTEXT_MONITOR.equalsIgnoreCase(value)) {
            type = PluginType.MONITOR;
        } else if (ParsingUtil.SENSOR.equalsIgnoreCase(value)) {
            type = PluginType.SENSOR;
        } else if (ParsingUtil.TIMEBASE.equalsIgnoreCase(value)) {
            type = PluginType.TIMEBASE;
        } else if (ParsingUtil.EASY_CONFIG_PROP.equalsIgnoreCase(value)) {
            type = PluginType.EASY_CONFIG_PROP;
        } else {
            throw new FSParsingException("Unknown type of plugin: " + value);
        }
        return type;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class PluginBuffer {
        Plugin currentPlugin = null;
        Parameter currentParameter = null;
    }

}
