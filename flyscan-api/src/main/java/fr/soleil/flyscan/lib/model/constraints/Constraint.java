package fr.soleil.flyscan.lib.model.constraints;

public abstract class Constraint {

    protected static final String EQUALS = " = ";
    protected boolean fromPlugin;

    public Constraint(boolean fromPlugin) {
        this.fromPlugin = fromPlugin;
    }

    protected abstract String getType();

    protected static final StringBuilder getInitializedStringBuilder(StringBuilder builder) {
        StringBuilder sb = builder;
        if (sb == null) {
            sb = new StringBuilder();
        }
        return sb;
    }

    protected final StringBuilder appendTypeToStringBuilder(StringBuilder builder) {
        StringBuilder sb = getInitializedStringBuilder(builder);
        sb.append(getType());
        return sb;
    }

    protected abstract StringBuilder appendValueToStringBuilder(StringBuilder builder);

    protected boolean hasValue() {
        return true;
    }

    public final String getText() {
        StringBuilder builder = appendTypeToStringBuilder(new StringBuilder());
        if (hasValue()) {
            appendValueToStringBuilder(builder.append(EQUALS));
        }
        return builder.toString();
    }

    public boolean isFromPlugin() {
        return fromPlugin;
    }

}
