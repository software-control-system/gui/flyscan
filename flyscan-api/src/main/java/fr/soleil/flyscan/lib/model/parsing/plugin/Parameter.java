package fr.soleil.flyscan.lib.model.parsing.plugin;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Plugin Parameter
 * 
 * @author guerre-giordano
 */
public class Parameter {

    private String name;
    private String description;
    private EntryType type;
    private ParameterFormat format;
    private String unit;
    private String[] values;
    private String defaultValue;
    private final List<Constraint> constraints;
    private Boolean expert;

    /**
     * Constructor
     */
    public Parameter() {
        this(null, null, null, null, new ArrayList<>());
    }

    /**
     * Constructor
     * 
     * @param name String
     * @param description String
     * @param type EntryType
     * @param format ParameterFormat
     */
    public Parameter(String name, String description, EntryType type, ParameterFormat format,
            List<Constraint> constraints) {
        super();
        this.name = name;
        this.description = description;
        this.type = type;
        this.format = format;
        this.constraints = constraints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EntryType getType() {
        return type;
    }

    public void setType(EntryType type) {
        this.type = type;
    }

    public ParameterFormat getFormat() {
        return format;
    }

    public void setFormat(ParameterFormat format) {
        this.format = format;
    }

    public boolean isExpert() {
        Boolean exp = expert;
        return (exp != null) && exp.booleanValue();
    }

    public void setExpert(Boolean expert) {
        this.expert = expert;
    }

    public boolean isLevelSet() {
        return expert != null;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {

            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Parameter parameter = (Parameter) obj;
            equals = ObjectUtils.sameObject(getName(), parameter.getName())
                    && ObjectUtils.sameObject(getType(), parameter.getType())
                    && ObjectUtils.sameObject(getFormat(), parameter.getFormat())
                    && ObjectUtils.sameObject(getConstraints(), parameter.getConstraints())
                    && ObjectUtils.sameObject(getUnit(), parameter.getUnit())
                    && ObjectUtils.sameObject(getValues(), parameter.getValues())
                    && ObjectUtils.sameObject(getDefaultValue(), parameter.getDefaultValue())
                    && ObjectUtils.sameObject(getDescription(), parameter.getDescription())
                    && ObjectUtils.sameObject(expert, parameter.expert);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAB;
        int mult = 0xCF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + ObjectUtils.getHashCode(getType());
        code = code * mult + ObjectUtils.getHashCode(getFormat());
        code = code * mult + ObjectUtils.getHashCode(getUnit());
        code = code * mult + ObjectUtils.getHashCode(getValues());
        code = code * mult + ObjectUtils.getHashCode(getDefaultValue());
        code = code * mult + ObjectUtils.getHashCode(getConstraints());
        code = code * mult + ObjectUtils.getHashCode(expert);
        return code;
    }

    @Override
    public String toString() {
        return "FSParameter " + name + " (" + description + ") " + " type: " + type + " format: " + format;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        if (type == EntryType.ENUM) {
            this.values = values;
        }
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }

    public void addConstraint(Constraint constraint) {
        this.constraints.add(constraint);
    }

}
