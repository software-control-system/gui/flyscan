package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Recording section
 * 
 * @author guerre-giordano
 * 
 */
public class RecordingSection extends Section {

    /**
     * Constructor
     */
    public RecordingSection() {
        super(ParsingUtil.RECORDING);

    }

    /**
     * Set buffer name
     * 
     * @param bufferName String
     */
    public void setBufferName(String bufferName) {
        for (FSObject obj : getObjects()) {
            if (obj.getName().equalsIgnoreCase(ParsingUtil.BUFFER)) {
                Entry entry = obj.getEntry(ParsingUtil.NAME);
                if (entry != null) {
                    entry.setValue(bufferName);
                }
            }
        }
    }

    /**
     * Return buffer name
     * 
     * @return String
     */
    public String getBufferName() {
        String val = ObjectUtils.EMPTY_STRING;
        for (FSObject obj : getObjects()) {
            if (obj.getName().equalsIgnoreCase(ParsingUtil.BUFFER)) {
                Entry entry = obj.getEntry(ParsingUtil.NAME);
                if (entry != null) {
                    val = entry.getValue();
                }
            }
        }
        return val;
    }

    /**
     * Set buffer path
     * 
     * @param bufferPath String
     */
    public void setBufferPath(String bufferPath) {
        for (FSObject obj : getObjects()) {
            if (obj.getName().equalsIgnoreCase(ParsingUtil.BUFFER)) {
                Entry entry = obj.getEntry(ParsingUtil.PATH);
                if (entry != null) {
                    entry.setValue(bufferPath);
                }
            }
        }
    }

    /**
     * Return buffer path
     * 
     * @return String
     */
    public String getBufferPath() {
        String val = ObjectUtils.EMPTY_STRING;
        for (FSObject obj : getObjects()) {
            if (obj.getName().equalsIgnoreCase(ParsingUtil.BUFFER)) {
                Entry entry = obj.getEntry(ParsingUtil.PATH);
                if (entry != null) {
                    val = entry.getValue();
                }
            }
        }
        return val;
    }

}
