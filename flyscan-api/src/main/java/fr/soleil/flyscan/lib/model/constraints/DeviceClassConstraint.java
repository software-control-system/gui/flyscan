package fr.soleil.flyscan.lib.model.constraints;

import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class DeviceClassConstraint extends AStringListConstraint {

    public DeviceClassConstraint(boolean fromPlugin, Collection<String> possibleValues) {
        super(fromPlugin, possibleValues);
    }

    @Override
    protected String getType() {
        return ParsingUtil.FROM_CLASS;
    }

}
