package fr.soleil.flyscan.lib.model;

public enum ActorType {
    SENSOR, ACTUATOR, CONTINUOUS_ACTUATOR, TIMEBASE, HOOK, MONITOR
}