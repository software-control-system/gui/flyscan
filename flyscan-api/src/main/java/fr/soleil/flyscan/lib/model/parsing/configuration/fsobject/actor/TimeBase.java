package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class TimeBase extends Actor {

    public TimeBase(EntryContainer parent) {
        super(ParsingUtil.TIMEBASE, parent);
    }

}
