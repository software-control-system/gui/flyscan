package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

public class Interval {

    private double from;
    private double to;
    private int nbAcq;

    public Interval(double from, double to, int nbAcq) {
        this.from = from;
        this.to = to;
        this.nbAcq = nbAcq;
    }

    public double getFrom() {
        return from;
    }

    public void setFrom(double from) {
        this.from = from;
    }

    public double getTo() {
        return to;
    }

    public void setTo(double to) {
        this.to = to;
    }

    public int getNbAcq() {
        return nbAcq;
    }

    public void setNbAcq(int nbAcq) {
        this.nbAcq = nbAcq;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Interval interval = (Interval) obj;
            equals = (this.getFrom() == interval.getFrom()) && (this.getTo() == interval.getTo())
                    && (this.getNbAcq() == interval.getNbAcq());
        }
        return equals;
    }

    @Override
    public String toString() {
        return "Interval from " + from + " to " + to + " nbAcq " + nbAcq;
    }

}
