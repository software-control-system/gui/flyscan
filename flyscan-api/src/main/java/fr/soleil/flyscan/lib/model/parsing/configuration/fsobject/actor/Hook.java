package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class Hook extends Actor {

    public Hook(EntryContainer parent) {
        super(ParsingUtil.HOOK, parent);
    }

}
