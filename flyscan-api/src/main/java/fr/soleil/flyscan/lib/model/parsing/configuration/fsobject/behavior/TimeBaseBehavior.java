package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import static fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil.TIMEBASE;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;

/**
 * Time base object
 * 
 * @author guerre-giordano
 * 
 */
public class TimeBaseBehavior extends BehaviorObject {

    /**
     * Constructor
     */
    public TimeBaseBehavior(EntryContainer parent) {
        super(TIMEBASE, parent);
    }

}
