package fr.soleil.flyscan.lib.model.parsing.configuration;

import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;

public class TrajectoryEntry extends PredefinedEntry {

    private Trajectory trajectory;

    public TrajectoryEntry(String key, String value) {
        super(key, value, EntryType.TRAJECTORY);
    }

    public Trajectory getTrajectory() {
        return trajectory;
    }

    public void setTrajectory(Trajectory trajectory) {
        this.trajectory = trajectory;
    }

}
