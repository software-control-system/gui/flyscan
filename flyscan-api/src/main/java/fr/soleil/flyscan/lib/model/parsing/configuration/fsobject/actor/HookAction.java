package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

/**
 * Actions for hook actor
 * 
 * @author guerre-giordano
 * 
 */
public enum HookAction {
    preScan, postScan, preLine, postLine, preStep, postStep, preTimebase, postTimebase, preSensorIntegration, postSensorIntegration
}
