package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class EasyConfigPropSection extends Section {

    protected String content;

    public EasyConfigPropSection() {
        super(ParsingUtil.EASY_CONFIG_PROP);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        String content = this.content;
        if ((content == null) || (content.isEmpty())) {
            content = super.toString();
        }
        return content;
    }

}
