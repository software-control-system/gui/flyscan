package fr.soleil.flyscan.lib.model.constraints;

import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class DeviceConstraint extends AStringListConstraint {

    public DeviceConstraint(boolean fromPlugin, Collection<String> possibleValues) {
        super(fromPlugin, possibleValues);
    }

    @Override
    protected String getType() {
        return ParsingUtil.FROM_DEVICE;
    }

}
