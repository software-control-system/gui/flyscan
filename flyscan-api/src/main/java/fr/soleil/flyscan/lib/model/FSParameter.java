package fr.soleil.flyscan.lib.model;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.lib.project.ObjectUtils;

public class FSParameter {

    public static enum Type {
        NUMBER, STRING, ENUM, DEVICE, DEVICE_ATTRIBUTE
    };

    private Type type;
    private String name;
    private String value;
    private final List<Constraint> unionConstraint;

    public FSParameter() {
        this.unionConstraint = new ArrayList<>();
    }

    public FSParameter(final Type type, final String name, final String value) {
        this.type = type;
        this.name = name;
        this.value = value;
        this.unionConstraint = new ArrayList<>();
    }

    public FSParameter(final Type type, final String name, final String value, final List<Constraint> unionConstraint) {
        this.type = type;
        this.name = name;
        this.value = value;
        this.unionConstraint = unionConstraint;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Constraint> getConstraints() {
        return unionConstraint;
    }

    public void addConstraint(Constraint constraint) {
        this.unionConstraint.add(constraint);
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {

            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            FSParameter parameter = (FSParameter) obj;
            equals = ObjectUtils.sameObject(getName(), parameter.getName())
                    && ObjectUtils.sameObject(getType(), parameter.getType())
                    && ObjectUtils.sameObject(getConstraints(), parameter.getConstraints())
                    && ObjectUtils.sameObject(getValue(), parameter.getValue());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAA;
        int mult = 0xEE;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + ObjectUtils.getHashCode(getType());
        code = code * mult + ObjectUtils.getHashCode(getValue());
        code = code * mult + ObjectUtils.getHashCode(getConstraints());
        return code;
    }
}
