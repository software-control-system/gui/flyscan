package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.ActuatorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.ContextMonitorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.HookBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.SensorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.TimeBaseBehavior;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Behavior section
 * 
 * @author guerre-giordano
 * 
 */
public class BehaviorSection extends Section {

    private ContextMonitorBehavior contextMonitorObject;
    private ActuatorBehavior acuatorObject;
    private SensorBehavior sensorObject;
    private TimeBaseBehavior timeBaseObject;
    private HookBehavior hookObject;

    /**
     * Constructor
     */
    public BehaviorSection() {
        super(ParsingUtil.BEHAVIOR);

    }

    public ContextMonitorBehavior getContextMonitorObject() {
        return contextMonitorObject;
    }

    public ActuatorBehavior getAcuatorObject() {
        return acuatorObject;
    }

    public SensorBehavior getSensorObject() {
        return sensorObject;
    }

    public TimeBaseBehavior getTimeBaseObject() {
        return timeBaseObject;
    }

    public HookBehavior getHookObject() {
        return hookObject;
    }

}
