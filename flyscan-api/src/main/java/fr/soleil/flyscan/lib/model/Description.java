package fr.soleil.flyscan.lib.model;

import java.util.ArrayList;
import java.util.Collection;

public class Description {

    private final Collection<Actor> actors;

    public Description() {
        actors = new ArrayList<>();
    }

    public void addActor(Actor actor) {
        actors.add(actor);
    }

    public boolean removeActor(Actor actor) {
        boolean found = false;
        Actor toRemove = null;
        for (Actor a : actors) {
            if (a.equals(actor)) {
                toRemove = a;
            }
        }
        if (toRemove != null) {
            found = actors.remove(toRemove);
        }
        return found;
    }

    public Collection<Actor> getActors() {
        return actors;
    }

}
