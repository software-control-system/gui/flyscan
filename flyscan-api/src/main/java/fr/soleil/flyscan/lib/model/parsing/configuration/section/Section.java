package fr.soleil.flyscan.lib.model.parsing.configuration.section;

import java.util.ArrayList;
import java.util.Collection;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Section: part of config file beginning with a section name writen as [sectionName]
 * 
 * @author guerre-giordano
 * 
 */
public class Section extends EntryContainer {

    private static final String SECTION = "Section ";

    protected final Collection<FSObject> objects;

    /**
     * Constructor
     * 
     * @param name Section name
     */
    public Section(String name) {
        this(name, null);
    }

    /**
     * Constructor
     * 
     * @param name Section name
     * @param objects Objects to add in section
     */
    protected Section(String name, Collection<? extends FSObject> objects) {
        super(name);
        this.objects = objects == null || objects.isEmpty() ? new ArrayList<>() : new ArrayList<>(objects);
    }

    public void addFSObject(FSObject object) {
        if (object != null) {
            objects.add(object);
        }
    }

    public void removeFSObject(FSObject object) {
        if (object != null) {
            objects.remove(object);
        }
    }

    public Collection<FSObject> getObjects() {
        return new ArrayList<>(objects);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(SECTION + name);
        sb.append(ParsingUtil.CRLF);
        for (Entry entry : entries) {
            sb.append(entry.toString());
        }
        for (FSObject obj : objects) {
            if (obj != null) {
                sb.append(obj.toString());
            }
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {

            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Section section = (Section) obj;
            equals = ObjectUtils.sameObject(getName(), section.getName())
                    && ObjectUtils.sameObject(getType(), section.getType())
                    && ObjectUtils.sameObject(objects, section.objects)
                    && ObjectUtils.sameObject(entries, section.entries);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAE;
        int mult = 0xEF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + ObjectUtils.getHashCode(getType());
        code = code * mult + ObjectUtils.getHashCode(objects);
        code = code * mult + ObjectUtils.getHashCode(entries);
        return code;
    }

}
