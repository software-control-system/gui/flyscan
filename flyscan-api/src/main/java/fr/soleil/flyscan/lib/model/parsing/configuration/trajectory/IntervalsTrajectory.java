package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

public final class IntervalsTrajectory implements Trajectory {

    private List<Interval> intervals;

    public IntervalsTrajectory() {
        intervals = new ArrayList<>();
    }

    public IntervalsTrajectory(List<Interval> intervals) {
        super();
        this.intervals = intervals;
    }

    public List<Interval> getIntervals() {
        return intervals;
    }

    public void setIntervals(List<Interval> intervals) {
        this.intervals = intervals;
    }

    public void addInteval(Interval interval) {
        if (interval != null) {
            intervals.add(interval);
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            IntervalsTrajectory trajectory = (IntervalsTrajectory) obj;
            equals = ObjectUtils.sameObject(trajectory.getIntervals(), getIntervals());
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAE;
        int mult = 0xFD;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getIntervals());
        return code;
    }

    protected String getName() {
        return "Trajectory";
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getName());
        sb.append(ParsingUtil.CRLF);
        sb.append("Intervals: ");
        sb.append(ParsingUtil.CRLF);
        if (intervals != null) {
            for (Interval interval : intervals) {
                sb.append(interval.toString());
                sb.append(ParsingUtil.CRLF);
            }
        }

        return sb.toString();
    }

    @Override
    public String getStringTrajectory() {
        // [(0, 50, 6),(60, 100, 5)]
        StringBuilder sb = new StringBuilder();
        sb.append(ParsingUtil.BRACKET_OPENNING);
        if (intervals != null) {
            boolean added = false;
            for (Interval interval : intervals) {
                sb.append("(");
                sb.append(ParsingTool.toString(interval.getFrom()));
                sb.append(ParsingUtil.COMMA);
                sb.append(ParsingTool.toString(interval.getTo()));
                sb.append(ParsingUtil.COMMA);
                sb.append(interval.getNbAcq());
                sb.append("),");
                added = true;
            }
            if (added) {
                // remove last ","
                sb.delete(sb.length() - 1, sb.length());
            }
        }
        sb.append(ParsingUtil.BRACKET_CLOSING);
        return sb.toString();
    }

}
