package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Fly scan object containing all actors entries
 * 
 * @author guerre-giordano
 * 
 */
public abstract class Actor extends FSObject {

    /**
     * Constructor
     * 
     * @param actorType String
     */
    public Actor(String actorType, EntryContainer parent) {
        super(actorType, parent);
    }

    @Override
    public String getName() {
        return getStringValue(ParsingUtil.NAME);
    }

    @Override
    public void setName(String name) {
        setStringValue(ParsingUtil.NAME, name);
    }

    public String getIdentifier() {
        return getStringValue(ParsingUtil.IDENTIFIER);
    }

    public void setIdentifier(String identifier) {
        setStringValue(ParsingUtil.IDENTIFIER, identifier);
    }

    public int getDimension() {
        return getIntValue(ParsingUtil.DIMENSION);
    }

    public void setDimension(int dimension) {
        setStringValue(ParsingUtil.DIMENSION, Integer.toString(dimension));
    }

    public String getTrajectory() {
        return getStringValue(ParsingUtil.TRAJECTORY);
    }

    public void setTrajectory(String trajectory) {
        setStringValue(ParsingUtil.TRAJECTORY, trajectory);
    }
}
