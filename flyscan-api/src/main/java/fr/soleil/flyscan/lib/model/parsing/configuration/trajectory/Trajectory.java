package fr.soleil.flyscan.lib.model.parsing.configuration.trajectory;

public interface Trajectory {

    public String getStringTrajectory();

}
