package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;

public class ActuatorBehavior extends BehaviorObject {

    public static final String ACTUATOR = "actuator";

    /**
     * Constructor
     */
    public ActuatorBehavior(EntryContainer parent) {
        super(ACTUATOR, parent);
    }

}
