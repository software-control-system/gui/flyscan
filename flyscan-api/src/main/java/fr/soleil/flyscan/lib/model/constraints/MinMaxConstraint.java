package fr.soleil.flyscan.lib.model.constraints;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class MinMaxConstraint extends Constraint {

    private double min;
    private double max;

    public MinMaxConstraint(boolean fromPlugin) {
        super(fromPlugin);
    }

    public MinMaxConstraint(boolean fromPlugin, double min, double max) {
        super(fromPlugin);
        this.min = min;
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        if (this.min != min) {
            this.min = min;
            fromPlugin = false;
        }
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        if (this.max != max) {
            this.max = max;
            fromPlugin = false;
        }
    }

    @Override
    protected String getType() {
        return ParsingUtil.IN_RANGE_KEYWORD;
    }

    @Override
    protected final StringBuilder appendValueToStringBuilder(StringBuilder builder) {
        StringBuilder sb = getInitializedStringBuilder(builder);
        sb.append(ParsingUtil.BRACKET_OPENNING);
        sb.append(ParsingTool.toString(getMin()));
        sb.append(" ").append(ParsingUtil.COMMA);
        sb.append(ParsingTool.toString(getMax()));
        sb.append(ParsingUtil.BRACKET_CLOSING);
        return sb;
    }

}
