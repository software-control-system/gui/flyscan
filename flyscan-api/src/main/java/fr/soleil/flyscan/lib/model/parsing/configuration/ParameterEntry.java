package fr.soleil.flyscan.lib.model.parsing.configuration;

import fr.soleil.lib.project.ObjectUtils;

/**
 * Parameter entry
 * 
 * @author guerre-giordano
 * 
 */
public class ParameterEntry extends Entry {

    /**
     * Constructor
     */
    public ParameterEntry() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {

            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            ParameterEntry parameter = (ParameterEntry) obj;
            equals = ObjectUtils.sameObject(getKey(), parameter.getKey())
                    && ObjectUtils.sameObject(getEntryType(), parameter.getEntryType())
                    && ObjectUtils.sameObject(getConstraints(), parameter.getConstraints())
                    && ObjectUtils.sameObject(getValue(), parameter.getValue());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAA;
        int mult = 0xEE;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getKey());
        code = code * mult + ObjectUtils.getHashCode(getEntryType());
        code = code * mult + ObjectUtils.getHashCode(getValue());
        code = code * mult + ObjectUtils.getHashCode(getConstraints());
        return code;
    }

}
