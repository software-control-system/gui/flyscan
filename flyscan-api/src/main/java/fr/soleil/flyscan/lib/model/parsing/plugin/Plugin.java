package fr.soleil.flyscan.lib.model.parsing.plugin;

import java.util.Collection;
import java.util.LinkedHashSet;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Plugin specified in .prop file, containing parameters descriptions
 * 
 * @author guerre-giordano
 * 
 */
public class Plugin {

    private String name, typeName;
    private PluginType type;
    private String longName;
    private final Collection<Parameter> parameters;

    /**
     * Constructor
     */
    public Plugin() {
        parameters = new LinkedHashSet<>();
    }

    public void addParameter(Parameter parameter) {
        parameters.add(parameter);
    }

    public PluginType getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setType(PluginType type, String typeName) {
        this.type = type;
        this.typeName = typeName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public Collection<Parameter> getParameters() {
        return parameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {

            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Plugin parameter = (Plugin) obj;
            equals = ObjectUtils.sameObject(getName(), parameter.getName())
                    && ObjectUtils.sameObject(getType(), parameter.getType())
                    && ObjectUtils.sameObject(getTypeName(), parameter.getTypeName())
                    && ObjectUtils.sameObject(getParameters(), parameter.getParameters())
                    && ObjectUtils.sameObject(getLongName(), parameter.getLongName());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xAD;
        int mult = 0xCF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + ObjectUtils.getHashCode(getType());
        code = code * mult + ObjectUtils.getHashCode(getTypeName());
        code = code * mult + ObjectUtils.getHashCode(getParameters());
        code = code * mult + ObjectUtils.getHashCode(getLongName());
        return code;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Plugin ").append(name);
        sb.append(ParsingUtil.CRLF);
        sb.append("Type: ").append(typeName);
        sb.append(ParsingUtil.CRLF);
        sb.append("Description: ").append(longName);
        sb.append(ParsingUtil.CRLF);
        for (Parameter parameter : parameters) {
            sb.append(parameter);
            sb.append(ParsingUtil.CRLF);
        }
        return sb.toString();
    }

}
