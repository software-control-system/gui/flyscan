package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Hook object
 * 
 * @author guerre-giordano
 * 
 */
public class HookBehavior extends BehaviorObject {

    /**
     * Constructor
     */
    public HookBehavior(EntryContainer parent) {
        super(ParsingUtil.HOOK, parent);
    }

}
