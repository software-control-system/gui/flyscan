package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

/**
 * Past scan actions for Actuators (Jira SCAN-390)
 * 
 * @author GIRARDOT
 */
public enum PostScanAction {

    GOTO_PRE_SCAN_POS("goto_pre_scan_pos"), GOTO_FIRST_SCAN_POS("goto_first_scan_pos"), DO_NOTHING("do_nothing");

    private final String name;

    private PostScanAction(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static PostScanAction parsePostScanAction(String value) {
        PostScanAction result = null;
        if (value != null) {
            value = value.trim();
            for (PostScanAction action : values()) {
                if (action.name.equalsIgnoreCase(value)) {
                    result = action;
                    break;
                }
            }
        }
        return result;
    }

}
