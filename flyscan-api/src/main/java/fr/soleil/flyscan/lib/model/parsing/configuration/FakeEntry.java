package fr.soleil.flyscan.lib.model.parsing.configuration;

import fr.soleil.flyscan.lib.model.parsing.plugin.Parameter;

/**
 * Entry shown in GUI while it is not part of the configuration
 * 
 * @author guerre-giordano
 * 
 */
public class FakeEntry extends Entry {

    private Parameter param;

    public FakeEntry(String key, String value, boolean expert, Parameter param) {
        super(key, value, expert);
        this.param = param;
    }

    public Parameter getParam() {
        return param;
    }

    public void setParam(Parameter param) {
        this.param = param;
    }

}
