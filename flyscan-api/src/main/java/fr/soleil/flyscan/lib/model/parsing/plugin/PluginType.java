package fr.soleil.flyscan.lib.model.parsing.plugin;

/**
 * Plugin type
 * 
 * @author guerre-giordano
 * 
 */
public enum PluginType {
    ACTUATOR, SENSOR, TIMEBASE, HOOK, MONITOR, EASY_CONFIG_PROP
}
