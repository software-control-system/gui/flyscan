package fr.soleil.flyscan.lib.model.parsing.configuration;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Entry: pair of key and value within an EntryContainer
 * 
 * @author guerre-giordano
 * 
 */
public class Entry {

    protected String key;
    protected String value;
    protected boolean expert;
    protected String comment;
    protected EntryType type;
    protected final List<Constraint> cumultatedConstraints;
    protected boolean presentInConfig;

    /**
     * Constructor
     */
    public Entry() {
        this.cumultatedConstraints = new ArrayList<>();
        this.comment = ObjectUtils.EMPTY_STRING;
        this.presentInConfig = true;
    }

    /**
     * Constructor
     * 
     * @param key String
     * @param value String
     */
    public Entry(String key, String value) {
        this(key, value, false);
    }

    /**
     * Constructor
     * 
     * @param key String
     * @param value String
     * @param expert boolean
     */
    public Entry(String key, String value, boolean expert) {
        super();
        this.key = key;
        this.value = value;
        this.expert = expert;
        comment = ObjectUtils.EMPTY_STRING;
        cumultatedConstraints = new ArrayList<>();
        type = EntryType.STRING;
        presentInConfig = true;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        this.presentInConfig = true;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isExpert() {
        return expert;
    }

    public void setExpert(boolean expert) {
        this.expert = expert;
    }

    public EntryType getEntryType() {
        return type;
    }

    public void setEntryType(EntryType type) {
        this.type = type;
    }

    public List<Constraint> getConstraints() {
        return cumultatedConstraints;
    }

    public void addConstraint(Constraint constraint) {
        this.cumultatedConstraints.add(constraint);
    }

    public boolean isPresentInConfig() {
        return presentInConfig;
    }

    public void setPresentInConfig(boolean presentInConfig) {
        this.presentInConfig = presentInConfig;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            Entry entry = (Entry) obj;
            equals = entry.getKey().equalsIgnoreCase(getKey()) && entry.getValue().equalsIgnoreCase(getValue())
                    && (entry.isExpert() == (isExpert())) && (getComment() == entry.getComment())
                    && ObjectUtils.sameObject(getConstraints(), entry.getConstraints())
                    && (getEntryType() == entry.getEntryType());
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xEB;
        int mult = 0xCF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getKey());
        code = code * mult + ObjectUtils.getHashCode(getValue());
        code = code * mult + ObjectUtils.getHashCode(isExpert());
        code = code * mult + ObjectUtils.getHashCode(getComment());
        code = code * mult + ObjectUtils.getHashCode(getEntryType());
        code = code * mult + ObjectUtils.getHashCode(getConstraints());
        return code;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Entry Key:").append(key).append(" - Value: ").append(value)
                .append((isExpert() ? " expert" : " user")).append(" Comment '").append(comment).append("' Type ")
                .append(type).append(" constraints ").append(cumultatedConstraints);
        sb.append(ParsingUtil.CRLF);
        return sb.toString();
    }

}
