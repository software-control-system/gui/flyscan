package fr.soleil.flyscan.lib.model.constraints;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

public class ConstantConstraint extends Constraint {

    private String constant;

    public ConstantConstraint(boolean fromPlugin) {
        super(fromPlugin);
        setConstant(ObjectUtils.EMPTY_STRING);
    }

    public String getConstant() {
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }

    @Override
    protected boolean hasValue() {
        return false;
    }

    @Override
    protected String getType() {
        return ParsingUtil.CONSTANT_KEYWORD;
    }

    @Override
    protected StringBuilder appendValueToStringBuilder(StringBuilder builder) {
        return builder;
    }
}
