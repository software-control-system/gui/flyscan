package fr.soleil.flyscan.lib.model.parsing.plugin;

/**
 * Parameter format
 * 
 * @author guerre-giordano
 * 
 */
public enum ParameterFormat {
    LIST, SCALAR
}
