package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior;

/**
 * Possible type of error strategies
 * 
 * @author guerre-giordano
 * 
 */
public enum ErrorStrategy {
    PAUSE, ABORT

}
