package fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor;

import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

public class ContextMonitor extends Actor {

    public ContextMonitor(EntryContainer parent) {
        super(ParsingUtil.CONTEXT_MONITOR, parent);
    }

}
