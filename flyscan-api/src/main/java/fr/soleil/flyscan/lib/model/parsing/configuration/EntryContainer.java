package fr.soleil.flyscan.lib.model.parsing.configuration;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Generic entry container. Can be a FSObject or a section
 * 
 * @author guerre-giordano
 * 
 */
public abstract class EntryContainer {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntryContainer.class.getName());
    private static final String ERROR_MESSAGE_FORMAT = "Inappropriate value for %s: %s";

    protected String name;
    protected List<Entry> entries;

    /**
     * Constructor
     * 
     * @param name
     */
    public EntryContainer(String name) {
        this.name = name;
        entries = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Entry> getEntries() {
        return new ArrayList<>(entries);
    }

    public void setEntries(List<Entry> entries) {
        if (this.entries != entries) {
            this.entries.clear();
            if (entries != null) {
                this.entries.addAll(entries);
            }
        }
    }

    public void addEntry(Entry entry) {
        boolean canAdd = true;
        if (entry instanceof ParameterEntry) {
            for (Entry e : entries) {
                if (e instanceof ParameterEntry) {
                    if (((ParameterEntry) e).getKey().equalsIgnoreCase(((ParameterEntry) entry).getKey())) {
                        e = entry;
                        canAdd = false;
                        break;
                    }
                }
            }
        }
        if (canAdd) {
            int index = -1;
            int i = -1;
            for (Entry e : entries) {
                i++;
                if (e.getKey().equalsIgnoreCase(entry.getKey())) {
                    index = i;
                }
            }
            if (index != -1) {
                entries.set(index, entry);
            } else {
                entries.add(entry);
            }
        }
    }

    public Entry getEntry(String key) {
        Entry found = null;
        if (key != null) {
            for (Entry entry : entries) {
                if ((entry != null) && key.equalsIgnoreCase(entry.getKey())) {
                    found = entry;
                    break;
                }
            }
        }
        return found;
    }

    /**
     * Set a string value to an entry from which the key is given
     * 
     * @param key String
     * @param value String
     */
    protected void setStringValue(String key, String value) {
        Entry entry = getEntry(key);
        if (entry != null) {
            entry.setValue(value);
        }
    }

    protected boolean getBooleanValue(Entry entry) {
        boolean val = false;
        if (entry != null) {
            String value = entry.getValue();
            try {
                val = Boolean.parseBoolean(value);
            } catch (Exception e) {
                LOGGER.warn(String.format(ERROR_MESSAGE_FORMAT, entry.getKey(), value));
            }
        }
        return val;
    }

    /**
     * Return a boolean value from an entry from which the key is given
     * 
     * @param entryName String
     * @return boolean
     */
    protected boolean getBooleanValue(String entryName) {
        return getBooleanValue(getEntry(entryName));
    }

    public int getIntValue(Entry entry) {
        int val = -1;
        if (entry != null) {
            String value = entry.getValue();
            try {
                val = Integer.parseInt(value);
            } catch (Exception e) {
                LOGGER.warn(String.format(ERROR_MESSAGE_FORMAT, entry.getKey(), value));
            }
        }
        return val;
    }

    /**
     * Return an int value from an entry from which the key is given
     * 
     * @param entryName String
     * @return int
     */
    public int getIntValue(String entryName) {
        return getIntValue(getEntry(entryName));
    }

    protected long getLongValue(Entry entry) {
        long val = -1;
        if (entry != null) {
            String value = entry.getValue();
            try {
                val = Long.parseLong(value);
            } catch (Exception e) {
                LOGGER.warn(String.format(ERROR_MESSAGE_FORMAT, entry.getKey(), value));
            }
        }
        return val;
    }

    /**
     * Return a long value from an entry from which the key is given
     * 
     * @param entryName String
     * @return long
     */
    protected long getLongValue(String entryName) {
        return getLongValue(getEntry(entryName));
    }

    /**
     * Return a String value from an entry from which the key is given
     * 
     * @param entryName String
     * @return String
     */
    protected String getStringValue(String entryName) {
        String val = ObjectUtils.EMPTY_STRING;
        Entry entry = getEntry(entryName);
        if (entry != null) {
            val = entry.getValue();
        }
        return val;
    }

    /**
     * Return the type of the EntryContainer
     * 
     * @return String
     */
    public String getType() {
        String val = ObjectUtils.EMPTY_STRING;
        Entry entry = getEntry(ParsingUtil.TYPE);
        if (entry != null) {
            val = entry.getValue().trim();
        }
        return val;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {

            equals = true;
        } else if (obj.getClass().equals(getClass())) {
            EntryContainer container = (EntryContainer) obj;
            equals = ObjectUtils.sameObject(getName(), container.getName())
                    && ObjectUtils.sameObject(getType(), container.getType())
                    && ObjectUtils.sameObject(entries, container.entries);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xEE;
        int mult = 0xEF;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.getHashCode(getName());
        code = code * mult + ObjectUtils.getHashCode(getType());
        code = code * mult + ObjectUtils.getHashCode(entries);
        return code;
    }

}
