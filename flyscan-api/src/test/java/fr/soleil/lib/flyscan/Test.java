package fr.soleil.lib.flyscan;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.flyscan.lib.model.parsing.configuration.ConfigParser;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

@Ignore
public class Test {

    private final static Logger LOGGER = LoggerFactory.getLogger(Test.class.getName());

    public static void main(String[] args) {
        BufferedReader br = null;
        try {
            StringBuilder sb = new StringBuilder();

            String sCurrentLine;
            br = new BufferedReader(new FileReader("ffs_cfg_example_2.cfg"));
            while ((sCurrentLine = br.readLine()) != null) {
                sb.append(sCurrentLine);
            }
            String content = sb.toString();
            String[] lines = content.split(ParsingUtil.CRLF);
            Configuration result = ConfigParser.parse("ffs_cfg_example_2.cfg", lines);
            System.out.println(result);

//            List<Plugin> results = PluginParser.newInstance().parse("pySensor.prop");
//            for (Plugin result : results) {
//                System.out.println(result);
//            }

        } catch (FSParsingException e) {
            LOGGER.error(e.getMessage());
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }

    }

}
