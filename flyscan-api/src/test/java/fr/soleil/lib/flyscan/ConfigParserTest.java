package fr.soleil.lib.flyscan;

import java.util.List;

import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraint;
import fr.soleil.flyscan.lib.model.parsing.configuration.ConfigParser;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Sensor;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.ActorSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.InfoSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.IntervalsTrajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TabStepByStepTrajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TrajectoryParser;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import junit.framework.TestCase;

public class ConfigParserTest extends TestCase {

    /**
     * Test that config parser class
     * is able to parse sections within a configuration, object within the sections and
     * entries within sections and objects
     * is able to recognize a comment writen over several lines
     * is able to parse a step by step interval trajectory
     * 
     * @throws Exception
     */
    public void testParsingBasicsAndStepByStepTrajectory() throws FSParsingException {
        String[] content = new String[49];
        content[0] = "[info]";
        content[1] = "name = config1";
        content[2] = "keywords = test";
        content[3] = "authors = chuck norris";
        content[4] = "comment = \\";
        content[5] = "on fait des tests\\";
        content[6] = "et on regarde si ca marche";
        content[7] = "revision = 1";
        content[8] = "version =  1.0";
        content[9] = "flyscan_version = 1.0";

        content[10] = "[acquisition]";
        content[11] = "dimensions = [5]";
        content[12] = "continuous = false";
        content[13] = "infinite_mode = false";
        content[14] = "zigzag = false";
        content[15] = " parameter:level:zigzag = expert";
        content[16] = "master_clock_period = 10";

        content[17] = "[behavior]";
        content[18] = " # default strategy";
        content[19] = "error_strategy = abort";

        content[20] = "monitor:";
        content[21] = "error_strategy = pause";

        content[22] = "[actors]";
        content[23] = "actuator:";
        content[24] = "type = SimulatedMotor";
        content[25] = "name = motor_1";
        content[26] = "dimension = 1";
        content[27] = "trajectory =  [(40,50,5)]";
        content[28] = "trajectory_mode = relative | absolute";
        content[29] = "post_scan_action = nothing | goto_prescan_position | goto_first_scan_position";
        content[30] = "-";
        content[31] = "sensor:";
        content[32] = "type = SimulatedSensor";
        content[33] = "name = sensor_1";
        content[34] = "parameter:param2:value = titi";
        content[35] = "-";
        content[36] = "timebase:";
        content[37] = "type = SoftTimeBase";
        content[38] = "name = timebase_1";
        content[39] = "parameter:num_pulses:value = 1";
        content[40] = "parameter:period:value = 10";
        content[41] = " -";
        content[42] = " hook:";
        content[43] = " type = NullHook";
        content[44] = " name = null_hook_order_3";
        content[45] = " order = 3";
        content[46] = "enable = true";
        content[47] = "action = preScan";
        content[48] = "parameter:execution_time_ms:value = 2000";

        Configuration configuration = ConfigParser.parse("configTest", content);

        assertNotNull(configuration);

        assertEquals(4, configuration.getSections().size());

        InfoSection infoSection = null;
        for (Section s : configuration.getSections()) {
            if (s instanceof InfoSection) {
                infoSection = (InfoSection) s;
            }
        }

        assertNotNull(infoSection);

        assertEquals(0, infoSection.getObjects().size());

        assertEquals(7, infoSection.getEntries().size());

        ActorSection actorSection = null;
        for (Section s : configuration.getSections()) {
            if (s instanceof ActorSection) {
                actorSection = (ActorSection) s;
            }
        }

        Trajectory trajectory = null;
        String trajectoryStr = null;
        for (Actor a : actorSection.getActors()) {
            if (a.getName().trim().equalsIgnoreCase("motor_1") && (a instanceof Actuator)) {
                trajectoryStr = ((Actuator) a).getTrajectory();
            }
        }

        assertNotNull(trajectoryStr);

        trajectory = TrajectoryParser.parse("config1", trajectoryStr, "motor_1");

        assertNotNull(trajectory);

        assertEquals("[(40.0,50.0,5)]", trajectory.getStringTrajectory());

        IntervalsTrajectory isbsTrajectory = ((IntervalsTrajectory) trajectory);

        assertEquals(1, isbsTrajectory.getIntervals().size());

        assertEquals(40.0, isbsTrajectory.getIntervals().get(0).getFrom());

        assertEquals(50.0, isbsTrajectory.getIntervals().get(0).getTo());

        assertEquals(5, isbsTrajectory.getIntervals().get(0).getNbAcq());

    }

    public void testParsingContinuousTrajectory() throws FSParsingException {
        String[] content = new String[73];
        content[0] = "[info]";
        content[1] = "name = configContinuous";
        content[2] = "keywords = test";
        content[3] = "authors = zlatan";
        content[4] = "comment = on fait des tests de scan continu";
        content[5] = "revision = 1";
        content[6] = "version =  1.0";
        content[7] = "flyscan_version = 1.0";

        content[8] = "[acquisition]";
        content[9] = "dimensions = [1000]";
        content[10] = "continuous = true";
        content[11] = "zigzag = false";

        content[12] = "[behavior]";
        content[13] = "context_monitor:";
        content[14] = "error_strategy = abort";
        content[15] = "-";
        content[16] = "actuator:";
        content[17] = "delay = 1000";
        content[18] = "error_strategy = abort ";
        content[19] = "-";
        content[20] = "sensor:";
        content[21] = "error_strategy = abort";
        content[22] = "-";
        content[23] = "timebase:";
        content[24] = "error_strategy = abort";
        content[25] = "-";
        content[26] = "hook:";
        content[27] = "error_strategy = notify";
        content[28] = "-";
        content[29] = "monitor:";
        content[30] = "error_strategy = notify";
        content[31] = "-";
        content[32] = "[recording]";
        content[33] = "[actors]";
        content[34] = "continuous_actuator:";
        content[35] = "type = ContinuousMotor";
        content[36] = "name = rz";
        content[37] = "trajectory = [(50,100,5)]";
        content[38] = "parameter:motor_device:value = flyscan/mos/mt_rz";
        content[39] = "parameter:motor_position_attr:value = position";
        content[40] = "parameter:motor_move_state:value = MOVING";
        content[41] = "parameter:motor_abort_cmd:value = Stop";
        content[42] = "parameter:motor_standby_states:value = STANDBY";
        content[43] = "parameter:continuous_velocity:value = 50";
        content[44] = "parameter:step_by_step_velocity:value = 100";
        content[45] = "parameter:polling_period:value = 100";
        content[46] = "parameter:timeout:value = 120";
        content[47] = "parameter:min_poll_delay:value = 50";
        content[48] = "parameter:error_states:value = FAULT,INIT";
        content[49] = "parameter:max_retry:value = 2";
        content[50] = "parameter:retry_delay:value = 100";
        content[51] = "-";
        content[52] = "sensor:";
        content[53] = "type = NI6602Sensor";
        content[54] = "name = MyNI6602Sensor";
        content[55] = "parameter:ni6602_device:value = flyscan/cpt/coders.1";
        content[56] = "parameter:integration_time:value = 0.010";
        content[57] = "parameter:buffer_depth:value  = 2";
        content[58] = "-";
        content[59] = "hook:";
        content[60] = "type = WaitDeviceState";
        content[61] = "name = wait_datamerger";
        content[62] = "action = postScan";
        content[63] = "parameter:device:value = flyscan/data/datamerger";
        content[64] = "parameter:polling_period:value = 1000";
        content[65] = "parameter:waiting_state:value = STANDBY";
        content[66] = "-";
        content[67] = "monitor:";
        content[68] = "type = BoolAttrMonitor";
        content[69] = "name = basic_context_validation";
        content[70] = "parameter:attribute:value = sys/tg_test/1/boolean_scalar";
        content[71] = "parameter:invalid_value:value = false";
        content[72] = "-";

        Configuration configuration = ConfigParser.parse("configTest", content);

        assertNotNull(configuration);

        ActorSection actorSection = null;
        for (Section s : configuration.getSections()) {
            if (s instanceof ActorSection) {
                actorSection = (ActorSection) s;
            }
        }

        Trajectory trajectory = null;
        String trajectoryStr = null;
        for (Actor a : actorSection.getActors()) {
            if (a.getName().trim().equalsIgnoreCase("rz") && (a instanceof Actuator)) {
                trajectoryStr = ((Actuator) a).getTrajectory();
            }
        }

        assertNotNull(trajectoryStr);

        trajectory = TrajectoryParser.parse("configContinuous", trajectoryStr, "rz");

        assertNotNull(trajectory);

        assertEquals("[(50.0,100.0,5)]", trajectory.getStringTrajectory());

        IntervalsTrajectory cTrajectory = ((IntervalsTrajectory) trajectory);

        assertEquals(1, cTrajectory.getIntervals().size());

        assertEquals(50.0, cTrajectory.getIntervals().get(0).getFrom());

        assertEquals(100.0, cTrajectory.getIntervals().get(0).getTo());

        assertEquals(5, cTrajectory.getIntervals().get(0).getNbAcq());

    }

    public void testParsingStepByStepTrajectoryAndConstraints() throws FSParsingException {
        String[] content = new String[103];
        content[0] = "[info]";
        content[1] = "name = config0";
        content[2] = "keywords = hook, test, sequencer";
        content[3] = "authors = the flyscan team";
        content[4] = "comment = flyscan test 0";
        content[5] = "revision = 1";
        content[6] = "version =  1.0";
        content[7] = "flyscan_version = 1.0";

        content[8] = "[acquisition]";
        content[9] = "dimensions = [10][2]";
        content[10] = "continuous = false";
        content[11] = "zigzag = false";
        content[12] = "master_clock_period = 0";
        content[13] = "-";
        content[14] = "[behavior]";
        content[15] = "error_strategy = abort";
        content[16] = "-";
        content[17] = "context_monitor:";
        content[18] = "error_strategy = pause";
        content[19] = "-";
        content[20] = "actuator:";
        content[21] = "delay = 0";
        content[22] = "error_strategy = pause";
        content[23] = "-";
        content[24] = "sensor:";
        content[25] = "error_strategy = abort";
        content[26] = "-";
        content[27] = "timebase:";
        content[28] = "error_strategy = abort";
        content[29] = "-";
        content[30] = "hook:";
        content[31] = "error_strategy = notify";
        content[32] = "-";
        content[33] = "monitor:";
        content[34] = "error_strategy = abort";
        content[35] = "-";

        content[36] = "[recording]";

        content[37] = "[actors]";
        content[38] = "hook:";
        content[39] = "type = PythonHook";
        content[40] = "name = pyhook";
        content[41] = "action = preStep";
        content[42] = "parameter:file_path:value = /home/nicolas/devices/FlyScanServer/Plugins/Hook/PythonHook/example/pyHookExample.py";
        content[43] = "parameter:sys_path:value = [/home/nicolas/flyscandeviceservers/plugins/python/actors]";
        content[44] = "-";
        content[45] = "hook:";
        content[46] = "type = Sequencer";
        content[47] = "name = calibration";
        content[48] = "action = preScan";
        content[49] = "parameter:config_file_path:value = /home/nicolas/flyscanconfig/sequences/seq0.cfg";
        content[50] = "-";
        content[51] = "actuator:";
        content[52] = "type = PythonActuator";
        content[53] = "name = actuator_x";
        content[54] = "dimension = 1";
        content[55] = "trajectory = [10,20,30,40,50,60,70,80,90,100]";
        content[56] = "enable = true";
        content[57] = "parameter:file_path:value = /home/nicolas/devices/FlyScanServer/Plugins/Actuator/PythonActuator/example/pyActuatorExample.py";
        content[58] = "parameter:sys_path:value = [/home/nicolas/flyscandeviceservers/plugins/python/actors]";
        content[59] = "-";
        content[60] = "actuator:";
        content[61] = "type = PythonActuator";
        content[62] = "name = actuator_y";
        content[63] = "dimension = 2";
        content[64] = "trajectory = [10,20]";
        content[65] = "enable = true";
        content[66] = "parameter:file_path:value = /home/nicolas/devices/FlyScanServer/Plugins/Actuator/PythonActuator/example/pyActuatorExample.py";
        content[67] = "parameter:sys_path:value = [/home/nicolas/flyscandeviceservers/plugins/python/actors]";
        content[68] = "-";
        content[69] = "actuator:";
        content[70] = "type = PythonActuator";
        content[71] = "name = actuator_z";
        content[72] = "dimension = 3";
        content[73] = "trajectory = [0,5,10,15,20]";
        content[74] = "enable = false";
        content[75] = "parameter:file_path:value = /home/nicolas/devices/FlyScanServer/Plugins/Actuator/PythonActuator/example/pyActuatorExample.py";
        content[76] = "parameter:sys_path:value = [/home/nicolas/flyscandeviceservers/plugins/python/actors]";
        content[77] = "-";
        content[78] = "sensor:";
        content[79] = "type = SimulatedSensor";
        content[80] = "name = sensor_a";
        content[81] = "parameter:file_path:value = /home/nicolas/devices/FlyScanServer/Plugins/Sensor/PythonSensor/example/pySensorExample.py";
        content[82] = "parameter:sys_path:value = [/home/nicolas/flyscandeviceservers/plugins/python/actors]";
        content[83] = "parameter:bool_scalar:value = True";
        content[84] = "parameter:measurements:value = 3.14116";
        content[85] = "parameter:measurements:in_list = [-100.0, 100.0, 150.0]";
        content[86] = "parameter:str_scalar:value = this is an immutable string";
        content[87] = "parameter:str_scalar:constant";
        content[88] = "parameter:bool_list:value = [true, false, true, false]";
        content[89] = "parameter:num_scalar_2:value = 20";
        content[90] = "parameter:num_scalar_2:in_list = [0, 5, 10, 15, 20, 25, 30, 35, 40]";
        content[91] = "-";
        content[92] = "sensor:";
        content[93] = "type = PythonSensor";
        content[94] = " name = sensor_b";
        content[95] = "parameter:file_path:value = /home/nicolas/devices/FlyScanServer/Plugins/Sensor/PythonSensor/example/pySensorExample.py";
        content[96] = "parameter:sys_path:value = [/home/nicolas/flyscandeviceservers/plugins/python/actors]";
        content[97] = "parameter:bool_scalar:value = True";
        content[98] = "parameter:num_scalar_1:value = 123.456";
        content[99] = "parameter:str_scalar:value =  this is an mutable string";
        content[100] = "parameter:bool_list:value = [False, True, False, True]";
        content[101] = "parameter:num_scalar:value_2 = 30";
        content[102] = "-";

        Configuration configuration = ConfigParser.parse("configTest", content);

        assertNotNull(configuration);

        ActorSection actorSection = null;
        for (Section s : configuration.getSections()) {
            if (s instanceof ActorSection) {
                actorSection = (ActorSection) s;
            }
        }

        Trajectory trajectory = null;
        String trajectoryStr = null;
        for (Actor a : actorSection.getActors()) {
            if (a.getName().trim().equalsIgnoreCase("actuator_x") && (a instanceof Actuator)) {
                trajectoryStr = ((Actuator) a).getTrajectory();
            }
        }

        assertNotNull(trajectoryStr);

        trajectory = TrajectoryParser.parse("config0", trajectoryStr, "actuator_x");

        assertNotNull(trajectory);

        assertEquals("[10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0,100.0]", trajectory.getStringTrajectory());

        TabStepByStepTrajectory tTrajectory = ((TabStepByStepTrajectory) trajectory);

        assertEquals(10, tTrajectory.getPoints().size());

        assertEquals(10.0, tTrajectory.getPoints().get(0));

        assertEquals(20.0, tTrajectory.getPoints().get(1));

        assertEquals(30.0, tTrajectory.getPoints().get(2));

        assertEquals(40.0, tTrajectory.getPoints().get(3));

        assertEquals(50.0, tTrajectory.getPoints().get(4));

        assertEquals(60.0, tTrajectory.getPoints().get(5));

        assertEquals(70.0, tTrajectory.getPoints().get(6));

        assertEquals(80.0, tTrajectory.getPoints().get(7));

        assertEquals(90.0, tTrajectory.getPoints().get(8));

        assertEquals(100.0, tTrajectory.getPoints().get(9));

        Sensor sensor = null;

        for (Actor a : actorSection.getActors()) {
            if ((a instanceof Sensor) && (a.getName().equalsIgnoreCase("sensor_a"))) {
                sensor = (Sensor) a;
            }
        }

        assertNotNull(sensor);

        Entry measurementsEntry = sensor.getEntry("measurements");

        assertNotNull(measurementsEntry);

        List<Constraint> constraints = measurementsEntry.getConstraints();

        assertTrue(!constraints.isEmpty());

        Constraint constraint = constraints.get(0);

        assertTrue(constraint instanceof InConstraint);

        InConstraint inConstraint = (InConstraint) constraint;

        assertEquals(3, inConstraint.getPossibleValues().size());

        int i = 0;
        Number nb0 = null, nb1 = null, nb2 = null;
        for (Number nb : inConstraint.getPossibleValues()) {
            switch (i) {
                case 0:
                    nb0 = nb;
                    break;
                case 1:
                    nb1 = nb;
                    break;
                case 2:
                    nb2 = nb;
                    break;
            }
            i++;
        }

        assertEquals(-100.0, nb0);

        assertEquals(100.0, nb1);

        assertEquals(150.0, nb2);

        boolean isConstraintSatisfied = false;

        Double measurement = Double.valueOf(measurementsEntry.getValue());

        assertEquals(3.14116, measurement);

        for (Number d : inConstraint.getPossibleValues()) {
            if (d.doubleValue() == measurement.doubleValue()) {
                isConstraintSatisfied = true;
            }
        }

        assertFalse(isConstraintSatisfied);

    }
}
