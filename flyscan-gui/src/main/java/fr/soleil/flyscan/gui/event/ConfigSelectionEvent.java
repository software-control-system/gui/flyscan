package fr.soleil.flyscan.gui.event;

import java.util.EventObject;

/**
 * Event fired when a configuration is selected.
 * 
 * @author GIRARDOT
 */
public class ConfigSelectionEvent extends EventObject {

    private static final long serialVersionUID = -1075700725882161050L;

    private final String configName;

    /**
     * Constructs a new {@link ConfigSelectionEvent}.
     * 
     * @param source The source of this event.
     * @param configName The name of the selected configuration.
     */
    public ConfigSelectionEvent(Object source, String configName) {
        super(source);
        this.configName = configName;
    }

    /**
     * Returns the name of the selected configuration.
     * 
     * @return A {@link String}.
     */
    public String getConfigName() {
        return configName;
    }

}
