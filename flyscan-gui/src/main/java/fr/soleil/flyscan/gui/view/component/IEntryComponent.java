package fr.soleil.flyscan.gui.view.component;

import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;

public interface IEntryComponent {

    public void resetToDefault(String defaultValue) throws FSParsingException;

}
