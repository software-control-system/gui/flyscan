package fr.soleil.flyscan.gui.view.renderer;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.box.adapter.SimpleStateAdapter;
import fr.soleil.data.exception.DataAdaptationException;
import fr.soleil.flyscan.gui.FlyScanTangoBox;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.lib.project.awt.ColorUtils;

public class DevStateComboBoxRenderer extends ColoredSelectionListCellRenderer {

    private static final long serialVersionUID = -2464782628735776428L;

    private static final Font SELECTION_FONT = new Font(Font.DIALOG, Font.BOLD | Font.ITALIC, 14);
    private static final Font DEFAULT_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);

    private final SimpleStateAdapter flyScanStateAdapter = new SimpleStateAdapter();

    protected static final Logger LOGGER = LoggerFactory.getLogger(FlyScanTangoBox.class.getName());

    public DevStateComboBoxRenderer(JComponent comp) {
        super(comp);
        setOpaque(true);
        setHorizontalAlignment(JLabel.LEFT);
        setFont(DEFAULT_FONT);
        useDefaultFont = false;
        useSelectionBackground = false;
    }

    @Override
    protected void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        JLabel label = (JLabel) comp;
        final String valueStr = ParsingTool.toString(value);
        try {
            Color color = ColorTool.getColor(flyScanStateAdapter.adapt(valueStr));
            Color selectionBG = getSelectionBackground();
            if (isSelected && (selectionBG != null) && (color != null)) {
                color = ColorUtils.blend(selectionBG, color);
            }
            foreground = Math.round(ColorUtils.getRealGray(color)) < 128 ? Color.WHITE : Color.BLACK;
            comp.setBackground(color);
            font = isSelected ? SELECTION_FONT : DEFAULT_FONT;
        } catch (DataAdaptationException e1) {
            LOGGER.error(e1.getMessage(), e1);
        }
        label.setText(valueStr);
        super.applyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
    }

}
