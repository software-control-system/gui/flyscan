package fr.soleil.flyscan.gui.view.delegate;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.listener.CustomParameterListener;
import fr.soleil.flyscan.gui.listener.FSKeySelector;
import fr.soleil.flyscan.gui.listener.FSMouseSelector;
import fr.soleil.flyscan.gui.listener.LabelErrorMouseAdapter;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.combo.DevStateComboBox;
import fr.soleil.flyscan.gui.view.combo.DeviceComboBox;
import fr.soleil.flyscan.gui.view.combo.FSComboBox;
import fr.soleil.flyscan.gui.view.component.CustomParameterPanel;
import fr.soleil.flyscan.gui.view.component.FSCheckBox;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedCheckComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedNumberListComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedSpinnerComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedStringListComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedTextFieldComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedWheelSwitchComponent;
import fr.soleil.flyscan.gui.view.renderer.DeviceRenderer;
import fr.soleil.flyscan.gui.view.text.FSAdaptableTextArea;
import fr.soleil.flyscan.gui.view.trajectory.ATrajectoryComponent;
import fr.soleil.flyscan.gui.view.trajectory.IntervalsTrajectoryComponent;
import fr.soleil.flyscan.gui.view.trajectory.TabStepByStepTrajectoryComponent;
import fr.soleil.flyscan.lib.model.constraints.ConstantConstraint;
import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.constraints.CountConstraint;
import fr.soleil.flyscan.lib.model.constraints.DeviceClassConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraintStr;
import fr.soleil.flyscan.lib.model.constraints.MinMaxConstraint;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.ParameterEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.IntervalsTrajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TabStepByStepTrajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TrajectoryParser;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;
import fr.soleil.flyscan.lib.model.parsing.plugin.Parameter;
import fr.soleil.flyscan.lib.model.parsing.plugin.ParameterFormat;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

/**
 * Class to do some stuff that were previously done in {@link ConfigurationGUI}.
 * <p>
 * It was created to reduce the code inside {@link ConfigurationGUI}.
 * </p>
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class UIDelegate {
    public static final int GAP = 4, HALF_GAP = GAP / 2;

    public static final String AND = " & ";
    private static final String ANY_DEVICE_CLASS = Utilities.getMessage("flyscan.device.class.any");
    private static final String SINGLE_DEVICE_CONSTRAINTS_ERROR = Utilities
            .getMessage("flyscan.error.device.single.constraints");
    private static final String MULTI_DEVICES_CONSTRAINTS_ERROR = Utilities
            .getMessage("flyscan.error.device.many.constraints");
    private static final String CHECK_CONFIGURATION_FILE = Utilities
            .getMessage("flyscan.error.configuration.file.check");
    private static final String DEVICE_CONSTRAINTS_ERROR = Utilities.getMessage("flyscan.error.device.constraints");
    private static final String COUNT_CONSTRAINT_NOT_RESPECTED_FOR = "Count constraint not respected for ";
    private static final String COLON = ": ";
    private static final String INSTEAD_OF = " instead of ";
    private static final String COUNT_CONSTRAINTS_INCONSISTENT_FOR = "Count constraints inconsistent for ";
    private static final String COUNT_IS = ": count = ";
    private static final String AND_COUNT_IS = " AND count = ";
    private static final String OPEN_PAR = " (";
    private static final String CLOSE_PAR = ") ";
    private static final String NO_RUNNING_DEVICE_MATCHES_THE_GIVEN_CONSTRAINTS = "No running device matches the given constraints:";
    private static final String PLUGIN_PARAMETER = "Plugin parameter ";
    private static final String MUST_HAVE_A_VALUES_PROPERTY = " must have a values property.";
    private static final String NO_VALID_VALUE_FOR_PARAMETER = "No valid value for parameter ";
    private static final String DUE_TO_DECLARED_CONSTRAINTS = " due to declared constraints (please review configuration and plugin files)";
    private static final String IS_NOT_AN_AUTHORIZED_VALUE_FOR = " is not an authorized value for ";
    private static final String PLEASE_REVIEW_CONFIGURATION_AND_PLUGIN_FILES = " (please review configuration and plugin files)";
    private static final String DEVICE_STATE = "device state";
    private static final String FAILED_TO_CREATE_EDITOR_FOR = "Failed to create editor for ";
    private static final String NO_PARAMETER_FOUND_FOR = "No parameter found for ";
    private static final String INVALID_VALUE_ERROR = Utilities.getMessage("flyscan.error.invalid");
    public static final String[] POSSIBLE_VALUES = { "UNKNOWN", "OFF", "ON", "CLOSE", "OPEN", "INSERT", "EXTRACT",
            "MOVING", "STANDBY", "FAULT", "INIT", "RUNNING", "ALARM", "DISABLE" };
    private static final Color HIGHLIGHT = new Color(230, 250, 255);

    private UIDelegate() {
        // Hide constructor
    }

    public static Border buildErrorBorder(final JComponent component) {
        Border border;
        Border compBorder = component.getBorder();
        if (compBorder instanceof TitledBorder) {
            TitledBorder titledBorder = (TitledBorder) compBorder;
            border = new TitledBorder(new LineBorder(Color.RED), titledBorder.getTitle(),
                    titledBorder.getTitleJustification(), titledBorder.getTitlePosition(), titledBorder.getTitleFont(),
                    Color.RED);
        } else {
            border = new LineBorder(Color.RED);
        }
        return border;
    }

    public static GridBagConstraints createContraints(final int fill, final int gridx, final int gridy,
            final double weightx, final double weighty) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = fill;
        constraints.gridx = gridx;
        constraints.gridy = gridy;
        constraints.weightx = weightx;
        constraints.weighty = weighty;
        return constraints;
    }

    public static GridBagConstraints createConstraints(final int y) {
        GridBagConstraints constraints = createContraints(GridBagConstraints.HORIZONTAL, 0, y, 1, 0);
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets = new Insets(5, 5, 5, 5);
        return constraints;
    }

    public static void addFinalGlue(final JPanel panel) {
        GridBagConstraints glueConstraints = createContraints(GridBagConstraints.BOTH, 0, Utilities.getNextY(panel), 1,
                1);
        glueConstraints.gridwidth = GridBagConstraints.REMAINDER;
        glueConstraints.gridheight = GridBagConstraints.REMAINDER;
        panel.add(Box.createGlue(), glueConstraints);
    }

    public static void addParametersPanel(final JPanel panel, final JPanel parametersPanel, int topGap,
            boolean changeVisibility) {
        if ((panel != null) && (parametersPanel != null)) {
            GridBagConstraints parametersPanelConstraints = createContraints(GridBagConstraints.HORIZONTAL, 0,
                    Utilities.getNextY(panel), 1, 0);
            parametersPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
            parametersPanelConstraints.insets = new Insets(topGap, 5, 5, 5);
            panel.add(parametersPanel, parametersPanelConstraints);
            if (changeVisibility) {
                parametersPanel.setVisible(parametersPanel.getComponentCount() > 0);
            }
        }
    }

    public static void updateComponentDimension(final Component comp, final int dim, final boolean width) {
        Dimension size = comp.getPreferredSize();
        boolean changed = false;
        if (width) {
            if (size.width < dim) {
                size.width = dim;
                comp.setPreferredSize(size);
                changed = true;
            }
        } else if (size.height < dim) {
            size.height = dim;
            comp.setPreferredSize(size);
            changed = true;
        }
        if (changed) {
            Container parent = comp.getParent();
            if (parent != null) {
                parent.revalidate();
                if (parent.isShowing()) {
                    parent.repaint();
                }
            }
        }
    }

    public static void updateTopAndBottomGlue(final JPanel... listPanels) {
        if (listPanels != null) {
            for (JPanel listPanel : listPanels) {
                int count = listPanel.getComponentCount();
                if (count > 0) {
                    updateComponentDimension(listPanel.getComponent(0), GAP, false);
                    updateComponentDimension(listPanel.getComponent(count - 1), GAP, false);
                    addFinalGlue(listPanel);
                }
            }
        }
    }

    public static JComponent createGlue(final boolean horizontal, final int dim) {
        JComponent glue = (JComponent) (horizontal ? Box.createHorizontalStrut(dim) : Box.createVerticalStrut(dim));
        glue.setOpaque(true);
        return glue;
    }

    public static void insertTopOrBottomGlue(final JPanel listPanel, final int y, final MouseListener highlighter) {
        JComponent glue = createGlue(false, GAP / 2);
        glue.addMouseListener(highlighter);
        GridBagConstraints constraints = createContraints(GridBagConstraints.HORIZONTAL, 0, y, 0, 0);
        constraints.gridwidth = 7;
        listPanel.add(glue, constraints);
    }

    public static void prepareEntryComponents(final JLabel refLabel, final JComponent... entryComponents) {
        if ((refLabel != null) && (entryComponents != null) && (entryComponents.length > 0)) {
            for (JComponent entryComponent : entryComponents) {
                if (entryComponent != null) {
                    updateComponentDimension(refLabel, entryComponent.getPreferredSize().width, true);
                    updateComponentDimension(entryComponent, refLabel.getPreferredSize().width, true);
                    refLabel.addPropertyChangeListener(Utilities.PREFERRED_SIZE, e -> {
                        updateComponentDimension(entryComponent, refLabel.getPreferredSize().width, true);
                    });
                }
            }
        }
    }

    public static void setupAndAddComponents(final JPanel listPanel, final JLabel nameTitle, final JLabel typeTitle,
            final JLabel enableTitle, final JRadioButton selector, final JComponent nameComponent,
            final JComponent typeComponent, final JComponent enableComponent, final JComponent topGlue,
            final JComponent glue1, final JComponent glue2, final JComponent glue3, final JComponent bottomGlue) {
        if (nameComponent != null) {
            int y = Utilities.getNextY(listPanel);
            if (nameComponent instanceof JTextField) {
                ((JTextField) nameComponent).setDisabledTextColor(Color.GRAY);
            }
            MouseAdapter mouseAdapter = new FSMouseSelector(selector, glue1);
            KeyAdapter keyAdapter = new FSKeySelector(selector);
            topGlue.addMouseListener(mouseAdapter);
            topGlue.addKeyListener(keyAdapter);
            GridBagConstraints constraints = createContraints(GridBagConstraints.HORIZONTAL, 0, y++, 1, 0);
            constraints.gridwidth = 7;
            listPanel.add(topGlue, constraints);
            glue1.addMouseListener(mouseAdapter);
            glue1.addKeyListener(keyAdapter);
            constraints = createContraints(GridBagConstraints.VERTICAL, 0, y, 0, 0);
            listPanel.add(glue1, constraints);
            constraints = createContraints(GridBagConstraints.NONE, 1, y, 0, 0);
            listPanel.add(selector, constraints);
            nameComponent.addMouseListener(mouseAdapter);
            nameComponent.addKeyListener(keyAdapter);
            constraints = createContraints(GridBagConstraints.BOTH, 2, y, 0, 0);
            listPanel.add(nameComponent, constraints);
            prepareEntryComponents(nameTitle, nameComponent);
            glue2.addMouseListener(mouseAdapter);
            glue2.addKeyListener(keyAdapter);
            if ((typeTitle == null) && (enableTitle == null)) {
                constraints = createContraints(GridBagConstraints.BOTH, 3, y, 1, 0);
                constraints.gridwidth = GridBagConstraints.REMAINDER;
            } else {
                constraints = createContraints(GridBagConstraints.VERTICAL, 3, y, 0, 0);
            }
            listPanel.add(glue2, constraints);
            if ((typeTitle != null) && (typeComponent != null)) {
                if (typeComponent instanceof JTextField) {
                    ((JTextField) typeComponent).setDisabledTextColor(Color.GRAY);
                }
                glue3.addMouseListener(mouseAdapter);
                glue3.addKeyListener(keyAdapter);
                typeComponent.addMouseListener(mouseAdapter);
                typeComponent.addKeyListener(keyAdapter);
                constraints = createContraints(GridBagConstraints.BOTH, 4, y, 0, 0);
                listPanel.add(typeComponent, constraints);
                constraints = createContraints(GridBagConstraints.VERTICAL, 5, y, 0, 0);
                listPanel.add(glue3, constraints);
                prepareEntryComponents(typeTitle, typeComponent);
            }
            if ((enableTitle != null) && (enableComponent != null)) {
                enableComponent.addMouseListener(mouseAdapter);
                enableComponent.addKeyListener(keyAdapter);
                constraints = createContraints(GridBagConstraints.BOTH, 6, y, 1, 0);
                listPanel.add(enableComponent, constraints);
                if (enableComponent instanceof ConstrainedCheckBox) {
                    ((ConstrainedCheckBox) enableComponent).addActionListener(e -> {
                        selector.setSelected(true);
                    });
                } else if (enableComponent instanceof FSCheckBox) {
                    ((FSCheckBox) enableComponent).addActionListener(e -> {
                        selector.setSelected(true);
                    });
                }
                updateComponentDimension(enableComponent, enableTitle.getPreferredSize().width, true);
            }
            y++;
            bottomGlue.addMouseListener(mouseAdapter);
            bottomGlue.addKeyListener(keyAdapter);
            constraints = createContraints(GridBagConstraints.HORIZONTAL, 0, y++, 1, 0);
            constraints.gridwidth = 7;
            listPanel.add(bottomGlue, constraints);
            // revalidate listPanel
            listPanel.revalidate();
            // revalidate leftPanel
            listPanel.getParent().revalidate();
            // revalidate leftScrollPane
            listPanel.getParent().getParent().getParent().revalidate();
        } // end if (nameComponent != null)
    }

    public static void checkDividerLocations(final JComponent... components) {
        if (components != null) {
            int maxLocation = 0;
            for (JComponent component : components) {
                if (component instanceof JSplitPane) {
                    JSplitPane splitPane = (JSplitPane) component;
                    splitPane.setDividerLocation(
                            splitPane.getLeftComponent().getPreferredSize().width + splitPane.getDividerSize());
                    if (splitPane.getDividerLocation() > maxLocation) {
                        maxLocation = splitPane.getDividerLocation();
                    }
                }
            }
            if (maxLocation > 0) {
                for (JComponent component : components) {
                    if (component instanceof JSplitPane) {
                        JSplitPane splitPane = (JSplitPane) component;
                        splitPane.setDividerLocation(maxLocation);
                    }
                }
            }
        }
    }

    public static String getErrorLocation(final EntryContainer entryContainer, final FSObject fsObject,
            final Entry entry) {
        String location = Utilities.SPACE;
        StringBuilder locationSB = new StringBuilder(
                entryContainer == null ? ObjectUtils.EMPTY_STRING : entryContainer.getName()).append(Utilities.SLASH);
        if (fsObject != null) {
            locationSB.append(fsObject.getName() + Utilities.SLASH);
        }
        if (entry != null) {
            locationSB.append(entry.getKey());
        }
        location = locationSB.toString();
        return location;
    }

    public static String getErrorLocation(final Entry entry, final EntryContainer entryContainer) {
        FSObject fsObject = null;
        Section section = null;
        if (entryContainer instanceof FSObject) {
            fsObject = (FSObject) entryContainer;
            final EntryContainer fsObjectParent = ((FSObject) entryContainer).getParent();
            if (fsObjectParent instanceof Section) {
                section = (Section) fsObjectParent;
            }
        } else if (entryContainer instanceof Section) {
            section = (Section) entryContainer;
        }
        String location = getErrorLocation(section, fsObject, entry);
        return location;
    }

    public static JTextArea getTextAreaError(final String msg, final String localisation, final JComponent component,
            final JLabel nameLabel, final JRadioButton selector, final String tabTitle,
            final ConfigurationGUI configurationGUI) {
        JTextArea errorLabel = Utilities.getTextAreaError(localisation, msg);
        if (nameLabel != null) {
            nameLabel.setForeground(Color.RED);
        }
        errorLabel.addMouseListener(
                new LabelErrorMouseAdapter(configurationGUI, component, nameLabel, selector, tabTitle));
        return errorLabel;
    }

    /**
     * Return plugin
     * 
     * @param textMatrixTarget ITextMatrixTarget
     * @param fsObject FSObject
     * @return Plugin
     * @throws FSParsingException
     */
    public static Plugin getPlugin(final Collection<Plugin> plugins, final FSObject fsObject)
            throws FSParsingException {
        Plugin plugin = null;
        if (plugins != null) {
            String type = fsObject.getType().trim();
            for (Plugin p : plugins) {
                String pluginName = p.getName().trim();
                if (pluginName.equalsIgnoreCase(type)) {
                    plugin = p;
                }
            }
        }
        return plugin;
    }

    public static void registerError(final StringBuilder errorBuilder, final Object defaultDevices,
            final String configName, final Parameter param, final EntryContainer entryContainer, final Entry entry,
            final JComponent editorComponent, final JLabel labelName, final JRadioButton selector,
            final String tabTitle, final JPanel errorPanel, final Map<Component, JTextComponent> errorsMap,
            final ConfigurationGUI configurationGUI) {
        boolean constraintsOk;
        Collection<Constraint> constraints = param.getConstraints();
        if ((constraints == null) || (constraints.isEmpty())) {
            constraintsOk = false;
        } else {
            constraintsOk = true;
            errorBuilder.append(Utilities.SPACE).append(Utilities.BRACKET_OPENNING);
            boolean add = false;
            for (Constraint constraint : constraints) {
                if (constraint != null) {
                    if (add) {
                        errorBuilder.append(AND);
                    } else {
                        add = true;
                    }
                    errorBuilder.append(constraint.getText());
                }
            }
            errorBuilder.append(Utilities.BRACKET_CLOSING);
        }
        if (!constraintsOk) {
            errorBuilder.append(Utilities.SPACE).append(ANY_DEVICE_CLASS);
        }
        errorBuilder.append(Utilities.POINT);
        if (defaultDevices != null) {
            errorBuilder.append(Utilities.SPACE).append(ArrayUtils.toString(defaultDevices)).append(Utilities.SPACE);
            if (defaultDevices instanceof String) {
                errorBuilder.append(SINGLE_DEVICE_CONSTRAINTS_ERROR);
            } else {
                errorBuilder.append(MULTI_DEVICES_CONSTRAINTS_ERROR);
            }
        }
        errorBuilder.append(Utilities.SPACE).append(String.format(CHECK_CONFIGURATION_FILE, configName));
        String location = getErrorLocation(entryContainer, null, entry);
        JTextArea labelError = getTextAreaError(errorBuilder.toString(), location, editorComponent, labelName, selector,
                tabTitle, configurationGUI);
        errorPanel.add(labelError);
        errorPanel.revalidate();
        errorsMap.put(editorComponent, labelError);
    }

    public static void notifyEdition(final boolean beingEdited, final ConfigurationGUI configurationGUI) {
        for (ConfigurationEditionStateListener listener : configurationGUI.getBeingEditedListeners()) {
            listener.setConfigurationBeingEdited(beingEdited);
        }
    }

    public static void applyValue(final boolean beingEdited, final Entry entry, final String value,
            final Configuration configuration, final ConfigurationGUI configurationGUI) {
        notifyEdition(beingEdited, configurationGUI);
        if (entry != null) {
            entry.setValue(value);
            if (beingEdited) {
                entry.setPresentInConfig(true);
            }
        }
    }

    public static JPanel buildEntryPanel(final EntryContainer entryContainer, final int fontStyle, final int fontSize) {
        JPanel panel = new JPanel(new GridBagLayout());
        // Entry title
        final TitledBorder titledBorder = new TitledBorder(entryContainer.getName());
        titledBorder.setTitleFont(new Font(Font.DIALOG, fontStyle, fontSize));
        panel.setBorder(titledBorder);
        return panel;
    }

    public static JPanel buildObjectPanel(final EntryContainer entryContainer) {
        return buildEntryPanel(entryContainer, Font.PLAIN, 15);
    }

    public static CustomParameterPanel buildCustomParametersPanel(final boolean easyConfig, final String tabTitle,
            final EntryContainer entryContainer, final JRadioButton selector, final boolean enabled,
            final Configuration configuration, final ConfigurationGUI configurationGUI) {
        CustomParameterPanel panel = easyConfig
                ? new CustomParameterPanel(Utilities.MASTER_PARAMETERS, configurationGUI, configuration, enabled)
                : new CustomParameterPanel(configurationGUI, configuration, enabled);
        panel.addCustomParameterListener(new CustomParameterListener() {
            @Override
            public void parameterChanged(Entry entry) {
                configurationGUI.fireEditionStateChanged(true, entry, entryContainer, entry.getValue(), null, null,
                        selector, tabTitle);
            }
        });
        return panel;
    }

    /**
     * Display error
     * 
     * @param entry Entry
     * @param entryContainer EntryContainer
     * @param fsObject FSObject
     * @param nameLabel JLabel
     * @param component Component
     * @param text String
     * @param tabTitle String
     */
    public static void displayError(final Entry entry, final EntryContainer entryContainer, final FSObject fsObject,
            final JLabel nameLabel, final JComponent component, final JRadioButton selector, final String text,
            String tabTitle, final JPanel errorPanel, final Map<Component, JTextComponent> errorsMap,
            final ConfigurationGUI configurationGUI) {
        String location = getErrorLocation(entryContainer, fsObject, entry);
        JTextArea errorLabel = getTextAreaError(text, location, component, nameLabel, selector, tabTitle,
                configurationGUI);
        errorPanel.add(errorLabel);
        if (component != null) {
            errorsMap.put(component, errorLabel);
            component.setBorder(buildErrorBorder(component));
        }
    }

    public static void prepareHighlighting(JRadioButton selector, JPanel listPanel, JPanel infoPanel, CardLayout layout,
            EntryContainer entryContainer, JComponent... toHighlight) {
        selector.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (toHighlight != null) {
                    for (JComponent comp : toHighlight) {
                        if (comp != null) {
                            comp.setBackground(HIGHLIGHT);
                            if (comp instanceof FSCheckBox) {
                                ((FSCheckBox) comp).setCometeBackground(ColorTool.getCometeColor(HIGHLIGHT));
                            }
                        }
                    }
                }
                layout.show(infoPanel, entryContainer.getName());
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                if (toHighlight != null) {
                    for (JComponent comp : toHighlight) {
                        if (comp != null) {
                            if (comp instanceof JTextField) {
                                comp.setBackground(Color.WHITE);
                            } else {
                                comp.setBackground(listPanel.getBackground());
                                if (comp instanceof FSCheckBox) {
                                    ((FSCheckBox) comp)
                                            .setCometeBackground(ColorTool.getCometeColor(listPanel.getBackground()));
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public static void updateComponentsFromProperty(PropertyChangeEvent evt, JComponent refComponent,
            JComponent... toUpdate) {
        if ((evt != null) && Utilities.PREFERRED_SIZE.equals(evt.getPropertyName()) && (refComponent != null)
                && (toUpdate != null) && (toUpdate.length > 0)) {
            Dimension refSize = refComponent.getPreferredSize(), size = null;
            for (JComponent comp : toUpdate) {
                size = comp.getPreferredSize();
                if (size.width < refSize.width) {
                    comp.setPreferredSize(new Dimension(refSize.width, size.height));
                }
            }
        }
    }

    public static boolean mayThrowParsingException(Entry entry) {
        return (entry == null) || entry.isPresentInConfig();
    }

    public static boolean mayDisplayEntryComponent(Entry entry, String configName) {
        return mayThrowParsingException(entry) || !Utilities.CURRENT_CONFIG.equals(configName);
    }

    public static FSParsingException generateParsingException(String message, Throwable cause, JLabel nameLabel,
            JComponent component, Map<FSParsingException, JLabel> errorLabels,
            Map<FSParsingException, JComponent> errorComponents) {
        FSParsingException exception;
        if (cause == null) {
            exception = new FSParsingException(message);
        } else {
            exception = new FSParsingException(message, cause);
        }
        if (nameLabel != null) {
            errorLabels.put(exception, nameLabel);
        }
        if (component != null) {
            errorComponents.put(exception, component);
        }
        return exception;
    }

    public static void applySelectedValue(JComboBox<? super String> box, String valueToSelect) {
        int index = -1;
        if (valueToSelect != null && !valueToSelect.isEmpty()) {
            for (int i = 0; i < box.getItemCount(); i++) {
                if (valueToSelect.equalsIgnoreCase((String) box.getItemAt(i))) {
                    index = i;
                    break;
                }
            }
            if (index < 0) {
                index = box.getItemCount();
                box.addItem(valueToSelect);
            }
        }
        box.setSelectedIndex(index);
    }

    public static void treatDeviceError(String defaultDevice, ComboBox box, String[] deviceClasses, String configName,
            Entry entry, JLabel labelName, Parameter param, EntryContainer entryContainer, String tabTitle,
            JComponent editorComponent, JRadioButton selector, final JPanel errorPanel,
            final Map<Component, JTextComponent> errorsMap, final ConfigurationGUI configurationGUI) {
        applySelectedValue(box, defaultDevice);
        StringBuilder errorBuilder = new StringBuilder(
                String.format(DEVICE_CONSTRAINTS_ERROR, String.valueOf(defaultDevice)));
        registerError(errorBuilder, null, configName, param, entryContainer, entry, editorComponent, labelName,
                selector, tabTitle, errorPanel, errorsMap, configurationGUI);
    }

    public static <T> void checkList(Collection<T> values, int count, final Entry entry,
            final JComponent editorComponent, final JLabel nameLabel, Map<FSParsingException, JLabel> errorLabels,
            Map<FSParsingException, JComponent> errorComponents) throws FSParsingException {
        if ((count > -1) && (values.size() != count) && mayThrowParsingException(entry)) {
            StringBuilder builder = new StringBuilder(COUNT_CONSTRAINT_NOT_RESPECTED_FOR);
            if (entry == null) {
                builder.append(entry);
            } else {
                builder.append(entry.getKey());
            }
            builder.append(COLON).append(values.size()).append(INSTEAD_OF).append(count);
            throw generateParsingException(builder.toString(), null, nameLabel, editorComponent, errorLabels,
                    errorComponents);
        }
    }

    public static int getCountConstraint(final Collection<Constraint> constraints, final Entry entry,
            final JComponent editorComponent, final JLabel nameLabel, Map<FSParsingException, JLabel> errorLabels,
            Map<FSParsingException, JComponent> errorComponents) throws FSParsingException {
        int count = -1;
        if (constraints != null) {
            for (Constraint constraint : constraints) {
                if (constraint instanceof CountConstraint) {
                    CountConstraint countConstraint = (CountConstraint) constraint;
                    if (count < 0) {
                        count = countConstraint.getValue();
                    } else if ((count != countConstraint.getValue()) && mayThrowParsingException(entry)) {
                        StringBuilder builder = new StringBuilder(COUNT_CONSTRAINTS_INCONSISTENT_FOR);
                        if (entry == null) {
                            builder.append(entry);
                        } else {
                            builder.append(entry.getKey());
                        }
                        builder.append(COUNT_IS).append(count);
                        builder.append(AND_COUNT_IS).append(countConstraint.getValue());
                        throw generateParsingException(builder.toString(), null, nameLabel, editorComponent,
                                errorLabels, errorComponents);
                    }
                }
            }
        }
        return count;
    }

    public static int getCountConstraint(final Entry entry, final Parameter param, final JComponent editorComponent,
            final JLabel nameLabel, final Map<FSParsingException, JLabel> errorLabels,
            final Map<FSParsingException, JComponent> errorComponents) throws FSParsingException {
        int count = -1;
        if ((param == null) || (param.getFormat() == ParameterFormat.LIST)) {
            // check count constraint;
            Collection<Constraint> constraints = Utilities.getConstraints(entry, param, false);
            count = getCountConstraint(constraints, entry, editorComponent, nameLabel, errorLabels, errorComponents);
            constraints.clear();
        }
        return count;
    }

    public static JComponent getDefaultComponent(String value) {
        JComponent component;
        if ((value == null) || (!value.contains(ParsingUtil.CRLF))) {
            component = Utilities.createTextField(value);
        } else {
            component = new FSAdaptableTextArea(value);
        }
        return component;
    }

    public static void checkValue(Number val, Collection<Constraint> constraints,
            Collection<Constraint> nonSatisfiedConstraints) {
        for (Constraint constraint : constraints) {
            if (constraint instanceof InConstraint) {
                Collection<Number> possibleValues = ((InConstraint) constraint).getPossibleValues();
                if (!ParsingTool.contains(possibleValues, val)) {
                    nonSatisfiedConstraints.add(constraint);
                }
            } else if (constraint instanceof MinMaxConstraint) {
                double min = ((MinMaxConstraint) constraint).getMin();
                double max = ((MinMaxConstraint) constraint).getMax();
                if (val.doubleValue() < min || val.doubleValue() > max) {
                    nonSatisfiedConstraints.add(constraint);
                }
            } else if (constraint instanceof ConstantConstraint) {
                Number constant = ParsingTool.parseNumber(((ConstantConstraint) constraint).getConstant());
                if (!ParsingTool.sameValue(val, constant)) {
                    nonSatisfiedConstraints.add(constraint);
                }
            }
        }
    }

    public static Collection<Constraint> getNonSatisfiedConstraintsForNumericValue(String value,
            Collection<Constraint> constraints) {
        Collection<Constraint> nonSatisfiedConstraints = new ArrayList<>();
        if (value != null) {
            try {
                if (value.indexOf(',') > -1) {
                    String[] split = value.replace(Utilities.INTERVAL_START, ObjectUtils.EMPTY_STRING)
                            .replace(Utilities.INTERVAL_END, ObjectUtils.EMPTY_STRING).split(Utilities.COMMA);
                    for (String val : split) {
                        checkValue(Double.valueOf(ParsingTool.parseDouble(val)), constraints, nonSatisfiedConstraints);
                    }
                } else {
                    checkValue(Double.valueOf(ParsingTool.parseDouble(value)), constraints, nonSatisfiedConstraints);
                }
            } catch (NumberFormatException e) {
                Utilities.LOGGER.error(e.getMessage(), e);
            }
        }
        return nonSatisfiedConstraints;
    }

    public static Collection<Constraint> getNonSatisfiedConstraintsForStringValue(String value,
            Collection<Constraint> constraints) {
        Collection<Constraint> nonSatisfiedConstraints = new ArrayList<>();
        if (value != null) {
            value = value.trim();
            for (Constraint constraint : constraints) {
                if (constraint instanceof InConstraintStr) {
                    Collection<String> possibleValues = ((InConstraintStr) constraint).getPossibleValues();
                    boolean contains = false;
                    for (String s : possibleValues) {
                        s = s.trim();
                        if (s.equalsIgnoreCase(value)) {
                            contains = true;
                        }
                    }
                    if (!contains) {
                        nonSatisfiedConstraints.add(constraint);
                    }
                } else if (constraint instanceof ConstantConstraint) {
                    String constant = ((ConstantConstraint) constraint).getConstant();
                    if (constant != null) {
                        constant = constant.trim();
                        if (!value.equalsIgnoreCase(constant)) {
                            nonSatisfiedConstraints.add(constraint);
                        }
                    }

                }
            }
        }

        return nonSatisfiedConstraints;
    }

    public static Collection<Constraint> getNonSatisfiedConstraints(Entry entry, String value,
            Collection<Constraint> constraints) {
        Collection<Constraint> nonSatisfiedConstraints = new ArrayList<>();
        if (constraints != null && !constraints.isEmpty() && entry.getEntryType() != null) {
            switch (entry.getEntryType()) {
                case NUMBER:
                case FLOAT:
                case INT:
                    nonSatisfiedConstraints.addAll(getNonSatisfiedConstraintsForNumericValue(value, constraints));
                    break;
                case STRING:
                    nonSatisfiedConstraints.addAll(getNonSatisfiedConstraintsForStringValue(value, constraints));
                    break;
                default:
                    break;
            }
        }
        return nonSatisfiedConstraints;
    }

    public static void applyComboValue(final JComboBox<?> comboBox, final JLabel nameLabel, final Entry entry,
            final String className, final Map<FSParsingException, JLabel> errorLabels,
            final Map<FSParsingException, JComponent> errorComponents) throws FSParsingException {
        if (comboBox != null) {
            int index = -1;
            String value = entry.getValue();
            if (value == null) {
                value = ObjectUtils.EMPTY_STRING;
            } else {
                value = value.toLowerCase();

            }
            for (int i = 0; i < comboBox.getItemCount(); i++) {
                if (value.equalsIgnoreCase(String.valueOf(comboBox.getItemAt(i)))) {
                    index = i;
                    break;
                }
            }
            comboBox.setSelectedIndex(index);
            if (index < 0 && mayThrowParsingException(entry)) {
                throw generateParsingException(String.format(INVALID_VALUE_ERROR, className, entry.getValue()), null,
                        nameLabel, comboBox, errorLabels, errorComponents);
            }
        }
    }

    public static JComponent getTrajectoryComponent(final EntryContainer entryContainer, final Entry entry,
            final Trajectory trajectory, final String tabTitle, final JLabel nameLabel, final JRadioButton selector,
            final JPanel errorPanel, final Map<Component, JTextComponent> errorsMap,
            final ConfigurationGUI configurationGUI, final String configName, final String actuatorName) {
        final ATrajectoryComponent<?> trajectoryComponent;
        String location = getErrorLocation(entry, entryContainer);
        if (trajectory instanceof IntervalsTrajectory) {
            trajectoryComponent = new IntervalsTrajectoryComponent(errorPanel, errorsMap,
                    (IntervalsTrajectory) trajectory, location, configurationGUI, nameLabel, selector, tabTitle,
                    configName, actuatorName);
        } else if (trajectory instanceof TabStepByStepTrajectory) {
            trajectoryComponent = new TabStepByStepTrajectoryComponent(errorPanel, errorsMap,
                    (TabStepByStepTrajectory) trajectory, location, configurationGUI, nameLabel, selector, tabTitle,
                    configName, actuatorName);
        } else {
            // Unknown Trajectory
            trajectoryComponent = null;
        }
        if (trajectoryComponent != null) {
            trajectoryComponent.addBeingEditedListener(new ConfigurationEditionStateListener() {
                @Override
                public void setConfigurationBeingEdited(boolean isBeingEdited) {
                    Trajectory traj = trajectoryComponent.getTrajectory();
                    configurationGUI.fireEditionStateChanged(isBeingEdited, entry, entryContainer,
                            traj.getStringTrajectory(), trajectoryComponent, nameLabel, selector, tabTitle);
                }
            });
        }
        return trajectoryComponent;
    }

    public static String getSimplifiedValue(String value) {
        return value == null ? ObjectUtils.EMPTY_STRING
                : value.replace(ParsingUtil.ESCAPED_LF, ObjectUtils.EMPTY_STRING)
                        .replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                        .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
    }

    public static void prepareEditorComponent(final String configName, final Entry entry, final JLabel nameLabel,
            final JRadioButton selector, final Parameter param, final EntryContainer entryContainer,
            final String tabTitle, final Object[] editorAndException, final Map<FSParsingException, JLabel> errorLabels,
            final Map<FSParsingException, JComponent> errorComponents, final JPanel errorPanel,
            final Map<Component, JTextComponent> errorsMap, final ConfigurationGUI configurationGUI) {
        JComponent editorComponent = null;
        FSParsingException pe = null;
        if ((param != null) && (param.getType() != null)) {
            try {
                if ((nameLabel != null) && (param.getUnit() != null) && (!param.getUnit().isEmpty())) {
                    nameLabel.setText(param.getName().trim() + OPEN_PAR + param.getUnit() + CLOSE_PAR);
                }
                String value = entry.getValue() == null ? ObjectUtils.EMPTY_STRING : entry.getValue();
                int count = getCountConstraint(entry, param, editorComponent, nameLabel, errorLabels, errorComponents);
                Collection<String> authorizedValues = null;
                List<Number> authorizedNumberValues = null;
                switch (param.getType()) {
                    case BOOL:
                        if (param.getFormat() == ParameterFormat.LIST) {
                            Collection<Boolean> values = new ArrayList<>();
                            String s = getSimplifiedValue(value);
                            String defaultValue = getSimplifiedValue(param.getDefaultValue());
                            if (s.isEmpty()) {
                                s = defaultValue;
                            }
                            if (!s.isEmpty()) {
                                String[] tmp = s.split(ParsingUtil.COMMA);
                                for (String str : tmp) {
                                    Boolean d = Boolean.parseBoolean(str.trim());
                                    values.add(d);
                                }
                            }
                            MultivaluedCheckComponent component = new MultivaluedCheckComponent(values, count,
                                    mayThrowParsingException(entry));
                            if (!defaultValue.isEmpty()) {
                                // SCAN-978: At least 1 element when there is a default value.
                                component.setMininimumNumberOfElements(1);
                            }
                            editorComponent = component;
                            checkList(values, count, entry, editorComponent, nameLabel, errorLabels, errorComponents);
                        } else {
                            FSCheckBox checkbox = new FSCheckBox();
                            if (value.toLowerCase().contains(Utilities.TRUE)) {
                                checkbox.setSelected(true);
                            } else {
                                checkbox.setSelected(false);
                            }
                            editorComponent = checkbox;
                        }
                        break;
                    case DEV:
                        authorizedValues = Utilities.getAuthorizedValues(param);
                        boolean hasFoundExpectedDevices = false;
                        String[] deviceClasses = null;
                        Collection<Constraint> constraints = param.getConstraints();
                        if (constraints != null) {
                            Collection<String> classList = new ArrayList<>();
                            boolean firstTime = true;
                            for (Constraint constraint : constraints) {
                                if (constraint instanceof DeviceClassConstraint) {
                                    Collection<String> values = ((DeviceClassConstraint) constraint)
                                            .getPossibleValues();
                                    if (values != null) {
                                        if (firstTime) {
                                            classList.addAll(values);
                                            firstTime = false;
                                        } else {
                                            classList.retainAll(values);
                                        }
                                    }
                                }
                            }
                            if (!firstTime) {
                                deviceClasses = classList.toArray(new String[classList.size()]);
                                classList.clear();
                            }
                        }
                        Collection<String> devices = new ArrayList<>();

                        Object defaultDevices = Utilities.getDefaultDevices(entry, param);
                        if (deviceClasses == null) {
                            if (param.getFormat() == ParameterFormat.LIST) {
                                Collection<String> values = new ArrayList<>();
                                String[] tmp = (String[]) defaultDevices;
                                for (String str : tmp) {
                                    str = str.trim();
                                    if (!str.isEmpty()) {
                                        hasFoundExpectedDevices = true;
                                    }
                                    values.add(str);
                                }
                                MultivaluedTextFieldComponent component = new MultivaluedTextFieldComponent(values,
                                        count, mayThrowParsingException(entry));
                                if (tmp.length > 0) {
                                    // SCAN-978: At least 1 element when there is a default value.
                                    component.setMininimumNumberOfElements(1);
                                }
                                editorComponent = component;
                                checkList(values, count, entry, editorComponent, nameLabel, errorLabels,
                                        errorComponents);
                            } else {
                                // if no from-class parameter is specified, use a text field
                                editorComponent = getDefaultComponent(value);
                                if (editorComponent instanceof JTextField) {
                                    hasFoundExpectedDevices = !((JTextField) editorComponent).getText().trim()
                                            .isEmpty();
                                } else {
                                    hasFoundExpectedDevices = !((FSAdaptableTextArea) editorComponent).getTextArea()
                                            .getText().trim().isEmpty();
                                }
                            }
                        } else { // end if (deviceClasses == null)
                            if (deviceClasses.length != 0) {
                                if (param.getFormat() == ParameterFormat.LIST) {
                                    hasFoundExpectedDevices = Utilities.filterDevicesForClasses(authorizedValues,
                                            devices, deviceClasses, (String[]) defaultDevices);
                                } else {
                                    hasFoundExpectedDevices = Utilities.filterDevicesForClasses(authorizedValues,
                                            devices, deviceClasses, (String) defaultDevices);
                                }
                            }
                            // Use the renderer that displays strikethrough text for unexported devices
                            if (param.getFormat() == ParameterFormat.LIST) {
                                Collection<String> values = new ArrayList<>();
                                String[] tmp = (String[]) defaultDevices;
                                for (String str : tmp) {
                                    values.add(str);
                                }
                                MultivaluedStringListComponent component = new MultivaluedStringListComponent(devices,
                                        values, count, false, false, DeviceRenderer.class,
                                        mayThrowParsingException(entry));
                                if (tmp.length > 0) {
                                    // SCAN-978: At least 1 element when there is a default value.
                                    component.setMininimumNumberOfElements(1);
                                }
                                editorComponent = component;
                                checkList(values, count, entry, editorComponent, nameLabel, errorLabels,
                                        errorComponents);
                            } else {
                                final DeviceComboBox deviceBox = new DeviceComboBox();
                                editorComponent = deviceBox;
                                String defaultDevice = (String) defaultDevices;
                                if (deviceClasses.length == 0) {
                                    if (entry.isPresentInConfig()) {
                                        treatDeviceError(defaultDevice, deviceBox.getDeviceBox(), deviceClasses,
                                                configName, entry, nameLabel, param, entryContainer, tabTitle,
                                                editorComponent, selector, errorPanel, errorsMap, configurationGUI);
                                    } else {
                                        applySelectedValue(deviceBox.getDeviceBox(), defaultDevice);
                                    }
                                } else if (hasFoundExpectedDevices) {
                                    boolean deviceFound = false;
                                    for (String s : devices) {
                                        deviceBox.getDeviceBox().addItem(s);
                                        if (s.equalsIgnoreCase(defaultDevice)) {
                                            deviceBox.getDeviceBox().setSelectedItem(s);
                                            deviceFound = true;
                                        }
                                    }
                                    if (!deviceFound) {
                                        if (entry.isPresentInConfig()) {
                                            treatDeviceError(defaultDevice, deviceBox.getDeviceBox(), deviceClasses,
                                                    configName, entry, nameLabel, param, entryContainer, tabTitle,
                                                    editorComponent, selector, errorPanel, errorsMap, configurationGUI);
                                        } else {
                                            applySelectedValue(deviceBox.getDeviceBox(), defaultDevice);
                                        }
                                    }
                                }
                                // TODO add device even if it does not match constraints
                            } // end if (param.getFormat() == ParameterFormat.LIST) ... else
                        } // end if (deviceClasses==null) ... else

                        if (entry.isPresentInConfig() && !hasFoundExpectedDevices) {
                            if (editorComponent instanceof DeviceComboBox) {
                                DeviceComboBox deviceBox = (DeviceComboBox) editorComponent;
                                applySelectedValue(deviceBox.getDeviceBox(), (String) defaultDevices);
                            }
                            StringBuilder errorBuilder = new StringBuilder(
                                    NO_RUNNING_DEVICE_MATCHES_THE_GIVEN_CONSTRAINTS);
                            registerError(errorBuilder, defaultDevices, configName, param, entryContainer, entry,
                                    editorComponent, nameLabel, selector, tabTitle, errorPanel, errorsMap,
                                    configurationGUI);
                        }
                        break;
                    case ENUM:
                        if (param.getFormat() == ParameterFormat.LIST) {
                            Collection<String> values = new ArrayList<>();
                            String s = value.replace(ParsingUtil.ESCAPED_LF, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            if (!s.isEmpty()) {
                                String[] tmp = s.split(ParsingUtil.COMMA);
                                for (String str : tmp) {
                                    values.add(str);
                                }
                            }
                            checkList(values, count, entry, editorComponent, nameLabel, errorLabels, errorComponents);
                            if ((param.getValues() == null) || (param.getValues().length == 0)) {
                                if (mayThrowParsingException(entry)) {
                                    throw generateParsingException(
                                            PLUGIN_PARAMETER + param.getName() + MUST_HAVE_A_VALUES_PROPERTY, null,
                                            nameLabel, editorComponent, errorLabels, errorComponents);
                                }
                            } else {
                                Collection<String> possibleValues = new ArrayList<>();
                                for (String str : param.getValues()) {
                                    possibleValues.add(str);
                                }
                                editorComponent = new MultivaluedStringListComponent(possibleValues, values, count,
                                        false, false, null, mayThrowParsingException(entry));
                            }

                        } else {
                            FSComboBox combo = new FSComboBox();
                            Utilities.prepareComboBoxLookAndFeel(combo);
                            editorComponent = combo;
                            if (param.getValues() == null || param.getValues().length == 0) {
                                if (mayThrowParsingException(entry)) {
                                    throw generateParsingException(
                                            PLUGIN_PARAMETER + param.getName() + MUST_HAVE_A_VALUES_PROPERTY, null,
                                            nameLabel, editorComponent, errorLabels, errorComponents);
                                }
                            } else {
                                for (String s : param.getValues()) {
                                    combo.addItem(s);
                                }
                                applyComboValue(combo, nameLabel, entry, param.getName(), errorLabels, errorComponents);
                            }
                        }
                        break;
                    case INT:
                    case NUMBER:
                    case FLOAT:
                        boolean intOnly = (param.getType() == EntryType.INT);
                        authorizedNumberValues = Utilities.getAuthorizedNumberValues(param);
                        if (authorizedNumberValues == null) {
                            if (intOnly) {
                                if (param.getFormat() == ParameterFormat.LIST) {
                                    Collection<Long> values = new ArrayList<>();
                                    String s = value.replace(ParsingUtil.ESCAPED_LF, ObjectUtils.EMPTY_STRING)
                                            .replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                            .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                                    if (!s.isEmpty()) {
                                        String[] tmp = s.split(ParsingUtil.COMMA);
                                        for (String str : tmp) {
                                            values.add(Long.valueOf(ParsingTool.parseLong(str)));
                                        }
                                    }
                                    MultivaluedSpinnerComponent multiSpinner = new MultivaluedSpinnerComponent(values,
                                            count, mayThrowParsingException(entry));
                                    // SCAN-977: Whatever the default choice, we want to be sure the dimensions are
                                    // always represented as an array.
                                    multiSpinner.setScalarContentAllowed(false);
                                    // SCAN-978: At least 1 dimension.
                                    multiSpinner.setMininimumNumberOfElements(1);
                                    editorComponent = multiSpinner;
                                    checkList(values, count, entry, editorComponent, nameLabel, errorLabels,
                                            errorComponents);
                                } else {
                                    editorComponent = Utilities.createSpinner();
                                    ((Spinner) editorComponent).setNumberValue(ParsingTool.parseLong(value));
                                }
                            } else {
                                if (param.getFormat() == ParameterFormat.LIST) {
                                    Collection<Double> values = new ArrayList<>();
                                    String s = getSimplifiedValue(value);
                                    String defaultValue = getSimplifiedValue(param.getDefaultValue());
                                    if (s.isEmpty()) {
                                        s = defaultValue;
                                    }
                                    if (!s.isEmpty()) {
                                        String[] tmp = s.split(ParsingUtil.COMMA);
                                        for (String str : tmp) {
                                            Double d = Double.valueOf(ParsingTool.parseDouble(str));
                                            values.add(d);
                                        }
                                    }
                                    MultivaluedWheelSwitchComponent component = new MultivaluedWheelSwitchComponent(
                                            values, count, null, mayThrowParsingException(entry));
                                    if (!defaultValue.isEmpty()) {
                                        // SCAN-978: At least 1 element when there is a default value.
                                        component.setMininimumNumberOfElements(1);
                                    }
                                    editorComponent = component;
                                    checkList(values, count, entry, editorComponent, nameLabel, errorLabels,
                                            errorComponents);
                                } else {
                                    editorComponent = Utilities.createWheelSwitch();
                                    double val;
                                    val = ParsingTool.parseDouble(value);
                                    ((WheelSwitch) editorComponent).setNumberValue(val);
                                }
                            }
                        } else if (authorizedNumberValues.isEmpty()) {
                            editorComponent = Utilities.createTextField(null);
                            if (mayThrowParsingException(entry)) {
                                throw generateParsingException(
                                        NO_VALID_VALUE_FOR_PARAMETER + param.getName() + DUE_TO_DECLARED_CONSTRAINTS,
                                        null, nameLabel, editorComponent, errorLabels, errorComponents);
                            }
                        } else {
                            if (param.getFormat() == ParameterFormat.LIST) {
                                Collection<Number> values = new ArrayList<>();
                                String s = getSimplifiedValue(value);
                                String defaultValue = getSimplifiedValue(param.getDefaultValue());
                                if (s.isEmpty()) {
                                    s = getSimplifiedValue(defaultValue);
                                }
                                if (!s.isEmpty()) {
                                    String[] tmp = s.split(ParsingUtil.COMMA);
                                    if (intOnly) {
                                        for (String str : tmp) {
                                            values.add(Long.valueOf(ParsingTool.parseLong(str)));
                                        }
                                    } else {
                                        for (String str : tmp) {
                                            values.add(Double.valueOf(ParsingTool.parseDouble(str)));
                                        }
                                    }
                                    MultivaluedNumberListComponent component = new MultivaluedNumberListComponent(
                                            authorizedNumberValues, values, count, false, null,
                                            mayThrowParsingException(entry), intOnly);
                                    if (!defaultValue.isEmpty()) {
                                        // SCAN-978: At least 1 element when there is a default value.
                                        component.setMininimumNumberOfElements(1);
                                    }
                                    editorComponent = component;

                                    checkList(values, count, entry, editorComponent, nameLabel, errorLabels,
                                            errorComponents);
                                }
                            } else {
                                FSComboBox combo = new FSComboBox();
                                Utilities.prepareComboBoxLookAndFeel(combo);
                                combo.setValueList(authorizedNumberValues.toArray());
                                editorComponent = combo;
                                if (!value.trim().isEmpty()) {
                                    Number nb = ParsingTool.parseNumber(value);
                                    int index = ParsingTool.indexOf(authorizedNumberValues, nb);
                                    if (index > -1) {
                                        combo.setSelectedIndex(index);
                                    } else if (mayThrowParsingException(entry)) {
                                        throw generateParsingException(
                                                nb + IS_NOT_AN_AUTHORIZED_VALUE_FOR + param.getName()
                                                        + PLEASE_REVIEW_CONFIGURATION_AND_PLUGIN_FILES,
                                                null, nameLabel, editorComponent, errorLabels, errorComponents);
                                    }
                                }
                            }
                        } // end if (authorizedNumberValues == null) else if (authorizedNumberValues.isEmpty()) ... else
                        break;
                    case DEV_ATTR:
                    case STRING:
                        authorizedValues = Utilities.getAuthorizedValues(param);
                        if (param.getFormat() == ParameterFormat.LIST) {
                            Collection<String> values = new ArrayList<>();
                            String s = value.replace(ParsingUtil.ESCAPED_LF, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            if (!s.isEmpty()) {
                                String[] tmp = s.split(ParsingUtil.COMMA);
                                for (String str : tmp) {
                                    values.add(str.trim());
                                }
                            }
                            if (authorizedValues == null) {
                                editorComponent = new MultivaluedTextFieldComponent(values, count,
                                        mayThrowParsingException(entry));
                            } else {
                                editorComponent = new MultivaluedStringListComponent(authorizedValues, values, count,
                                        false, false, null, mayThrowParsingException(entry));
                            }
                            checkList(values, count, entry, editorComponent, nameLabel, errorLabels, errorComponents);
                        } else if (authorizedValues == null) {
                            if (param.getType() == EntryType.DEV_ATTR) {
                                editorComponent = Utilities.createTextField(value);
                            } else {
                                editorComponent = getDefaultComponent(value);
                            }
                        } else {
                            FSComboBox combo = new FSComboBox();
                            Utilities.prepareComboBoxLookAndFeel(combo);
                            editorComponent = combo;
                            for (String s : authorizedValues) {
                                combo.addItem(s);
                            }
                            applyComboValue(combo, nameLabel, entry, param.getName(), errorLabels, errorComponents);
                        }
                        break;
                    case JSON:
                        editorComponent = new FSAdaptableTextArea(value);
                        break;
                    case DEV_STATE:
                        authorizedValues = Utilities.getAuthorizedValues(param);
                        Collection<String> possibleValues = new ArrayList<>();
                        for (String str : POSSIBLE_VALUES) {
                            boolean add = true;
                            if (authorizedValues != null) {
                                add = false;
                                for (String val : authorizedValues) {
                                    if (str.equalsIgnoreCase(val)) {
                                        add = true;
                                        break;
                                    }
                                }
                            }
                            if (add) {
                                possibleValues.add(str);
                            }
                        }
                        if (param.getFormat() == ParameterFormat.LIST) {
                            Collection<String> values = new ArrayList<>();
                            String s = value.replace(ParsingUtil.ESCAPED_LF, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            if (!s.isEmpty()) {
                                String[] tmp = s.split(ParsingUtil.COMMA);
                                for (String str : tmp) {
                                    values.add(str);
                                }
                            }
                            editorComponent = new MultivaluedStringListComponent(possibleValues, values, count, true,
                                    true, null, mayThrowParsingException(entry));
                            checkList(values, count, entry, editorComponent, nameLabel, errorLabels, errorComponents);
                        } else {
                            DevStateComboBox combo = new DevStateComboBox(possibleValues);
                            for (String s : possibleValues) {
                                if ((s != null) && s.equalsIgnoreCase(value)) {
                                    combo.setSelectedItem(s);
                                    break;
                                }
                            }
                            editorComponent = combo;
                            applyComboValue(combo, nameLabel, entry, DEVICE_STATE, errorLabels, errorComponents);
                        }
                        break;
                    case TRAJECTORY:
                        // Custom trajectory (SCAN-395, Case II)
                        String className;
                        if (entryContainer instanceof Actuator) {
                            className = Actuator.class.getSimpleName();
                        } else {
                            className = entryContainer.getClass().getSimpleName();
                        }
                        String actuatorName = className + Utilities.SPACE + entryContainer.getName();
                        Trajectory trajectory = TrajectoryParser.parse(configName, value, actuatorName);
                        editorComponent = getTrajectoryComponent(entryContainer, entry, trajectory, tabTitle, nameLabel,
                                selector, errorPanel, errorsMap, configurationGUI, configName, actuatorName);
                        break;
                    default:
                        break;

                }
            } catch (Exception e) {
                if (e instanceof FSParsingException) {
                    pe = (FSParsingException) e;
                } else {
                    String msg = FAILED_TO_CREATE_EDITOR_FOR + (entry == null ? null : entry.getKey());
                    pe = new FSParsingException(msg, e);
                }
            }
        } else {
            Utilities.LOGGER.error(NO_PARAMETER_FOUND_FOR + entry.getKey());
        }

        editorAndException[0] = editorComponent;
        editorAndException[1] = pe;
    }

    public static void processParams(final FSObject fsObject, final Plugin plugin, final String configName) {
        // If a field is specified within a plugin, it must be shown in the GUI even if it is not overridden
        for (Parameter param : plugin.getParameters()) {
            // If a default value has been specified
            String defaultValue = param.getDefaultValue();
            // If no entry is found with this parameter name
            if ((fsObject.getEntry(param.getName()) == null) && (!Utilities.CURRENT_CONFIG.equals(configName))) {
                // Create a new entry for GUI
                ParameterEntry entry = new ParameterEntry();
                entry.setKey(param.getName());
                String name = param.getName();
                if (name == null) {
                    name = ObjectUtils.EMPTY_STRING;
                } else {
                    name = name.trim();
                }
                entry.setValue(defaultValue);
                entry.setEntryType(param.getType());
                entry.setExpert(param.isExpert());
                Collection<Constraint> constraints = param.getConstraints();
                if (constraints != null) {
                    for (Constraint constraint : constraints) {
                        entry.addConstraint(constraint);
                    }
                }
                fsObject.addEntry(entry);
                entry.setPresentInConfig(false);
            }
        } // end for (Parameter param : pluginObj.getParameters())

        // Sort entries (SCAN-961)
        Utilities.sortEntries(fsObject, plugin);
    }

    public static int getNbRecordingNumericEntriesDifferentOfZero(final Section section) {
        int nb = 0;
        for (final Entry entry : section.getEntries()) {
            String key = entry.getKey();
            if (key.equalsIgnoreCase(ParsingUtil.SPLIT) || key.equalsIgnoreCase(ParsingUtil.MAX_SCAN_LINES_PER_FILE)
                    || key.equalsIgnoreCase(ParsingUtil.MAX_STEPS_PER_FILE)) {
                if (section.getIntValue(entry) != 0) {
                    nb++;
                }
            }

        }
        return nb;
    }

    public static void updateName(String name, JRadioButton objectSelector, JScrollPane objectPanelScrollPane) {
        Container parent = objectPanelScrollPane.getParent();
        if (parent instanceof JPanel) {
            JPanel infoPanel = (JPanel) parent;
            LayoutManager infoLayout = infoPanel.getLayout();
            if (infoLayout instanceof CardLayout) {
                CardLayout layout = (CardLayout) infoLayout;
                SwingUtilities.invokeLater(() -> {
                    objectSelector.setName(name);
                    infoPanel.remove(objectPanelScrollPane);
                    infoPanel.add(objectPanelScrollPane, name);
                    if (objectSelector.isSelected()) {
                        layout.show(infoPanel, name);
                    }
                });
            }
        }
    }

}
