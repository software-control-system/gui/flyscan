package fr.soleil.flyscan.gui.view.scrollpane;

import java.awt.Component;
import java.awt.Dimension;

/**
 * A {@link FSScrollPane} that takes some margin for its preferred size according to its vertical scroll bar.
 * 
 * @author GIRARDOT
 */
public class FSVerticalScrollPane extends FSScrollPane {

    private static final long serialVersionUID = 454829599326542987L;

    /**
     * Creates an empty (no viewport view) {@link FSVerticalScrollPane} where both horizontal and vertical scrollbars
     * appear when needed.
     */
    public FSVerticalScrollPane() {
        super();
    }

    /**
     * Creates a {@link FSVerticalScrollPane} that displays the contents of the specified component, where both
     * horizontal and vertical scrollbars appear whenever the component's contents are larger than the view.
     * 
     * @param view the component to display in the scrollpane's viewport
     */
    public FSVerticalScrollPane(Component view) {
        super(view);
    }

    /**
     * Creates an empty (no viewport view) {@link FSVerticalScrollPane} with specified scrollbar policies. The available
     * policy settings are listed at #setVerticalScrollBarPolicy and #setHorizontalScrollBarPolicy.
     * 
     * @param vsbPolicy an integer that specifies the vertical scrollbar policy
     * @param hsbPolicy an integer that specifies the horizontal scrollbar policy
     * @see #setViewportView(Component)
     */
    public FSVerticalScrollPane(int vsbPolicy, int hsbPolicy) {
        super(vsbPolicy, hsbPolicy);
    }

    /**
     * Creates a {@link FSVerticalScrollPane} that displays the view component in a viewport whose view position can be
     * controlled with a pair of scrollbars. The scrollbar policies specify when the scrollbars are displayed, for
     * example, if <code>vsbPolicy</code> is <code>VERTICAL_SCROLLBAR_AS_NEEDED</code> then the vertical scrollbar only
     * appears if the view doesn't fit vertically. The available policy settings are listed at
     * #setVerticalScrollBarPolicy and #setHorizontalScrollBarPolicy.
     * 
     * @param view the component to display in the scrollpanes viewport
     * @param vsbPolicy an integer that specifies the vertical scrollbar policy
     * @param hsbPolicy an integer that specifies the horizontal scrollbar policy
     * @see #setViewportView(Component)
     */
    public FSVerticalScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
        super(view, vsbPolicy, hsbPolicy);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size = super.getPreferredSize();
        if (!isPreferredSizeSet()) {
            size.width += getVerticalScrollBar().getPreferredSize().width + 5;
        }
        return size;
    }

}
