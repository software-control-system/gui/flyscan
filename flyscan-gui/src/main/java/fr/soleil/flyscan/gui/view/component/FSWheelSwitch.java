package fr.soleil.flyscan.gui.view.component;

import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;

public class FSWheelSwitch extends WheelSwitch implements IEntryComponent {

    private static final long serialVersionUID = 4789746498004229041L;

    public FSWheelSwitch() {
        super();
        setFormat("%9.4f");
        setFormatEditable(true);
        setMinIncluded(true);
        setMaxIncluded(true);
        setInfinityAllowed(true);
        setNanAllowed(true);
    }

    @Override
    public void resetToDefault(String defaultValue) throws FSParsingException {
        if (defaultValue != null) {
            setNumberValue(ParsingTool.parseDouble(defaultValue));
        }
    }

}
