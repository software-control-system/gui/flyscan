package fr.soleil.flyscan.gui.view.text;

import fr.soleil.comete.swing.AdaptableTextArea;
import fr.soleil.comete.swing.util.DynamicSizeTextArea;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;

public class FSAdaptableTextArea extends AdaptableTextArea implements IEntryComponent {

    private static final long serialVersionUID = -7530196552486998279L;

    public FSAdaptableTextArea(String text) {
        super(text);
    }

    public FSAdaptableTextArea() {
        super();
    }

    @Override
    public EscapedTextArea getTextArea() {
        return (EscapedTextArea) super.getTextArea();
    }

    @Override
    protected DynamicSizeTextArea generateTextArea() {
        return new EscapedTextArea();
    }

    @Override
    public void resetToDefault(String defaultValue) throws FSParsingException {
        if (defaultValue != null) {
            setText(defaultValue);
        }
    }

}
