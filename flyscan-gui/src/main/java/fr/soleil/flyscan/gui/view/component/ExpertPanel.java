package fr.soleil.flyscan.gui.view.component;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JPanel;
import javax.swing.border.Border;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.lib.project.swing.border.FoldableBorder;

/**
 * {@link JPanel} dedicated to expert entries.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ExpertPanel extends JPanel implements PropertyChangeListener {

    private static final long serialVersionUID = -4503228147170841704L;

    private EntryPanel mainPanel;
    private FoldableBorder border;

    public ExpertPanel() {
        super(new BorderLayout());
        mainPanel = new EntryPanel();
        add(mainPanel, BorderLayout.CENTER);
        border = new FoldableBorder(Utilities.EXPERT_PARAMETERS);
        border.setTitleFont(Utilities.PARAMETER_TITLE_FONT);
        border.setButtonPosition(FoldableBorder.LEFT);
        setBorder(border);
        border.addPropertyChangeListener(FoldableBorder.BORDER_FOLDED, this);
        border.setFolded(true);
        setVisible(false);
        addMouseListener(border);
        addMouseMotionListener(border);
    }

    @Override
    public FoldableBorder getBorder() {
        return border;
    }

    @Override
    public void setBorder(Border border) {
        if (border == this.border) {
            super.setBorder(border);
        }
    }

    @Override
    protected void addImpl(Component comp, Object constraints, int index) {
        if (comp == mainPanel) {
            super.addImpl(comp, constraints, index);
        }
    }

    @Override
    public void remove(Component comp) {
        // nop
    }

    @Override
    public void remove(int index) {
        // nop
    }

    @Override
    public void removeAll() {
        // nop
    }

    /**
     * Returns the {@link JPanel} in which to add components.
     * 
     * @return A {@link JPanel}.
     */
    public JPanel getMainPanel() {
        return mainPanel;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && FoldableBorder.BORDER_FOLDED.equals(evt.getPropertyName())) {
            Object value = evt.getNewValue();
            if (value instanceof Boolean) {
                mainPanel.setVisible(!((Boolean) value).booleanValue());
            }
        }
    }

    @Override
    protected void finalize() {
        if (mainPanel != null) {
            mainPanel.removeAll();
            mainPanel = null;
            if (getComponentCount() == 1) {
                super.remove(0);
            }
        }
        if (border != null) {
            removeMouseListener(border);
            removeMouseMotionListener(border);
            border.removePropertyChangeListener(this);
            border = null;
            super.setBorder(null);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * JPanel dedicated to displaying entries components.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    protected class EntryPanel extends JPanel {

        private static final long serialVersionUID = -763548425635998646L;

        public EntryPanel() {
            super(new GridBagLayout());
        }

        protected void updateVisibility() {
            boolean visible = EntryPanel.this.getComponentCount() > 0;
            if (ExpertPanel.this.isVisible() != visible) {
                ExpertPanel.this.setVisible(visible);
                ExpertPanel.this.revalidate();
                Container parent = ExpertPanel.this.getParent();
                if (parent != null) {
                    parent.revalidate();
                }
            }
        }

        @Override
        protected void addImpl(Component comp, Object constraints, int index) {
            super.addImpl(comp, constraints, index);
            updateVisibility();
        }

        @Override
        public void remove(Component comp) {
            super.remove(comp);
            updateVisibility();
        }

        @Override
        public void remove(int index) {
            super.remove(index);
            updateVisibility();
        }

        @Override
        public void removeAll() {
            super.removeAll();
            updateVisibility();
        }

    }

}
