package fr.soleil.flyscan.gui.view.ui;

import java.awt.Insets;

import javax.swing.plaf.metal.MetalTabbedPaneUI;

/**
 * A {@link MetalTabbedPaneUI} which uses the minimum possible space for tabs and which doesn't display the focus
 * border.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class FSTabbedPaneUI extends MetalTabbedPaneUI {

    // Margin to avoid tab corner
    private static final int GAP = 6;
    // TOP and BOTTOM tab: corner is on the left.
    private static final Insets TOP_TAB_INSETS = new Insets(1, GAP, 0, 1);
    private static final Insets BOTTOM_TAB_INSETS = new Insets(0, GAP, 1, 1);
    // LEFT and RIGHT tab: corner is at the top.
    private static final Insets LEFT_TAB_INSETS = new Insets(GAP, 1, 1, 0);
    private static final Insets RIGHT_TAB_INSETS = new Insets(GAP, 0, 1, 1);

    public FSTabbedPaneUI() {
        super();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        focus = selectColor; // Don't display surrounding focus border
    }

    @Override
    protected Insets getTabInsets(int tabPlacement, int tabIndex) {
        Insets insets;
        switch (tabPlacement) {
            case LEFT:
                insets = LEFT_TAB_INSETS;
                break;
            case RIGHT:
                insets = RIGHT_TAB_INSETS;
                break;
            case BOTTOM:
                insets = BOTTOM_TAB_INSETS;
                break;
            case TOP:
            default:
                insets = TOP_TAB_INSETS;
                break;
        }
        return insets;
    }

}
