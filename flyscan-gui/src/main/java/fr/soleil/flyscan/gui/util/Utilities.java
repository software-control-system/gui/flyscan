package fr.soleil.flyscan.gui.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.flyscan.gui.FlyScanTangoBox;
import fr.soleil.flyscan.gui.data.target.PluginsTarget;
import fr.soleil.flyscan.gui.listener.ButtonDecorationListener;
import fr.soleil.flyscan.gui.view.component.FSSpinner;
import fr.soleil.flyscan.gui.view.component.FSWheelSwitch;
import fr.soleil.flyscan.gui.view.renderer.ColoredSelectionListCellRenderer;
import fr.soleil.flyscan.gui.view.text.FSTextField;
import fr.soleil.flyscan.lib.model.constraints.AStringListConstraint;
import fr.soleil.flyscan.lib.model.constraints.ConstantConstraint;
import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.constraints.DeviceConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraint;
import fr.soleil.flyscan.lib.model.constraints.InConstraintStr;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.PredefinedEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.ContextMonitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Monitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.ActuatorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.BehaviorObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.HookBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.MonitorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.SensorBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.TimeBaseBehavior;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.AcquisitionSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.ActorSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.BehaviorSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.EasyConfigPropSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.EasyConfigSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.InfoSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.RecordingSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;
import fr.soleil.flyscan.lib.model.parsing.plugin.Parameter;
import fr.soleil.flyscan.lib.model.parsing.plugin.ParameterFormat;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.border.TextBubbleBorder;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.swing.text.FilteredCharactersDocument;

/**
 * Utilities class
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public final class Utilities implements ParsingUtil {

    private static final Icons ICONS = new Icons("fr.soleil.flyscan.gui.icons");
    private static final MessageManager MESSAGE_MANAGER = new MessageManager("fr.soleil.flyscan.gui");

    private static final String PROJECT_NAME = "project.name", PROJECT_VERSION = "project.version";
    private static final String BUILD_DATE = "build.date", DATE_START = " (", DATE_END = ")";

    public static final String DEVICE_PANEL_PROPERTY = System.getProperty("controlPanel", "atkpanel");

    public static final String EASY_CONFIG = "Easy Config";
    public static final String GENERAL = "General";
    public static final String ACTUATORS = "Actuators";
    public static final String SENSORS = "Sensors";
    public static final String TIMEBASE = "Timebases";
    public static final String HOOKS = "Hooks";
    public static final String MONITORS = "Monitors";

    public static final String NULL = "null";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String TANGO_DEVICE = "tango_device";
    public static final String AND_SO_ON = "...";

    public static final String ASTERISK = "*";
    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String POINT = ".";
    public static final String SLASH = "/";
    public static final String DOUBLE_POINT = ":";
    public static final String OPEN_PARENTHESIS = "(";
    public static final String CLOSE_PARENTHESIS = ")";
    public static final String NEW_LINE = "\n";
    public static final String NEW_HTML_LINE = "<br />";
    public static final String ADD = "+";
    public static final String REMOVE = "-";
    public static final String INTERVAL_START = "[";
    public static final String INTERVAL_END = "]";
    public static final String HTML_BODY = "<html><body>";
    public static final String BODY_HTML = "</body></html>";
    public static final String HTML_HEAD_STYLE = "<html><head><style>";
    public static final String STYLE_HEAD_BODY = "</style></head><body>";

    public static final String PREFERRED_SIZE = "preferredSize";

    public static final String TANGO_BASE = "tango://";

    public static final String PROJECT = "Project";
    public static final String FSS = "FlyscanServer";
    public static final String VERSION = "Version";
    public static final String DEFAULT_VERSION = "0.0.0";
    public static final String ONE = "1";
    public static final String ZERO = "0";
    public static final String ABORT = "abort";
    public static final String DEFAULT_TRAJECTORY = "[(0,0,0)]";

    public static final String OK = getMessage("flyscan.ok");
    public static final String CANCEL = getMessage("flyscan.cancel");
    public static final String NAME = getMessage("flyscan.name");
    public static final String TYPE = getMessage("flyscan.type");
    public static final String ENABLE = getMessage("flyscan.enable");
    public static final String CONFIRMATION = getMessage("flyscan.confirmation");
    public static final String CURRENT_CONFIG = getMessage("flyscan.configuration.current");
    public static final String NEW_CONFIG = getMessage("flyscan.configuration.new");
    public static final String DIMENSIONS = getMessage("flyscan.scan.dimensions");
    public static final String RESET = getMessage("flyscan.reset");

    public static final String CUSTOM_PARAMETERS = Utilities.getMessage("flyscan.parameter.custom");
    public static final String MASTER_PARAMETERS = Utilities.getMessage("flyscan.parameter.master");
    public static final String EXPERT_PARAMETERS = Utilities.getMessage("flyscan.parameter.expert");

    public static final String CONFIGURATION_NAME_DESCRIPTION = getMessage("flyscan.description.configuration.name");
    public static final String AUTHORS_DESCRIPTION = getMessage("flyscan.description.configuration.authors");
    public static final String COMMENT_DESCRIPTION = getMessage("flyscan.description.configuration.comment");
    public static final String FLYCAN_VERSION_DESCRIPTION = getMessage(
            "flyscan.description.configuration.flyscan.version");
    public static final String DIMENSIONS_DESCRIPTION = getMessage("flyscan.description.acquisition.dimensions");
    public static final String CONTINUOUS_DESCRIPTION = getMessage("flyscan.description.acquisition.continuous");
    public static final String ZIGZAG_DESCRIPTION = getMessage("flyscan.description.acquisition.zigzag");
    public static final String RECORDING_SPLIT_DESCRIPTION = getMessage("flyscan.description.recording.split");
    public static final String ERROR_STRATEGY_DESCRIPTION = getMessage("flyscan.description.behavior.error.strategy");
    public static final String ACTUATOR_ERROR_STRATEGY_DESCRIPTION = getMessage(
            "flyscan.description.behavior.error.strategy.actuator");
    public static final String ACTUATOR_DELAY_DESCRIPTION = getMessage("flyscan.description.behavior.actuator.delay");
    public static final String SENSOR_ERROR_STRATEGY_DESCRIPTION = getMessage(
            "flyscan.description.behavior.error.strategy.sensor");
    public static final String TIMEBASE_ERROR_STRATEGY_DESCRIPTION = getMessage(
            "flyscan.description.behavior.error.strategy.timebase");
    public static final String HOOK_ERROR_STRATEGY_DESCRIPTION = getMessage(
            "flyscan.description.behavior.error.strategy.hook");
    public static final String MONITOR_ERROR_STRATEGY_DESCRIPTION = getMessage(
            "flyscan.description.behavior.error.strategy.monitor");
    public static final String ACTUATOR_DIMENSION_DESCRIPTION = getMessage("flyscan.description.actuator.dimension");
    public static final String LAUNCH_DEVICE_PANEL = String.format(getMessage("flyscan.application.device.launch"),
            new File(DEVICE_PANEL_PROPERTY).getName());

    public static final String ACTUATOR_KEY = "flyscan.actuator";
    public static final String SENSOR_KEY = "flyscan.sensor";
    public static final String TIMEBASE_KEY = "flyscan.timebase";
    public static final String HOOK_KEY = "flyscan.hook";
    public static final String MONITOR_KEY = "flyscan.monitor";
    public static final String ADD_DECORATION_KEY = "flyscan.add.decoration";
    public static final String REMOVE_DECORATION_KEY = "flyscan.remove.decoration";

    public static final String DEFAULT_USER = System.getProperty("user.name", ObjectUtils.EMPTY_STRING);

    public static final String[] EMPTY_STRING_ARRAY = new String[0];

    public static final Font BASIC_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    public static final Font TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, 12);
    public static final Font ERROR_FONT = new Font(Font.DIALOG, Font.BOLD, 15);
    public static final Font BUTTON_FONT = new Font(Font.MONOSPACED, Font.PLAIN, 12);
    public static final Font PARAMETER_TITLE_FONT = new Font(Font.DIALOG, Font.ITALIC, 15);
    public static final Color CURRENT_CONFIG_COLOR = Color.BLUE;
    public static final Color HOVERED_COLOR = new Color(190, 190, 255);
    public static final Color PRESSED_COLOR = new Color(130, 130, 180);
    public static final Color DEFAULT_LABEL_COLOR = UIManager.getColor("Label.background");
    public static final Color DEFAULT_BUTTON_COLOR = UIManager.getColor("Button.background");
    public static final Border BUTTON_MOUSE_ENTERED_BORDER = BorderFactory.createLineBorder(Color.DARK_GRAY, 1);
    public static final int ARC = 6;
    public static final Border BUTTON_BORDER = new TextBubbleBorder(Color.GRAY, 1, ARC, 0);
    // EmptyBorder(1, 1, 2, 1) seems to be the best border to avoid button offsets (SCAN-961)
    public static final Border BIG_BUTTON_BORDER = new CompoundBorder(BUTTON_BORDER, new EmptyBorder(1, 1, 2, 1));
    public static final Border BUTTON_OVER_BORDER = new TextBubbleBorder(Color.DARK_GRAY, 2, ARC, 0);
    public static final Border BUTTON_OVER_SMALL_BORDER = new TextBubbleBorder(Color.DARK_GRAY, 1, ARC, 0);
    public static final Border EMPTY_BORDER = new EmptyBorder(2, 2, 2, 2);
    public static final EtchedBorder HOVERED_BORDER = new EtchedBorder(EtchedBorder.LOWERED, Color.LIGHT_GRAY,
            Color.GRAY);
    public static final EtchedBorder PRESSED_BORDER = new EtchedBorder(EtchedBorder.RAISED, Color.LIGHT_GRAY,
            Color.GRAY);

    public static final ImageIcon ACTUATOR_ICON = ICONS.getIcon(ACTUATOR_KEY);
    public static final ImageIcon SENSOR_ICON = ICONS.getIcon(SENSOR_KEY);
    public static final ImageIcon TIMEBASE_ICON = ICONS.getIcon(TIMEBASE_KEY);
    public static final ImageIcon HOOK_ICON = ICONS.getIcon(HOOK_KEY);
    public static final ImageIcon MONITOR_ICON = ICONS.getIcon(MONITOR_KEY);
    public static final ImageIcon DEVICE_ICON = ICONS.getIcon("flyscan.device");
    public static final ImageIcon FLYSCAN_ICON = ICONS.getIcon("flyscan.icon");
    public static final ImageIcon SPLASH_ICON = ICONS.getIcon("flyscan.splash");
    public static final ImageIcon GENERAL_ICON = ICONS.getIcon("flyscan.general");
    public static final ImageIcon EASY_CONFIG_ICON = ICONS.getIcon("flyscan.config.easy");
    public static final ImageIcon ERROR_ICON = ICONS.getIcon("flyscan.error");
    public static final ImageIcon HELP_ICON = ICONS.getIcon("flyscan.question");
    public static final ImageIcon HELP_ICON_PRESSED = ICONS.getIcon("flyscan.question.pressed");
    public static final ImageIcon SAVE_ICON = ICONS.getIcon("flyscan.save");
    public static final ImageIcon LOAD_ICON = ICONS.getIcon("flyscan.load");
    public static final ImageIcon CHECK_ICON = ICONS.getIcon("flyscan.check");
    public static final ImageIcon RELOAD_ICON = ICONS.getIcon("flyscan.reload");
    public static final ImageIcon RESET_ICON = ICONS.getIcon("flyscan.reset");
    public static final ImageIcon LOG_PAUSE_ICON = ICONS.getIcon("flyscan.log.pause");
    public static final ImageIcon LOG_PLAY_ICON = ICONS.getIcon("flyscan.log.play");
    public static final ImageIcon LOG_COPY_ICON = ICONS.getIcon("flyscan.log.copy");
    public static final ImageIcon LOADING_ICON = ICONS.getIcon("flyscan.loading");
    public static final ImageIcon SEARCH_ICON = ICONS.getIcon("flyscan.search");
    public static final ImageIcon RECORDING_ICON = ICONS.getIcon("flyscan.recording");
    public static final ImageIcon SCAN_CONFIGURATION_ICON = ICONS.getIcon("flyscan.configuration");
    public static final ImageIcon FOLDER_ICON = ICONS.getIcon("flyscan.folder");
    public static final ImageIcon FOLDER_OPEN_ICON = ICONS.getIcon("flyscan.folder.open");
    public static final ImageIcon DEVICE_PANEL_ICON = ICONS.getIcon("flyscan.device.panel");
    public static final ImageIcon EXPAND_SMALL_ICON = ICONS.getIcon("flyscan.expand.decoration");
    public static final ImageIcon COLLAPSE_SMALL_ICON = ICONS.getIcon("flyscan.collapse.decoration");

    public static final DecorableIcon NEW_CONFIG_ICON = getDecorableIcon("flyscan.configuration",
            "flyscan.add.decoration", SwingConstants.LEFT, SwingConstants.BOTTOM);
    public static final DecorableIcon DELETE_CONFIG_ICON = getDecorableIcon("flyscan.configuration",
            "flyscan.remove.decoration", SwingConstants.LEFT, SwingConstants.BOTTOM);

    public static final Dimension ZERO_DIM = new Dimension(0, 0);

    public static final Logger LOGGER = LoggerFactory.getLogger(FlyScanTangoBox.class.getName());

    private Utilities() {
        // Hide constructor
    }

    public static MouseListener createButtonBorderListener(boolean small, Color bg) {
        return new ButtonDecorationListener(small ? BUTTON_OVER_SMALL_BORDER : BUTTON_OVER_BORDER, bg);
    }

    public static MouseListener createButtonBorderListener() {
        return createButtonBorderListener(false, null);
    }

    public static ImageIcon getIcon(String key) {
        return ICONS.getIcon(key);
    }

    public static DecorableIcon getDecorableIcon(String icon, String modifier, int horizontalAlignment,
            int verticalAlignment) {
        DecorableIcon decorableIcon = ICONS.getDecorableIcon(icon, modifier);
        if (decorableIcon != null) {
            decorableIcon.setDecorationHorizontalAlignment(horizontalAlignment);
            decorableIcon.setDecorationVerticalAlignment(verticalAlignment);
            decorableIcon.setDecorated(true);
        }
        return decorableIcon;
    }

    /**
     * Return textArea displaying an error
     * 
     * @param location String
     * @param textError String
     * @return JTextArea
     */
    public static JTextArea getTextAreaError(String location, String textError) {
        JTextArea taError = new JTextArea();
        taError.setBackground(Color.WHITE);
        taError.setOpaque(true);
        taError.setEditable(false);
        taError.setBorder(null);
        taError.setText(location + " : " + textError);
        taError.setForeground(Color.RED);
        taError.setFont(Utilities.BASIC_FONT);
        return taError;
    }

    public static void prepareComboBoxLookAndFeel(final JComboBox<?> combo) {
        if (!(combo.getRenderer() instanceof ColoredSelectionListCellRenderer)) {
            combo.setRenderer(new ColoredSelectionListCellRenderer(combo));
        }
        combo.setOpaque(true);
    }

    public static FSWheelSwitch createWheelSwitch() {
        return new FSWheelSwitch();
    }

    public static FSSpinner createSpinner() {
        return new FSSpinner();
    }

    public static FSTextField createTextField(String value, int columns) {
        FSTextField textField;
        if ((value == null) || value.isEmpty()) {
            textField = new FSTextField(columns);
        } else {
            textField = new FSTextField(value);
        }
        textField.setDisabledTextColor(Color.GRAY);
        return textField;
    }

    public static FSTextField createTextField(String value) {
        return createTextField(value, 50);
    }

    public static JButton createButton(String text, boolean smallBorder, Color bg) {
        JButton button = new JButton(text);
        button.setHorizontalAlignment(SwingConstants.CENTER);
        button.setVerticalAlignment(SwingConstants.CENTER);
        button.setBorder(BUTTON_BORDER);
        button.setFont(BUTTON_FONT);
        button.addMouseListener(createButtonBorderListener(smallBorder, bg));
        return button;
    }

    public static FilteredCharactersDocument createNameFilteredDocument() {
        FilteredCharactersDocument document = new FilteredCharactersDocument();
        document.setFilteredCharacters(' ', '\t', '\n', '\r');
        document.setEscaped(true);
        return document;
    }

    public static JButton createButton(String text) {
        return createButton(text, false, null);
    }

    public static void addButtonPanel(JPanel panel, JButton addButton, JButton removeButton, boolean anchorSouth) {
        JPanel addPanel = new JPanel(new GridBagLayout());
        GridBagConstraints addGridBagConstraints = new GridBagConstraints();
        addGridBagConstraints.fill = GridBagConstraints.NONE;
        addGridBagConstraints.gridx = 0;
        addGridBagConstraints.gridy = 0;
        addGridBagConstraints.insets = new Insets(0, 0, 0, 5);
        addPanel.add(addButton, addGridBagConstraints);

        GridBagConstraints removeGridBagConstraints = new GridBagConstraints();
        removeGridBagConstraints.fill = GridBagConstraints.NONE;
        removeGridBagConstraints.gridx = 1;
        removeGridBagConstraints.gridy = 0;
        addPanel.add(removeButton, removeGridBagConstraints);

        GridBagConstraints addPanelConstraints = new GridBagConstraints();
        addPanelConstraints.fill = GridBagConstraints.NONE;
        addPanelConstraints.gridx = GridBagConstraints.RELATIVE;
        addPanelConstraints.gridy = 0;
        if (anchorSouth) {
            addPanelConstraints.anchor = GridBagConstraints.SOUTHWEST;
        }
        panel.add(addPanel, addPanelConstraints);
    }

    public static void updateSplitPane(final JSplitPane splitPane, Component errorComponent) {
        if (errorComponent.isVisible()) {
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerSize(8);
            SwingUtilities.invokeLater(() -> {
                if (!splitPane.isEnabled()) {
                    splitPane.setDividerLocation(splitPane.getHeight() / 5);
                }
            });
        } else {
            splitPane.setDividerLocation(0);
            splitPane.setDividerSize(0);
            splitPane.setOneTouchExpandable(false);
        }
    }

    public static JSplitPane createSplitPane(Component component, Component errorComponent) {
        final JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, errorComponent, component);
        updateSplitPane(splitPane, errorComponent);
        splitPane.setResizeWeight(0);
        return splitPane;
    }

    public static JMenu getParentMenu(Component comp) {
        JMenu menu = null;
        if (comp != null) {
            Container parent = comp.getParent();
            while (parent != null) {
                if (parent instanceof JMenu) {
                    menu = (JMenu) parent;
                    break;
                } else if (parent instanceof JPopupMenu) {
                    JPopupMenu popup = (JPopupMenu) parent;
                    Component invoker = popup.getInvoker();
                    if (invoker instanceof JMenu) {
                        menu = (JMenu) invoker;
                    }
                    break;
                } else if (parent instanceof Component) {
                    parent = parent.getParent();
                } else {
                    parent = null;
                }
            }
        }
        return menu;
    }

    public static String getApplicationNameAndVersion() {
        return getApplicationNameAndVersion(null);
    }

    public static String getApplicationNameAndVersion(String applicationName) {
        return appendApplicationNameAndVersion(applicationName, new StringBuilder()).toString();
    }

    public static StringBuilder appendApplicationNameAndVersion(String applicationName, StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(applicationName == null || applicationName.trim().isEmpty()
                ? MESSAGE_MANAGER.getAppMessage(PROJECT_NAME)
                : applicationName);
        builder.append(SPACE);
        appendApplicationVersion(builder);
        return builder;
    }

    public static StringBuilder appendApplicationName(String applicationName, StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(applicationName == null || applicationName.trim().isEmpty()
                ? MESSAGE_MANAGER.getAppMessage(PROJECT_NAME)
                : applicationName);
        return builder;
    }

    public static StringBuilder appendApplicationVersion(StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(MESSAGE_MANAGER.getAppMessage(PROJECT_VERSION));
        builder.append(DATE_START);
        builder.append(MESSAGE_MANAGER.getAppMessage(BUILD_DATE));
        builder.append(DATE_END);
        return builder;
    }

    public static final String getMessage(String key) {
        return MESSAGE_MANAGER.getMessage(key);
    }

    public static String toString(String... strings) {
        StringBuilder builder = new StringBuilder();
        if (strings == null) {
            builder.append(String.valueOf(null));
        } else if (strings.length > 0) {
            boolean add = false;
            for (String string : strings) {
                if (add) {
                    builder.append('\n');
                } else {
                    add = true;
                }
                builder.append(string);
            }
        }
        return builder.toString();
    }

    public static Collection<String> getDevicesForClasses(final String... deviceClasses) {
        Collection<String> devicesForClass = new ArrayList<>();
        if (deviceClasses != null) {
            for (String deviceClass : deviceClasses) {
                if ((deviceClass != null) && !deviceClass.trim().isEmpty()) {
                    String[] exportedDevices;
                    try {
                        exportedDevices = TangoDeviceHelper.getDatabase().get_device_exported_for_class(deviceClass);
                    } catch (DevFailed e) {
                        exportedDevices = null;
                    }
                    if (exportedDevices != null) {
                        for (String device : exportedDevices) {
                            devicesForClass.add(device);
                        }
                    }
                }
            }
        }
        return devicesForClass;
    }

    public static final Collection<Constraint> getConstraints(Entry entry, Parameter param, boolean checkName) {
        Collection<Constraint> constraints;
        if (entry == null) {
            constraints = null;
        } else {
            constraints = new LinkedHashSet<>();
            if (entry.getConstraints() != null) {
                constraints.addAll(entry.getConstraints());
            }
            if ((param != null) && (param.getConstraints() != null) && ((!checkName)
                    || ((param.getName() != null) && param.getName().equalsIgnoreCase(entry.getKey())))) {
                constraints.addAll(param.getConstraints());
            }
        }
        return constraints;
    }

    public static Object getDefaultDevices(Entry entry, Parameter param) {
        Object result;
        if (param.getFormat() == ParameterFormat.LIST) {
            String s = (entry == null) || (entry.getValue() == null) ? ObjectUtils.EMPTY_STRING
                    : entry.getValue().replace(ParsingUtil.ESCAPED_LF, ObjectUtils.EMPTY_STRING)
                            .replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                            .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
            String[] tmp = s.split(ParsingUtil.COMMA);
            for (int i = 0; i < tmp.length; i++) {
                tmp[i] = tmp[i].trim();
            }
            result = tmp;
        } else {
            String val = entry.getValue();
            result = val == null ? null : val.trim();
        }
        return result;
    }

    public static boolean filterDevicesForClasses(final Collection<String> authorizedValues,
            final Collection<String> devices, final String[] deviceClasses, final String... defaultDevices) {
        boolean hasFoundExpectedDevices = false;
        Collection<String> devicesForClass = getDevicesForClasses(deviceClasses);
        Collection<String> tmp = new ArrayList<>();
        if ((defaultDevices != null) && (defaultDevices.length > 0)) {
            for (String device : defaultDevices) {
                if ((device != null) && !contains(device, devicesForClass)) {
                    tmp.add(device);
                }
            }
        }
        for (String device : tmp) {
            if (isOfClass(device, deviceClasses)) {
                devicesForClass.add(device);
            }
        }
        tmp.clear();
        if (authorizedValues == null) {
            devices.addAll(devicesForClass);
            hasFoundExpectedDevices = !devicesForClass.isEmpty();
        } else if (!authorizedValues.isEmpty()) {
            for (String s : devicesForClass) {
                if (contains(s, authorizedValues)) {
                    hasFoundExpectedDevices = true;
                    devices.add(s);
                }
            }
        }
        devicesForClass.clear();
        return hasFoundExpectedDevices;
    }

    public static boolean isOfClass(String device, String... deviceClasses) {
        boolean classOk;
        if (device.startsWith(TANGO_BASE)) {
            // XXX no quick way to check such devices
            classOk = true;
        } else {
            boolean shouldCheck = false;
            for (String deviceClass : deviceClasses) {
                if ((deviceClass != null) && !deviceClass.isEmpty()) {
                    shouldCheck = true;
                }
            }
            if ((device == null) || device.isEmpty()) {
                classOk = false;
            } else if (shouldCheck) {
                classOk = false;
                String devClass = TangoDeviceHelper.getDeviceClass(device);
                if (devClass != null) {
                    for (String deviceClass : deviceClasses) {
                        if (ObjectUtils.sameObject(deviceClass, devClass)) {
                            classOk = true;
                            break;
                        }
                    }
                }
            } else {
                classOk = true;
            }
        }
        return classOk;
    }

    public static boolean contains(String device, Collection<String> devices) {
        boolean present = false;
        for (String tmp : devices) {
            if (device.equalsIgnoreCase(tmp)) {
                present = true;
                break;
            }
        }
        return present;
    }

    public static List<Number> getAuthorizedNumberValues(Parameter param) {
        boolean firstInit = true;
        List<Number> authorizedValues = new ArrayList<>();
        Collection<Constraint> constraints = param.getConstraints();
        for (Constraint constraint : constraints) {
            if (constraint instanceof InConstraint) {
                InConstraint inConstraint = (InConstraint) constraint;
                if (firstInit) {
                    firstInit = false;
                    // first time? add
                    authorizedValues.addAll(inConstraint.getPossibleValues());
                } else {
                    // not first time? intersect
                    Collection<Number> toRemove = new ArrayList<>();
                    for (Number value : authorizedValues) {
                        if (!ParsingTool.contains(inConstraint.getPossibleValues(), value)) {
                            toRemove.add(value);
                        }
                    }
                    authorizedValues.removeAll(toRemove);
                    toRemove.clear();
                }
            } else if (constraint instanceof ConstantConstraint) {
                Number constant = ParsingTool.parseNumber(((ConstantConstraint) constraint).getConstant());
                if (constant != null) {
                    if (firstInit) {
                        // first time? add
                        firstInit = false;
                        authorizedValues.add(constant);
                    } else {
                        // not first time? intersect
                        boolean keep = ParsingTool.contains(authorizedValues, constant);
                        authorizedValues.clear();
                        if (keep) {
                            authorizedValues.add(constant);
                        }
                    }
                }
            }
        }
        if (firstInit) {
            authorizedValues = null;
        }
        return authorizedValues;
    }

    public static Collection<String> getAuthorizedValues(Parameter param) {
        boolean firstInit = true;
        Collection<String> authorizedValues = new ArrayList<>();
        Collection<Constraint> constraints = param.getConstraints();
        for (Constraint constraint : constraints) {
            if ((constraint instanceof InConstraintStr) || (constraint instanceof DeviceConstraint)) {
                if (firstInit) {
                    firstInit = false;
                    // first time? add
                    authorizedValues.addAll(((AStringListConstraint) constraint).getPossibleValues());
                } else {
                    // not first time? intersect
                    authorizedValues.retainAll(((AStringListConstraint) constraint).getPossibleValues());
                }
            } else if (constraint instanceof ConstantConstraint) {
                if (firstInit) {
                    // first time? add
                    firstInit = false;
                    authorizedValues.add(((ConstantConstraint) constraint).getConstant());
                } else {
                    // not first time? intersect
                    String constant = ((ConstantConstraint) constraint).getConstant();
                    boolean keep = false;
                    if (constant == null) {
                        for (String val : authorizedValues) {
                            if (val == null) {
                                keep = true;
                                break;
                            }
                        }
                    } else {
                        for (String val : authorizedValues) {
                            if (constant.equalsIgnoreCase(val)) {
                                keep = true;
                                break;
                            }
                        }
                    }
                    authorizedValues.clear();
                    if (keep) {
                        authorizedValues.add(constant);
                    }
                }
            }
        }
        if (firstInit) {
            authorizedValues = null;
        }
        return authorizedValues;
    }

    public static Collection<Plugin> getPlugins(PluginsTarget target) {
        return target == null ? new ArrayList<>() : target.getPlugins();
    }

    public static boolean isMonitor(Actor actor) {
        return (actor instanceof Monitor) || (actor instanceof ContextMonitor);
    }

    public static boolean isActuator(Actor actor) {
        return actor instanceof Actuator;
    }

    public static String getCategory(Actor actor) {
        String category;
        if (actor == null) {
            category = null;
        } else if (isActuator(actor)) {
            category = ParsingUtil.ACTUATOR;
        } else if (isMonitor(actor)) {
            category = ParsingUtil.MONITOR;
        } else {
            category = actor.getClass().getSimpleName().toLowerCase();
        }
        return category;
    }

    public static Actor getActorNamedLike(String name, Section parentSection, Actor toAvoid) {
        Actor actor = null;
        if ((name != null) && (parentSection != null)) {
            for (FSObject tmp : parentSection.getObjects()) {
                if ((tmp instanceof Actor) && (tmp != toAvoid)) {
                    Actor other = (Actor) tmp;
                    if (name.equalsIgnoreCase(other.getName())) {
                        actor = other;
                        break;
                    }
                }
            }
        }
        return actor;
    }

    public static int getLastY(final JPanel panel) {
        int y;
        int count = panel == null ? 0 : panel.getComponentCount();
        if (count == 0) {
            y = -1;
        } else {
            int max = 0;
            GridBagLayout layout = (GridBagLayout) panel.getLayout();
            for (Component comp : panel.getComponents()) {
                GridBagConstraints constraints = layout.getConstraints(comp);
                if (constraints.gridy > max) {
                    max = constraints.gridy;
                }
            }
            y = max;
        }
        return y;
    }

    public static int getNextY(final JPanel panel) {
        return getLastY(panel) + 1;
    }

    protected static void addPredefinedEntry(EntryContainer parent, String key, String value, String description,
            EntryType entryType) {
        PredefinedEntry entry = new PredefinedEntry(key, value, entryType);
        entry.setPresentInConfig(true);
        if ((description != null) && !description.trim().isEmpty()) {
            entry.setComment(description);
        }
        parent.addEntry(entry);
    }

    public static void sortEntries(final FSObject fsObject, final Plugin plugin) {
        if ((fsObject != null) && (plugin != null)) {
            List<Entry> entries = fsObject.getEntries();
            List<Entry> newEntries = new ArrayList<>(entries);
            Collections.sort(newEntries, new EntryComparator(plugin, entries));
            fsObject.setEntries(newEntries);
            entries.clear();
        }
    }

    protected static void addPredefinedEntry(EntryContainer parent, String key, String value, EntryType entryType) {
        addPredefinedEntry(parent, key, value, null, entryType);
    }

    protected static void prepareAndAddBehavior(BehaviorSection behaviorSection, BehaviorObject behavior,
            String description) {
        addPredefinedEntry(behavior, ParsingUtil.ERROR_STRATEGY, ABORT, description, EntryType.ENUM);
        behaviorSection.addFSObject(behavior);
    }

    public static Configuration createNewConfiguration(String name, String flyscanVersion, int dimensions,
            boolean continuous) {
        Configuration configuration;
        if ((name == null) || (dimensions < 1)) {
            configuration = null;
        } else {
            configuration = new Configuration(name);
            int index = name.lastIndexOf('/');
            String entryName = index > -1 ? name.substring(index + 1) : name;
            EasyConfigPropSection easyConfigPropSection = new EasyConfigPropSection();
            EasyConfigSection easyConfigSection = new EasyConfigSection();
            addPredefinedEntry(easyConfigSection, ParsingUtil.ENABLE, FALSE, EntryType.BOOL);
            addPredefinedEntry(easyConfigSection, ParsingUtil.CONFIGURATOR, ObjectUtils.EMPTY_STRING, EntryType.STRING);
            InfoSection infoSection = new InfoSection();
            addPredefinedEntry(infoSection, ParsingUtil.NAME, entryName, CONFIGURATION_NAME_DESCRIPTION,
                    EntryType.STRING);
            addPredefinedEntry(infoSection, ParsingUtil.KEYWORDS, ObjectUtils.EMPTY_STRING, EntryType.STRING);
            addPredefinedEntry(infoSection, ParsingUtil.AUTHORS, DEFAULT_USER, AUTHORS_DESCRIPTION, EntryType.STRING);
            addPredefinedEntry(infoSection, ParsingUtil.COMMENT, ObjectUtils.EMPTY_STRING, COMMENT_DESCRIPTION,
                    EntryType.STRING);
            addPredefinedEntry(infoSection, ParsingUtil.REVISION, ONE, EntryType.STRING);
            addPredefinedEntry(infoSection, ParsingUtil.VERSION, DEFAULT_VERSION, EntryType.STRING);
            addPredefinedEntry(infoSection, ParsingUtil.FLYSCAN_VERSION,
                    flyscanVersion == null || flyscanVersion.trim().isEmpty() ? DEFAULT_VERSION : flyscanVersion,
                    FLYCAN_VERSION_DESCRIPTION, EntryType.STRING);
            AcquisitionSection acquisitionSection = new AcquisitionSection();
            addPredefinedEntry(acquisitionSection, ParsingUtil.DIMENSIONS, ObjectUtils.EMPTY_STRING,
                    DIMENSIONS_DESCRIPTION, EntryType.INT_ARRAY);
            int[] scanDimensions = new int[dimensions];
            Arrays.fill(scanDimensions, 1); // at least 1 step on each dimension
            acquisitionSection.setScanDimensions(scanDimensions);
            addPredefinedEntry(acquisitionSection, ParsingUtil.CONTINUOUS, Boolean.toString(continuous),
                    CONTINUOUS_DESCRIPTION, EntryType.BOOL);
            addPredefinedEntry(acquisitionSection, ParsingUtil.ZIGZAG, FALSE, ZIGZAG_DESCRIPTION, EntryType.BOOL);
            RecordingSection recordingSection = new RecordingSection();
            addPredefinedEntry(recordingSection, ParsingUtil.SPLIT, ONE, RECORDING_SPLIT_DESCRIPTION, EntryType.INT);
            ActorSection actorSection = new ActorSection();
            BehaviorSection behaviorSection = new BehaviorSection();
            addPredefinedEntry(behaviorSection, ParsingUtil.ERROR_STRATEGY, ABORT, ERROR_STRATEGY_DESCRIPTION,
                    EntryType.ENUM);
            ActuatorBehavior actuatorBehavior = new ActuatorBehavior(behaviorSection);
            addPredefinedEntry(actuatorBehavior, ParsingUtil.DELAY, ZERO, ACTUATOR_DELAY_DESCRIPTION, EntryType.INT);
            prepareAndAddBehavior(behaviorSection, actuatorBehavior, ACTUATOR_ERROR_STRATEGY_DESCRIPTION);
            SensorBehavior sensorBehavior = new SensorBehavior(behaviorSection);
            prepareAndAddBehavior(behaviorSection, sensorBehavior, SENSOR_ERROR_STRATEGY_DESCRIPTION);
            TimeBaseBehavior timeBaseBehavior = new TimeBaseBehavior(behaviorSection);
            prepareAndAddBehavior(behaviorSection, timeBaseBehavior, TIMEBASE_ERROR_STRATEGY_DESCRIPTION);
            HookBehavior hookBehavior = new HookBehavior(behaviorSection);
            prepareAndAddBehavior(behaviorSection, hookBehavior, HOOK_ERROR_STRATEGY_DESCRIPTION);
            MonitorBehavior monitorBehavior = new MonitorBehavior(behaviorSection);
            prepareAndAddBehavior(behaviorSection, monitorBehavior, MONITOR_ERROR_STRATEGY_DESCRIPTION);
            configuration.addSections(easyConfigPropSection, easyConfigSection, infoSection, acquisitionSection,
                    recordingSection, actorSection, behaviorSection);
        }
        return configuration;
    }

    protected static Entry getContinuousEntry(Configuration configuration) {
        Entry continuous = null;
        if (configuration != null) {
            for (Section section : configuration.getSections()) {
                if (section instanceof AcquisitionSection) {
                    Entry entry = section.getEntry(ParsingUtil.CONTINUOUS);
                    if (entry != null) {
                        continuous = entry;
                        break;
                    }
                }
            }
        }
        return continuous;
    }

    public static boolean isContinuous(Configuration configuration) {
        boolean continuous;
        Entry entry = getContinuousEntry(configuration);
        if (entry == null) {
            continuous = false;
        } else {
            try {
                continuous = Boolean.parseBoolean(entry.getValue());
            } catch (Exception e) {
                continuous = false;
            }
        }
        return continuous;
    }

    public static boolean isStepByStep(Configuration configuration) {
        boolean sbs;
        Entry entry = getContinuousEntry(configuration);
        if (entry == null) {
            sbs = false;
        } else {
            try {
                sbs = !Boolean.parseBoolean(entry.getValue());
            } catch (Exception e) {
                sbs = false;
            }
        }
        return sbs;
    }

    public static BatchExecutor createBatchExecutor(String device) {
        StringWriter writer = new StringWriter() {
            @Override
            public void flush() {
                LOGGER.error(toString());
            }
        };
        BatchExecutor executor = new BatchExecutor(DEVICE_PANEL_PROPERTY, Arrays.asList(device));
        executor.setErrorOutputWriter(writer);
        return executor;
    }

    public static String getNotNullDeviceName(String device) {
        return device == null ? ObjectUtils.EMPTY_STRING : device.trim();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private static class EntryComparator implements Comparator<Entry> {
        private Map<String, Integer> params;
        private Collection<Parameter> parameters;
        private WeakReference<List<Entry>> entriesRef;
        private Collator comparator;

        public EntryComparator(Plugin plugin, List<Entry> entries) {
            params = new HashMap<>();
            parameters = new ArrayList<>(plugin.getParameters());
            entriesRef = entries == null ? null : new WeakReference<>(entries);
            comparator = Collator.getInstance();
        }

        private int paramIndex(String name) {
            int i = -1;
            boolean found = false;
            for (Parameter param : parameters) {
                i++;
                if (ObjectUtils.sameObject(name, param.getName())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                i = -1;
            }
            return i;
        }

        protected int bufferedIndex(String key) {
            int index;
            Integer i = params.get(key);
            if (i == null) {
                index = paramIndex(key);
                i = Integer.valueOf(index);
                params.put(key, i);
            } else {
                index = i.intValue();
            }
            return index;
        }

        @Override
        public int compare(Entry o1, Entry o2) {
            // Comparison choice: unknown entries at the beginning.
            int result;
            if (o1 == null) {
                if (o2 == null) {
                    result = 0;
                } else {
                    result = -1;
                }
            } else if (o2 == null) {
                result = 1;
            } else if (o1 == o2) {
                result = 0;
            } else {
                String key1 = o1.getKey(), key2 = o2.getKey();
                if (ObjectUtils.sameObject(key1, key2)) {
                    result = 0;
                } else if (key1 == null) {
                    result = 1;
                } else if (key2 == null) {
                    result = -1;
                } else {
                    int i1 = bufferedIndex(key1), i2 = bufferedIndex(key2);
                    if (i1 < 0 && i2 < 0) {
                        List<Entry> entries = ObjectUtils.recoverObject(entriesRef);
                        if (entries != null) {
                            i1 = entries.indexOf(o1);
                            i2 = entries.indexOf(o2);
                        }
                    }
                    if (i1 < 0 && i2 < 0) {
                        result = comparator.compare(key1, key2);
                    } else {
                        result = Integer.compare(i1, i2);
                    }
                }
            }
            return result;
        }

        @Override
        protected void finalize() {
            params.clear();
            parameters.clear();
            entriesRef = null;
            comparator = null;
        }

    }
}
