package fr.soleil.flyscan.gui.view.combo;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Collection;

import javax.accessibility.Accessible;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.basic.ComboPopup;

import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;
import fr.soleil.flyscan.gui.view.renderer.DevStateComboBoxRenderer;
import fr.soleil.lib.project.swing.combo.DynamicRenderingComboBox;
import fr.soleil.lib.project.swing.renderer.AdaptedFontListCellRenderer;

/**
 * Combo box for dev state values
 * 
 * @author guerre-giordano
 * 
 */
public class DevStateComboBox extends DynamicRenderingComboBox<String> implements IEntryComponent {

    private static final long serialVersionUID = -6994476497575890938L;

    private final EventListenerList listeners;
    private Color savedBG;

    public DevStateComboBox(Collection<String> possibleValues) {
        this(possibleValues == null ? null : possibleValues.toArray(new String[possibleValues.size()]));
    }

    public DevStateComboBox(String... possibleValues) {
        super(possibleValues == null ? Utilities.EMPTY_STRING_ARRAY : possibleValues);
        listeners = new EventListenerList();
        ListCellRenderer<Object> devStateRenderer = new DevStateComboBoxRenderer(this);
        setRenderer(devStateRenderer);
        addItemListener(e -> {
            fireEditionStateChanged(true);
        });
        savedBG = getBackground();
        addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent e) {
                updateRendering();
            }

            @Override
            public void focusGained(FocusEvent e) {
                updateRendering();
            }
        });
    }

    @Override
    public void setBackground(Color bg) {
        doSetBackground(bg);
        savedBG = getBackground();
    }

    protected void doSetBackground(Color bg) {
        super.setBackground(bg);
    }

    public void addBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.add(ConfigurationEditionStateListener.class, listener);
    }

    public void removeBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.remove(ConfigurationEditionStateListener.class, listener);
    }

    public ConfigurationEditionStateListener[] getBeingEditedListeners() {
        return listeners.getListeners(ConfigurationEditionStateListener.class);
    }

    protected void fireEditionStateChanged(boolean isBeingEdited) {
        if (isBeingEdited) {
            this.setBorder(null);
        }
        for (ConfigurationEditionStateListener listener : getBeingEditedListeners()) {
            listener.setConfigurationBeingEdited(isBeingEdited);
        }
    }

    @Override
    public void updateRendering() {
        super.updateRendering();
        AdaptedFontListCellRenderer<? super String> renderer = getRenderer();
        if (renderer != null) {
            JComponent comp = renderer.getListCellRendererComponent(new JList<>(getModel()), getSelectedItem(),
                    getSelectedIndex(), hasFocus(), false, false);
            if (comp == null) {
                doSetBackground(savedBG);
                doSetForeground(null);
                doSetFont(null);
                doSetToolTipText(null);
            } else {
                doSetBackground(comp.getBackground());
                doSetForeground(comp.getForeground());
                doSetFont(comp.getFont());
                doSetToolTipText(comp.getToolTipText());
            }
            Accessible a = getUI().getAccessibleChild(this, 0);
            if (a instanceof ComboPopup) {
                // Listen for changes to the popup menu selection.
                JList<?> popupList = ((ComboPopup) a).getList();
                if (popupList != null) {
                    popupList.setSelectionBackground(comp.getBackground());
                }
            }
            repaint();
        }
    }

    @Override
    protected void selectedItemChanged() {
        super.selectedItemChanged();
        updateRendering();
    }

    @Override
    public void resetToDefault(String defaultDevice) {
        if (defaultDevice != null) {
            UIDelegate.applySelectedValue(this, defaultDevice);
        }
    }

}
