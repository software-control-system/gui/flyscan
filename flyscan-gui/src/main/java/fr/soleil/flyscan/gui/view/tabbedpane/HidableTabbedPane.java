package fr.soleil.flyscan.gui.view.tabbedpane;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Icon;

import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link BoldTitleTabbedPane} with hidable tabs.
 * 
 * @author GIRARDOT
 */
public class HidableTabbedPane extends BoldTitleTabbedPane {

    private static final long serialVersionUID = 5976352222517147359L;

    private final Map<String, Component> tabMap, tabComponentMap;
    private final Map<String, Integer> tabIndexMap;
    private final Map<String, Icon> iconMap;
    private final Map<String, String> tipMap;

    public HidableTabbedPane() {
        super();
        tabMap = new ConcurrentHashMap<>();
        tabComponentMap = new ConcurrentHashMap<>();
        tabIndexMap = new ConcurrentHashMap<>();
        iconMap = new ConcurrentHashMap<>();
        tipMap = new ConcurrentHashMap<>();
    }

    public HidableTabbedPane(int tabPlacement) {
        super(tabPlacement);
        tabMap = new ConcurrentHashMap<>();
        tabComponentMap = new ConcurrentHashMap<>();
        tabIndexMap = new ConcurrentHashMap<>();
        iconMap = new ConcurrentHashMap<>();
        tipMap = new ConcurrentHashMap<>();
    }

    public HidableTabbedPane(int tabPlacement, int tabLayoutPolicy) {
        super(tabPlacement, tabLayoutPolicy);
        tabMap = new ConcurrentHashMap<>();
        tabComponentMap = new ConcurrentHashMap<>();
        tabIndexMap = new ConcurrentHashMap<>();
        iconMap = new ConcurrentHashMap<>();
        tipMap = new ConcurrentHashMap<>();
    }

    protected void prepareTab(String title, Icon icon, Component component, String tip, Component tabComponent) {
        tabMap.put(title, component);
        if (tabComponent != null) {
            tabComponentMap.put(title, tabComponent);
        }
        Integer index = tabIndexMap.get(title);
        if (index == null) {
            index = Integer.valueOf(tabIndexMap.size());
            tabIndexMap.put(title, index);
        }
        if (icon != null) {
            iconMap.put(title, icon);
        }
        if (tip != null) {
            tipMap.put(title, tip);
        }
    }

    public void addTab(String title, Component component, Component tabComponent, boolean hidden) {
        remove(title);
        if ((title != null) && (component != null)) {
            prepareTab(title, null, component, null, null);
            if (!hidden) {
                super.addTab(title, component, tabComponent);
            }
        }
    }

    protected void addTab(String title, Icon icon, Component component, String tip, Component tabComponent,
            boolean hidden, boolean remove) {
        if (remove) {
            remove(title);
        }
        if ((title != null) && (component != null)) {
            prepareTab(title, icon, component, tip, tabComponent);
            if (!hidden) {
                super.addTab(title, icon, component, tip, tabComponent);
            }
        }
    }

    public void addTab(String title, Icon icon, Component component, String tip, Component tabComponent,
            boolean hidden) {
        addTab(title, icon, component, tip, tabComponent, hidden, true);
    }

    @Override
    public void addTab(String title, Component component, Component tabComponent) {
        addTab(title, component, tabComponent, false);
    }

    @Override
    public void addTab(String title, Icon icon, Component component, String tip, Component tabComponent) {
        addTab(title, icon, component, tip, tabComponent, false);
    }

    protected void showTab(String title, boolean select) {
        if (title != null) {
            int index = getTabIndexForTitle(title);
            if (index < 0) {
                Integer knownIndex = tabIndexMap.get(title);
                Component comp = tabMap.get(title), tabComp = tabComponentMap.get(title);
                if (knownIndex != null) {
                    String selectedTitle = null;
                    if (!select) {
                        int selected = getSelectedIndex();
                        if (selected > -1) {
                            Component selectedComp = getComponentAt(selected);
                            for (Entry<String, Component> entry : tabMap.entrySet()) {
                                if (selectedComp == entry.getValue()) {
                                    selectedTitle = entry.getKey();
                                    break;
                                }
                            }
                        }
                    }
                    if (knownIndex.intValue() < getTabCount()) {
                        insertTab(title, iconMap.get(title), comp, tipMap.get(title), knownIndex.intValue());
                        setTabComponentAt(knownIndex.intValue(), tabComp);
                    } else {
                        int effectiveIndex = knownIndex.intValue();
                        Integer tmpIndex;
                        for (int i = 0; i < getTabCount(); i++) {
                            tmpIndex = tabIndexMap.get(getTabTitleAt(i));
                            if (tmpIndex.intValue() > effectiveIndex) {
                                effectiveIndex = i;
                                break;
                            }
                        }
                        if (effectiveIndex < getTabCount()) {
                            insertTab(title, iconMap.get(title), comp, tipMap.get(title), effectiveIndex);
                            setTabComponentAt(effectiveIndex, tabComp);
                        } else {
                            addTab(title, iconMap.get(title), comp, tipMap.get(title), tabComp, false, false);
                        }
                    }
                    if (select) {
                        super.setSelectedTab(title);
                    } else {
                        int selected = getSelectedIndex();
                        if ((selectedTitle != null) && (selected > -1)
                                && !ObjectUtils.sameObject(selectedTitle, getTabTitleAt(selected))) {
                            super.setSelectedTab(selectedTitle);
                        }
                    }
                }
            } else if (select) {
                setSelectedIndex(index);
            }
        }
    }

    @Override
    public void setSelectedTab(String title) {
        showTab(title, true);
    }

    @Override
    public void remove(String title) {
        super.remove(title);
        if (title != null) {
            tabMap.remove(title);
            tabComponentMap.remove(title);
            tabIndexMap.remove(title);
            iconMap.remove(title);
            tipMap.remove(title);
        }
    }

    public void hideAllTabsBut(String title) {
        Collection<String> titles = new ArrayList<>(tabMap.keySet());
        if (title != null) {
            titles.remove(title);
        }
        for (String tabTitle : titles) {
            super.remove(tabTitle);
        }
        titles.clear();
    }

    public void showAllTabs() {
        for (String key : tabMap.keySet()) {
            showTab(key, false);
        }
    }

    public void clearMemory() {
        tabMap.clear();
        tabComponentMap.clear();
        tabIndexMap.clear();
        iconMap.clear();
        tipMap.clear();
    }

    @Override
    public void removeAll() {
        super.removeAll();
        clearMemory();
    }

    @Override
    protected void finalize() throws Throwable {
        clearMemory();
    }

}
