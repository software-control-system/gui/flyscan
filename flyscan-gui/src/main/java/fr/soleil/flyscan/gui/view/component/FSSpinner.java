package fr.soleil.flyscan.gui.view.component;

import java.awt.Color;
import java.awt.ComponentOrientation;

import javax.swing.JFormattedTextField;
import javax.swing.SpinnerModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import fr.soleil.comete.swing.Spinner;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;

public class FSSpinner extends Spinner implements IEntryComponent {

    private static final long serialVersionUID = 4355391584334059257L;

    public FSSpinner() {
        super();
        setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        setHorizontalAlignment(SwingConstants.LEFT);
        JFormattedTextField jftf = (JFormattedTextField) getEditor().getComponent(0);
        jftf.setHorizontalAlignment(SwingConstants.LEFT);
        setNumberValue(Long.valueOf(0));
        // XXX need to set border, otherwise there is a spinner layout bug
        setBorder(new LineBorder(Color.GRAY, 1));
        revalidate();
    }

    @Override
    public void setModel(SpinnerModel model) {
        int alignment = getHorizontalAlignment();
        ComponentOrientation orientation = getComponentOrientation();
        super.setModel(model);
        setComponentOrientation(orientation);
        setHorizontalAlignment(alignment);
        JFormattedTextField jftf = (JFormattedTextField) getEditor().getComponent(0);
        jftf.setHorizontalAlignment(alignment);
        revalidate();
    }

    @Override
    public void resetToDefault(String defaultValue) throws FSParsingException {
        if (defaultValue != null) {
            setNumberValue(ParsingTool.parseLong(defaultValue));
        }
    }

}
