package fr.soleil.flyscan.gui.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.flyscan.gui.FlyScanTangoBox;

public interface IFlyScanConstants {
    public static final String FSS = "FlyScanServer";
    public static final String RECORDING_MANAGER = "RecordingManager";
    public static final Logger LOGGER = LoggerFactory.getLogger(FlyScanTangoBox.class.getName());
}
