package fr.soleil.flyscan.gui.listener;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.lang.ref.WeakReference;

import javax.swing.JComponent;
import javax.swing.JRadioButton;

import fr.soleil.lib.project.ObjectUtils;

public class FSKeySelector extends KeyAdapter {
    private WeakReference<JRadioButton> selectorRef;
    private int selectorIndex;

    public FSKeySelector(JRadioButton selector) {
        super();
        selectorRef = selector == null ? null : new WeakReference<>(selector);
        selectorIndex = -1;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        JRadioButton selector = ObjectUtils.recoverObject(selectorRef);
        if (selector != null) {
            Boolean up;
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                up = Boolean.TRUE;
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                up = Boolean.FALSE;
            } else {
                up = null;
            }
            if (up != null) {
                Component comp = e.getComponent();
                if (comp != null) {
                    Component[] components = comp.getParent().getComponents();
                    if (components != null) {
                        if (selectorIndex < 0) {
                            for (int i = 0; i < components.length && selectorIndex < 0; i++) {
                                if (components[i] == selector) {
                                    selectorIndex = i;
                                }
                            }
                        }
                        if (selectorIndex > -1) {
                            int index = -1;
                            for (int i = Math.max(0, selectorIndex - 2); i < components.length && index < 0; i++) {
                                if (components[i] == comp) {
                                    index = i;
                                }
                            }
                            if (index > -1) {
                                int delta = index - selectorIndex, nextSelectorIndex = -1;
                                if (up.booleanValue()) {
                                    for (int i = selectorIndex - 1; i > -1 && nextSelectorIndex < 0; i--) {
                                        Component tmp = components[i];
                                        if (tmp instanceof JRadioButton) {
                                            nextSelectorIndex = i;
                                        }
                                    }
                                } else {
                                    for (int i = selectorIndex + 1; i < components.length
                                            && nextSelectorIndex < 0; i++) {
                                        Component tmp = components[i];
                                        if (tmp instanceof JRadioButton) {
                                            nextSelectorIndex = i;
                                        }
                                    }
                                }
                                if (nextSelectorIndex > 0) {
                                    ((JRadioButton) components[nextSelectorIndex]).setSelected(true);
                                    if (nextSelectorIndex + delta > 0) {
                                        Component nextComp = components[nextSelectorIndex + delta];
                                        if (nextComp instanceof JComponent) {
                                            ((JComponent) nextComp).grabFocus();
                                        }
                                    }
                                }
                            } // end if (index > -1)
                        } // end if (selectorIndex > -1)
                    } // end if (components != null)
                } // end if (comp != null)
            } // end if (up != null)
        } // end if (selector != null) {
    }

}
