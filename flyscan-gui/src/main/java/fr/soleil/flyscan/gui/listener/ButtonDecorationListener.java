package fr.soleil.flyscan.gui.listener;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.WeakHashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.Border;

/**
 * A {@link MouseAdapter} that dynamically changes the border and the background color of a {@link JButton}
 * (to mark that button as hovered or not hovered).
 * 
 * @author GIRARDOT
 */
public class ButtonDecorationListener extends MouseAdapter {

    protected final Border overBorder;
    protected final Color bg;
    protected final Map<JButton, Border> borderMap;
    protected final Map<JButton, Color> backgroundMap;

    public ButtonDecorationListener(Border overBorder) {
        this(overBorder, null);
    }

    public ButtonDecorationListener(Border overBorder, Color bg) {
        super();
        this.overBorder = overBorder;
        this.bg = bg;
        borderMap = new WeakHashMap<>();
        backgroundMap = new WeakHashMap<>();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        JButton button = recoverButton(e);
        if ((button != null) && button.isEnabled()) {
            synchronized (borderMap) {
                borderMap.put(button, button.getBorder());
            }
            button.setBorder(overBorder);
            if (bg != null) {
                synchronized (backgroundMap) {
                    backgroundMap.put(button, button.getBackground());
                }
                button.setBackground(bg);
            }
            button.repaint();
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        JButton button = recoverButton(e);
        if ((button != null) && button.isEnabled()) {
            Border border;
            synchronized (borderMap) {
                border = borderMap.get(button);
            }
            if (border == null) {
                border = BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1);
            }
            button.setBorder(border);
            if (bg != null) {
                Color background;
                synchronized (backgroundMap) {
                    background = backgroundMap.get(button);
                }
                button.setBackground(background);
            }
            button.repaint();
        }
    }

    protected JButton recoverButton(MouseEvent e) {
        JButton button = null;
        if ((e != null) && (e.getSource() instanceof JButton)) {
            button = (JButton) e.getSource();
        }
        return button;
    }

}
