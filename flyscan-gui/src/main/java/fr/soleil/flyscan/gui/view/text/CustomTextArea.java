package fr.soleil.flyscan.gui.view.text;

public class CustomTextArea extends FSAdaptableTextArea {

    private static final long serialVersionUID = 4852091779560114948L;

    public CustomTextArea() {
        super();
    }

    public CustomTextArea(String text) {
        super(text);
    }

}