package fr.soleil.flyscan.gui.util;

public class TimedData<D> {
    private long when;
    private D data;

    public TimedData(long when, D data) {
        super();
        this.when = when;
        this.data = data;
    }

    public long getWhen() {
        return when;
    }

    public D getData() {
        return data;
    }
}
