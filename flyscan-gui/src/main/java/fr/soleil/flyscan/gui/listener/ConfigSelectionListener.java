package fr.soleil.flyscan.gui.listener;

import java.util.EventListener;

import fr.soleil.flyscan.gui.event.ConfigSelectionEvent;

/**
 * An {@link EventListener} that listens to the selection of a configuration.
 * 
 * @author GIRARDOT
 */
public interface ConfigSelectionListener extends EventListener {

    /**
     * Notifies this {@link ConfigSelectionListener} for the selection of a configuration.
     * 
     * @param e The {@link ConfigSelectionEvent} that indicates which configuration is selected.
     */
    public void configSelectionChanged(ConfigSelectionEvent e);

}
