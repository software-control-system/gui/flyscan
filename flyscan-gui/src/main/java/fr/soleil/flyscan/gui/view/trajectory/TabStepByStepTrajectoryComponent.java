package fr.soleil.flyscan.gui.view.trajectory;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.scrollpane.FSScrollPane;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TabStepByStepTrajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.lib.project.swing.text.DocumentNumber;

public class TabStepByStepTrajectoryComponent extends ATrajectoryComponent<TabStepByStepTrajectory> {

    private static final long serialVersionUID = -3691117306121287486L;

    private final JPanel indexesPanel;
    private final Collection<JTextField> textFields;
    private final JButton addButton;
    private final JButton removeButton;

    public TabStepByStepTrajectoryComponent(JPanel errorPanel, Map<Component, JTextComponent> errorsMap,
            TabStepByStepTrajectory pTrajectory, String location, ConfigurationGUI configurationGUI, JLabel nameLabel,
            JRadioButton selector, String tabTitle, final String configName, final String actuatorName) {
        super(errorPanel, errorsMap, location, configurationGUI, nameLabel, selector, tabTitle, configName,
                actuatorName);
        textFields = new ArrayList<>();
        trajectory = pTrajectory;

        setLayout(new GridBagLayout());

        indexesPanel = new JPanel();
        indexesPanel.setLayout(new BoxLayout(indexesPanel, BoxLayout.LINE_AXIS));

        for (int i = 0; i < trajectory.getPoints().size(); i++) {
            addNewPoint(i);
        }

        GridBagConstraints indexesGridBagConstraints = new GridBagConstraints();
        indexesGridBagConstraints.gridx = 0;
        indexesGridBagConstraints.gridy = 0;
        indexesGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        indexesGridBagConstraints.weightx = 1;

        final FSScrollPane scrollbox = new FSScrollPane(indexesPanel);
        scrollbox.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        add(scrollbox, indexesGridBagConstraints);

        addButton = Utilities.createButton(Utilities.ADD, true, Color.WHITE);
        addButton.setMargin(CometeUtils.getzInset());

        removeButton = Utilities.createButton(Utilities.REMOVE, true, Color.WHITE);
        removeButton.setMargin(CometeUtils.getzInset());
        removeButton.setEnabled(trajectory.getPoints().size() > 1);

        addButton.addActionListener(e -> {
            int index = trajectory.getPoints().size();
            trajectory.getPoints().add(0d);
            fireEditionStateChanged(true);
            addNewPoint(index);
            removeButton.setEnabled(trajectory.getPoints().size() > 1);
            indexesPanel.validate();
            indexesPanel.updateUI();
            scrollbox.getViewport().validate();
            scrollbox.getViewport().updateUI();
        });

        removeButton.addActionListener(e -> {
            int lastIndex = trajectory.getPoints().size() - 1;
            if (lastIndex > 0) {
                trajectory.getPoints().remove(lastIndex);
                fireEditionStateChanged(true);
                indexesPanel.removeAll();
                textFields.clear();
                for (int i = 0; i < trajectory.getPoints().size(); i++) {
                    addNewPoint(i);
                }
                removeButton.setEnabled(trajectory.getPoints().size() > 1);
                indexesPanel.validate();
                indexesPanel.updateUI();
                scrollbox.getViewport().validate();
                scrollbox.getViewport().updateUI();
            }
        });

        Utilities.addButtonPanel(this, addButton, removeButton, true);
    }

    protected void addNewPoint(final int index) {
        DocumentNumber doc = new DocumentNumber();
        doc.setAllowFloatValues(true);
        doc.setAllowNegativeValues(true);
        final JTextField pointTextField = new JTextField(doc, ParsingTool.toString(trajectory.getPoints().get(index)),
                5);
        pointTextField.setDisabledTextColor(Color.GRAY);
        pointTextField.setMaximumSize(new Dimension(40, 23));
        pointTextField.setMinimumSize(new Dimension(40, 23));
        pointTextField.setPreferredSize(new Dimension(40, 23));
        pointTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                fireEditionStateChanged(true);
                super.mouseClicked(e);
            }
        });

        pointTextField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = pointTextField.getText();
                updateValue(index, text);

            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = pointTextField.getText();
                updateValue(index, text);

            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }

            protected void updateValue(int index, String text) {
                try {
                    if (!text.isEmpty()) {
                        double value = ParsingTool.parseDouble(text.trim());
                        fireEditionStateChanged(true);
                        trajectory.getPoints().set(index, value);
                    }
                } catch (NumberFormatException e1) {
                    displayError(String
                            .format(Utilities.getMessage("flyscan.error.trajectory.parameter.number.invalid"), text));
                }
            }
        });

        indexesPanel.add(pointTextField);
        textFields.add(pointTextField);
        pointTextField.requestFocusInWindow();
    }

    @Override
    public TabStepByStepTrajectory getTrajectory() {
        return trajectory;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (JTextField tf : textFields) {
            tf.setEnabled(enabled);
        }

        addButton.setEnabled(enabled);
        removeButton.setEnabled(enabled);
    }

    @Override
    protected boolean isMyTrajectoryClass(Trajectory trajectory) {
        return trajectory instanceof TabStepByStepTrajectory;
    }

}
