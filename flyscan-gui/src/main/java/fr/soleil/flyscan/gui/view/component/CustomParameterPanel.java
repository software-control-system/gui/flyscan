package fr.soleil.flyscan.gui.view.component;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.LinkedHashSet;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.definition.listener.ITextAreaListener;
import fr.soleil.comete.swing.Button;
import fr.soleil.flyscan.gui.listener.CustomParameterListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.text.CustomTextArea;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;

/**
 * Panel for custom parameters display and edition
 * 
 * @author guerre-giordano
 * 
 */
public class CustomParameterPanel extends JPanel {

    private static final long serialVersionUID = -7144311195846416752L;

    private final Configuration configuration;
    private final ConfigurationGUI configurationGUI;
    private final boolean isEnabled;
    private final Collection<CustomParameterListener> listeners;

    public CustomParameterPanel(final ConfigurationGUI configurationGUI, final Configuration configuration,
            boolean isEnabled) {
        this(Utilities.CUSTOM_PARAMETERS, configurationGUI, configuration, isEnabled);
    }

    public CustomParameterPanel(final String title, final ConfigurationGUI configurationGUI,
            final Configuration configuration, boolean isEnabled) {
        this.configuration = configuration;
        this.configurationGUI = configurationGUI;
        this.isEnabled = isEnabled;
        this.listeners = new LinkedHashSet<>();

        setLayout(new GridBagLayout());
        final TitledBorder titledBorder = new TitledBorder(title);
        titledBorder.setTitleFont(Utilities.PARAMETER_TITLE_FONT);
        setBorder(titledBorder);
    }

    public void addCustomParameterListener(CustomParameterListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeCustomParameterListener(CustomParameterListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    public void fireCustomParameterChange(Entry entry) {
        Collection<CustomParameterListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (CustomParameterListener listener : copy) {
            listener.parameterChanged(entry);
        }
        copy.clear();
    }

    public String getMatchingEntryKey(String name) {
        String matchingKey = null;
        for (Section section : configuration.getSections()) {
            Entry entry = section.getEntry(name);
            if (entry != null) {
                matchingKey = entry.getKey();
            }
            if (matchingKey == null) {
                for (FSObject obj : section.getObjects()) {
                    if (obj != null) {
                        entry = obj.getEntry(name);
                        if (entry != null) {
                            matchingKey = entry.getKey();
                            break;
                        }
                    }
                }
            }
        }
        return matchingKey;
    }

    /**
     * Add new entry to custom parameters panel
     * 
     * @param entry Entry
     * @param actor EntryContainer
     */
    public void addNewCustomParameter(final Entry entry, final EntryContainer actor) {
        JLabel label = new FSLabel(entry.getKey());

        final CustomTextArea editor = new CustomTextArea(entry.getValue());
        editor.getTextArea().addTextAreaListener(new ITextAreaListener() {
            protected void applyChange() {
                entry.setValue(editor.getTextArea().getAdaptedText());
                fireCustomParameterChange(entry);
            }

            @Override
            public void textChanged(EventObject event) {
                applyChange();
            }

            @Override
            public void actionPerformed(EventObject event) {
                applyChange();
            }
        });

        editor.setEnabled(isEnabled);

        int y = Utilities.getNextY(this);

        GridBagConstraints labelGridBagConstraints = new GridBagConstraints();
        labelGridBagConstraints.gridx = 0;
        labelGridBagConstraints.gridy = y++;
        labelGridBagConstraints.fill = GridBagConstraints.NONE;
        labelGridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        labelGridBagConstraints.weightx = 0;
        labelGridBagConstraints.insets = new Insets(5, 5, 5, 5);

        GridBagConstraints textFieldGridBagConstraints = new GridBagConstraints();
        textFieldGridBagConstraints.gridx = 1;
        textFieldGridBagConstraints.gridy = y++;
        textFieldGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        textFieldGridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        textFieldGridBagConstraints.weightx = 1;
        textFieldGridBagConstraints.insets = new Insets(5, 0, 5, 5);

        add(label, labelGridBagConstraints);
        add(editor, textFieldGridBagConstraints);

        revalidate();
        repaint();
    }

    /**
     * Return the label, the text field and the delete button used to display and edit a custom parameter within an
     * array of components
     * 
     * @param entry Entry
     * @return Component[]
     */
    protected Component[] getCustomParameterComponents(Entry entry) {
        Component[] components = new Component[3];
        for (int i = 0; i < getComponentCount(); i++) {
            Component c = getComponent(i);
            if (c instanceof JLabel) {
                JLabel label = (JLabel) c;
                if (label.getText().equalsIgnoreCase(entry.getKey())) {
                    components[0] = label;
                    Component cPlus = getComponent(i + 1);
                    if (cPlus instanceof CustomTextArea) {
                        components[1] = cPlus;
                    }
                    Component cPlusPlus = getComponent(i + 2);
                    if (cPlusPlus instanceof Button) {
                        components[2] = cPlusPlus;
                    }
                }
            }
        }
        return components;
    }

    public ConfigurationGUI getConfigurationGUI() {
        return configurationGUI;
    }

}
