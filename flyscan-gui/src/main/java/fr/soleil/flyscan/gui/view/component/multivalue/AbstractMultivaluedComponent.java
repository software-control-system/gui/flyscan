package fr.soleil.flyscan.gui.view.component.multivalue;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Abstract panel for entry of list format
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public abstract class AbstractMultivaluedComponent<C extends JComponent, V> extends JPanel implements IEntryComponent {

    private static final long serialVersionUID = -512126702118932505L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractMultivaluedComponent.class.getName());
    protected static final String EMPTY = "[]";

    protected final EventListenerList listeners = new EventListenerList();
    protected final int count;
    private int mininimumNumberOfElements; // this one is private to force using getter/setter.
    protected boolean scalarContentAllowed;
    protected final Map<Integer, C> components;
    protected final JPanel componentsPanel;
    protected final JButton addButton;
    protected final JButton removeButton;
    protected final Class<V> valueClass;

    public AbstractMultivaluedComponent(Class<V> valueClass, Collection<? extends V> values, int count,
            boolean mayThrowException, Object... otherData) throws FSParsingException {
        super();
        this.valueClass = valueClass;
        this.count = count;
        setLayout(new GridBagLayout());
        components = new LinkedHashMap<>();

        JPanel intermediatePanel = new JPanel(new GridBagLayout());

        componentsPanel = new JPanel(new GridBagLayout());

        addButton = Utilities.createButton(Utilities.ADD, true, Color.WHITE);
        removeButton = Utilities.createButton(Utilities.REMOVE, true, Color.WHITE);
        addButton.addActionListener(e -> {
            add();
        });
        removeButton.addActionListener(e -> {
            remove();
        });

        // SCAN-977: change to true to restore previous behavior.
        scalarContentAllowed = false;
        // change to 1 to restore previous behavior.
        mininimumNumberOfElements = 0;
        initBeforeValues(otherData);

        // for each value to edit, create an editing component
        addComponents(values, mayThrowException);

        GridBagConstraints intermediateGridBagConstraints = new GridBagConstraints();
        intermediateGridBagConstraints.fill = GridBagConstraints.NONE;
        intermediateGridBagConstraints.gridx = 0;
        intermediateGridBagConstraints.gridy = 0;
        intermediateGridBagConstraints.weightx = 1;
        intermediateGridBagConstraints.anchor = GridBagConstraints.WEST;
        add(intermediatePanel, intermediateGridBagConstraints);

        GridBagConstraints comboPanelGridBagConstraints = new GridBagConstraints();
        comboPanelGridBagConstraints.fill = GridBagConstraints.BOTH;
        comboPanelGridBagConstraints.gridx = 0;
        comboPanelGridBagConstraints.gridy = 0;
        comboPanelGridBagConstraints.insets = new Insets(5, 0, 5, 10);
        comboPanelGridBagConstraints.anchor = GridBagConstraints.NORTHWEST;

        intermediatePanel.add(componentsPanel, comboPanelGridBagConstraints);

        Utilities.addButtonPanel(intermediatePanel, addButton, removeButton, false);

        if (count > -1) {
            addButton.setEnabled(false);
            removeButton.setEnabled(false);
        }
    }

    protected void initBeforeValues(Object... otherData) {
        // nothing to do by default
    }

    protected void add() {
        Integer rank = Integer.valueOf(components.size());
        try {
            addComponent(rank, null, true);
        } catch (FSParsingException e) {
            // Should not happen as value is not initialized
            LOGGER.error("Unexpected error while adding component", e);
        }
        removeButton.setEnabled(isThereAnElementToRemove());
        fireEditionStateChanged(true);
        revalidate();
    }

    protected void addComponents(Collection<? extends V> values, boolean mayThrowException) throws FSParsingException {
        int i = 0;
        for (V value : values) {
            addComponent(i, value, mayThrowException);
            i++;
        }
        updateButtons();
    }

    protected void updateButtons() {
        removeButton.setEnabled(isThereAnElementToRemove());
    }

    protected void remove(boolean updateButtons, boolean warn) {
        int lastIndex = components.size() - 1;
        if (lastIndex > -1) {
            removeComponent(lastIndex);
            if (updateButtons) {
                updateButtons();
            }
            if (warn) {
                fireEditionStateChanged(true);
            }
            revalidate();
        }
    }

    protected void remove() {
        remove(true, true);
    }

    protected void clear(boolean updateButtons, boolean warn) {
        while (components.size() > 0) {
            remove(false, false);
        }
        if (updateButtons) {
            updateButtons();
        }
        if (warn) {
            fireEditionStateChanged(true);
        }
    }

    /**
     * Add text field to the panel
     * 
     * @param rank The value index
     * @param value String
     * @param mayThrowException Whether a {@link FSParsingException} can be thrown in case of problem.
     * @throws FSParsingException If a problem occurred while setting <code>value</code>.
     */
    protected void addComponent(Integer rank, V value, boolean mayThrowException) throws FSParsingException {
        C component = createComponent();
        FSParsingException exception = null;
        try {
            setValueToComponent(component, value, rank);
        } catch (FSParsingException e) {
            exception = e;
        }
        listenToComponent(component);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridy = 0;
        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.anchor = GridBagConstraints.NORTHWEST;

        componentsPanel.add(component, constraints);

        components.put(rank, component);
        if ((exception != null) && mayThrowException) {
            throw exception;
        }
    }

    protected abstract void setValueToComponent(C component, V value, Integer rank) throws FSParsingException;

    /**
     * Removes a {@link JComponent} from the panel
     * 
     * @param rank The value index
     */
    protected void removeComponent(int rank) {
        C toRemove = components.remove(Integer.valueOf(rank));
        if (toRemove != null) {
            componentsPanel.remove(toRemove);
            forgetComponent(toRemove);
        }
    }

    protected abstract void listenToComponent(C component);

    protected abstract void forgetComponent(C component);

    /**
     * Build a {@link JComponent} with possible values
     * 
     * @return a {@link JComponent}
     */
    protected abstract C createComponent();

    /**
     * Returns the data managed by this component, as a {@link Collection}.
     * 
     * @return A {@link Collection}
     */
    public final Collection<V> getValues() {
        Collection<V> toReturn = new ArrayList<>();
        for (C component : components.values()) {
            toReturn.add(getValue(component));
        }
        return toReturn;
    }

    protected abstract V getValue(C component);

    /**
     * Returns the string representation of the data managed by this component.
     * 
     * @return A {@link String}.
     */
    public final String getStringValue() {
        String val;
        Collection<V> values = getValues();
        // SCAN-977: check isScalarContentAllowed() to decide whether to allow returning scalar value and/or empty
        // string.
        if (isScalarContentAllowed() && values.size() < 2) {
            if (values.size() == 1) {
                val = toString(ObjectUtils.getFirstElement(values));
            } else {
                val = ObjectUtils.EMPTY_STRING;
            }
        } else if (values.size() == 0) {
            val = EMPTY;
        } else {
            StringBuilder sb = new StringBuilder(ParsingUtil.BRACKET_OPENNING);
            int i = 0;
            for (V value : values) {
                String s = toString(value);
                sb.append(s);
                if (i++ < values.size() - 1) {
                    sb.append(ParsingUtil.COMMA);
                }
            }
            sb.append(ParsingUtil.BRACKET_CLOSING);
            val = sb.toString();
        }
        return val;
    }

    protected abstract String toString(V value);

    protected boolean isThereAnElementToRemove() {
        return components.size() > getMininimumNumberOfElements();
    }

    public boolean isScalarContentAllowed() {
        return scalarContentAllowed;
    }

    /**
     * Sets whether this component can return, in {@link #getStringValue()} method, the scalar representation of its
     * content.
     * 
     * @param scalarContentAllowed Whether this component can return, in {@link #getStringValue()} method, the scalar
     *            representation of its content.
     *            <ul>
     *            <li>
     *            If <code>true</code>:
     *            <ul>
     *            <li>If this component represents no value, {@link #getStringValue()} will return an empty string.</li>
     *            <li>If this component represents 1 value, {@link #getStringValue()} will return the string
     *            representation of that value.</li>
     *            <li>Otherwise, {@link #getStringValue()} will return <code>"[value1, value2, ...]"</code> as
     *            expected.</li>
     *            </ul>
     *            </li>
     *            <li>If <code>false</code>, {@link #getStringValue()} will always return the values surrounded with
     *            <code>[]</code><i>(<code>"[]"</code> for no value, <code>"[value]"</code> for 1 value,
     *            <code>"[value1, value2, ...]"</code> for other cases)</i>.</li>
     *            </ul>
     * @see #getStringValue().
     */
    public void setScalarContentAllowed(boolean scalarContentAllowed) {
        this.scalarContentAllowed = scalarContentAllowed;
    }

    /**
     * Returns the minimum number of elements allowed for this component.
     * 
     * @return An <code>int</code>, always <code>&ge;0</code>.
     */
    public int getMininimumNumberOfElements() {
        return mininimumNumberOfElements;
    }

    /**
     * Sets the minimum number of elements allowed for this component.
     * 
     * @param mininimumNumberOfElements The minimum number of elements allowed for this component.
     *            A value under 0 will be replaced by 0.
     */
    public void setMininimumNumberOfElements(int mininimumNumberOfElements) {
        this.mininimumNumberOfElements = Math.max(0, mininimumNumberOfElements);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (JComponent tf : components.values()) {
            tf.setEnabled(enabled);
        }
        if (enabled) {
            enabled = (count < 0);
        }
        addButton.setEnabled(enabled);
        removeButton.setEnabled(enabled && isThereAnElementToRemove());
    }

    public void addBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.add(ConfigurationEditionStateListener.class, listener);
    }

    public void removeBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.remove(ConfigurationEditionStateListener.class, listener);
    }

    public ConfigurationEditionStateListener[] getBeingEditedListeners() {
        return listeners.getListeners(ConfigurationEditionStateListener.class);
    }

    protected void fireEditionStateChanged(boolean isBeingEdited) {
        if (isBeingEdited) {
            setBorder(null);
        }
        for (ConfigurationEditionStateListener listener : getBeingEditedListeners()) {
            listener.setConfigurationBeingEdited(isBeingEdited);
        }
    }

    protected abstract V parse(String value);

    @Override
    public void resetToDefault(String defaultValue) throws FSParsingException {
        if ((defaultValue != null) && !defaultValue.trim().equals(getStringValue())) {
            Collection<V> values = new ArrayList<>();
            String simplified = UIDelegate.getSimplifiedValue(defaultValue);
            if (!simplified.isEmpty()) {
                String[] tmp = simplified.split(ParsingUtil.COMMA);
                for (String str : tmp) {
                    values.add(parse(str));
                }
            }
            clear(false, false);
            addComponents(values, true);
            updateButtons();
            fireEditionStateChanged(true);
        }
    }

}
