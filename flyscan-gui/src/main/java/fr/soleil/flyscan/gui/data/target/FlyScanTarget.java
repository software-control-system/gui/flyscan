package fr.soleil.flyscan.gui.data.target;

import java.lang.reflect.Array;

import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.matrix.ITextMatrixTarget;

/**
 * Target used to retrieve data from pluginsInfo attribute without displaying it
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public abstract class FlyScanTarget implements ITextMatrixTarget {

    protected final StringMatrix matrix;
    protected final TargetDelegate delegate;

    public FlyScanTarget() {
        matrix = new StringMatrix();
        delegate = new TargetDelegate();
    }

    protected abstract void updateFromStringArray(String... data);

    protected final void updateData() {
        updateFromStringArray(getFlatStringMatrix());
    }

    @Override
    public String[] getFlatStringMatrix() {
        return matrix.getFlatValue();
    }

    @Override
    public String[][] getStringMatrix() {
        return matrix.getValue();
    }

    @Override
    public final void setFlatStringMatrix(String[] value, int width, int height) {
        matrix.setFlatValue(value, width, height);
        updateData();
    }

    @Override
    public final void setStringMatrix(String[][] value) {
        matrix.setValue(value);
        updateData();
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public final boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

}
