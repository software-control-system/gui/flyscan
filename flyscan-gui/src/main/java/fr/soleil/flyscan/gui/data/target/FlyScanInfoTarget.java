package fr.soleil.flyscan.gui.data.target;

import fr.soleil.flyscan.gui.util.Utilities;

public class FlyScanInfoTarget extends FlyScanTarget {

    private String flyScanVersion;

    public FlyScanInfoTarget() {
        super();
        flyScanVersion = Utilities.DEFAULT_VERSION;
    }

    @Override
    protected void updateFromStringArray(String... data) {
        String version = Utilities.DEFAULT_VERSION;
        if (data != null) {
            boolean identified = false;
            for (String line : data) {
                int index = line.indexOf(':');
                if (index > -1) {
                    String key = line.substring(0, index).trim();
                    String value = line.substring(index + 1).trim();
                    if (Utilities.PROJECT.equalsIgnoreCase(key) && Utilities.FSS.equalsIgnoreCase(value)) {
                        identified = true;
                    } else if (identified && Utilities.VERSION.equalsIgnoreCase(key)) {
                        version = value;
                        break;
                    }
                }
            }
        }
        this.flyScanVersion = version;
    }

    public String getFlyScanVersion() {
        return flyScanVersion;
    }

}
