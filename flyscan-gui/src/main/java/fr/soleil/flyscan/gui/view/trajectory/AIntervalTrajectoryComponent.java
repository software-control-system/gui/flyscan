package fr.soleil.flyscan.gui.view.trajectory;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Interval;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.lib.project.swing.text.DocumentNumber;

public abstract class AIntervalTrajectoryComponent<F extends Trajectory> extends ATrajectoryComponent<F> {

    private static final long serialVersionUID = 775749506734357472L;

    protected final List<Interval> intervals;
    protected final List<JTextField> textFields;
    private JButton addButton;
    private JButton removeButton;

    public AIntervalTrajectoryComponent(JPanel errorPanel, Map<Component, JTextComponent> errorsMap, String location,
            ConfigurationGUI configurationGUI, JLabel nameLabel, JRadioButton selector, String tabTitle,
            final String configName, final String actuatorName) {
        super(errorPanel, errorsMap, location, configurationGUI, nameLabel, selector, tabTitle, configName,
                actuatorName);
        intervals = new ArrayList<>();
        textFields = new ArrayList<>();
    }

    /**
     * Build panel used to display and to edit intervals
     * 
     * @param intervals List<Interval>
     */
    protected void buildMainPanel(final List<Interval> intervals) {
        this.intervals.addAll(intervals);

        setLayout(new GridBagLayout());

        final JPanel intervalsPanel = new JPanel();
        intervalsPanel.setLayout(new GridBagLayout());

        for (Interval interval : intervals) {
            addNewInterval(interval, intervalsPanel);
        }

        JPanel intermediatePanel = new JPanel();
        intermediatePanel.setLayout(new GridBagLayout());

        addButton = Utilities.createButton(Utilities.ADD, true, Color.WHITE);
        addButton.addActionListener(e -> {
            Interval interval = new Interval(0, 0, 0);
            AIntervalTrajectoryComponent.this.intervals.add(interval);
            removeButton.setEnabled(isEnabled() && AIntervalTrajectoryComponent.this.intervals.size() > 1);
            addNewInterval(interval, intervalsPanel);
            fireEditionStateChanged(true);
            AIntervalTrajectoryComponent.this.validate();
            AIntervalTrajectoryComponent.this.updateUI();
        });

        removeButton = Utilities.createButton(Utilities.REMOVE, true, Color.WHITE);
        removeButton.setEnabled(isEnabled() && this.intervals.size() > 1);
        removeButton.addActionListener(e -> {
            int lastIndex = AIntervalTrajectoryComponent.this.intervals.size() - 1;
            if (lastIndex > 0) {
                AIntervalTrajectoryComponent.this.intervals.remove(lastIndex);
                removeButton.setEnabled(isEnabled() && AIntervalTrajectoryComponent.this.intervals.size() > 1);
                intervalsPanel.removeAll();
                textFields.clear();
                for (Interval interval : AIntervalTrajectoryComponent.this.intervals) {
                    addNewInterval(interval, intervalsPanel);
                }
                fireEditionStateChanged(true);
                AIntervalTrajectoryComponent.this.validate();
                AIntervalTrajectoryComponent.this.updateUI();
            }
        });

        GridBagConstraints intermediateGridBagConstraints = new GridBagConstraints();
        intermediateGridBagConstraints.fill = GridBagConstraints.NONE;
        intermediateGridBagConstraints.gridx = 0;
        intermediateGridBagConstraints.gridy = 0;
        intermediateGridBagConstraints.weightx = 0.4;
        intermediateGridBagConstraints.anchor = GridBagConstraints.WEST;
        add(intermediatePanel, intermediateGridBagConstraints);

        GridBagConstraints intervalsGridBagConstraints = new GridBagConstraints();
        intervalsGridBagConstraints.fill = GridBagConstraints.NONE;
        intervalsGridBagConstraints.gridx = 0;
        intervalsGridBagConstraints.gridy = 0;
        intervalsGridBagConstraints.anchor = GridBagConstraints.WEST;
        intermediatePanel.add(intervalsPanel, intervalsGridBagConstraints);

        Utilities.addButtonPanel(intermediatePanel, addButton, removeButton, true);
    }

    protected void updateInterval(JTextField field, Interval interval, IntervalType type) {
        String text = field.getText();
        double value;
        try {
            value = ParsingTool.parseDouble(text.trim());
        } catch (NumberFormatException nfe) {
            value = Double.NaN;
        }
        switch (type) {
            case FROM:
                interval.setFrom(value);
                break;
            case TO:
                interval.setTo(value);
                break;
            case NB:
                interval.setNbAcq(Double.isNaN(value) ? -1 : (int) value);
                break;
        }
        fireEditionStateChanged(true);
    }

    protected DocumentNumber generateDocumentNumber() {
        DocumentNumber doc = new DocumentNumber();
        doc.setAllowFloatValues(true);
        doc.setAllowNegativeValues(true);
        doc.setAllowScientificValues(true);
        return doc;
    }

    protected JPanel generatePanel(String title, JTextField textField) {
        JPanel panel = new JPanel(new BorderLayout(0, 0));
        panel.setBorder(new TitledBorder(title));
        panel.add(textField, BorderLayout.CENTER);
        return panel;
    }

    protected void addNewInterval(final Interval interval, JPanel intervalsPanel) {
        JPanel intervalPanel = new JPanel();
        intervalPanel.setLayout(new GridBagLayout());

        DocumentNumber fromDoc = generateDocumentNumber();
        final JTextField fromTextField = new JTextField(fromDoc, ParsingTool.toString(interval.getFrom()), 15);
        fromTextField.setBackground(getBackground());
        fromTextField.setDisabledTextColor(Color.GRAY);
        fromTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                updateValue(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateValue(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }

            protected void updateValue(DocumentEvent e) {
                updateInterval(fromTextField, interval, IntervalType.FROM);
            }
        });

        DocumentNumber toDoc = generateDocumentNumber();
        final JTextField toTextField = new JTextField(toDoc, ParsingTool.toString(interval.getTo()), 15);
        toTextField.setDisabledTextColor(Color.GRAY);
        toTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                updateValue(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateValue(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }

            protected void updateValue(DocumentEvent e) {
                updateInterval(toTextField, interval, IntervalType.TO);
            }
        });

        DocumentNumber nbAcqDoc = new DocumentNumber();
        nbAcqDoc.setAllowFloatValues(false);
        nbAcqDoc.setAllowNegativeValues(false);
        final JTextField nbAcqTextField = new JTextField(nbAcqDoc, Integer.toString(interval.getNbAcq()), 15);
        nbAcqTextField.setDisabledTextColor(Color.GRAY);
        nbAcqTextField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = nbAcqTextField.getText();
                updateValue(interval, text);

            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = nbAcqTextField.getText();
                updateValue(interval, text);

            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }

            protected void updateValue(final Interval interval, String text) {
                updateInterval(nbAcqTextField, interval, IntervalType.NB);
            }

        });

        GridBagConstraints fromConstraints = new GridBagConstraints();
        fromConstraints.gridx = 0;
        fromConstraints.gridy = 0;
        intervalPanel.add(generatePanel(Utilities.getMessage("flyscan.interval.from"), fromTextField), fromConstraints);

        GridBagConstraints toConstraints = new GridBagConstraints();
        toConstraints.gridx = 1;
        toConstraints.gridy = 0;
        intervalPanel.add(generatePanel(Utilities.getMessage("flyscan.interval.to"), toTextField), toConstraints);

        GridBagConstraints nbAcqConstraints = new GridBagConstraints();
        nbAcqConstraints.gridx = 2;
        nbAcqConstraints.gridy = 0;
        intervalPanel.add(generatePanel(Utilities.getMessage("flyscan.interval.points"), nbAcqTextField),
                nbAcqConstraints);

        GridBagConstraints intervalPanelConstraints = new GridBagConstraints();
        intervalPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        intervalPanelConstraints.gridx = 0;
        intervalPanelConstraints.gridy = Utilities.getNextY(intervalsPanel);
        intervalPanelConstraints.anchor = GridBagConstraints.WEST;

        intervalsPanel.add(intervalPanel, intervalPanelConstraints);

        textFields.add(fromTextField);
        textFields.add(toTextField);
        textFields.add(nbAcqTextField);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        for (JTextField tf : textFields) {
            tf.setEnabled(enabled);
        }
        addButton.setEnabled(enabled);
        removeButton.setEnabled(enabled);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static enum IntervalType {
        FROM, TO, NB
    }

}
