package fr.soleil.flyscan.gui.view.ui;

import java.awt.Color;

import javax.swing.plaf.basic.BasicMenuItemUI;

public class ConfigMenuItemUI extends BasicMenuItemUI {

    protected Color defaultSelectionBackground;

    public ConfigMenuItemUI() {
        super();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        defaultSelectionBackground = selectionBackground;
    }

    public Color getSelectionBackground() {
        return selectionBackground;
    }

    public void setSelectionBackground(Color selectionBackground) {
        this.selectionBackground = selectionBackground == null ? defaultSelectionBackground : selectionBackground;
    }

}
