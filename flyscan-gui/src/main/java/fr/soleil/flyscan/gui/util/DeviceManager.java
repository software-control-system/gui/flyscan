package fr.soleil.flyscan.gui.util;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.esrf.TangoApi.DbDatum;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.Splash;

public class DeviceManager implements IFlyScanConstants {

    private DeviceManager() {
        // hide constructor
    }

    // SCAN-925: get RecordingManager from device property
    protected static String recoverRecordingManager(String fss) {
        String recordingManager = ObjectUtils.EMPTY_STRING;
        DbDatum prop = TangoDeviceHelper.createDbDatum(fss, RECORDING_MANAGER);
        if (prop != null) {
            try {
                recordingManager = prop.extractStringArray()[0];
                if (recordingManager == null) {
                    recordingManager = ObjectUtils.EMPTY_STRING;
                }
            } catch (Exception e) {
                LOGGER.error(String.format(Utilities.getMessage("flyscan.error.property.read"), RECORDING_MANAGER,
                        TangoExceptionHelper.getErrorMessage(e), e));
            }
        }
        return recordingManager;
    }

    // Utility method only used in main method
    private static GridBagConstraints generateConstraints(int fill, int gridx, int gridy, double weightx,
            double weighty) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = fill;
        constraints.gridx = gridx;
        constraints.gridy = gridy;
        constraints.weightx = weightx;
        constraints.weighty = weighty;
        return constraints;
    }

    private static void updateFSSAndRecordingManager(Map<String, TimedData<String>> propertyMap, long deltaTime,
            String... devices) {
        if (devices[0] == null) {
            devices[0] = ObjectUtils.EMPTY_STRING;
        }
        String fss = devices[0];
        if (fss.isEmpty()) {
            devices[1] = ObjectUtils.EMPTY_STRING;
        } else {
            TimedData<String> timedProp = propertyMap.get(fss);
            long time = System.currentTimeMillis();
            String prop = ObjectUtils.EMPTY_STRING;
            if (timedProp == null || time - timedProp.getWhen() >= deltaTime) {
                try {
                    prop = recoverRecordingManager(fss);
                    timedProp = new TimedData<>(time, prop);
                    TimedData<String> tmp = propertyMap.putIfAbsent(fss, timedProp);
                    if (tmp != null) {
                        timedProp = tmp;
                        prop = tmp.getData();
                        if (prop == null) {
                            prop = ObjectUtils.EMPTY_STRING;
                        }
                    }
                } catch (Exception e) {
                    prop = ObjectUtils.EMPTY_STRING;
                }
            }
            devices[1] = prop;
        }
    }

    private static String[] updateDeviceClasses(String deviceClass, Map<String, TimedData<String[]>> deviceClassMap,
            long deltaTime, JComboBox<String> combo, JDialog deviceSelectionDialog) {
        long time = System.currentTimeMillis();
        boolean visible = deviceSelectionDialog.isShowing();
        TimedData<String[]> data = deviceClassMap.get(deviceClass);
        String[] devices = Utilities.EMPTY_STRING_ARRAY;
        if (data == null || time - data.getWhen() >= deltaTime) {
            String[] formerDevices = data == null || data.getData() == null ? Utilities.EMPTY_STRING_ARRAY
                    : data.getData();
            devices = TangoDeviceHelper.getExportedDevicesOfClass(deviceClass);
            data = new TimedData<>(time, devices);
            TimedData<String[]> tmp = deviceClassMap.putIfAbsent(deviceClass, data);
            if (tmp != null) {
                data = tmp;
                devices = tmp.getData();
            }
            if ((!ObjectUtils.sameObject(devices, formerDevices)) && (combo != null)) {
                final String[] items = devices;
                SwingUtilities.invokeLater(() -> {
                    String item = (String) combo.getSelectedItem();
                    combo.removeAllItems();
                    for (String device : items) {
                        if ((device != null) && !device.isEmpty()) {
                            combo.addItem(device);
                        }
                    }
                    if (item != null) {
                        int index = -1;
                        for (int i = 0; i < combo.getItemCount(); i++) {
                            if (item.equalsIgnoreCase(combo.getItemAt(i))) {
                                index = i;
                                break;
                            }
                        }
                        if (index > -1) {
                            combo.setSelectedIndex(index);
                        }
                    }
                    combo.revalidate();
                    SwingUtilities.invokeLater(() -> {
                        deviceSelectionDialog.pack();
                        if (!visible) {
                            deviceSelectionDialog.setLocationRelativeTo(null);
                        }
                    });
                });
            }
        }
        return devices;
    }

    private static void updateOkButton(JButton okButton, String... devices) {
        okButton.setEnabled((!devices[0].isEmpty()) && (!devices[1].isEmpty()));
    }

    private static void updatedRecordingManagerDevices(String device, Map<String, TimedData<String>> propertyMap,
            long deltaTime, JButton okButton, JComboBox<String> recordingManagerDevices, JLabel recordingManagerLabel,
            JPanel recordingManagerPanel, CardLayout recordingManagerLayout, String label, String combo,
            JDialog deviceSelectionDialog, String... devices) {
        devices[0] = device;
        updateFSSAndRecordingManager(propertyMap, deltaTime, devices);
        SwingUtilities.invokeLater(() -> {
            recordingManagerLayout.show(recordingManagerPanel, combo);
        });
        if (devices[1].isEmpty()) {
            SwingUtilities.invokeLater(() -> {
                String item = (String) recordingManagerDevices.getSelectedItem();
                devices[1] = item == null ? ObjectUtils.EMPTY_STRING : item;
            });
        } else if (!devices[0].isEmpty()) {
            String recordingManagerProp = devices[1];
            SwingUtilities.invokeLater(() -> {
                recordingManagerLabel.setText(recordingManagerProp);
                recordingManagerLayout.show(recordingManagerPanel, label);
                int index = -1;
                for (int i = 0; i < recordingManagerDevices.getItemCount(); i++) {
                    if (recordingManagerProp.equalsIgnoreCase(recordingManagerDevices.getItemAt(i))) {
                        index = i;
                        break;
                    }
                }
                if (index > -1) {
                    recordingManagerDevices.setSelectedIndex(index);
                }
            });
        }
        devices[0] = device == null ? ObjectUtils.EMPTY_STRING : device;
        if ((device != null) && !device.isEmpty()) {
            TimedData<String> timedProp = propertyMap.get(device);
            long time = System.currentTimeMillis();
            String prop = ObjectUtils.EMPTY_STRING;
            if (timedProp == null || time - timedProp.getWhen() >= deltaTime) {
                try {
                    prop = recoverRecordingManager(device);
                    timedProp = new TimedData<>(time, prop);
                    TimedData<String> tmp = propertyMap.putIfAbsent(device, timedProp);
                    if (tmp != null) {
                        timedProp = tmp;
                        prop = tmp.getData();
                        if (prop == null) {
                            prop = ObjectUtils.EMPTY_STRING;
                        }
                    }
                } catch (Exception e) {
                    prop = ObjectUtils.EMPTY_STRING;
                }
            } else if (timedProp != null) {
                prop = timedProp.getData();
            }
            if (prop.isEmpty()) {
                SwingUtilities.invokeLater(() -> {
                    String item = (String) recordingManagerDevices.getSelectedItem();
                    devices[1] = item == null ? ObjectUtils.EMPTY_STRING : item;
                    recordingManagerLayout.show(recordingManagerPanel, combo);
                });
            } else {
                devices[1] = prop;
                final String recordingManagerProp = prop;
                SwingUtilities.invokeLater(() -> {
                    recordingManagerLabel.setText(recordingManagerProp);
                    recordingManagerLayout.show(recordingManagerPanel, label);
                });
            }
        } // end if ((device != null) && !device.isEmpty())
        SwingUtilities.invokeLater(() -> {
            updateOkButton(okButton, devices);
            deviceSelectionDialog.pack();
        });
    }

    public static String[] getDevices(Splash splash, String... devices) {
        Map<String, TimedData<String>> propertyMap = new ConcurrentHashMap<>();
        Map<String, TimedData<String[]>> deviceClassMap = new ConcurrentHashMap<>();
        long deltaTime = 30000;// 30000ms = 30s
        String fss = devices[0], recordingManager = devices[1];
        if (recordingManager.isEmpty() && !fss.isEmpty()) {
            // SCAN-925: get RecordingManager from device property
            System.out.println("try to recover " + RECORDING_MANAGER + " from fss properties...");
            updateFSSAndRecordingManager(propertyMap, deltaTime, devices);
            recordingManager = devices[1];
        }
        if (fss.isEmpty() || recordingManager.isEmpty()) {
            final String argRecordingManager = recordingManager;
            boolean oneFSS = !fss.isEmpty(), oneRecordingManager = !recordingManager.isEmpty();
            System.out.println("prepare device selection dialog...");
            // SCAN-925 & SCAN-957: Prepare and display device selection dialog
            String cancelled = "device selection cancelled";
            JDialog deviceSelectionDialog = new JDialog((JFrame) null, "Choose your device(s)", true);
            deviceSelectionDialog.setAlwaysOnTop(true);
            deviceSelectionDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            deviceSelectionDialog.setResizable(false);
            deviceSelectionDialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    System.out.println(cancelled);
                    System.exit(0);
                }
            });
            Insets buttonMargin = new Insets(2, 2, 2, 2);
            JButton okButton = new JButton("OK");
            okButton.setMargin(buttonMargin);
            okButton.addActionListener(e -> {
                deviceSelectionDialog.setVisible(false);
            });
            updateOkButton(okButton, devices);
            JButton cancelButton = new JButton("Cancel");
            cancelButton.setMargin(buttonMargin);
            cancelButton.addActionListener(e -> {
                System.out.println(cancelled);
                System.exit(0);
            });
            JComboBox<String> fssDevices = new JComboBox<>(), recordingManagerDevices = new JComboBox<>();
            Font basicFont = fssDevices.getFont().deriveFont(Font.PLAIN);
            fssDevices.setFont(basicFont);
            recordingManagerDevices.setFont(basicFont);
            JLabel fssLabel = new JLabel(), recordingManagerLabel = new JLabel();
            fssLabel.setFont(basicFont);
            recordingManagerLabel.setFont(basicFont);
            JLabel fssTitleLabel = new JLabel(FSS);
            JLabel recordingManagerTitleLabel = new JLabel(RECORDING_MANAGER);
            CardLayout fssLayout = new CardLayout(), recordingManagerLayout = new CardLayout();
            JPanel fssPanel = new JPanel(fssLayout), recordingManagerPanel = new JPanel(recordingManagerLayout);
            String combo = "combo", label = "label";
            fssPanel.add(fssDevices, combo);
            fssPanel.add(fssLabel, label);
            recordingManagerPanel.add(recordingManagerDevices, combo);
            recordingManagerPanel.add(recordingManagerLabel, label);
            ItemListener recordingManagerDevicesListener = e -> {
                SwingUtilities.invokeLater(() -> {
                    if (argRecordingManager.isEmpty()) {
                        String item = (String) recordingManagerDevices.getSelectedItem();
                        devices[1] = item == null ? ObjectUtils.EMPTY_STRING : item;
                        updateOkButton(okButton, devices);
                    }
                });
            };
            ItemListener fssDevicesListener = e -> {
                SwingUtilities.invokeLater(() -> {
                    String selected = (String) fssDevices.getSelectedItem();
                    if (argRecordingManager.isEmpty()) {
                        updatedRecordingManagerDevices(selected, propertyMap, deltaTime, okButton,
                                recordingManagerDevices, recordingManagerLabel, recordingManagerPanel,
                                recordingManagerLayout, label, combo, deviceSelectionDialog, devices);
                    } else {
                        devices[0] = selected;
                    }
                });
            };
            if (oneFSS) {
                fssLabel.setText(fss);
                fssLayout.show(fssPanel, label);
                fssPanel.revalidate();
                devices[0] = fss;
            } else {
                fssDevices.addItemListener(fssDevicesListener);
                String[] deviceList = updateDeviceClasses(FSS, deviceClassMap, deltaTime, fssDevices,
                        deviceSelectionDialog);
                if (deviceList.length == 1) {
                    devices[0] = deviceList[0];
                    fssLabel.setText(devices[0]);
                    fssLayout.show(fssPanel, label);
                    oneFSS = true;
                } else {
                    fssLayout.show(fssPanel, combo);
                    if (deviceList.length > 0) {
                        devices[0] = deviceList[0];
                    }
                }
                fssPanel.revalidate();
            }
            if (oneRecordingManager) {
                recordingManagerLabel.setText(devices[1]);
                recordingManagerLayout.show(recordingManagerPanel, label);
            } else {
                recordingManagerDevices.addItemListener(recordingManagerDevicesListener);
                // SCAN-925: Let user choose RecordingManager if there is more than 1 available.
                String[] deviceList = updateDeviceClasses(RECORDING_MANAGER, deviceClassMap, deltaTime,
                        recordingManagerDevices, deviceSelectionDialog);
                if (deviceList.length == 1) {
                    devices[1] = deviceList[0];
                    recordingManagerLabel.setText(devices[1]);
                    recordingManagerLayout.show(recordingManagerPanel, label);
                    oneRecordingManager = true;
                } else {
                    recordingManagerLayout.show(recordingManagerPanel, combo);
                    if (deviceList.length > 0) {
                        devices[1] = deviceList[0];
                    }
                }
                recordingManagerPanel.revalidate();
            }
            if ((!oneFSS) || (!oneRecordingManager)) {
                if (splash != null) {
                    splash.setVisible(false);
                }
                updateOkButton(okButton, devices);
                JPanel deviceSelectionPanel = new JPanel(new GridBagLayout());
                int x = 0, y = 0;
                GridBagConstraints constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x++, y, 0.5, 0);
                int gap = 5;
                constraints.insets = new Insets(gap, gap, 0, 0);
                deviceSelectionPanel.add(fssTitleLabel, constraints);
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 0.5, 0);
                constraints.insets = new Insets(gap, gap, 0, gap);
                deviceSelectionPanel.add(recordingManagerTitleLabel, constraints);
                x = 0;
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x++, y, 0.5, 0);
                constraints.insets = new Insets(0, gap, 0, 0);
                deviceSelectionPanel.add(fssPanel, constraints);
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 0.5, 0);
                constraints.insets = new Insets(0, gap, 0, gap);
                deviceSelectionPanel.add(recordingManagerPanel, constraints);
                x = 0;
                JPanel buttonPanel = new JPanel(new GridBagLayout());
                constraints = generateConstraints(GridBagConstraints.NONE, x++, 0, 0, 0);
                constraints.anchor = GridBagConstraints.CENTER;
                buttonPanel.add(okButton, constraints);
                constraints = new GridBagConstraints();
                constraints = generateConstraints(GridBagConstraints.NONE, x++, 0, 0, 0);
                constraints.fill = GridBagConstraints.NONE;
                constraints.anchor = GridBagConstraints.CENTER;
                constraints.insets = new Insets(0, gap * 2, 0, 0);
                buttonPanel.add(cancelButton, constraints);
                x = 0;
                constraints = new GridBagConstraints();
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 1, 0);
                constraints.gridwidth = GridBagConstraints.REMAINDER;
                constraints.insets = new Insets(gap, gap, gap, gap);
                deviceSelectionPanel.add(buttonPanel, constraints);
                deviceSelectionDialog.setContentPane(deviceSelectionPanel);
                deviceSelectionDialog.pack();
                deviceSelectionDialog.setLocationRelativeTo(null);
                System.out.println("display device selection dialog...");
                SwingUtilities.invokeLater(() -> {
                    deviceSelectionDialog.pack();
                    deviceSelectionDialog.setLocationRelativeTo(null);
                });
                deviceSelectionDialog.setVisible(true);
                if (splash != null) {
                    splash.setVisible(true);
                }
            }
        } // end if (fss.isEmpty() || recordingManager.isEmpty())
        propertyMap.clear();
        deviceClassMap.clear();
        return devices;
    }

}
