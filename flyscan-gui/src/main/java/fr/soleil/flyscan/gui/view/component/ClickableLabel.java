package fr.soleil.flyscan.gui.view.component;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * A {@link JLabel} that has a hovered and pressed status.
 * 
 * @author GIRARDOT
 */
public class ClickableLabel extends JLabel {

    private static final long serialVersionUID = 7422462342879032339L;

    private boolean pressed, hovered;

    public ClickableLabel() {
        super();
    }

    public ClickableLabel(String text) {
        super(text);
    }

    public ClickableLabel(Icon image) {
        super(image);
    }

    public ClickableLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public ClickableLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public ClickableLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    public boolean isPressed() {
        return pressed;
    }

    public void setPressed(boolean pressed) {
        this.pressed = pressed;
    }

    public boolean isHovered() {
        return hovered;
    }

    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

}
