package fr.soleil.flyscan.gui.view.ui;

import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreePath;

import fr.soleil.flyscan.gui.util.Utilities;

public class ConfigurationTreeUI extends BasicTreeUI {

    public ConfigurationTreeUI() {
        super();
    }

    @Override
    public Icon getExpandedIcon() {
        return Utilities.COLLAPSE_SMALL_ICON;
    }

    @Override
    public Icon getCollapsedIcon() {
        return Utilities.EXPAND_SMALL_ICON;
    }

    @Override
    protected void paintHorizontalLine(Graphics g, JComponent c, int y, int left, int right) {
        // do nothing
    }

    @Override
    protected void paintHorizontalPartOfLeg(Graphics g, Rectangle clipBounds, Insets insets, Rectangle bounds,
            TreePath path, int row, boolean isExpanded, boolean hasBeenExpanded, boolean isLeaf) {
        // do nothing
    }

    @Override
    protected void paintVerticalLine(Graphics g, JComponent c, int x, int top, int bottom) {
        // do nothing
    }

    @Override
    protected void paintVerticalPartOfLeg(Graphics g, Rectangle clipBounds, Insets insets, TreePath path) {
        // do nothing
    }

}
