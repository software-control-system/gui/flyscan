package fr.soleil.flyscan.gui.view.component;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.EventObject;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.listener.ICheckBoxListener;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.ICheckBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.flyscan.gui.util.Utilities;

/**
 * A {@link JCheckBox} that is only clickable in the icon zone
 * 
 * @author GIRARDOT
 */
public class FSCheckBox extends JPanel implements IEntryComponent, ICheckBox {

    private static final long serialVersionUID = -3373301967577693642L;

    protected final CheckBox checkBox;

    public FSCheckBox() {
        super(new BorderLayout());
        checkBox = new CheckBox();
        add(checkBox, BorderLayout.WEST);
    }

    @Override
    public boolean isSelected() {
        return checkBox.isSelected();
    }

    @Override
    public void setSelected(boolean selected) {
        checkBox.setSelected(selected);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        checkBox.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        checkBox.removeMediator(mediator);
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        checkBox.setCometeBackground(color);
        setBackground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeBackground() {
        return checkBox.getCometeBackground();
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        checkBox.setCometeForeground(color);
        setForeground(ColorTool.getColor(color));
    }

    @Override
    public CometeColor getCometeForeground() {
        return checkBox.getCometeForeground();
    }

    @Override
    public void setCometeFont(CometeFont font) {
        checkBox.setCometeFont(font);
        setFont(FontTool.getFont(font));
    }

    @Override
    public CometeFont getCometeFont() {
        return checkBox.getCometeFont();
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        checkBox.setHorizontalAlignment(halign);
    }

    @Override
    public int getHorizontalAlignment() {
        return checkBox.getHorizontalAlignment();
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
        checkBox.setPreferredSize(width, height);
    }

    @Override
    public boolean isEditingData() {
        return checkBox.isEditingData();
    }

    @Override
    public void setTitledBorder(String title) {
        checkBox.setTitledBorder(title);
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        checkBox.addMouseListener(listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        checkBox.removeMouseListener(listener);
    }

    @Override
    public void removeAllMouseListeners() {
        checkBox.removeAllMouseListeners();
    }

    @Override
    public void setEditable(boolean editable) {
        checkBox.setEditable(editable);
    }

    @Override
    public boolean isEditable() {
        return checkBox.isEditable();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled() && (checkBox == null || checkBox.isEnabled());
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (checkBox != null) {
            checkBox.setEnabled(enabled);
        }
        super.setEnabled(enabled);
    }

    @Override
    public void addCheckBoxListener(ICheckBoxListener listener) {
        checkBox.addCheckBoxListener(listener);
    }

    @Override
    public void removeCheckBoxListener(ICheckBoxListener listener) {
        checkBox.removeCheckBoxListener(listener);
    }

    @Override
    public void fireSelectedChanged(EventObject event) {
        checkBox.fireSelectedChanged(event);
    }

    @Override
    public void setFalseLabel(String falseLabel) {
        checkBox.setFalseLabel(falseLabel);
    }

    @Override
    public String getFalseLabel() {
        return checkBox.getFalseLabel();
    }

    @Override
    public void setTrueLabel(String trueLabel) {
        checkBox.setTrueLabel(trueLabel);
    }

    @Override
    public String getTrueLabel() {
        return checkBox.getTrueLabel();
    }

    public void addItemListener(ItemListener listener) {
        checkBox.addItemListener(listener);
    }

    public void removeItemListener(ItemListener listener) {
        checkBox.removeItemListener(listener);
    }

    @Override
    public void setToolTipText(String text) {
        super.setToolTipText(text);
        if (checkBox != null) {
            checkBox.setToolTipText(text);
        }
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        checkBox.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

    public void addActionListener(ActionListener listener) {
        checkBox.addActionListener(listener);
    }

    public void removeActionListener(ActionListener listener) {
        checkBox.removeActionListener(listener);
    }

    @Override
    public void resetToDefault(String defaultValue) {
        if (defaultValue != null) {
            boolean selected = Utilities.TRUE.equalsIgnoreCase(defaultValue.trim());
            if (selected != isSelected()) {
                setSelected(selected);
            }
        }
    }

}
