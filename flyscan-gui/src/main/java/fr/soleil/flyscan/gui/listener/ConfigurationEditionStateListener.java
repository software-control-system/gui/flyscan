package fr.soleil.flyscan.gui.listener;

import java.util.EventListener;

/**
 * Listener used to be aware if a configuration is being edited
 * 
 * @author guerre-giordano
 * 
 */
public interface ConfigurationEditionStateListener extends EventListener {

    void setConfigurationBeingEdited(boolean isBeingEdited);

}
