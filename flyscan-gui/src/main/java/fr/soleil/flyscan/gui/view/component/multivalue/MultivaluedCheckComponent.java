package fr.soleil.flyscan.gui.view.component.multivalue;

import java.util.Collection;
import java.util.EventObject;

import fr.soleil.comete.definition.listener.ICheckBoxListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.FSCheckBox;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;

public class MultivaluedCheckComponent extends AbstractMultivaluedComponent<FSCheckBox, Boolean>
        implements ICheckBoxListener {

    private static final long serialVersionUID = -1801558510536731038L;

    public MultivaluedCheckComponent(Collection<Boolean> values, int count, boolean mayThrowException)
            throws FSParsingException {
        super(Boolean.class, values, count, mayThrowException);
    }

    @Override
    protected void setValueToComponent(FSCheckBox component, Boolean value, Integer rank) {
        if (value != null) {
            component.setSelected(value.booleanValue());
        }
    }

    @Override
    protected void listenToComponent(FSCheckBox component) {
        component.addCheckBoxListener(this);
    }

    @Override
    protected void forgetComponent(FSCheckBox component) {
        component.removeCheckBoxListener(this);
    }

    @Override
    protected FSCheckBox createComponent() {
        return new FSCheckBox();
    }

    @Override
    protected Boolean getValue(FSCheckBox component) {
        return Boolean.valueOf(component.isSelected());
    }

    @Override
    protected String toString(Boolean value) {
        return ((value != null) && value.booleanValue() ? Utilities.TRUE : Utilities.FALSE);
    }

    @Override
    public void selectedChanged(EventObject event) {
        fireEditionStateChanged(true);
    }

    @Override
    protected Boolean parse(String value) {
        return Utilities.TRUE.equalsIgnoreCase(value.trim());
    }

}
