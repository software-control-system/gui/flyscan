package fr.soleil.flyscan.gui.view.renderer;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.JList;

import fr.soleil.comete.swing.util.DefaultValueListCellRenderer;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.lib.project.awt.ColorUtils;

public class ColoredSelectionListCellRenderer extends DefaultValueListCellRenderer {

    private static final long serialVersionUID = 1204174251476200993L;

    protected static final Color DEFAULT_SELECTION_BACKGROUND = new Color(230, 230, 255);
    private Color selectionBackground;
    protected boolean useDefaultFont;
    protected boolean useSelectionBackground;

    public ColoredSelectionListCellRenderer(JComponent comp) {
        super(comp);
        selectionBackground = DEFAULT_SELECTION_BACKGROUND;
        useDefaultFont = true;
        useSelectionBackground = true;
    }

    public Color getSelectionBackground() {
        return selectionBackground;
    }

    public void setSelectionBackground(Color bg) {
        this.selectionBackground = bg == null ? DEFAULT_SELECTION_BACKGROUND : bg;
    }

    @Override
    protected void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        if (useDefaultFont) {
            font = Utilities.BASIC_FONT;
        }
        if (isSelected && useSelectionBackground) {
            comp.setBackground(selectionBackground);
        }
        foreground = Math.round(ColorUtils.getRealGray(comp.getBackground())) < 128 ? Color.WHITE : Color.BLACK;
        doApplyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus, derive);
    }

    protected void doApplyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        super.applyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
    }

}
