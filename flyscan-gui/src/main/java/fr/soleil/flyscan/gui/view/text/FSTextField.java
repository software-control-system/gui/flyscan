package fr.soleil.flyscan.gui.view.text;

import javax.swing.JTextField;
import javax.swing.text.Document;

import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;

public class FSTextField extends JTextField implements IEntryComponent {

    private static final long serialVersionUID = 5110392700850014859L;

    public FSTextField() {
        super();
    }

    public FSTextField(String text) {
        super(text);
    }

    public FSTextField(int columns) {
        super(columns);
    }

    public FSTextField(String text, int columns) {
        super(text, columns);
    }

    public FSTextField(Document doc, String text, int columns) {
        super(doc, text, columns);
    }

    @Override
    public void resetToDefault(String defaultValue) throws FSParsingException {
        if (defaultValue != null) {
            setText(defaultValue);
        }
    }

}
