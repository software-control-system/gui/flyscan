package fr.soleil.flyscan.gui.data;

public enum ScanType {
    CONTINUOUS, ZIGZAG, STEP_BY_STEP
}
