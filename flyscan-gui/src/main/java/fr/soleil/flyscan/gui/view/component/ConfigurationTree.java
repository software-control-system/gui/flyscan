package fr.soleil.flyscan.gui.view.component;

import java.awt.Component;
import java.awt.Container;

import javax.swing.DefaultCellEditor;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import fr.soleil.flyscan.gui.listener.ConfigSelectionListener;
import fr.soleil.flyscan.gui.listener.ConfigurationTreeDynamicListener;
import fr.soleil.flyscan.gui.view.model.ConfigurationTreeModel;
import fr.soleil.flyscan.gui.view.renderer.ConfigurationTreeRenderer;
import fr.soleil.flyscan.gui.view.ui.ConfigurationTreeUI;
import fr.soleil.lib.project.swing.tree.ExpandableTree;

/**
 * An {@link ExpandableTree} to display available configurations.
 * 
 * @author GIRARDOT
 */
public class ConfigurationTree extends ExpandableTree {

    private static final long serialVersionUID = 6938184532191149226L;

    private ConfigurationTreeModelListener modelListener;

    public ConfigurationTree(ConfigSelectionListener listener) {
        super(new ConfigurationTreeModel());
        setUI(new ConfigurationTreeUI());
        setCellRenderer(new ConfigurationTreeRenderer());
        setRootVisible(false);
        ConfigurationTreeDynamicListener adapter = new ConfigurationTreeDynamicListener(listener);
        addMouseListener(adapter);
        addMouseMotionListener(adapter);
    }

    public void expandRoot() {
        ConfigurationTreeModel model = getModel();
        if (model != null) {
            DefaultMutableTreeNode root = model.getRoot();
            if (root != null) {
                TreePath path = new TreePath(root.getPath());
                if (!isExpanded(path)) {
                    expandPath(path);
                }
            }
        }
    }

    @Override
    public void updateUI() {
        setUI(new ConfigurationTreeUI());
        SwingUtilities.updateComponentTreeUI((Component) getCellRenderer());
        TreeCellEditor editor = getCellEditor();
        if (editor instanceof DefaultCellEditor) {
            SwingUtilities.updateComponentTreeUI(((DefaultCellEditor) editor).getComponent());
        } else if (editor instanceof Component) {
            SwingUtilities.updateComponentTreeUI((Component) editor);
        }
    }

    @Override
    public ConfigurationTreeModel getModel() {
        return (ConfigurationTreeModel) super.getModel();
    }

    @Override
    public void setModel(TreeModel newModel) {
        if (newModel instanceof ConfigurationTreeModel) {
            TreeModel previous = getModel();
            if ((previous != null) && (modelListener != null)) {
                previous.removeTreeModelListener(modelListener);
            }
            super.setModel(newModel);
            if (modelListener == null) {
                modelListener = new ConfigurationTreeModelListener();
            }
            newModel.addTreeModelListener(modelListener);
            expandRoot();
        }
    }

    @Override
    protected void setExpandedState(TreePath path, boolean state) {
        boolean changed = (!hasBeenExpanded(path)) || (state != isExpanded(path));
        super.setExpandedState(path, state);
        // Hack to update JPopupMenu best size, when this tree is in a JMenu.
        revalidate();
        Container parent = getParent();
        Container firstParent = parent;
        while (parent != null && !(parent instanceof JPopupMenu)) {
            parent = parent.getParent();
        }
        if (changed) {
            if (parent instanceof JPopupMenu) {
                JPopupMenu window = (JPopupMenu) parent;
                if (window.isShowing()) {
                    SwingUtilities.invokeLater(() -> {
                        window.pack();
                        window.repaint();
                    });
                }
            } else {
                parent = firstParent;
                while (parent != null && !(parent instanceof ConfigurationsPanel)) {
                    parent = parent.getParent();
                }
                if (parent instanceof ConfigurationsPanel) {
                    ((ConfigurationsPanel) parent).updateConfigTreeLater();
                }
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ConfigurationTreeModelListener implements TreeModelListener {

        public ConfigurationTreeModelListener() {
            super();
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {
            revalidate();
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {
            // Nothing to do
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            if (e != null) {
                TreePath path = e.getTreePath();
                if (path != null) {
                    TreePath parent = path.getParentPath();
                    // Expand parent path if it is root
                    if ((parent != null) && (parent.getParentPath() == null)) {
                        SwingUtilities.invokeLater(() -> {
                            setShowsRootHandles(getModel().hasFolders());
                            expandPath(parent);
                        });
                    }
                }
            }
        }

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
            // Nothing to do
        }

    }

}
