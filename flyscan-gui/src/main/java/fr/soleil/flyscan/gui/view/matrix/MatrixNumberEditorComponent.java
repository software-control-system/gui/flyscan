package fr.soleil.flyscan.gui.view.matrix;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import fr.soleil.comete.definition.listener.ISpinnerListener;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * Single line integers matrix editor
 * 
 * @author guerre-giordano
 */
public class MatrixNumberEditorComponent extends JPanel {

    private static final long serialVersionUID = -301304920733915981L;

    private final List<Spinner> spinners;
    private final EventListenerList listeners;
    private final boolean oldValues;

    /**
     * Constructor
     * 
     * @param values List<Integer>
     */
    public MatrixNumberEditorComponent(List<Long> values, boolean oldValues, String title, Long minValue) {
        super(new GridBagLayout());
        this.oldValues = oldValues;
        spinners = new ArrayList<>();
        listeners = new EventListenerList();

        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        for (int index = 0; index < values.size(); index++) {
            long l = values.get(index);
            JLabel label = new JSmoothLabel(title + (index + 1), JLabel.RIGHT);
            label.setFont(Utilities.BASIC_FONT);

            GridBagConstraints lblConstraints = new GridBagConstraints();
            lblConstraints.gridx = GridBagConstraints.RELATIVE;
            lblConstraints.gridy = 0;
            lblConstraints.anchor = GridBagConstraints.NORTHWEST;
            lblConstraints.fill = GridBagConstraints.VERTICAL;
            lblConstraints.insets = new Insets(5, 10, 5, 0);
            panel.add(label, lblConstraints);
            if (values.size() < 2) {
                label.setVisible(false);
            }

            Spinner sp = Utilities.createSpinner();
            if (minValue != null) {
                sp.setMinimum(minValue);
            }
            sp.setNumberValue(Long.valueOf(l));
            sp.addSpinnerListener(new ISpinnerListener() {
                @Override
                public void valueChanged(EventObject event) {
                    fireEditionStateChanged(true);
                }
            });
            spinners.add(sp);

            GridBagConstraints wsConstraints = new GridBagConstraints();
            wsConstraints.gridx = GridBagConstraints.RELATIVE;
            wsConstraints.gridy = 0;
            wsConstraints.anchor = GridBagConstraints.NORTHWEST;
            wsConstraints.fill = GridBagConstraints.VERTICAL;
            wsConstraints.insets = new Insets(5, 0, 5, 5);
            panel.add(sp, wsConstraints);
        }

        GridBagConstraints pnlConstraints = new GridBagConstraints();
        pnlConstraints.gridx = 0;
        pnlConstraints.gridy = 0;
        pnlConstraints.anchor = GridBagConstraints.NORTHWEST;
        pnlConstraints.fill = GridBagConstraints.VERTICAL;

        this.add(panel, pnlConstraints);
        Component glue = Box.createGlue();

        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.gridx = 1;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.gridwidth = GridBagConstraints.REMAINDER;

        this.add(glue, glueConstraints);
    }

    public List<Long> getValues() {
        List<Long> lValues = new ArrayList<>();
        for (Spinner sp : spinners) {
            lValues.add((Long) sp.getNumberValue());
        }
        return lValues;
    }

    public String getStringValue() {
        StringBuilder sb = new StringBuilder();
        if (!oldValues) {
            sb.append(ParsingUtil.BRACKET_OPENNING);
        }
        int last = spinners.size() - 1;
        int index = 0;
        for (Spinner sp : spinners) {
            if (oldValues) {
                sb.append(ParsingUtil.BRACKET_OPENNING);
            }
            sb.append(sp.getNumberValue().longValue());
            if (oldValues) {
                sb.append(ParsingUtil.BRACKET_CLOSING);
            } else if (index < last) {
                sb.append(ParsingUtil.COMMA);
            }
            index++;
        }
        if (!oldValues) {
            sb.append(ParsingUtil.BRACKET_CLOSING);
        }
        return sb.toString();
    }

    public void addBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.add(ConfigurationEditionStateListener.class, listener);
    }

    public void removeBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.remove(ConfigurationEditionStateListener.class, listener);
    }

    public ConfigurationEditionStateListener[] getBeingEditedListeners() {
        return listeners.getListeners(ConfigurationEditionStateListener.class);
    }

    protected void fireEditionStateChanged(boolean isBeingEdited) {
        for (ConfigurationEditionStateListener listener : getBeingEditedListeners()) {
            listener.setConfigurationBeingEdited(isBeingEdited);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (Spinner sp : spinners) {
            sp.setEnabled(enabled);
        }
    }

}
