package fr.soleil.flyscan.gui.view.component;

import java.awt.Dimension;

import javax.swing.Icon;

import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * A {@link JSmoothLabel} that exaggerates its preferred size
 * 
 * @author GIRARDOT
 */
public class FSLabel extends JSmoothLabel {

    private static final long serialVersionUID = 7378644125535811939L;

    public static final int MARGIN = 20;

    public FSLabel() {
        super();
    }

    public FSLabel(String text) {
        super(text);
    }

    public FSLabel(Icon image) {
        super(image);
    }

    public FSLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public FSLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public FSLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension prefSize;
        Dimension refSize = super.getPreferredSize();
        if (isPreferredSizeSet()) {
            prefSize = refSize;
        } else {
            prefSize = new Dimension(refSize.width + MARGIN, refSize.height);
        }
        return prefSize;
    }
}
