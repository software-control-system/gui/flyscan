package fr.soleil.flyscan.gui.view.component.multivalue;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import fr.soleil.comete.definition.event.WheelSwitchEvent;
import fr.soleil.comete.definition.listener.IWheelSwitchListener;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;

public class MultivaluedWheelSwitchComponent extends AbstractMultivaluedComponent<WheelSwitch, Number>
        implements IWheelSwitchListener, PropertyChangeListener {

    private static final long serialVersionUID = 1044097674577781281L;

    protected String format;

    public MultivaluedWheelSwitchComponent(Collection<? extends Number> values, int count, String format,
            boolean mayThrowException) throws FSParsingException {
        super(Number.class, values, count, mayThrowException, format);
        for (WheelSwitch ws : components.values()) {
            if (Format.isNumberFormatOK(format)) {
                ws.setFormat(format);
            }
        }
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        if ((!ObjectUtils.sameObject(format, this.format)) && Format.isNumberFormatOK(format)) {
            this.format = format;
            for (WheelSwitch ws : components.values()) {
                ws.setFormat(format);
            }
        }
    }

    @Override
    protected void initBeforeValues(Object... otherData) {
        this.format = (String) otherData[0];
    }

    @Override
    protected void setValueToComponent(WheelSwitch component, Number value, Integer rank) {
        if (value == null) {
            value = Double.valueOf(0.0);
        }
        component.setValue(value.doubleValue(), true, false);
    }

    @Override
    protected void listenToComponent(WheelSwitch component) {
        component.addWheelSwitchListener(this);
        component.addPropertyChangeListener(Utilities.FORMAT, this);
    }

    @Override
    protected void forgetComponent(WheelSwitch component) {
        component.removeWheelSwitchListener(this);
        component.removePropertyChangeListener(Utilities.FORMAT, this);
    }

    @Override
    protected WheelSwitch createComponent() {
        final WheelSwitch ws = Utilities.createWheelSwitch();
        if (Format.isNumberFormatOK(format)) {
            ws.setFormat(format);
        }
        return ws;
    }

    @Override
    protected Number getValue(WheelSwitch component) {
        return Double.valueOf(component.getValue());
    }

    @Override
    protected String toString(Number value) {
        return ParsingTool.toString(value);
    }

    @Override
    public void valueChange(WheelSwitchEvent evt) {
        fireEditionStateChanged(true);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ((evt != null) && (evt.getSource() instanceof WheelSwitch) && Utilities.FORMAT.equals(evt.getPropertyName())
                && (evt.getNewValue() instanceof String)) {
            setFormat((String) evt.getNewValue());
        }
    }

    @Override
    protected Number parse(String value) {
        return Double.valueOf(ParsingTool.parseDouble(value));
    }

}
