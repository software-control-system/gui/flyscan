package fr.soleil.flyscan.gui.view.combo;

import fr.soleil.comete.swing.ComboBox;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;

public class FSComboBox extends ComboBox implements IEntryComponent {

    private static final long serialVersionUID = -7084043827753077347L;

    public FSComboBox() {
        super();
    }

    @Override
    public void resetToDefault(String defaultDevice) {
        if (defaultDevice != null) {
            UIDelegate.applySelectedValue(this, defaultDevice);
        }
    }

}
