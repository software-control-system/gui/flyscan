package fr.soleil.flyscan.gui.view.dialog;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.lib.project.ObjectUtils;

public abstract class ANamedDataChooser extends JDialog {

    private static final long serialVersionUID = -299432188220454882L;

    protected static final String CFG_ERROR_MESSAGE = Utilities.getMessage("flyscan.name.cfg.reserved");
    protected static final String ERROR_MESSAGE_FORMAT = Utilities.getMessage("flyscan.name.used");
    protected static final Font ERROR_FONT = new Font(Font.DIALOG, Font.ITALIC | Font.BOLD, 10);
    protected static final int DEFAULT_MARGIN = 5, TITLE_GAP = 10, GAP = 30;
    protected static final String CFG_EXT = ".cfg";

    protected boolean validated;
    protected final JLabel nameLabel, errorLabel;
    protected final JTextField nameField;
    protected final JButton okButton, cancelButton;

    protected ANamedDataChooser(Window owner, String title) {
        super(owner, title, DEFAULT_MODALITY_TYPE);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
        validated = false;
        cancelButton = new JButton(Utilities.CANCEL);
        cancelButton.addActionListener(e -> {
            dispose();
        });
        okButton = new JButton(Utilities.OK);
        ActionListener okListener = e -> {
            validated = true;
            setVisible(false);
        };
        okButton.addActionListener(okListener);
        okButton.setPreferredSize(cancelButton.getPreferredSize());
        okButton.setEnabled(false);
        nameLabel = new JLabel(Utilities.NAME, SwingConstants.RIGHT);
        nameLabel.setFont(Utilities.TITLE_FONT);
        errorLabel = new JLabel(CFG_ERROR_MESSAGE);
        errorLabel.setForeground(Color.RED);
        errorLabel.setFont(ERROR_FONT);
        nameField = Utilities.createTextField(ObjectUtils.EMPTY_STRING, 20);
        nameField.setDocument(Utilities.createNameFilteredDocument());
        nameField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkName(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                checkName(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }
        });
        nameField.addActionListener(e -> {
            if (okButton.isEnabled()) {
                okListener.actionPerformed(e);
            }
        });

        setContentPane(new JPanel(new GridBagLayout()));
    }

    protected void checkName(DocumentEvent e) {
        String name = nameField.getText();
        if (name == null) {
            okButton.setEnabled(false);
            errorLabel.setText(ObjectUtils.EMPTY_STRING);
        } else {
            name = name.trim();
            boolean empty = name.isEmpty();
            boolean notFound = empty ? true : getExisting(name) == null;
            boolean cfg = name.toLowerCase().endsWith(CFG_EXT);
            okButton.setEnabled((!empty) && notFound && !cfg);
            if (empty) {
                errorLabel.setText(ObjectUtils.EMPTY_STRING);
            } else if (cfg) {
                errorLabel.setText(CFG_ERROR_MESSAGE);
            } else if (notFound) {
                errorLabel.setText(ObjectUtils.EMPTY_STRING);
            } else {
                errorLabel.setText(String.format(ERROR_MESSAGE_FORMAT, name));
            }
        }
    }

    protected abstract String getExisting(final String name);

    protected GridBagConstraints addNameComponents(JPanel mainPanel) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = new Insets(DEFAULT_MARGIN, DEFAULT_MARGIN, 0, TITLE_GAP);
        mainPanel.add(nameLabel, constraints);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.insets = new Insets(DEFAULT_MARGIN, 0, 0, DEFAULT_MARGIN);
        mainPanel.add(nameField, constraints);
        return constraints;
    }

    protected int addErrorLabel(JPanel mainPanel) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = Utilities.getNextY(mainPanel);
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.insets = new Insets(0, DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN);
        mainPanel.add(errorLabel, constraints);
        return constraints.gridy + 1;
    }

    protected void addButtons(JPanel mainPanel) {
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        int x = 0;
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = x++;
        constraints.gridy = 0;
        constraints.weightx = 0.5;
        constraints.weighty = 0;
        buttonPanel.add(Box.createGlue(), constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = x++;
        constraints.gridy = 0;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = new Insets(0, 0, 0, GAP);
        buttonPanel.add(okButton, constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = x++;
        constraints.gridy = 0;
        constraints.weightx = 0;
        constraints.weighty = 0;
        buttonPanel.add(cancelButton, constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = x++;
        constraints.gridy = 0;
        constraints.weightx = 0.5;
        constraints.weighty = 0;
        buttonPanel.add(Box.createGlue(), constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = Utilities.getNextY(mainPanel);
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.insets = new Insets(0, 5, 5, 5);
        mainPanel.add(buttonPanel, constraints);
    }

    @Override
    protected void finalize() {
        nameField.setText(ObjectUtils.EMPTY_STRING);
    }

}
