package fr.soleil.flyscan.gui.listener;

import java.util.EventListener;

import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;

/**
 * An {@link EventListener} that listens to the changes of a custom parameter.
 * 
 * @author GIRARDOT
 */
public interface CustomParameterListener extends EventListener {

    /**
     * Notifies this {@link CustomParameterListener} for the changes of a custom parameter.
     * 
     * @param entry The concerned custom parameter.
     */
    public void parameterChanged(Entry entry);

}
