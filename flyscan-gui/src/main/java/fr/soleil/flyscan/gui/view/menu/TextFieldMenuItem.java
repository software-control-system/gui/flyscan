package fr.soleil.flyscan.gui.view.menu;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link JMenuItem} that contains a {@link JTextField}.
 * 
 * @author guerre-giordano
 */
public class TextFieldMenuItem extends JMenuItem {

    private static final long serialVersionUID = -6649144364009190316L;

    private final JPanel panel;
    private final JTextField field;

    public TextFieldMenuItem() {
        super();
        panel = new JPanel(new BorderLayout());
        field = Utilities.createTextField(ObjectUtils.EMPTY_STRING, 10);
        field.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                field.requestFocusInWindow();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                field.requestFocusInWindow();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }
        });

        panel.add(field, BorderLayout.CENTER);
        panel.add(new JLabel(Utilities.SEARCH_ICON), BorderLayout.EAST);
        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        setBorder(null);
    }

    public String getFieldText() {
        String text = ObjectUtils.EMPTY_STRING;
        if (field != null) {
            text = field.getText();
        }
        return text;
    }

    @Override
    public Dimension getPreferredSize() {
        return panel.getPreferredSize();
    }

    public JTextField getTextFieldComponent() {
        return field;
    }
}
