package fr.soleil.flyscan.gui.view.scrollpane;

import java.awt.Component;

import javax.swing.JScrollPane;

import fr.soleil.flyscan.gui.FlyScanTangoBox;

/**
 * A {@link JScrollPane} that respects {@link FlyScanTangoBox#SCROLL_SPEED} as vertical scrollbar increment.
 * 
 * @author GIRARDOT
 */
public class FSScrollPane extends JScrollPane {

    private static final long serialVersionUID = -4206068613051718007L;

    /**
     * Creates an empty (no viewport view) {@link FSScrollPane} where both horizontal and vertical scrollbars
     * appear when needed.
     */
    public FSScrollPane() {
        super();
        init();
    }

    /**
     * Creates a {@link FSScrollPane} that displays the contents of the specified component, where both
     * horizontal and vertical scrollbars appear whenever the component's contents are larger than the view.
     * 
     * @param view the component to display in the scrollpane's viewport
     */
    public FSScrollPane(Component view) {
        super(view);
        init();
    }

    /**
     * Creates an empty (no viewport view) {@link FSScrollPane} with specified scrollbar policies. The available
     * policy settings are listed at #setVerticalScrollBarPolicy and #setHorizontalScrollBarPolicy.
     * 
     * @param vsbPolicy an integer that specifies the vertical scrollbar policy
     * @param hsbPolicy an integer that specifies the horizontal scrollbar policy
     * @see #setViewportView(Component)
     */
    public FSScrollPane(int vsbPolicy, int hsbPolicy) {
        super(vsbPolicy, hsbPolicy);
        init();
    }

    /**
     * Creates a {@link FSScrollPane} that displays the view component in a viewport whose view position can be
     * controlled with a pair of scrollbars. The scrollbar policies specify when the scrollbars are displayed, for
     * example, if <code>vsbPolicy</code> is <code>VERTICAL_SCROLLBAR_AS_NEEDED</code> then the vertical scrollbar only
     * appears if the view doesn't fit vertically. The available policy settings are listed at
     * #setVerticalScrollBarPolicy and #setHorizontalScrollBarPolicy.
     * 
     * @param view the component to display in the scrollpanes viewport
     * @param vsbPolicy an integer that specifies the vertical scrollbar policy
     * @param hsbPolicy an integer that specifies the horizontal scrollbar policy
     * @see #setViewportView(Component)
     */
    public FSScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
        super(view, vsbPolicy, hsbPolicy);
        init();
    }

    /**
     * Does some initializations.
     */
    protected void init() {
        getVerticalScrollBar().setUnitIncrement(FlyScanTangoBox.SCROLL_SPEED);
    }

}
