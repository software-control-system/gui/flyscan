package fr.soleil.flyscan.gui.view.tabbedpane;

import java.awt.Component;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JTabbedPane;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.TextTabComponent;
import fr.soleil.flyscan.gui.view.ui.FSTabbedPaneUI;

/**
 * A {@link JTabbedPane} that puts in bold selected tab
 * 
 * @author GIRARDOT
 */
public class BoldTitleTabbedPane extends JTabbedPane {

    private static final long serialVersionUID = 1676615405261586474L;

    public BoldTitleTabbedPane() {
        super();
        setTabLayoutPolicy(SCROLL_TAB_LAYOUT);
        addChangeListener();
    }

    public BoldTitleTabbedPane(int tabPlacement) {
        super(tabPlacement);
        setTabLayoutPolicy(SCROLL_TAB_LAYOUT);
        addChangeListener();
    }

    public BoldTitleTabbedPane(int tabPlacement, int tabLayoutPolicy) {
        super(tabPlacement, tabLayoutPolicy);
        setTabLayoutPolicy(SCROLL_TAB_LAYOUT);
        addChangeListener();
    }

    @Override
    public void updateUI() {
        setUI(new FSTabbedPaneUI());
    }

    @Override
    public void setTabComponentAt(int index, Component component) {
        // XXX RG: Ugly hack to avoid jgoodies-looks double title painting bug
        if (component instanceof TextTabComponent) {
            super.setTitleAt(index,
                    ((TextTabComponent) component).getTitle().replaceAll(Utilities.POINT, Utilities.SPACE));
        }
        super.setTabComponentAt(index, component);
        updateSelectedTab();
    }

    public void addTab(String title, Component component, Component tabComponent) {
        addTab(title, component);
        setTabComponentAt(indexOfComponent(component), tabComponent);
    }

    public void addTab(String title, Icon icon, Component component, String tip, Component tabComponent) {
        addTab(title, icon, component, tip);
        setTabComponentAt(indexOfComponent(component), tabComponent);
    }

    protected void updateSelectedTab() {
        int index = getSelectedIndex();
        for (int i = 0; i < getTabCount(); i++) {
            Component comp = getTabComponentAt(i);
            if (comp instanceof TextTabComponent) {
                TextTabComponent tab = (TextTabComponent) comp;
                if (i == index) {
                    tab.setTitleFont(tab.getTitleFont().deriveFont(Font.BOLD));
                } else {
                    tab.setTitleFont(null);
                }
            }
        }
    }

    protected void addChangeListener() {
        addChangeListener((e) -> {
            updateSelectedTab();
        });
    }

    public String getTabTitleAt(int index) {
        String title;
        if (index > -1) {
            TextTabComponent tab = getTextTabComponentAt(index);
            if (tab == null) {
                title = null;
            } else {
                title = tab.getTitle();
            }
            if (title == null) {
                title = getTitleAt(index);
            }
        } else {
            title = null;
        }
        return title;
    }

    public TextTabComponent getTextTabComponentAt(int index) {
        TextTabComponent tab = null;
        Component comp = getTabComponentAt(index);
        if (comp instanceof TextTabComponent) {
            tab = (TextTabComponent) comp;
        }
        return tab;
    }

    public int getTabIndexForTitle(String title) {
        int index = -1;
        for (int i = 0; i < getTabCount(); i++) {
            String tabTitle = getTabTitleAt(i);
            if (tabTitle.equalsIgnoreCase(title)) {
                index = i;
            }
        }
        return index;
    }

    public void remove(String title) {
        int index = getTabIndexForTitle(title);
        if (index > -1) {
            remove(index);
        }
    }

    public void setSelectedTab(String title) {
        int index = getTabIndexForTitle(title);
        if (index > -1) {
            setSelectedIndex(index);
        }
    }

    public Component getComponent(String title) {
        Component comp;
        int index = getTabIndexForTitle(title);
        if (index > -1) {
            comp = getComponentAt(index);
        } else {
            comp = null;
        }
        return comp;
    }

    @Override
    public void setTitleAt(int index, String title) {
        TextTabComponent tab = getTextTabComponentAt(index);
        if (tab == null) {
            super.setTitleAt(index, title);
        } else {
            tab.setTitle(title);
        }
    }

}
