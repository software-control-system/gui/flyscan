package fr.soleil.flyscan.gui.view.delegate;

import java.awt.CardLayout;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import fr.soleil.flyscan.gui.view.component.ExpertPanel;

/**
 * A class used to store prepared UI in {@link fr.soleil.flyscan.gui.view.ConfigurationGUI}
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class UIBuffer {

    private JPanel leftPanel, listPanel, sectionPanel, infoPanel, titlePanel;
    private ExpertPanel sectionExpertPanel;
    private CardLayout layout;
    private JScrollPane leftScrollPane;
    private JLabel nameLabel, typeLabel, enableLabel;
    private ButtonGroup group;
    private JRadioButton selector;
    private JComponent panel;

    public UIBuffer(JPanel leftPanel, JPanel listPanel, JPanel sectionPanel, ExpertPanel sectionExpertPanel,
            JPanel infoPanel, JPanel titlePanel, CardLayout layout, JScrollPane leftScrollPane, JLabel nameLabel,
            JLabel typeLabel, JLabel enableLabel, ButtonGroup group, JRadioButton selector, JComponent panel) {
        super();
        this.leftPanel = leftPanel;
        this.listPanel = listPanel;
        this.sectionPanel = sectionPanel;
        this.sectionExpertPanel = sectionExpertPanel;
        this.infoPanel = infoPanel;
        this.titlePanel = titlePanel;
        this.layout = layout;
        this.leftScrollPane = leftScrollPane;
        this.nameLabel = nameLabel;
        this.typeLabel = typeLabel;
        this.enableLabel = enableLabel;
        this.group = group;
        this.selector = selector;
        this.panel = panel;
    }

    public JPanel getLeftPanel() {
        return leftPanel;
    }

    public JPanel getListPanel() {
        return listPanel;
    }

    public JPanel getSectionPanel() {
        return sectionPanel;
    }

    public ExpertPanel getSectionExpertPanel() {
        return sectionExpertPanel;
    }

    public JPanel getInfoPanel() {
        return infoPanel;
    }

    public JPanel getTitlePanel() {
        return titlePanel;
    }

    public CardLayout getLayout() {
        return layout;
    }

    public JScrollPane getLeftScrollPane() {
        return leftScrollPane;
    }

    public JLabel getNameLabel() {
        return nameLabel;
    }

    public JLabel getTypeLabel() {
        return typeLabel;
    }

    public JLabel getEnableLabel() {
        return enableLabel;
    }

    public ButtonGroup getGroup() {
        return group;
    }

    public JRadioButton getSelector() {
        return selector;
    }

    public JComponent getPanel() {
        return panel;
    }

    @Override
    protected void finalize() {
        leftPanel = listPanel = sectionPanel = infoPanel = titlePanel = null;
        layout = null;
        leftScrollPane = null;
        nameLabel = typeLabel = enableLabel = null;
        group = null;
        selector = null;
        panel = null;
    }

}
