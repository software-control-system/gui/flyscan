package fr.soleil.flyscan.gui.view.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Icon;

import fr.soleil.lib.project.awt.SizeUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

public class TabLabel extends JSmoothLabel implements TextTabComponent {

    private static final long serialVersionUID = -5321053401026127998L;

    public TabLabel() {
        super();
    }

    public TabLabel(String text) {
        super(text);
    }

    public TabLabel(Icon image) {
        super(image);
    }

    public TabLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public TabLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public TabLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    @Override
    public void setTitle(String title) {
        setText(title);
    }

    @Override
    public String getTitle() {
        return getText();
    }

    @Override
    public Font getTitleFont() {
        return getFont();
    }

    @Override
    public void setTitleFont(Font font) {
        setFont(font);
    }

    @Override
    public Color getTitleForeground() {
        return getForeground();
    }

    @Override
    public void setTitleForeground(Color fg) {
        setForeground(fg);
    }

    @Override
    public Dimension getPreferredSize() {
        return SizeUtils.getAdaptedPreferredSize(this, super.getPreferredSize());
    }

}
