package fr.soleil.flyscan.gui.listener;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.util.EventObject;

import javax.swing.AbstractButton;
import javax.swing.border.Border;
import javax.swing.plaf.ButtonUI;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ui.ConfigMenuItemUI;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link MouseAdapter} that will update the display of a configuration menu item, to dynamically show it as
 * hovered or pressed.
 * 
 * @author GIRARDOT
 */
public class ConfigItemDynamicListener extends MouseAdapter implements ActionListener {

    private WeakReference<AbstractButton> lastButtonRef;

    public ConfigItemDynamicListener() {
        super();
    }

    protected void updateButton(AbstractButton button, Border border, Color bg) {
        if (button != null) {
            if (button.getBorder() != border) {
                button.setBorder(border);
            }
            if (bg == null) {
                bg = button.getBackground();
            }
            ButtonUI ui = button.getUI();
            if (ui instanceof ConfigMenuItemUI) {
                ConfigMenuItemUI currentConfigUI = (ConfigMenuItemUI) ui;
                if (!ObjectUtils.sameObject(bg, currentConfigUI.getSelectionBackground())) {
                    currentConfigUI.setSelectionBackground(bg);
                }
            }
            button.repaint();
        }
    }

    protected void cleanButton(AbstractButton button) {
        updateButton(button, Utilities.EMPTY_BORDER, null);
    }

    protected AbstractButton getButon(EventObject e) {
        AbstractButton button = null;
        if (e != null) {
            Object src = e.getSource();
            if (src instanceof AbstractButton) {
                button = (AbstractButton) src;
            }
        }
        return button;
    }

    protected void fullClean(EventObject e) {
        cleanButton(getButon(e));
        cleanButton(ObjectUtils.recoverObject(lastButtonRef));
        lastButtonRef = null;
    }

    protected void treetMouseMove(MouseEvent e) {
        // When mouse is over a button: display that button as hovered
        AbstractButton button = getButon(e);
        if (button == null) {
            lastButtonRef = null;
        } else {
            AbstractButton lastButton = ObjectUtils.recoverObject(lastButtonRef);
            if (lastButton != button) {
                cleanButton(lastButton);
                lastButtonRef = new WeakReference<>(button);
            }
            updateButton(button, Utilities.HOVERED_BORDER, Utilities.HOVERED_COLOR);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        treetMouseMove(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        treetMouseMove(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Mouse exited button: display button as not hovered
        fullClean(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        treetMouseMove(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Mouse pressed: display button as pressed
        updateButton(getButon(e), Utilities.PRESSED_BORDER, Utilities.PRESSED_COLOR);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Mouse released: display button as not pressed
        updateButton(getButon(e), Utilities.HOVERED_BORDER, Utilities.HOVERED_COLOR);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        fullClean(e);
    }

}
