package fr.soleil.flyscan.gui.event;

/**
 * Event fired when a configuration is being edited
 * 
 * @author guerre-giordano
 */
public class ConfigurationEvent {

    private boolean isBeingEdited;

    public ConfigurationEvent(boolean isBeingEdited) {
        super();
        this.isBeingEdited = isBeingEdited;
    }

    public boolean isBeingEdited() {
        return isBeingEdited;
    }

    public void setBeingEdited(boolean isBeingEdited) {
        this.isBeingEdited = isBeingEdited;
    }

}
