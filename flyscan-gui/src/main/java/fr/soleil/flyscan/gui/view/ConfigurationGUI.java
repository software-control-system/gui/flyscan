package fr.soleil.flyscan.gui.view;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.JTextComponent;

import fr.soleil.comete.definition.listener.ITextAreaListener;
import fr.soleil.comete.definition.util.CometeDefinitionUtils;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.flyscan.gui.listener.ButtonDecorationListener;
import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.combo.DeviceComboBox;
import fr.soleil.flyscan.gui.view.component.CustomParameterPanel;
import fr.soleil.flyscan.gui.view.component.ExpertPanel;
import fr.soleil.flyscan.gui.view.component.FSCheckBox;
import fr.soleil.flyscan.gui.view.component.FSLabel;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.gui.view.component.TabLabel;
import fr.soleil.flyscan.gui.view.component.multivalue.AbstractMultivaluedComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedSpinnerComponent;
import fr.soleil.flyscan.gui.view.component.multivalue.MultivaluedStringListComponent;
import fr.soleil.flyscan.gui.view.delegate.UIBuffer;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;
import fr.soleil.flyscan.gui.view.dialog.ActorChooser;
import fr.soleil.flyscan.gui.view.scrollpane.FSScrollPane;
import fr.soleil.flyscan.gui.view.scrollpane.FSVerticalScrollPane;
import fr.soleil.flyscan.gui.view.tabbedpane.BoldTitleTabbedPane;
import fr.soleil.flyscan.gui.view.tabbedpane.HidableTabbedPane;
import fr.soleil.flyscan.gui.view.text.CustomTextArea;
import fr.soleil.flyscan.gui.view.text.FSAdaptableTextArea;
import fr.soleil.flyscan.lib.model.constraints.ConstantConstraint;
import fr.soleil.flyscan.lib.model.constraints.Constraint;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.EntryContainer;
import fr.soleil.flyscan.lib.model.parsing.configuration.FakeEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.ParameterEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.PredefinedEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.TrajectoryEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Hook;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.HookAction;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Monitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.PostScanAction;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Sensor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.TimeBase;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.ErrorStrategy;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.behavior.OrderStrategy;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.AcquisitionSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.ActorSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.EasyConfigSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.RecordingSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TrajectoryMode;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.plugin.Parameter;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.plugin.PluginType;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.border.RoundedBorder;
import fr.soleil.lib.project.swing.text.FilteredCharactersDocument;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;

/**
 * Panel displaying a single configuration
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public class ConfigurationGUI extends JPanel {

    private static final long serialVersionUID = 4565760220344243314L;

    private static final String NO_DESCRIPTION_AVAILABLE = "No description available";
    private static final String RECORDING_ENTRIES_MUTUALLY_EXCLUSIVE_ERROR = Utilities
            .getMessage("flyscan.error.section.recording.entries.exclusive");
    private static final String DESCRIPTION = Utilities.getMessage("flyscan.description");
    private static final String CONSTRAINTS_UNSATISFIED = Utilities.getMessage("flyscan.error.constraints.unsatisfied");
    private static final String SECTION = Utilities.getMessage("flyscan.section");
    private static final String ADD_MESSAGE = Utilities.getMessage("flyscan.actor.add.text");
    private static final String REMOVE_MESSAGE = Utilities.getMessage("flyscan.actor.remove.text");
    private static final String REMOVE_POPUP_TITLE = Utilities.getMessage("flyscan.actor.remove.confirmation.title");
    private static final String REMOVE_POPUP_MESSAGE = Utilities
            .getMessage("flyscan.actor.remove.confirmation.message");
    private static final String WARNING = Utilities.getMessage("flyscan.warning");
    private static final String ACTOR_NAME_CHANGE_REFUSED = Utilities.getMessage("flyscan.actor.name.change.refused");

    private static final Dimension SPLITPANE_MINIMUM_SIZE = new Dimension(10, 10);

    private final EventListenerList listeners = new EventListenerList();

    private final Configuration configuration;

    private final JPanel errorPanel;
    private final HidableTabbedPane tabbedPane;
    private JComponent actuatorPanel, sensorPanel, timebasePanel, hookPanel, monitorPanel;
    private JComponent generalPanel, easyConfigPanel;
    private JPanel actuatorListPanel, sensorListPanel, timebaseListPanel, hookListPanel, monitorListPanel;
    private JPanel generalListPanel, easyConfigListPanel;
    private JPanel actuatorInfoPanel, sensorInfoPanel, timebaseInfoPanel, hookInfoPanel, monitorInfoPanel;
    private JPanel generalInfoPanel, easyConfigInfoPanel;
    private JPanel actuatorSectionPanel, sensorSectionPanel, timebaseSectionPanel, hookSectionPanel;
    private JPanel monitorSectionPanel, easyConfigSectionPanel;
    private ExpertPanel generalExpertPanel, easyConfigExpertPanel;
    private ExpertPanel actuatorSectionExpertPanel, sensorSectionExpertPanel, timebaseSectionExpertPanel;
    private ExpertPanel hookSectionExpertPanel, monitorSectionExpertPanel;
    private CardLayout actuatorLayout, sensorLayout, timebaseLayout, hookLayout, monitorLayout;
    private CardLayout generalLayout, easyConfigLayout;
    private JLabel actuatorNameLabel, sensorNameLabel, timebaseNameLabel, hookNameLabel, monitorNameLabel;
    private JLabel generalNameLabel, easyConfigNameLabel;
    private JLabel actuatorTypeLabel, sensorTypeLabel, timebaseTypeLabel, hookTypeLabel, monitorTypeLabel;
    private JLabel actuatorEnableLabel, sensorEnableLabel, timebaseEnableLabel, hookEnableLabel, monitorEnableLabel;
    private JLabel easyConfigEnableLabel;
    private JRadioButton actuatorSelector, sensorSelector, timebaseSelector, hookSelector, monitorSelector;
    private JRadioButton easyConfigSelector;
    private ButtonGroup actuatorGroup, sensorGroup, timebaseGroup, hookGroup, monitorGroup;
    private ButtonGroup generalGroup, easyConfigGroup;
    private final Map<Component, JTextComponent> errorsMap;
    private final Map<JComponent, JLabel> recordingComponents;
    private final Map<FSParsingException, JLabel> errorLabels;
    private final Map<FSParsingException, JComponent> errorComponents;
    private final Map<JRadioButton, FSObject> buttonObjectMap;
    // SCAN-967: don't use FSObject as key. Prefer a String instead.
    private final Map<String, JComponent[]> objectComponentsMap;
    private final Collection<IdManager> idManagers;
    private ActorSection actorSection;
    private final boolean expert, enabled;
    private final boolean[] actuatorAdded, sensorAdded, timebaseAdded, hookAdded, monitorAdded;
    private final boolean[] generalAdded, easyConfigAdded;

    /**
     * Constructor
     * 
     * @param configuration Configuration
     * @param plugins Collection<Plugin>
     * @param categories Map<PluginType, Collection<Plugin>> (plugins split by categories)
     * @param errorPanel JPanel
     * @param expert boolean
     * @param enabled boolean
     * @param easyConfigOnly boolean
     */
    public ConfigurationGUI(final Configuration configuration, final Collection<Plugin> plugins,
            final Map<PluginType, Collection<Plugin>> categories, final JPanel errorPanel, final boolean expert,
            final boolean enabled, final boolean easyConfigOnly) {
        super(new BorderLayout());
        errorsMap = new HashMap<>();
        recordingComponents = new HashMap<>();
        errorLabels = new WeakHashMap<>();
        errorComponents = new WeakHashMap<>();
        buttonObjectMap = new WeakHashMap<>();
        objectComponentsMap = new WeakHashMap<>();
        idManagers = Collections.newSetFromMap(new ConcurrentHashMap<>());
        actuatorAdded = new boolean[1];
        sensorAdded = new boolean[1];
        timebaseAdded = new boolean[1];
        hookAdded = new boolean[1];
        monitorAdded = new boolean[1];
        generalAdded = new boolean[1];
        easyConfigAdded = new boolean[1];
        this.configuration = configuration;
        this.enabled = enabled;
        this.errorPanel = errorPanel;
        this.expert = expert;
        tabbedPane = new HidableTabbedPane();
//        tabbedPane = new HidableTabbedPane(SwingConstants.LEFT);
        tabbedPane.addChangeListener(e -> {
            int selected = tabbedPane.getSelectedIndex();
            for (int i = 0; i < tabbedPane.getTabCount(); i++) {
                Component comp = tabbedPane.getTabComponentAt(i);
                if (comp != null) {
                    comp.setFont(i == selected ? Utilities.TITLE_FONT : Utilities.BASIC_FONT);
                }
            }
            tabbedPane.repaint();
        });

        add(tabbedPane, BorderLayout.CENTER);

        boolean shouldListen = false;
        actorSection = null;
        if (configuration != null) {
            if (easyConfigPanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.EASY_CONFIG, null, categories, SECTION,
                        Utilities.EASY_CONFIG_ICON, null, false, false, true, false, easyConfigAdded);
                easyConfigListPanel = buffer.getListPanel();
                easyConfigSectionPanel = buffer.getSectionPanel();
                easyConfigExpertPanel = buffer.getSectionExpertPanel();
                easyConfigLayout = buffer.getLayout();
                easyConfigInfoPanel = buffer.getInfoPanel();
                easyConfigNameLabel = buffer.getNameLabel();
                easyConfigEnableLabel = buffer.getEnableLabel();
                easyConfigGroup = buffer.getGroup();
                easyConfigSelector = buffer.getSelector();
                easyConfigPanel = buffer.getPanel();
                shouldListen = true;
            }
            if (generalPanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.GENERAL, null, categories, SECTION,
                        Utilities.GENERAL_ICON, null, false, false, false, false, generalAdded);
                generalListPanel = buffer.getListPanel();
                generalLayout = buffer.getLayout();
                generalInfoPanel = buffer.getInfoPanel();
                generalNameLabel = buffer.getNameLabel();
                generalGroup = buffer.getGroup();
                generalPanel = buffer.getPanel();
                generalExpertPanel = buffer.getSectionExpertPanel();
                shouldListen = true;
            }
            if (actuatorPanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.ACTUATORS, PluginType.ACTUATOR, categories,
                        Utilities.NAME, Utilities.ACTUATOR_ICON, Utilities.ACTUATOR_KEY, easyConfigOnly, true, true,
                        true, actuatorAdded);
                actuatorListPanel = buffer.getListPanel();
                actuatorSectionPanel = buffer.getSectionPanel();
                actuatorSectionExpertPanel = buffer.getSectionExpertPanel();
                actuatorLayout = buffer.getLayout();
                actuatorInfoPanel = buffer.getInfoPanel();
                actuatorNameLabel = buffer.getNameLabel();
                actuatorTypeLabel = buffer.getTypeLabel();
                actuatorEnableLabel = buffer.getEnableLabel();
                actuatorGroup = buffer.getGroup();
                actuatorSelector = buffer.getSelector();
                actuatorPanel = buffer.getPanel();
                shouldListen = true;
            }
            if (sensorPanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.SENSORS, PluginType.SENSOR, categories,
                        Utilities.NAME, Utilities.SENSOR_ICON, Utilities.SENSOR_KEY, easyConfigOnly, true, true, true,
                        sensorAdded);
                sensorListPanel = buffer.getListPanel();
                sensorSectionPanel = buffer.getSectionPanel();
                sensorSectionExpertPanel = buffer.getSectionExpertPanel();
                sensorLayout = buffer.getLayout();
                sensorInfoPanel = buffer.getInfoPanel();
                sensorNameLabel = buffer.getNameLabel();
                sensorTypeLabel = buffer.getTypeLabel();
                sensorEnableLabel = buffer.getEnableLabel();
                sensorGroup = buffer.getGroup();
                sensorSelector = buffer.getSelector();
                sensorPanel = buffer.getPanel();
                shouldListen = true;
            }
            if (timebasePanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.TIMEBASE, PluginType.TIMEBASE, categories,
                        Utilities.NAME, Utilities.TIMEBASE_ICON, Utilities.TIMEBASE_KEY, easyConfigOnly, true, true,
                        true, timebaseAdded);
                timebaseListPanel = buffer.getListPanel();
                timebaseSectionPanel = buffer.getSectionPanel();
                timebaseSectionExpertPanel = buffer.getSectionExpertPanel();
                timebaseLayout = buffer.getLayout();
                timebaseInfoPanel = buffer.getInfoPanel();
                timebaseNameLabel = buffer.getNameLabel();
                timebaseTypeLabel = buffer.getTypeLabel();
                timebaseEnableLabel = buffer.getEnableLabel();
                timebaseGroup = buffer.getGroup();
                timebaseSelector = buffer.getSelector();
                timebasePanel = buffer.getPanel();
                shouldListen = true;
            }
            if (hookPanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.HOOKS, PluginType.HOOK, categories,
                        Utilities.NAME, Utilities.HOOK_ICON, Utilities.HOOK_KEY, easyConfigOnly, true, true, true,
                        hookAdded);
                hookListPanel = buffer.getListPanel();
                hookSectionPanel = buffer.getSectionPanel();
                hookSectionExpertPanel = buffer.getSectionExpertPanel();
                hookLayout = buffer.getLayout();
                hookInfoPanel = buffer.getInfoPanel();
                hookNameLabel = buffer.getNameLabel();
                hookTypeLabel = buffer.getTypeLabel();
                hookEnableLabel = buffer.getEnableLabel();
                hookGroup = buffer.getGroup();
                hookSelector = buffer.getSelector();
                hookPanel = buffer.getPanel();
                shouldListen = true;
            }
            if (monitorPanel == null) {
                UIBuffer buffer = initPanels(configuration, Utilities.MONITORS, PluginType.MONITOR, categories,
                        Utilities.NAME, Utilities.MONITOR_ICON, Utilities.MONITOR_KEY, easyConfigOnly, true, true, true,
                        monitorAdded);
                monitorListPanel = buffer.getListPanel();
                monitorSectionPanel = buffer.getSectionPanel();
                monitorSectionExpertPanel = buffer.getSectionExpertPanel();
                monitorLayout = buffer.getLayout();
                monitorInfoPanel = buffer.getInfoPanel();
                monitorNameLabel = buffer.getNameLabel();
                monitorTypeLabel = buffer.getTypeLabel();
                monitorEnableLabel = buffer.getEnableLabel();
                monitorGroup = buffer.getGroup();
                monitorSelector = buffer.getSelector();
                monitorPanel = buffer.getPanel();
                shouldListen = true;
            }
            if (shouldListen) {
                shouldListen = false;
                // Name
                UIDelegate.prepareEntryComponents(actuatorNameLabel, sensorNameLabel, timebaseNameLabel, hookNameLabel,
                        monitorNameLabel);
                UIDelegate.prepareEntryComponents(actuatorNameLabel, sensorNameLabel, timebaseNameLabel, hookNameLabel,
                        monitorNameLabel);
                UIDelegate.prepareEntryComponents(sensorNameLabel, actuatorNameLabel, timebaseNameLabel, hookNameLabel,
                        monitorNameLabel);
                UIDelegate.prepareEntryComponents(timebaseNameLabel, actuatorNameLabel, sensorNameLabel, hookNameLabel,
                        monitorNameLabel);
                UIDelegate.prepareEntryComponents(hookNameLabel, actuatorNameLabel, sensorNameLabel, timebaseNameLabel,
                        monitorNameLabel);
                UIDelegate.prepareEntryComponents(monitorNameLabel, actuatorNameLabel, sensorNameLabel,
                        timebaseNameLabel, hookNameLabel);
                PropertyChangeListener listener = e -> {
                    updateGeneralAndEasyConfigLabels();
                };
                actuatorNameLabel.addPropertyChangeListener(Utilities.PREFERRED_SIZE, listener);
                actuatorTypeLabel.addPropertyChangeListener(Utilities.PREFERRED_SIZE, listener);
                // Type
                UIDelegate.prepareEntryComponents(actuatorTypeLabel, sensorTypeLabel, timebaseTypeLabel, hookTypeLabel,
                        monitorTypeLabel);
                UIDelegate.prepareEntryComponents(actuatorTypeLabel, sensorTypeLabel, timebaseTypeLabel, hookTypeLabel,
                        monitorTypeLabel);
                UIDelegate.prepareEntryComponents(sensorTypeLabel, actuatorTypeLabel, timebaseTypeLabel, hookTypeLabel,
                        monitorTypeLabel);
                UIDelegate.prepareEntryComponents(timebaseTypeLabel, actuatorTypeLabel, sensorTypeLabel, hookTypeLabel,
                        monitorTypeLabel);
                UIDelegate.prepareEntryComponents(hookTypeLabel, actuatorTypeLabel, sensorTypeLabel, timebaseTypeLabel,
                        monitorTypeLabel);
                UIDelegate.prepareEntryComponents(monitorTypeLabel, actuatorTypeLabel, sensorTypeLabel,
                        timebaseTypeLabel, hookTypeLabel);
                // Enable
                UIDelegate.prepareEntryComponents(actuatorEnableLabel, sensorEnableLabel, timebaseEnableLabel,
                        hookEnableLabel, monitorEnableLabel, easyConfigEnableLabel);
                UIDelegate.prepareEntryComponents(actuatorEnableLabel, sensorEnableLabel, timebaseEnableLabel,
                        hookEnableLabel, monitorEnableLabel, easyConfigEnableLabel);
                UIDelegate.prepareEntryComponents(sensorEnableLabel, actuatorEnableLabel, timebaseEnableLabel,
                        hookEnableLabel, monitorEnableLabel, easyConfigEnableLabel);
                UIDelegate.prepareEntryComponents(timebaseEnableLabel, actuatorEnableLabel, sensorEnableLabel,
                        hookEnableLabel, monitorEnableLabel, easyConfigEnableLabel);
                UIDelegate.prepareEntryComponents(hookEnableLabel, actuatorEnableLabel, sensorEnableLabel,
                        timebaseEnableLabel, monitorEnableLabel, easyConfigEnableLabel);
                UIDelegate.prepareEntryComponents(monitorEnableLabel, actuatorEnableLabel, sensorEnableLabel,
                        timebaseEnableLabel, hookEnableLabel, easyConfigEnableLabel);
                UIDelegate.prepareEntryComponents(easyConfigEnableLabel, actuatorEnableLabel, sensorEnableLabel,
                        timebaseEnableLabel, hookEnableLabel, monitorEnableLabel);
            }

            Collection<Plugin> usedPlugins;

            boolean[] added;
            JLabel nameLabel, typeLabel, enableLabel;
            JComponent nameComponent, typeComponent, enableComponent;
            JComponent topGlue, glue1, glue2, glue3, bottomGlue;
            JRadioButton selector, sectionSelector;
            Object[] editorAndException;
            ButtonGroup group;
            String title;
            JPanel objectPanel, sectionPanel, listPanel, infoPanel;
            ExpertPanel objectExpertPanel, sectionExpertPanel;
            JScrollPane objectPanelScrollPane;
            CardLayout layout;
            for (final Section section : configuration.getSections()) {
                if (section instanceof ActorSection) {
                    actorSection = (ActorSection) section;
                }
                nameComponent = typeComponent = enableComponent = null;
                nameLabel = typeLabel = enableLabel = null;
                topGlue = glue1 = glue2 = glue3 = bottomGlue = null;
                usedPlugins = plugins;
                objectPanel = sectionPanel = listPanel = infoPanel = null;
                objectExpertPanel = sectionExpertPanel = null;
                objectPanelScrollPane = null;
                selector = sectionSelector = null;
                layout = null;
                List<Entry> entries = section.getEntries();
                Collection<FSObject> objects = section.getObjects();
                if ((!entries.isEmpty()) || (!objects.isEmpty())) {
                    if (section instanceof ActorSection) {
                        for (final FSObject fsObject : objects) {
                            if (fsObject instanceof Actuator) {
                                group = actuatorGroup;
                                sectionSelector = actuatorSelector;
                                sectionPanel = actuatorSectionPanel;
                                sectionExpertPanel = actuatorSectionExpertPanel;
                                listPanel = actuatorListPanel;
                                infoPanel = actuatorInfoPanel;
                                nameLabel = actuatorNameLabel;
                                typeLabel = actuatorTypeLabel;
                                enableLabel = actuatorEnableLabel;
                                layout = actuatorLayout;
                                added = actuatorAdded;
                                title = Utilities.ACTUATORS;
                            } else if (fsObject instanceof Sensor) {
                                group = sensorGroup;
                                sectionSelector = sensorSelector;
                                sectionPanel = sensorSectionPanel;
                                sectionExpertPanel = sensorSectionExpertPanel;
                                listPanel = sensorListPanel;
                                infoPanel = sensorInfoPanel;
                                nameLabel = sensorNameLabel;
                                typeLabel = sensorTypeLabel;
                                enableLabel = sensorEnableLabel;
                                layout = sensorLayout;
                                added = sensorAdded;
                                title = Utilities.SENSORS;
                            } else if (fsObject instanceof TimeBase) {
                                group = timebaseGroup;
                                sectionSelector = timebaseSelector;
                                sectionPanel = timebaseSectionPanel;
                                sectionExpertPanel = timebaseSectionExpertPanel;
                                listPanel = timebaseListPanel;
                                infoPanel = timebaseInfoPanel;
                                nameLabel = timebaseNameLabel;
                                typeLabel = timebaseTypeLabel;
                                enableLabel = timebaseEnableLabel;
                                layout = timebaseLayout;
                                added = timebaseAdded;
                                title = Utilities.TIMEBASE;
                            } else if (fsObject instanceof Hook) {
                                group = hookGroup;
                                sectionSelector = hookSelector;
                                sectionPanel = hookSectionPanel;
                                sectionExpertPanel = hookSectionExpertPanel;
                                listPanel = hookListPanel;
                                infoPanel = hookInfoPanel;
                                nameLabel = hookNameLabel;
                                typeLabel = hookTypeLabel;
                                enableLabel = hookEnableLabel;
                                layout = hookLayout;
                                added = hookAdded;
                                title = Utilities.HOOKS;
                            } else if (fsObject instanceof Monitor) {
                                group = monitorGroup;
                                sectionSelector = monitorSelector;
                                sectionPanel = monitorSectionPanel;
                                sectionExpertPanel = monitorSectionExpertPanel;
                                listPanel = monitorListPanel;
                                infoPanel = monitorInfoPanel;
                                nameLabel = monitorNameLabel;
                                typeLabel = monitorTypeLabel;
                                enableLabel = monitorEnableLabel;
                                layout = monitorLayout;
                                added = monitorAdded;
                                title = Utilities.MONITORS;
                            } else {
                                // unknown case
                                group = null;
                                sectionSelector = null;
                                sectionPanel = listPanel = infoPanel = null;
                                sectionExpertPanel = null;
                                nameLabel = typeLabel = enableLabel = null;
                                layout = null;
                                added = null;
                                title = null;
                            }
                            nameComponent = typeComponent = enableComponent = null;
                            objectExpertPanel = buildObjectPanel(title, added, fsObject, section, usedPlugins, group,
                                    infoPanel, listPanel, sectionPanel, sectionExpertPanel, layout, sectionSelector,
                                    nameLabel, typeLabel, enableLabel);
                        } // end for (final FSObject fsObject : section.getObjects())
                    } else { // end if (section instanceof ActorSection)
                        topGlue = UIDelegate.createGlue(false, UIDelegate.HALF_GAP);
                        glue1 = UIDelegate.createGlue(true, UIDelegate.GAP);
                        glue2 = UIDelegate.createGlue(true, UIDelegate.GAP);
                        glue3 = UIDelegate.createGlue(true, UIDelegate.GAP);
                        bottomGlue = UIDelegate.createGlue(false, UIDelegate.HALF_GAP);
                        boolean easy, recording;
                        String tabTitle;
                        if (section instanceof EasyConfigSection) {
                            // Jira SCAN-577: EasyConfigProp section is in fact a plugin for EasyConfig.
                            // This plugin was registered directly in EasyConfigSection
                            easy = true;
                            recording = false;
                            usedPlugins = ((EasyConfigSection) section).getPlugins();
                            if (usedPlugins == null) {
                                usedPlugins = new ArrayList<>();
                            }
                            tabTitle = Utilities.EASY_CONFIG;
                            group = easyConfigGroup;
                            sectionSelector = easyConfigSelector;
                            sectionPanel = easyConfigSectionPanel;
                            sectionExpertPanel = easyConfigExpertPanel;
                            listPanel = easyConfigListPanel;
                            infoPanel = easyConfigInfoPanel;
                            nameLabel = easyConfigNameLabel;
                            typeLabel = null;
                            enableLabel = easyConfigEnableLabel;
                            layout = easyConfigLayout;
                            added = easyConfigAdded;
                            infoPanel.add(new FSScrollPane(sectionPanel), section.getName());
                        } else {
                            tabTitle = Utilities.GENERAL;
                            easy = false;
                            recording = (section instanceof RecordingSection);
                            group = generalGroup;
                            sectionSelector = null;
                            sectionPanel = null;
                            sectionExpertPanel = generalExpertPanel;
                            listPanel = generalListPanel;
                            infoPanel = generalInfoPanel;
                            nameLabel = generalNameLabel;
                            typeLabel = null;
                            enableLabel = null;
                            layout = generalLayout;
                            added = generalAdded;
                        }
                        boolean levelRestriction;
                        CustomParameterPanel customParametersPanel;
                        Plugin plugin;
                        selector = null;
                        if (easy) {
                            // Jira SCAN-577: EasyProp section is in fact a plugin for EasyConfig.
                            // This plugin was registered directly in EasyConfigSection
                            if (usedPlugins.isEmpty()) {
                                plugin = null;
                            } else {
                                plugin = ObjectUtils.getFirstElement(usedPlugins);
                            }
                            sectionPanel.setBorder(null);
                            customParametersPanel = UIDelegate.buildCustomParametersPanel(true, tabTitle, section,
                                    sectionSelector, enabled, configuration, this);
                            levelRestriction = false;
                        } else {
                            plugin = null;
                            customParametersPanel = null;
                            levelRestriction = true;
                        }
                        if ((!objects.isEmpty()) || (!entries.isEmpty())) {
                            if (sectionPanel == null) {
                                sectionPanel = new JPanel(new GridBagLayout());
                                sectionSelector = createSelector(group);
                                if (infoPanel != null) {
                                    infoPanel.add(new FSScrollPane(sectionPanel), section.getName());
                                }
                            }
                            for (final Entry entry : entries) {
                                final Map<JComponent, JLabel> componentsPair = recording ? new HashMap<>() : null;
                                editorAndException = new Object[2];
                                buildEntryGUI(configuration.getName(), objectPanel, objectExpertPanel,
                                        objectPanelScrollPane, sectionPanel, sectionExpertPanel, selector,
                                        sectionSelector, section, null, plugin, entry, tabTitle, customParametersPanel,
                                        levelRestriction, componentsPair, editorAndException);
                                if (ParsingUtil.TYPE.equalsIgnoreCase(entry.getKey())) {
                                    if (editorAndException[0] instanceof JComponent) {
                                        typeComponent = (JComponent) editorAndException[0];
                                    }
                                } else if (ParsingUtil.ENABLE.equalsIgnoreCase(entry.getKey())) {
                                    if (editorAndException[0] instanceof JComponent) {
                                        enableComponent = (JComponent) editorAndException[0];
                                    }
                                }
                                if (editorAndException[1] instanceof FSParsingException) {
                                    FSParsingException e = (FSParsingException) editorAndException[1];
                                    Utilities.LOGGER.error(e.getMessage());
                                    UIDelegate.displayError(entry, section, null, errorLabels.get(e),
                                            errorComponents.get(e), sectionSelector, e.getMessage(), tabTitle,
                                            errorPanel, errorsMap, this);
                                }
                            }
                            if (nameComponent == null) {
                                nameComponent = Utilities
                                        .createTextField(easy ? Utilities.EASY_CONFIG : section.getName());
                                nameComponent.setEnabled(false);
                            }
                            if (recording) {
                                int nbEntriesEdited = UIDelegate.getNbRecordingNumericEntriesDifferentOfZero(section);
                                if (nbEntriesEdited > 1 && !(entries.isEmpty()) && !(recordingComponents.isEmpty())) {
                                    for (java.util.Map.Entry<JComponent, JLabel> compAndLabel : recordingComponents
                                            .entrySet()) {
                                        JComponent comp = compAndLabel.getKey();
                                        if (comp instanceof WheelSwitch) {
                                            WheelSwitch w = ((WheelSwitch) comp);
                                            if (w.getValue() > 0) {
                                                JLabel labelName = compAndLabel.getValue();
                                                Entry entry = null;
                                                for (Entry e : entries) {
                                                    if (e.getKey().equalsIgnoreCase(labelName.getText().trim())) {
                                                        entry = e;
                                                    }
                                                }
                                                UIDelegate.displayError(entry, section, null, labelName, comp,
                                                        sectionSelector, RECORDING_ENTRIES_MUTUALLY_EXCLUSIVE_ERROR,
                                                        tabTitle, errorPanel, errorsMap, this);
                                            }
                                        } else if (comp instanceof Spinner) {
                                            Spinner w = ((Spinner) comp);
                                            if (w.getNumberValue().longValue() > 0) {
                                                JLabel labelName = compAndLabel.getValue();
                                                Entry entry = null;
                                                for (Entry e : entries) {
                                                    if (e.getKey().equalsIgnoreCase(labelName.getText().trim())) {
                                                        entry = e;
                                                    }
                                                }
                                                UIDelegate.displayError(entry, section, null, labelName, comp,
                                                        sectionSelector, RECORDING_ENTRIES_MUTUALLY_EXCLUSIVE_ERROR,
                                                        tabTitle, errorPanel, errorsMap, this);
                                            }
                                        }
                                    } // end for (java.util.Map.Entry<JComponent, JLabel> compAndLabel :
                                      // recordingComponents.entrySet())
                                } // end if (nbEntriesEdited > 1 && !(section.getEntries().isEmpty()) &&
                                  // !(recordingComponents.isEmpty()))
                            } // end if (recording)
                            for (final FSObject fsObject : objects) {
                                if (fsObject != null) {
                                    objectPanel = UIDelegate.buildObjectPanel(fsObject);
                                    objectExpertPanel = new ExpertPanel();
                                    objectPanelScrollPane = null;
                                    Plugin pluginObj = getPluginAndProcessParamsWithDefaultValues(usedPlugins, section,
                                            fsObject, tabTitle, configuration.getName(), selector);
                                    sectionPanel.add(objectPanel,
                                            UIDelegate.createConstraints(Utilities.getNextY(sectionPanel)));
                                    for (final Entry entry : fsObject.getEntries()) {
                                        if ((!entry.isExpert()) || expert) {
                                            editorAndException = new Object[2];
                                            buildEntryGUI(configuration.getName(), objectPanel, objectExpertPanel,
                                                    objectPanelScrollPane, sectionPanel, sectionExpertPanel, selector,
                                                    sectionSelector, fsObject, section, pluginObj, entry, tabTitle,
                                                    customParametersPanel, levelRestriction, null, editorAndException);
                                            if (editorAndException[1] instanceof FSParsingException) {
                                                FSParsingException e = (FSParsingException) editorAndException[1];
                                                Utilities.LOGGER.error(e.getMessage());
                                                UIDelegate.displayError(entry, section, fsObject, errorLabels.get(e),
                                                        errorComponents.get(e), selector, e.getMessage(), tabTitle,
                                                        errorPanel, errorsMap, this);
                                            }
                                        }

                                    }
                                    UIDelegate.addParametersPanel(objectPanel, objectExpertPanel, 20, false);
                                }
                            }
                            if ((customParametersPanel != null) && (customParametersPanel.getComponentCount() > 0)) {
                                sectionPanel.add(customParametersPanel,
                                        UIDelegate.createConstraints(Utilities.getNextY(sectionPanel)));
                            }
                            UIDelegate.setupAndAddComponents(listPanel, nameLabel, typeLabel, enableLabel,
                                    sectionSelector, nameComponent, typeComponent, enableComponent, topGlue, glue1,
                                    glue2, glue3, bottomGlue);
                            UIDelegate.prepareHighlighting(sectionSelector, listPanel, infoPanel, layout, section,
                                    nameComponent, typeComponent, enableComponent, topGlue, glue1, glue2, glue3,
                                    bottomGlue);
                            if ((added != null) && !added[0]) {
                                added[0] = true;
                                sectionSelector.setSelected(true);
                                glue1.grabFocus();
                            }
                            if (sectionPanel.getComponentCount() > 0) {
                                UIDelegate.addParametersPanel(sectionPanel, sectionExpertPanel, 20, false);
                                UIDelegate.addFinalGlue(sectionPanel);
                            }
                        } // end if ((!section.getObjects().isEmpty()) || (!section.getEntries().isEmpty()))
                    } // end if (section instanceof ActorSection) ... else
                } // end if (!section.getEntries().isEmpty() || !section.getObjects().isEmpty())

            } // end for (final Section section : configuration.getSections())

            // Remove easy config tab if empty.
            cleanTab(Utilities.EASY_CONFIG, easyConfigSectionPanel, false, configuration);
            // Remove empty actor tabs in non-expert mode.
            cleanTab(Utilities.ACTUATORS, actuatorListPanel, expert, configuration);
            cleanTab(Utilities.SENSORS, sensorListPanel, expert, configuration);
            cleanTab(Utilities.TIMEBASE, timebaseListPanel, expert, configuration);
            cleanTab(Utilities.HOOKS, hookListPanel, expert, configuration);
            cleanTab(Utilities.MONITORS, monitorListPanel, expert, configuration);
            UIDelegate.updateTopAndBottomGlue(generalListPanel, actuatorListPanel, sensorListPanel, timebaseListPanel,
                    hookListPanel, monitorListPanel, easyConfigListPanel);
            if ((actuatorListPanel.getComponentCount() == 0) && (sensorListPanel.getComponentCount() == 0)
                    && (timebaseListPanel.getComponentCount() == 0) && (hookListPanel.getComponentCount() == 0)
                    && (monitorListPanel.getComponentCount() == 0)) {
                updateGeneralAndEasyConfigLabels();
            }
            UIDelegate.checkDividerLocations(generalPanel, actuatorPanel, sensorPanel, timebasePanel, hookPanel,
                    monitorPanel, easyConfigPanel);
        } // end if (configuration != null)
    }

    protected void cleanTab(String tabTile, JPanel panel, boolean expert, Configuration configuration) {
        if ((panel.getComponentCount() == 0)
                && ((!expert) || Utilities.CURRENT_CONFIG.equals(configuration.getName()))) {
            tabbedPane.remove(tabTile);
        }
    }

    protected void updateGeneralAndEasyConfigLabels() {
        Dimension size = easyConfigNameLabel.getPreferredSize();
        int width = actuatorNameLabel.getPreferredSize().width + UIDelegate.GAP
                + actuatorTypeLabel.getPreferredSize().width;
        if (size.width < width) {
            easyConfigNameLabel.setPreferredSize(new Dimension(width, size.height));
        }
        size = generalNameLabel.getPreferredSize();
        width += actuatorEnableLabel.getPreferredSize().width;
        if (size.width < width) {
            generalNameLabel.setPreferredSize(new Dimension(width, size.height));
        }
    }

    protected JComponent preparePanelsAndAddTab(JLabel nameLabel, JLabel typeLabel, JLabel enableLabel,
            JPanel titlePanel, JPanel infoPanel, JScrollPane listScrollPane, JPanel buttonPanel, String tabTitle,
            Icon tabIcon, boolean hidden) {
        int x = 0, y = 0;
        GridBagConstraints constraints;
        constraints = UIDelegate.createContraints(GridBagConstraints.VERTICAL, x++, y, 0, 0);
        titlePanel.add(UIDelegate.createGlue(true, UIDelegate.GAP), constraints);
        constraints = UIDelegate.createContraints(GridBagConstraints.HORIZONTAL, x++, y,
                (typeLabel == null) && (enableLabel == null) ? 1 : 0, 0);
        titlePanel.add(nameLabel, constraints);
        constraints = UIDelegate.createContraints(GridBagConstraints.VERTICAL, x++, y, 0, 0);
        titlePanel.add(UIDelegate.createGlue(true, UIDelegate.GAP), constraints);
        if (typeLabel != null) {
            constraints = UIDelegate.createContraints(GridBagConstraints.HORIZONTAL, x++, y, 0, 0);
            titlePanel.add(typeLabel, constraints);
            constraints = UIDelegate.createContraints(GridBagConstraints.VERTICAL, x++, y, 0, 0);
            titlePanel.add(UIDelegate.createGlue(true, UIDelegate.GAP), constraints);
        }
        if (enableLabel != null) {
            constraints = UIDelegate.createContraints(GridBagConstraints.HORIZONTAL, x++, y, 0, 0);
            titlePanel.add(enableLabel, constraints);
            constraints = UIDelegate.createContraints(GridBagConstraints.BOTH, x++, y, 1, 0);
            titlePanel.add(UIDelegate.createGlue(true, UIDelegate.GAP), constraints);
        }
        listScrollPane.setColumnHeaderView(titlePanel);
        listScrollPane.setMinimumSize(Utilities.ZERO_DIM);
        if (!(listScrollPane.getBorder() instanceof CompoundBorder)) {
            listScrollPane.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 1), listScrollPane.getBorder()));
        }
        if (!(infoPanel.getBorder() instanceof CompoundBorder)) {
            infoPanel.setBorder(new CompoundBorder(new EmptyBorder(0, 2, 0, 0), infoPanel.getBorder()));
        }
        infoPanel.setMinimumSize(Utilities.ZERO_DIM);
        JPanel controlPanel = new JPanel(new BorderLayout());
        controlPanel.add(listScrollPane, BorderLayout.CENTER);
        controlPanel.add(buttonPanel, BorderLayout.SOUTH);
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, controlPanel, infoPanel);
        splitPane.setUI(new ColoredSplitPaneUI());
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerSize(8);
        splitPane.setResizeWeight(0);
        splitPane.setMinimumSize(SPLITPANE_MINIMUM_SIZE);
        TabLabel label = new TabLabel(tabTitle, tabIcon, JLabel.LEFT);
//        RotatedLabel label = new RotatedLabel(tabTitle, tabIcon, JLabel.LEFT);
//        label.setDirection(Direction.VERTICAL_UP);
        tabbedPane.addTab(tabTitle, null, splitPane, tabTitle, label, hidden);
        label.setFont(tabbedPane.getTabIndexForTitle(tabTitle) == tabbedPane.getSelectedIndex() ? Utilities.TITLE_FONT
                : Utilities.BASIC_FONT);
        return splitPane;
    }

    protected JRadioButton createSelector(ButtonGroup group) {
        JRadioButton selector = new JRadioButton();
        selector.setVisible(false);
        if (group != null) {
            group.add(selector);
        }
        return selector;
    }

    protected UIBuffer initPanels(final Configuration configuration, final String title, final PluginType pluginType,
            final Map<PluginType, Collection<Plugin>> categories, final String nameText, final ImageIcon icon,
            final String iconKey, final boolean easyConfigOnly, final boolean type, final boolean enable,
            final boolean actor, final boolean[] added) {
        JPanel leftPanel = new JPanel(new BorderLayout());
        JPanel listPanel = new JPanel(new GridBagLayout());
        leftPanel.add(listPanel, BorderLayout.NORTH);
        listPanel.setName(title);
        String element;
        if (title.charAt(title.length() - 1) == 's') {
            element = title.toLowerCase().substring(0, title.length() - 1);
        } else {
            element = title.toLowerCase();
        }
        JPanel sectionPanel = new JPanel(new GridBagLayout());
        ExpertPanel sectionExpertPanel = new ExpertPanel();
        CardLayout layout = new CardLayout();
        JPanel infoPanel = new JPanel(layout);
        JPanel titlePanel = new JPanel(new GridBagLayout());
        JScrollPane leftScrollPane = new FSVerticalScrollPane(leftPanel);
        JLabel nameLabel = new FSLabel(nameText);
        JLabel typeLabel = type ? new FSLabel(Utilities.TYPE) : null;
        JLabel enableLabel = enable ? new FSLabel(Utilities.ENABLE) : null;
        ButtonGroup group = new ButtonGroup();
        JRadioButton selector = createSelector(group);
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        JComponent panel = preparePanelsAndAddTab(nameLabel, typeLabel, enableLabel, titlePanel, infoPanel,
                leftScrollPane, buttonPanel, title, icon, easyConfigOnly);
        // In expert mode, user might add actors.
        if (actor && expert && (pluginType != null) && !Utilities.CURRENT_CONFIG.equals(configuration.getName())) {
            JButton addButton = Utilities.createButton(String.format(ADD_MESSAGE, element), true, Color.WHITE);
            addButton.setFont(Utilities.BASIC_FONT);
            addButton.setIcon(Utilities.getDecorableIcon(iconKey, Utilities.ADD_DECORATION_KEY, SwingConstants.LEFT,
                    SwingConstants.BOTTOM));
            addButton.addActionListener(e -> {
                addButton.setBackground(null);
                if (pluginType != null) {
                    Collection<Plugin> plugins = categories.get(pluginType);
                    ActorSection actorSection = null;
                    for (Section tmp : configuration.getSections()) {
                        if (tmp instanceof ActorSection) {
                            actorSection = (ActorSection) tmp;
                            break;
                        }
                    }
                    if (actorSection == null) {
                        actorSection = new ActorSection();
                        configuration.addSections(actorSection);
                    }
                    Actor newActor = ActorChooser.chooseActor(addButton, pluginType.name().toLowerCase(),
                            configuration == null ? ObjectUtils.EMPTY_STRING : configuration.getName(), actorSection,
                            plugins);
                    if (newActor != null) {
                        actorSection.addFSObject(newActor);
                        UIDelegate.notifyEdition(true, this);
                        // Remove filling glue
                        if (listPanel.getComponentCount() > 0) {
                            listPanel.remove(listPanel.getComponent(listPanel.getComponentCount() - 1));
                        }
                        // Update last bottom glue
                        if (listPanel.getComponentCount() > 0) {
                            JComponent comp = (JComponent) listPanel.getComponent(listPanel.getComponentCount() - 1);
                            Dimension size = comp.getPreferredSize();
                            if (size.height != UIDelegate.HALF_GAP) {
                                comp.setPreferredSize(new Dimension(size.width, UIDelegate.HALF_GAP));
                            }
                        }
                        // Auto select added actor
                        if ((added != null) && (added.length > 0)) {
                            added[0] = false;
                        }
                        buildObjectPanel(title, added, newActor, actorSection, plugins, group, infoPanel, listPanel,
                                sectionPanel, sectionExpertPanel, layout, selector, nameLabel, typeLabel, enableLabel);
                        UIDelegate.updateTopAndBottomGlue(listPanel);
                        UIDelegate.checkDividerLocations(generalPanel, actuatorPanel, sensorPanel, timebasePanel,
                                hookPanel, monitorPanel, easyConfigPanel);
                    }
                }
            }); // end addButton.addActionListener
            Dimension addSize = addButton.getPreferredSize();
            JButton removeButton = Utilities.createButton(String.format(REMOVE_MESSAGE, element), true, Color.WHITE);
            removeButton.setFont(Utilities.BASIC_FONT);
            removeButton.setIcon(Utilities.getDecorableIcon(iconKey, Utilities.REMOVE_DECORATION_KEY,
                    SwingConstants.LEFT, SwingConstants.BOTTOM));
            removeButton.setEnabled(false);
            listPanel.addContainerListener(new ContainerListener() {
                @Override
                public void componentRemoved(ContainerEvent e) {
                    updateRemoveButton(removeButton, listPanel, added);
                }

                @Override
                public void componentAdded(ContainerEvent e) {
                    updateRemoveButton(removeButton, listPanel, added);
                }
            });
            removeButton.addActionListener(e -> {
                removeButton.setBackground(null);
                if (JOptionPane.showConfirmDialog(removeButton, String.format(REMOVE_POPUP_MESSAGE, element),
                        REMOVE_POPUP_TITLE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    JRadioButton selected = null;
                    Enumeration<AbstractButton> buttons = group.getElements();
                    while (buttons.hasMoreElements()) {
                        AbstractButton button = buttons.nextElement();
                        if (button.isSelected() && (button instanceof JRadioButton)) {
                            selected = (JRadioButton) button;
                            break;
                        }
                    }
                    if (selected != null) {
                        // Step 1: select another actor
                        JRadioButton previous = null, next = null;
                        boolean searchNext = false;
                        for (Component comp : listPanel.getComponents()) {
                            if (comp instanceof JRadioButton) {
                                JRadioButton button = (JRadioButton) comp;
                                if (button == selected) {
                                    searchNext = true;
                                } else if (searchNext) {
                                    next = button;
                                    break;
                                } else {
                                    previous = button;
                                }
                            }
                        }
                        if (next == null) {
                            if (previous != null) {
                                previous.setSelected(true);
                            }
                        } else {
                            next.setSelected(true);
                        }
                        // Step 2: remove button from group
                        group.remove(selected);
                        FSObject object = buttonObjectMap.remove(selected);
                        if (object != null) {
                            // Step 3: remove components
                            // SCAN-967: don't use FSObject as key. Prefer a String instead.
                            IdManager manager = getIdManager(object);
                            JComponent[] components;
                            if (manager == null) {
                                components = null;
                            } else {
                                components = objectComponentsMap.remove(manager.getId());
                                idManagers.remove(manager);
                                manager.clear();
                            }
                            if (components != null) {
                                // selector, nameComponent, typeComponent, enableComponent, topGlue, glue1, glue2,
                                // glue3, bottomGlue, objectPanel, objectPanelScrollPane
                                for (Component component : components) {
                                    if (component != null) {
                                        listPanel.remove(component);
                                    }
                                }
                                // 1 component = filling glue
                                if (listPanel.getComponentCount() == 1) {
                                    listPanel.removeAll();
                                }
                                if (components.length > 0) {
                                    // Last component in array is objectPanel
                                    Component last = components[components.length - 1];
                                    if (last != null) {
                                        // Remove scrollPane containing objectPanel
                                        infoPanel.remove(last);
                                    }
                                }
                                UIDelegate.updateTopAndBottomGlue(listPanel);
                            }
                            // Step 4: remove actor
                            if (actorSection != null) {
                                actorSection.removeFSObject(object);
                                UIDelegate.notifyEdition(true, this);
                            }
                        }
                    }
                }
            }); // end removeButton.addActionListener
            Dimension removeSize = removeButton.getPreferredSize();
            if (addSize.width > removeSize.width) {
                removeSize.width = addSize.width;
                removeButton.setPreferredSize(removeSize);
            } else {
                addSize.width = removeSize.width;
                addButton.setPreferredSize(addSize);
            }
            GridBagConstraints addConstraints = new GridBagConstraints();
            addConstraints.fill = GridBagConstraints.NONE;
            addConstraints.gridx = 0;
            addConstraints.gridy = 0;
            addConstraints.weightx = 0;
            addConstraints.weighty = 0;
            addConstraints.insets = new Insets(0, 0, 0, 20);
            buttonPanel.add(addButton, addConstraints);
            GridBagConstraints removeConstraints = new GridBagConstraints();
            removeConstraints.fill = GridBagConstraints.NONE;
            removeConstraints.gridx = 1;
            removeConstraints.gridy = 0;
            removeConstraints.weightx = 0;
            removeConstraints.weighty = 0;
            buttonPanel.add(removeButton, removeConstraints);
            buttonPanel.revalidate();
            buttonPanel.getParent().revalidate();
        } // end if (actor && expert && (pluginType != null) &&
          // !Utilities.CURRENT_CONFIG.equals(configuration.getName()))
        return new UIBuffer(leftPanel, listPanel, sectionPanel, sectionExpertPanel, infoPanel, titlePanel, layout,
                leftScrollPane, nameLabel, typeLabel, enableLabel, group, selector, panel);
    }

    protected static void updateRemoveButton(final JButton removeButton, final JPanel listPanel,
            final boolean[] added) {
        removeButton.setEnabled(listPanel.getComponentCount() > 0);
        if ((added != null) && !removeButton.isEnabled()) {
            added[0] = false;
        }
    }

    public BoldTitleTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    protected Plugin getPluginAndProcessParamsWithDefaultValues(Collection<Plugin> plugins, Section section,
            final FSObject fsObject, String tabTitle, String configName, JRadioButton selector) {
        Plugin plugin = null;
        try {
            plugin = UIDelegate.getPlugin(plugins, fsObject);
            if (plugin != null) {
                UIDelegate.processParams(fsObject, plugin, configName);
            }
        } catch (FSParsingException e1) {
            Utilities.LOGGER.error(e1.getMessage());
            String location = UIDelegate.getErrorLocation(section, fsObject, null);
            JTextArea errorLabel = UIDelegate.getTextAreaError(e1.getMessage(), location, this, null, selector,
                    tabTitle, this);
            errorPanel.add(errorLabel);
            errorsMap.put(this, errorLabel);
        }
        return plugin;
    }

    public boolean removeErrorTextComponent(JComponent parentComponent) {
        boolean removed;
        JTextComponent errorTextComponent = errorsMap.remove(parentComponent);
        if (errorTextComponent == null) {
            removed = false;
        } else {
            removed = true;
            errorPanel.remove(errorTextComponent);
            errorPanel.revalidate();
            ((JComponent) errorPanel.getParent()).revalidate();
        }
        return removed;
    }

    public void removeAllErrorTextComponents() {
        boolean removed = false;
        while (errorPanel.getComponentCount() > 0) {
            Component errorComponent = errorPanel.getComponent(0);
            errorPanel.remove(errorComponent);
            errorsMap.remove(errorComponent);
            removed = true;
        }
        if (removed) {
            errorPanel.revalidate();
        }
    }

    /**
     * Add a label indicating a check config error within the suitable panel
     * 
     * @param errorTextComponent JLabel
     */
    public void addErrorTextComponent(JTextComponent errorTextComponent, JComponent parentComponent) {
        removeErrorTextComponent(parentComponent);
        errorPanel.add(errorTextComponent);
        errorPanel.revalidate();
        errorsMap.put(parentComponent, errorTextComponent);
    }

    protected void warnConstraintsAreNotSatisfied(final String tabTitle, final JComponent component, JLabel nameLabel,
            JRadioButton selector, Collection<Constraint> nonSatisfiedConstraints, String location) {
        StringBuilder sb = new StringBuilder(CONSTRAINTS_UNSATISFIED);
        sb.append(ParsingUtil.CRLF);
        for (Constraint constraint : nonSatisfiedConstraints) {
            sb.append(constraint.getText());
            sb.append(ParsingUtil.CRLF);
        }
        addErrorTextComponent(
                UIDelegate.getTextAreaError(sb.toString(), location, component, nameLabel, selector, tabTitle, this),
                component);
        component.setBorder(UIDelegate.buildErrorBorder(component));
    }

    /**
     * Build GUI for a single entry
     * 
     * @param configName The config name
     * @param objectPanel JPanel
     * @param objectPanelScrollPane JScrollPane
     * @param sectionPanel JPanel
     * @param expertPanel The panel dedicated to expert entries
     * @param objectSelector
     * @param sectionSelector
     * @param entryContainer EntryContainer
     * @param parentSection Section parent of <code>entryContainer</code>.
     * @param plugin Plugin
     * @param entry Entry
     * @param tabTitle
     * @param customParametersPanel
     * @param levelRestriction
     * @param componentsPair
     * @param editorAndException
     */
    protected void buildEntryGUI(String configName, JPanel objectPanel, ExpertPanel objectExpertPanel,
            JScrollPane objectPanelScrollPane, JPanel sectionPanel, ExpertPanel sectionExpertPanel,
            JRadioButton objectSelector, JRadioButton sectionSelector, final EntryContainer entryContainer,
            final Section parentSection, Plugin plugin, final Entry entry, final String tabTitle,
            CustomParameterPanel customParametersPanel, boolean levelRestriction,
            Map<JComponent, JLabel> componentsPair, final Object[] editorAndException) {
        boolean autoAdd;
        JPanel parentPanel = null;
        FSParsingException exception = null;
        String description = null;
        JComponent editorComponent = null;
        ExpertPanel expertPanel = null;
        Collection<Constraint> constraints;
        String value, s;
        boolean isEditable;
        if (entry == null) {
            constraints = null;
            value = ObjectUtils.EMPTY_STRING;
            isEditable = false;
            autoAdd = true;
        } else {
            constraints = entry.getConstraints();
            value = entry.getValue();
            isEditable = true;
            for (Constraint constraint : constraints) {
                if (constraint instanceof ConstantConstraint) {
                    isEditable = false;
                }
            }
            isEditable = isEditable && ((!entry.isExpert()) || expert);
            String key = entry.getKey();
            if (key == null) {
                key = ObjectUtils.EMPTY_STRING;
            } else {
                key = key.toLowerCase().trim();
            }
            switch (key) {
                case ParsingUtil.NAME:
                    autoAdd = entryContainer instanceof Section;
                    break;
                case ParsingUtil.TYPE:
                case ParsingUtil.ENABLE:
                    autoAdd = false;
                    break;
                default:
                    autoAdd = true;
                    break;
            }
        }
        String paramName = entry.getKey();
        if (paramName == null) {
            paramName = ObjectUtils.EMPTY_STRING;
        } else {
            paramName = paramName.trim();
        }
        final JLabel nameLabel;
        if (autoAdd) {
            nameLabel = new FSLabel(paramName);
            nameLabel.setForeground(Color.BLACK);
        } else {
            nameLabel = null;
        }
        Collection<String> possibleValues;
        String[] tmp;
        Parameter parameter = null;
        if (entry instanceof ParameterEntry) {
            if (plugin != null) {
                for (Parameter param : plugin.getParameters()) {
                    editorAndException[0] = editorAndException[1] = null;
                    if (paramName.equalsIgnoreCase(param.getName().trim())) {
                        parameter = param;
                        entry.setEntryType(param.getType());
                        if (!entry.isExpert()) {
                            entry.setExpert(param.isExpert());
                        }
                        isEditable = isEditable && ((!entry.isExpert()) || expert);
                        description = param.getDescription();
                        UIDelegate.prepareEditorComponent(configName, entry, nameLabel, objectSelector, param,
                                entryContainer, tabTitle, editorAndException, errorLabels, errorComponents, errorPanel,
                                errorsMap, this);
                        editorComponent = (JComponent) editorAndException[0];
                        FSParsingException e = (FSParsingException) editorAndException[1];
                        if (e != null) {
                            exception = e;
                            if (nameLabel != null) {
                                errorLabels.put(e, nameLabel);
                            }
                            if (editorComponent == null) {
                                if (errorComponents.get(e) == null) {
                                    if (objectPanel != null) {
                                        errorComponents.put(e, objectPanel);
                                    } else if (!((entryContainer instanceof RecordingSection)
                                            && (entry.getKey().equalsIgnoreCase(ParsingUtil.MAX_STEPS_PER_FILE)))) {
                                        errorComponents.put(e, sectionPanel);
                                    }
                                }
                            } else {
                                errorComponents.put(e, editorComponent);
                            }
                        }
                        break;
                    }
                }
            }
        } else if (entry instanceof PredefinedEntry) { // end if (entry instanceof ParameterEntry)
            PredefinedEntry predefinedEntry = (PredefinedEntry) entry;
            int count;
            try {
                switch (predefinedEntry.getEntryType()) {
                    case BOOL:
                        FSCheckBox checkBox = new FSCheckBox();
                        if (value.toLowerCase().contains(Utilities.TRUE)) {
                            checkBox.setSelected(true);
                        } else {
                            checkBox.setSelected(false);
                        }
                        if (ParsingUtil.ENABLE.equalsIgnoreCase(entry.getKey())
                                && (entryContainer instanceof EasyConfigSection)) {
                            isEditable = isEditable && (expert || (!checkBox.isSelected()));
                        } else if (ParsingUtil.CONTINUOUS.equalsIgnoreCase(entry.getKey())
                                && (entryContainer instanceof AcquisitionSection)) {
                            // "continuous" should not be editable in acquisition section (SCAN-940)
                            isEditable = false;
                        }
                        editorComponent = checkBox;
                        break;
                    case DEV_STATE:
                        count = UIDelegate.getCountConstraint(predefinedEntry, null, editorComponent, nameLabel,
                                errorLabels, errorComponents);
                        possibleValues = new ArrayList<>();
                        for (String str : UIDelegate.POSSIBLE_VALUES) {
                            possibleValues.add(str);
                        }
                        Collection<String> strValues = new ArrayList<>();
                        s = UIDelegate.getSimplifiedValue(value);
                        tmp = s.split(ParsingUtil.COMMA);
                        s = null;
                        for (String str : tmp) {
                            strValues.add(str.trim());
                        }
                        Arrays.fill(tmp, null);
                        tmp = null;
                        editorComponent = new MultivaluedStringListComponent(possibleValues, strValues, count, true,
                                true, null, UIDelegate.mayThrowParsingException(entry));
                        UIDelegate.checkList(strValues, count, predefinedEntry, editorComponent, nameLabel, errorLabels,
                                errorComponents);
                        break;
                    case ENUM:
                        ComboBox comboBox = new ComboBox();
                        Utilities.prepareComboBoxLookAndFeel(comboBox);
                        editorComponent = comboBox;
                        if (ParsingUtil.ERROR_STRATEGY.equalsIgnoreCase(paramName)) {
                            ErrorStrategy[] values = ErrorStrategy.values();
                            for (ErrorStrategy es : values) {
                                comboBox.addItem(es.name().toLowerCase());
                            }
                            UIDelegate.applyComboValue(comboBox, nameLabel, predefinedEntry, ParsingUtil.ERROR_STRATEGY,
                                    errorLabels, errorComponents);
                        } else if (ParsingUtil.ORDER_STRATEGY.equalsIgnoreCase(paramName)) {
                            OrderStrategy[] values = OrderStrategy.values();
                            for (OrderStrategy os : values) {
                                ((ComboBox) editorComponent).addItem(os.name().toLowerCase());
                            }
                            UIDelegate.applyComboValue(comboBox, nameLabel, predefinedEntry, ParsingUtil.ORDER_STRATEGY,
                                    errorLabels, errorComponents);
                        } else if (ParsingUtil.ACTION.equalsIgnoreCase(paramName) && (entryContainer instanceof Hook)) {
                            editorComponent = null;
                            count = UIDelegate.getCountConstraint(predefinedEntry, null, editorComponent, nameLabel,
                                    errorLabels, errorComponents);
                            HookAction[] possibleValuesArray = HookAction.values();
                            possibleValues = new ArrayList<>();
                            for (HookAction ha : possibleValuesArray) {
                                possibleValues.add(ha.toString());
                            }
                            Collection<String> values = new ArrayList<>();
                            s = value.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim();
                            tmp = s.split(ParsingUtil.COMMA);
                            s = null;
                            for (String str : tmp) {
                                values.add(str.trim());
                            }
                            Arrays.fill(tmp, null);
                            tmp = null;
                            MultivaluedStringListComponent hookActionComponent = new MultivaluedStringListComponent(
                                    possibleValues, values, count, false, true, null,
                                    UIDelegate.mayThrowParsingException(entry));
                            // SCAN-978: At least 1 value for hook action
                            hookActionComponent.setMininimumNumberOfElements(1);
                            editorComponent = hookActionComponent;
                            UIDelegate.checkList(values, count, predefinedEntry, editorComponent, nameLabel,
                                    errorLabels, errorComponents);
                        } else if (entryContainer instanceof Actuator) {
                            ComboBox combo = null;
                            if (ParsingUtil.POST_SCAN_ACTION.equalsIgnoreCase(paramName)) {
                                combo = (ComboBox) editorComponent;
                                for (PostScanAction action : PostScanAction.values()) {
                                    combo.addItem(action.toString());
                                }
                            } else if (ParsingUtil.TRAJECTORY_MODE.equalsIgnoreCase(paramName)) {
                                combo = (ComboBox) editorComponent;
                                for (TrajectoryMode mode : TrajectoryMode.values()) {
                                    combo.addItem(mode.toString());
                                }
                            }
                            UIDelegate.applyComboValue(combo, nameLabel, entry, paramName, errorLabels,
                                    errorComponents);
                        }
                        break;
                    case NUMBER:
                    case FLOAT:
                        WheelSwitch wheelSwitch = Utilities.createWheelSwitch();
                        double val = ParsingTool.parseDouble(value);
                        wheelSwitch.setNumberValue(val);
                        editorComponent = wheelSwitch;
                        break;
                    case INT:
                        Spinner spinner = Utilities.createSpinner();
                        long intVal;
                        String str = value.trim();
                        try {
                            intVal = Long.parseLong(str);
                        } catch (Exception e) {
                            intVal = (long) Double.parseDouble(str);
                        }
                        if (ParsingUtil.ORDER.equalsIgnoreCase(paramName)) {
                            spinner.setNumberValue(1L);
                            spinner.setMinimum(1L);
                        }
                        if (paramName.startsWith(ParsingUtil.DIMENSION)) {
                            spinner.setMinimum(0L);
                        }
                        spinner.setNumberValue(intVal);
                        editorComponent = spinner;
                        break;
                    case STRING:
                        boolean name = false;
                        if (ParsingUtil.TYPE.equalsIgnoreCase(entry.getKey()) && (entryContainer instanceof Actor)) {
                            isEditable = false;
                        } else if (ParsingUtil.NAME.equalsIgnoreCase(entry.getKey())) {
                            isEditable = isEditable && (entryContainer instanceof Actor) && expert;
                            name = true;
                        } else if (ParsingUtil.CONFIGURATOR.equalsIgnoreCase(entry.getKey())
                                && (entryContainer instanceof EasyConfigSection)) {
                            isEditable = isEditable && expert;
                        }
                        editorComponent = UIDelegate.getDefaultComponent(predefinedEntry.getValue());
                        if (name && (editorComponent instanceof JTextField)) {
                            JTextField textField = (JTextField) editorComponent;
                            FilteredCharactersDocument document = Utilities.createNameFilteredDocument();
                            String text = textField.getText();
                            textField.setDocument(document);
                            document.setEnabled(false);
                            textField.setText(text);
                            document.setEnabled(true);
                        }
                        break;
                    case JSON:
                        editorComponent = new FSAdaptableTextArea(predefinedEntry.getValue());
                        break;
                    case INT_ARRAY:
                        Collection<Long> values = new ArrayList<>();
                        String entryVal = predefinedEntry.getValue();
                        if (entryVal != null) {
                            String[] arrayStr = entryVal.replace(ParsingUtil.BRACKET_OPENNING, ObjectUtils.EMPTY_STRING)
                                    .replace(ParsingUtil.BRACKET_CLOSING, ObjectUtils.EMPTY_STRING).trim()
                                    .split(ParsingUtil.COMMA);
                            for (String intStr : arrayStr) {
                                String toParse = intStr.trim();
                                try {
                                    values.add(Long.valueOf(toParse));
                                } catch (Exception e) {
                                    values.add(Long.valueOf((long) Double.parseDouble(toParse)));
                                }
                            }
                        }
                        Long minValue;
                        if (paramName.startsWith(ParsingUtil.DIMENSION)) {
                            minValue = Long.valueOf(0);
                        } else {
                            minValue = null;
                        }
                        final MultivaluedSpinnerComponent multiSpinnerComponent = new MultivaluedSpinnerComponent(
                                values, -1, true, minValue);
                        // SCAN-977: Whatever the default choice, we want to be sure the dimensions are always
                        // represented as an array.
                        multiSpinnerComponent.setScalarContentAllowed(false);
                        // SCAN-978: At least 1 dimension.
                        multiSpinnerComponent.setMininimumNumberOfElements(1);
                        editorComponent = multiSpinnerComponent;
                        break;
                    case TRAJECTORY:
                        Trajectory trajectory = ((TrajectoryEntry) entry).getTrajectory();
                        editorComponent = UIDelegate.getTrajectoryComponent(entryContainer, predefinedEntry, trajectory,
                                tabTitle, nameLabel, objectSelector, errorPanel, errorsMap, this, configName,
                                nameLabel.getText());
                        break;
                    default:
                        break;

                }
            } catch (FSParsingException e) {
                exception = e;
            }
        } else if (entry instanceof FakeEntry) { // end else if (entry instanceof PredefinedEntry)
            Parameter param = ((FakeEntry) entry).getParam();
            description = param.getDescription();
            editorAndException[0] = editorAndException[1] = null;
            UIDelegate.prepareEditorComponent(configName, entry, nameLabel, objectSelector, param, entryContainer,
                    tabTitle, editorAndException, errorLabels, errorComponents, errorPanel, errorsMap, this);
            editorComponent = (JComponent) editorAndException[0];
            FSParsingException e = (FSParsingException) editorAndException[1];
            if (e != null) {
                exception = e;
                errorLabels.put(e, nameLabel);
                if (errorComponents.get(e) == null) {
                    if (editorComponent == null) {
                        if (objectPanel != null) {
                            errorComponents.put(e, objectPanel);
                        } else if (!(ParsingUtil.MAX_STEPS_PER_FILE.equalsIgnoreCase(entry.getKey())
                                && (entryContainer instanceof RecordingSection))) {
                            errorComponents.put(e, sectionPanel);
                        }
                    } else {
                        errorComponents.put(e, editorComponent);
                    }
                }
            }
        } // end else if (entry instanceof FakeEntry)

        if ((editorComponent == null) && (customParametersPanel != null)
                && ((!levelRestriction) || (!ParsingUtil.LEVEL.equalsIgnoreCase(entry.getKey())))) {
            // Custom parameter (SCAN-395)
            customParametersPanel.addNewCustomParameter(entry, entryContainer);
        }
        if (exception != null) {
            if (editorComponent == null) {
                editorComponent = UIDelegate.getDefaultComponent(entry.getValue());
                editorComponent.setEnabled(false);
                errorComponents.put(exception, editorComponent);
                isEditable = false;
            } else if (errorComponents.get(exception) == null) {
                errorComponents.put(exception, editorComponent);
            }
            if (errorLabels.get(exception) == null) {
                errorLabels.put(exception, nameLabel);
            }
        }
        if (editorComponent != null) {
            editorComponent.setEnabled(isEditable && editorComponent.isEnabled());

            if (plugin != null) {
                for (Parameter param : plugin.getParameters()) {
                    if (param.getName().equalsIgnoreCase(entry.getKey()) && (param.getConstraints() != null)) {
                        constraints.addAll(param.getConstraints());
                    }
                }
            }

            Collection<Constraint> nonSatisfiedConstraints = UIDelegate.getNonSatisfiedConstraints(entry, value,
                    constraints);

            if (!nonSatisfiedConstraints.isEmpty()) {
                String location = UIDelegate.getErrorLocation(entry, entryContainer);
                warnConstraintsAreNotSatisfied(tabTitle, editorComponent, nameLabel, objectSelector,
                        nonSatisfiedConstraints, location);
            }

            // Listen to edition state change
            if (editorComponent instanceof JCheckBox) {
                final JCheckBox checkBox = (JCheckBox) editorComponent;
                checkBox.addItemListener(e -> {
                    fireEditionStateChanged(true, entry, entryContainer,
                            checkBox.isSelected() ? Utilities.TRUE : Utilities.FALSE, checkBox, nameLabel,
                            objectSelector, tabTitle);
                });
            } else if (editorComponent instanceof ConstrainedCheckBox) {
                final ConstrainedCheckBox checkBox = (ConstrainedCheckBox) editorComponent;
                checkBox.addItemListener(e -> {
                    fireEditionStateChanged(true, entry, entryContainer,
                            checkBox.isSelected() ? Utilities.TRUE : Utilities.FALSE, checkBox, nameLabel,
                            objectSelector, tabTitle);
                });
            } else if (editorComponent instanceof FSCheckBox) {
                final FSCheckBox checkBox = (FSCheckBox) editorComponent;
                checkBox.addItemListener(e -> {
                    fireEditionStateChanged(true, entry, entryContainer,
                            checkBox.isSelected() ? Utilities.TRUE : Utilities.FALSE, checkBox, nameLabel,
                            objectSelector, tabTitle);
                });
            } else if (editorComponent instanceof ComboBox) {
                final ComboBox comboBox = (ComboBox) editorComponent;
                comboBox.addItemListener(e -> {
                    fireEditionStateChanged(true, entry, entryContainer, (String) comboBox.getSelectedValue(), comboBox,
                            nameLabel, objectSelector, tabTitle);
                });
            } else if (editorComponent instanceof DeviceComboBox) {
                DeviceComboBox deviceComboBox = (DeviceComboBox) editorComponent;
                deviceComboBox.getDeviceBox().addItemListener(e -> {
                    fireEditionStateChanged(true, entry, entryContainer,
                            (String) deviceComboBox.getDeviceBox().getSelectedValue(), deviceComboBox, nameLabel,
                            objectSelector, tabTitle);
                });
            } else if (editorComponent instanceof JComboBox) {
                // Don't forget simple JComboBox case (SCAN-962)
                final JComboBox<?> comboBox = (JComboBox<?>) editorComponent;
                comboBox.addItemListener(e -> {
                    Object selected = comboBox.getSelectedItem();
                    fireEditionStateChanged(true, entry, entryContainer, selected == null ? null : selected.toString(),
                            comboBox, nameLabel, objectSelector, tabTitle);
                });
            } else if (editorComponent instanceof AbstractMultivaluedComponent<?, ?>) {
                final AbstractMultivaluedComponent<?, ?> multivaluedComponent = (AbstractMultivaluedComponent<?, ?>) editorComponent;
                multivaluedComponent.addBeingEditedListener(isBeingEdited -> {
                    String strValue = multivaluedComponent.getStringValue();
                    fireEditionStateChanged(isBeingEdited, entry, entryContainer, strValue, multivaluedComponent,
                            nameLabel, objectSelector, tabTitle);

                });
            } else if (editorComponent instanceof WheelSwitch) {
                final WheelSwitch wheelSwitch = (WheelSwitch) editorComponent;
                wheelSwitch.addWheelSwitchListener(evt -> {
                    Object obj = evt.getSource();
                    if (obj instanceof WheelSwitch) {
                        fireEditionStateChanged(true, entry, entryContainer,
                                ParsingTool.toString(wheelSwitch.getValue()), wheelSwitch, nameLabel, objectSelector,
                                tabTitle);
                    }
                });
            } else if (editorComponent instanceof Spinner) {
                final Spinner spinner = (Spinner) editorComponent;
                spinner.addSpinnerListener(event -> {
                    Object obj = event.getSource();
                    if (obj instanceof Spinner) {
                        fireEditionStateChanged(true, entry, entryContainer, String.valueOf(spinner.getNumberValue()),
                                spinner, nameLabel, objectSelector, tabTitle);
                    }
                });
            } else if (editorComponent instanceof JTextField) {
                final JTextField textField = (JTextField) editorComponent;
                textField.addKeyListener(new KeyAdapter() {
                    @Override
                    public void keyReleased(KeyEvent e) {
                        e.consume();// mandatory to avoid duplicate paste (SCAN-492). Don't ask why.
                    }
                });
                textField.getDocument().addDocumentListener(new DocumentListener() {
                    protected void applyChange(DocumentEvent e) {
                        boolean mayChange = true;
                        String category, name = textField.getText();
                        boolean actorNameCase;
                        if ((entryContainer instanceof Actor) && ParsingUtil.NAME.equalsIgnoreCase(entry.getKey())) {
                            Actor actor = Utilities.getActorNamedLike(name, parentSection, (Actor) entryContainer);
                            mayChange = (actor == null);
                            category = Utilities.getCategory(actor);
                            actorNameCase = true;
                        } else {
                            category = null;
                            actorNameCase = false;
                        }
                        if (mayChange) {
                            fireEditionStateChanged(true, entry, entryContainer, textField.getText(), textField,
                                    nameLabel, objectSelector, tabTitle);
                            if (actorNameCase && (objectPanelScrollPane != null)) {
                                UIDelegate.updateName(name, objectSelector, objectPanelScrollPane);
                            }
                        } else {
                            SwingUtilities.invokeLater(() -> {
                                textField.getDocument().removeDocumentListener(this);
                                textField.setText(entry.getValue());
                                textField.getDocument().addDocumentListener(this);
                                JOptionPane.showMessageDialog(textField,
                                        String.format(ACTOR_NAME_CHANGE_REFUSED, category, name, entry.getValue()),
                                        WARNING, JOptionPane.WARNING_MESSAGE);
                            });
                        }
                    }

                    @Override
                    public void removeUpdate(DocumentEvent e) {
                        applyChange(e);
                    }

                    @Override
                    public void insertUpdate(DocumentEvent e) {
                        applyChange(e);
                    }

                    @Override
                    public void changedUpdate(DocumentEvent e) {
                        applyChange(e);
                    }
                });
            } else if (editorComponent instanceof FSAdaptableTextArea) {
                final FSAdaptableTextArea textArea = (FSAdaptableTextArea) editorComponent;
                textArea.getTextArea().addTextAreaListener(new ITextAreaListener() {

                    @Override
                    public void textChanged(EventObject event) {
                        fireEditionStateChanged(true, entry, entryContainer, textArea.getTextArea().getAdaptedText(),
                                textArea, nameLabel, objectSelector, tabTitle);
                    }

                    @Override
                    public void actionPerformed(EventObject event) {
                        fireEditionStateChanged(true, entry, entryContainer, textArea.getTextArea().getAdaptedText(),
                                textArea, nameLabel, objectSelector, tabTitle);
                    }
                });
            } else if (editorComponent instanceof Spinner) {

                final Spinner spinner = (Spinner) editorComponent;
                spinner.addSpinnerListener(e -> {
                    fireEditionStateChanged(true, entry, entryContainer, ParsingTool.toString(spinner.getValue()),
                            spinner, nameLabel, objectSelector, tabTitle);
                });
            }
            if (!(editorComponent instanceof CustomTextArea)) {
                if (objectPanel == null) {
                    if (!(ParsingUtil.MAX_STEPS_PER_FILE.equalsIgnoreCase(entry.getKey())
                            && (entryContainer instanceof RecordingSection))) {
                        if ((entry instanceof ParameterEntry) && entry.isExpert() && (sectionExpertPanel != null)) {
                            expertPanel = sectionExpertPanel;
                            parentPanel = expertPanel.getMainPanel();
                        } else {
                            parentPanel = sectionPanel;
                        }
                    }
                } else {
                    if ((entry instanceof ParameterEntry) && entry.isExpert() && (objectExpertPanel != null)) {
                        expertPanel = objectExpertPanel;
                        parentPanel = expertPanel.getMainPanel();
                    } else {
                        parentPanel = objectPanel;
                    }
                }
            }

            if ((description == null) && (entry.getComment() != null) && !entry.getComment().trim().isEmpty()) {
                description = entry.getComment();
            }
            if (description == null) {
                description = NO_DESCRIPTION_AVAILABLE;
            } else {
                description = description.trim();
                if (description.isEmpty()) {
                    description = NO_DESCRIPTION_AVAILABLE;
                } else {
                    editorComponent.setToolTipText(description);
                    if (nameLabel != null) {
                        nameLabel.setToolTipText(description);
                    }
                }
            }

            final JButton questionButton, resetButton;
            if (autoAdd) {
                questionButton = new JButton(Utilities.HELP_ICON);
                questionButton.setOpaque(false);
                questionButton.getModel().addChangeListener(e -> {
                    if (questionButton.getModel().isArmed()) {
                        questionButton.setIcon(Utilities.HELP_ICON_PRESSED);
                    } else {
                        questionButton.setIcon(Utilities.HELP_ICON);
                    }
                    Container parent = questionButton.getParent();
                    if (parent instanceof JComponent) {
                        parent.repaint();
                    }
                });
                Dimension size = questionButton.getPreferredSize();
                questionButton
                        .setBorder(new RoundedBorder(parentPanel == null ? Color.WHITE : parentPanel.getBackground(),
                                size.width, size.height, new BasicStroke(2)));
                questionButton.setOpaque(false);
                questionButton.setVisible(UIDelegate.mayDisplayEntryComponent(entry, configName));

                questionButton.addMouseListener(new ButtonDecorationListener(
                        new RoundedBorder(Color.DARK_GRAY, size.width, size.height, new BasicStroke(2))));
                final String msg = description;
                questionButton.addActionListener(e -> {
                    JOptionPane.showMessageDialog(questionButton, msg, String.format(DESCRIPTION, nameLabel.getText()),
                            JOptionPane.INFORMATION_MESSAGE);
                });
                if ((!Utilities.CURRENT_CONFIG.equals(configName)) && (entry instanceof ParameterEntry)
                        && (editorComponent instanceof IEntryComponent) && (parameter != null)
                        && (parameter.getDefaultValue() != null) && (!parameter.getDefaultValue().trim().isEmpty())) {
                    IEntryComponent entryComponent = (IEntryComponent) editorComponent;
                    resetButton = new JButton(Utilities.RESET_ICON);
                    resetButton.setMargin(CometeUtils.getzInset());
                    resetButton.setToolTipText(Utilities.RESET);
                    final Parameter param = parameter;
                    final JComponent component = editorComponent;
                    resetButton.addActionListener(e -> {
                        try {
                            if (!ObjectUtils.sameObject(entry.getValue(), param.getDefaultValue())) {
                                entryComponent.resetToDefault(param.getDefaultValue());
                                fireEditionStateChanged(true, entry, entryContainer, param.getDefaultValue(), component,
                                        nameLabel, objectSelector, tabTitle);
                            }
                        } catch (FSParsingException e1) {
                            String errorMessage = Utilities.getMessage("flyscan.error.value.default.invalid");
                            Utilities.LOGGER.error(errorMessage, e1);
                            UIDelegate.displayError(entry, entryContainer, null, nameLabel, component, objectSelector,
                                    errorMessage, tabTitle, errorPanel, errorsMap, this);
                        }
                    });
                } else {
                    resetButton = null;
                }
            } else {
                questionButton = resetButton = null;
            }

            if (autoAdd) {
                int x = 0, y = Utilities.getNextY(parentPanel);

                GridBagConstraints questionButtonConstraints = new GridBagConstraints();
                questionButtonConstraints.fill = GridBagConstraints.NONE;
                questionButtonConstraints.gridx = x++;
                questionButtonConstraints.gridy = y;
                questionButtonConstraints.weightx = 0;
                questionButtonConstraints.weighty = 0;
                questionButtonConstraints.anchor = GridBagConstraints.WEST;
                questionButtonConstraints.insets = new Insets(5, 5, 5, 5);

                GridBagConstraints labelConstraints = new GridBagConstraints();
                labelConstraints.fill = GridBagConstraints.HORIZONTAL;
                labelConstraints.gridx = x++;
                labelConstraints.gridy = y;
                labelConstraints.weightx = 0;
                labelConstraints.weighty = 0;
                labelConstraints.anchor = GridBagConstraints.WEST;
                labelConstraints.insets = new Insets(5, 0, 5, 5);

                GridBagConstraints resetButtonConstraints = new GridBagConstraints();
                resetButtonConstraints.fill = GridBagConstraints.NONE;
                resetButtonConstraints.gridx = x++;
                resetButtonConstraints.gridy = y;
                resetButtonConstraints.weightx = 0;
                resetButtonConstraints.weighty = 0;
                resetButtonConstraints.anchor = GridBagConstraints.WEST;
                resetButtonConstraints.insets = new Insets(5, 0, 5, 5);

                GridBagConstraints editorConstraints = new GridBagConstraints();
                editorConstraints.fill = GridBagConstraints.HORIZONTAL;
                editorConstraints.gridx = x++;
                editorConstraints.gridy = y++;
                editorConstraints.weightx = 1;
                editorConstraints.weighty = 0;
                editorConstraints.anchor = GridBagConstraints.WEST;
                editorConstraints.insets = new Insets(5, 0, 5, 5);

                if (parentPanel != null) {
                    parentPanel.add(questionButton, questionButtonConstraints);
                    if (resetButton != null) {
                        parentPanel.add(resetButton, resetButtonConstraints);
                    }
                    parentPanel.add(nameLabel, labelConstraints);
                    parentPanel.add(editorComponent, editorConstraints);
                    if ((expertPanel != null) && !expert) {
                        expertPanel.setVisible(false);
                    }
                }
            }
            editorComponent.setEnabled(enabled && isEditable);
            editorComponent.setVisible(UIDelegate.mayDisplayEntryComponent(entry, configName));
        } // end if (editorComponent != null)

        if (nameLabel != null) {
            nameLabel.setVisible(UIDelegate.mayDisplayEntryComponent(entry, configName));
        }
        if ((componentsPair != null) && (editorComponent != null) && (nameLabel != null)) {
            componentsPair.put(editorComponent, nameLabel);
        }
        editorAndException[0] = editorComponent;
        editorAndException[1] = exception;
    }

    private IdManager getIdManager(FSObject fsObject) {
        IdManager manager = null;
        if (fsObject != null) {
            for (IdManager idManager : idManagers) {
                if (idManager.getObject() == fsObject) {
                    manager = idManager;
                    break;
                }
            }
        }
        return manager;
    }

    protected void registerObject(FSObject fsObject, JComponent... components) {
        if (fsObject != null) {
            // SCAN-967: don't use FSObject as key. Prefer a String instead.
            IdManager manager = getIdManager(fsObject);
            if (manager == null) {
                manager = new IdManager(fsObject);
                idManagers.add(manager);
            }
            objectComponentsMap.put(manager.getId(), components);
        }
    }

    protected ExpertPanel buildObjectPanel(String title, boolean[] added, FSObject fsObject, Section section,
            Collection<Plugin> plugins, ButtonGroup group, JPanel infoPanel, JPanel listPanel, JPanel sectionPanel,
            ExpertPanel sectionExpertPanel, CardLayout layout, JRadioButton sectionSelector, JLabel nameLabel,
            JLabel typeLabel, JLabel enableLabel) {
        ExpertPanel objectExpertPanel;
        if (fsObject == null) {
            objectExpertPanel = null;
        } else {
            JScrollPane objectPanelScrollPane = null;
            JComponent nameComponent = null, typeComponent = null, enableComponent = null;
            JComponent topGlue = UIDelegate.createGlue(false, UIDelegate.HALF_GAP);
            JComponent glue1 = UIDelegate.createGlue(true, UIDelegate.GAP);
            JComponent glue2 = UIDelegate.createGlue(true, UIDelegate.GAP);
            JComponent glue3 = UIDelegate.createGlue(true, UIDelegate.GAP);
            JComponent bottomGlue = UIDelegate.createGlue(false, UIDelegate.HALF_GAP);
            JRadioButton selector = createSelector(group);
            JPanel objectPanel = new JPanel(new GridBagLayout());
            objectExpertPanel = new ExpertPanel();
            objectExpertPanel.setName(fsObject.getName() + objectExpertPanel.getBorder().getTitle());
            if (infoPanel != null) {
                objectPanelScrollPane = new FSScrollPane(objectPanel);
                infoPanel.add(objectPanelScrollPane, fsObject.getName());
            }
            // Choose plugin
            Plugin pluginObj = getPluginAndProcessParamsWithDefaultValues(plugins, section, fsObject, Utilities.GENERAL,
                    configuration.getName(), selector);
            CustomParameterPanel customParametersPanel;
            if (title == null) {
                customParametersPanel = null;
            } else {
                customParametersPanel = (pluginObj == null ? null
                        : UIDelegate.buildCustomParametersPanel(false, title, fsObject, selector, enabled,
                                configuration, this));
                Object[] editorAndException = null;
                for (final Entry entry : fsObject.getEntries()) {
                    if ((!entry.isExpert()) || expert) {
                        editorAndException = new Object[2];
                        buildEntryGUI(configuration.getName(), objectPanel, objectExpertPanel, objectPanelScrollPane,
                                sectionPanel, sectionExpertPanel, selector, sectionSelector, fsObject, section,
                                pluginObj, entry, title, customParametersPanel, true, null, editorAndException);
                        if (ParsingUtil.NAME.equalsIgnoreCase(entry.getKey())) {
                            if (editorAndException[0] instanceof JComponent) {
                                nameComponent = (JComponent) editorAndException[0];
                            }
                        } else if (ParsingUtil.TYPE.equalsIgnoreCase(entry.getKey())) {
                            if (editorAndException[0] instanceof JComponent) {
                                typeComponent = (JComponent) editorAndException[0];
                            }
                        } else if (ParsingUtil.ENABLE.equalsIgnoreCase(entry.getKey())) {
                            if (editorAndException[0] instanceof JComponent) {
                                enableComponent = (JComponent) editorAndException[0];
                            }
                        }
                        if (editorAndException[1] instanceof FSParsingException) {
                            FSParsingException e = (FSParsingException) editorAndException[1];
                            Utilities.LOGGER.error(e.getMessage());
                            UIDelegate.displayError(entry, section, fsObject, errorLabels.get(e),
                                    errorComponents.get(e), selector, e.getMessage(), title, errorPanel, errorsMap,
                                    this);
                        }
                    }
                }
            } // end if (title == null) ... else
            UIDelegate.addParametersPanel(objectPanel, customParametersPanel, 5, true);
            UIDelegate.setupAndAddComponents(listPanel, nameLabel, typeLabel, enableLabel, selector, nameComponent,
                    typeComponent, enableComponent, topGlue, glue1, glue2, glue3, bottomGlue);
            buttonObjectMap.put(selector, fsObject);
            registerObject(fsObject, selector, nameComponent, typeComponent, enableComponent, topGlue, glue1, glue2,
                    glue3, bottomGlue, objectPanel, objectPanelScrollPane);
            UIDelegate.prepareHighlighting(selector, listPanel, infoPanel, layout, fsObject, nameComponent,
                    typeComponent, enableComponent, topGlue, glue1, glue2, glue3, bottomGlue);
            if ((added != null) && !added[0]) {
                added[0] = true;
                selector.setSelected(true);
                glue1.grabFocus();
            }
            UIDelegate.addParametersPanel(objectPanel, objectExpertPanel, 20, false);
            UIDelegate.addFinalGlue(objectPanel);
        } // end if (fsObject == null) ... else
        return objectExpertPanel;
    }

    public void addBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.add(ConfigurationEditionStateListener.class, listener);
    }

    public void removeBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.remove(ConfigurationEditionStateListener.class, listener);
    }

    public ConfigurationEditionStateListener[] getBeingEditedListeners() {
        return listeners.getListeners(ConfigurationEditionStateListener.class);
    }

    public void fireEditionStateChanged(boolean isBeingEdited, Entry entry, EntryContainer entryContainer, String value,
            JComponent component, JLabel nameLabel, JRadioButton selector, String tabTitle) {
        if (component != null) {
            if ((entry != null) && isBeingEdited) {
                entry.setPresentInConfig(true);
            }
            removeErrorTextComponent(component);

            Border border = component.getBorder();
            if (border instanceof LineBorder) {
                LineBorder lb = (LineBorder) border;
                if (Color.RED.equals(lb.getLineColor())) {
                    // Apply default border if current one is the error border
                    Border defaultBorder;
                    try {
                        defaultBorder = component.getClass().newInstance().getBorder();
                    } catch (Exception e) {
                        defaultBorder = null;
                    }
                    component.setBorder(defaultBorder);
                }
            }
            if (nameLabel != null) {
                nameLabel.setForeground(Color.BLACK);
            }
        }

        // If its a recording entry, verify that only one of the three predefined values is different from zero.
        String key = entry.getKey();
        if (recordingComponents != null) {
            if (component instanceof WheelSwitch
                    && (ParsingUtil.SPLIT.equalsIgnoreCase(key)
                            || ParsingUtil.MAX_SCAN_LINES_PER_FILE.equalsIgnoreCase(key)
                            || ParsingUtil.MAX_STEPS_PER_FILE.equalsIgnoreCase(key))
                    && (entryContainer instanceof Section)) {
                WheelSwitch ws = ((WheelSwitch) component);
                int nb = 0;
                for (JComponent c : recordingComponents.keySet()) {
                    if (c instanceof WheelSwitch) {
                        WheelSwitch w = ((WheelSwitch) c);
                        if (w.getValue() > 0) {
                            nb++;
                        }
                    }
                }
                if (nb > 1) {
                    UIDelegate.displayError(entry, entryContainer, null, nameLabel, ws, selector,
                            RECORDING_ENTRIES_MUTUALLY_EXCLUSIVE_ERROR, Utilities.GENERAL, errorPanel, errorsMap, this);
                } else {
                    for (java.util.Map.Entry<JComponent, JLabel> ent : recordingComponents.entrySet()) {
                        JTextComponent labelE = errorsMap.get(ent.getKey());
                        if (labelE != null) {
                            errorPanel.remove(labelE);
                            errorsMap.remove(ent.getKey());
                            ent.getKey().setBorder(null);
                            errorPanel.revalidate();
                            ((JComponent) errorPanel.getParent()).revalidate();
                        }
                    }

                }
            } else if (component instanceof Spinner
                    && (ParsingUtil.SPLIT.equalsIgnoreCase(key)
                            || ParsingUtil.MAX_SCAN_LINES_PER_FILE.equalsIgnoreCase(key)
                            || ParsingUtil.MAX_STEPS_PER_FILE.equalsIgnoreCase(key))
                    && (entryContainer instanceof Section)) {
                Spinner ws = ((Spinner) component);
                int nb = 0;
                for (JComponent c : recordingComponents.keySet()) {
                    if (c instanceof Spinner) {
                        Spinner w = ((Spinner) c);
                        if (w.getNumberValue().longValue() > 0) {
                            nb++;
                        }
                    }
                }
                if (nb > 1) {
                    UIDelegate.displayError(entry, entryContainer, null, nameLabel, ws, selector,
                            RECORDING_ENTRIES_MUTUALLY_EXCLUSIVE_ERROR, Utilities.GENERAL, errorPanel, errorsMap, this);
                } else {
                    for (java.util.Map.Entry<JComponent, JLabel> ent : recordingComponents.entrySet()) {
                        JTextComponent labelE = errorsMap.get(ent.getKey());
                        if (labelE != null) {
                            errorPanel.remove(labelE);
                            errorsMap.remove(ent.getKey());
                            ent.getKey().setBorder(null);
                            errorPanel.revalidate();
                            ((JComponent) errorPanel.getParent()).revalidate();
                        }
                    }

                }
            }
        } // end if (recordingComponents != null)

        // Verify if all defined constraints are satisfied
        Collection<Constraint> constraints = entry.getConstraints();
        Collection<Constraint> nonSatisfiedConstraints = UIDelegate.getNonSatisfiedConstraints(entry, value,
                constraints);
        if (!nonSatisfiedConstraints.isEmpty()) {
            String location = UIDelegate.getErrorLocation(entry, entryContainer);
            warnConstraintsAreNotSatisfied(tabTitle, component, nameLabel, selector, nonSatisfiedConstraints, location);
        }
        // SCAN-624: always apply value, for coherence between GUI and flyscan configuration
        UIDelegate.applyValue(isBeingEdited, entry, value, configuration, this);
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled() && enabled;
    }

    public void displayEasyConfigOnly(boolean easyConfigOnly) {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            if (tabbedPane != null) {
                if (easyConfigOnly) {
                    tabbedPane.hideAllTabsBut(Utilities.EASY_CONFIG);
                } else {
                    tabbedPane.showAllTabs();
                }
            }
        });
    }

    @Override
    protected void finalize() {
        actorSection = null;
        errorsMap.clear();
        recordingComponents.clear();
        errorLabels.clear();
        errorComponents.clear();
        buttonObjectMap.clear();
        objectComponentsMap.clear();
        idManagers.clear();
        tabbedPane.clearMemory();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Hack to avoid a problem detected in SCAN-967:<br />
     * When someone clicks on "Enable" button, it changes the content of the associated actor, which means it changes
     * the FSObject hashcode.<br />
     * Based on this, an FSObject can't be put as a key of a Map.<br />
     * Instead, a customized {@link String} should be used as key.<br />
     * {@link IdManager} is here to manage this customized {@link String}.
     * 
     * @author Rapha&euml;l GIRARDOT
     */
    private static final class IdManager {
        private FSObject object;
        private String id;

        public IdManager(FSObject object) {
            this.object = object;
            this.id = CometeDefinitionUtils.generateIdForClass(object.getClass(), 50000);
        }

        public FSObject getObject() {
            return object;
        }

        public String getId() {
            return id;
        }

        public void clear() {
            object = null;
            id = null;
        }

        @Override
        protected void finalize() {
            clear();
        }
    }
}
