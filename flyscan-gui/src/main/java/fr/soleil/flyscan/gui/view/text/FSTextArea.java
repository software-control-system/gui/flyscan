package fr.soleil.flyscan.gui.view.text;

import java.awt.Color;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.flyscan.gui.FlyScanTangoBox;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

public class FSTextArea extends TextArea implements IComponent {

    private static final long serialVersionUID = -310999316892397767L;

    private final DynamicForegroundLabel deviceLabel1;

    public FSTextArea(DynamicForegroundLabel deviceLabel1) {
        super();
        this.deviceLabel1 = deviceLabel1;
        super.setCometeBackground(CometeColor.WHITE);
        scrollPane.getVerticalScrollBar().setUnitIncrement(FlyScanTangoBox.SCROLL_SPEED);
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        applyTooltip(text);
    }

    protected void applyTooltip(String text) {
        setToolTipText(computeTooltip(text));
    }

    protected String computeTooltip(String text) {
        String tooltip;
        if ((text == null) || (text.trim().isEmpty())) {
            tooltip = text;
        } else {
            StringBuilder builder = new StringBuilder(Utilities.HTML_BODY);
            // builder.append(HtmlEscape.escape(text, true, true).replace("\n", "<br />"));

            // character escaping not well supported by tooltip
            builder.append(text.replace(Utilities.NEW_LINE, Utilities.NEW_HTML_LINE));
            builder.append(Utilities.BODY_HTML);
            tooltip = builder.toString();
        }
        return tooltip;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        Color fg = ColorTool.getColor(color);
        if (deviceLabel1 != null) {
            deviceLabel1.setBackground(fg);
        }
    }

}
