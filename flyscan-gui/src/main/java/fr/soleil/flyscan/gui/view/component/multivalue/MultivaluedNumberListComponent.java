package fr.soleil.flyscan.gui.view.component.multivalue;

import java.util.Collection;

import javax.swing.ListCellRenderer;

import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;

public class MultivaluedNumberListComponent extends AMultivaluedListComponent<Number> {

    private static final long serialVersionUID = 3023511991318084889L;

    protected final boolean intOnly;

    public MultivaluedNumberListComponent(Collection<Number> possibleValues, Collection<Number> values, int count,
            boolean areCBExclusive, Class<? extends ListCellRenderer<? super Number>> rendererClass,
            boolean mayThrowException, boolean intOnly) throws FSParsingException {
        super(Number.class, possibleValues, values, count, areCBExclusive, rendererClass, mayThrowException);
        this.intOnly = intOnly;
    }

    @Override
    protected boolean sameValue(Number value1, Number value2) {
        return ParsingTool.sameValue(value1, value2);
    }

    @Override
    protected String toString(Number value) {
        return String.valueOf(value);
    }

    @Override
    protected Number parse(String value) {
        return intOnly ? Long.valueOf(ParsingTool.parseLong(value)) : Double.valueOf(ParsingTool.parseDouble(value));
    }

}
