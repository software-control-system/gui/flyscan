package fr.soleil.flyscan.gui.view.component.multivalue;

import java.util.Collection;
import java.util.EventListener;

import javax.swing.JComboBox;
import javax.swing.ListCellRenderer;

import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.view.combo.DevStateComboBox;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;

/**
 * Component allowing to edit a multivalued list
 * 
 * @author guerre-giordano
 * 
 */
public class MultivaluedStringListComponent extends AMultivaluedListComponent<String> {

    private static final long serialVersionUID = -8046648795334594065L;

    private boolean useDevStateRenderer;

    public MultivaluedStringListComponent(Collection<String> possibleValues, Collection<String> values, int count,
            boolean useDevStateRenderer, boolean areCBExclusive,
            Class<? extends ListCellRenderer<? super String>> rendererClass, boolean mayThrowException)
            throws FSParsingException {
        super(String.class, possibleValues, values, count, areCBExclusive, rendererClass, mayThrowException,
                Boolean.valueOf(useDevStateRenderer));
    }

    @Override
    protected void initBeforeValues(Object... otherData) {
        super.initBeforeValues(otherData);
        this.useDevStateRenderer = ((Boolean) otherData[3]).booleanValue();
    }

    @Override
    protected EventListener prepareListener(final JComboBox<String> component) {
        EventListener listener;
        if (useDevStateRenderer) {
            DevStateComboBox combo = (DevStateComboBox) component;
            ConfigurationEditionStateListener tmp = new ConfigurationEditionStateListener() {
                @Override
                public void setConfigurationBeingEdited(boolean isBeingEdited) {
                    refreshUsedValuesListAndCombos(component);
                }
            };
            listener = tmp;
            combo.addBeingEditedListener(tmp);
        } else {
            listener = super.prepareListener(component);
        }
        return listener;
    }

    @Override
    protected void stopListening(EventListener listener, JComboBox<String> component) {
        if (useDevStateRenderer) {
            ((DevStateComboBox) component).removeBeingEditedListener((ConfigurationEditionStateListener) listener);
        } else {
            super.stopListening(listener, component);
        }
    }

    @Override
    protected JComboBox<String> createComponent() {
        JComboBox<String> component;
        if (useDevStateRenderer) {
            Collection<String> unusedValues = getUnusedValues();
            component = new DevStateComboBox(unusedValues);
        } else {
            component = super.createComponent();
        }
        return component;
    }

    @Override
    protected String getValue(JComboBox<String> component) {
        return ParsingTool.toString(component.getSelectedItem());
    }

    @Override
    protected String toString(String value) {
        return value;
    }

    @Override
    protected boolean sameValue(String value1, String value2) {
        boolean same;
        if (value1 == null) {
            same = (value2 == null);
        } else {
            same = value1.equalsIgnoreCase(value2);
        }
        return same;
    }

    @Override
    protected String parse(String value) {
        return value;
    }

}
