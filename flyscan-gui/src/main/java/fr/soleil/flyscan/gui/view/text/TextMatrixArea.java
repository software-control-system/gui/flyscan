package fr.soleil.flyscan.gui.view.text;

import javax.swing.JTextArea;

import fr.soleil.comete.definition.data.target.matrix.ITextMatrixComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;

/**
 * Text area for data of text matrix
 * 
 * @author guerre-giordano
 * 
 */
public class TextMatrixArea extends JTextArea implements ITextMatrixComponent {

    private static final long serialVersionUID = 7785088133830758332L;

    private final AbstractMatrix<String> matrix;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;

    public TextMatrixArea() {
        super();
        matrix = new StringMatrix();
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
    }

    @Override
    public String[][] getStringMatrix() {
        String[][] result = null;
        if (matrix != null) {
            result = (String[][]) matrix.getValue();
        }
        return result;
    }

    @Override
    public void setStringMatrix(String[][] array) {
        StringMatrix result = new StringMatrix();
        result.setValue(array);
        setText(result);
    }

    private void setText(StringMatrix result) {
        if (result != null && result.getValue() != null) {
            StringBuilder sb = new StringBuilder();
            for (String[] tb : result.getValue()) {
                for (String s : tb) {
                    sb.append(s);
                    sb.append(ParsingUtil.CRLF);
                }
            }
            setText(sb.toString());
        }
    }

    @Override
    public String[] getFlatStringMatrix() {
        String[] result = null;
        if (matrix != null) {
            result = (String[]) matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        StringMatrix matrix = new StringMatrix();
        matrix.setFlatValue(value, height, width);
        setText(matrix);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix != null) {
            width = 1;
        }
        return width;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix != null) {
            height = matrix.getWidth();
        }
        return height;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public void setCometeBackground(CometeColor color) {

    }

    @Override
    public CometeColor getCometeBackground() {
        return null;
    }

    @Override
    public void setCometeForeground(CometeColor color) {

    }

    @Override
    public CometeColor getCometeForeground() {
        return null;
    }

    @Override
    public void setCometeFont(CometeFont font) {

    }

    @Override
    public CometeFont getCometeFont() {
        return null;
    }

    @Override
    public void setHorizontalAlignment(int halign) {

    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setPreferredSize(int width, int height) {

    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {

    }

    @Override
    public void addMouseListener(IMouseListener listener) {

    }

    @Override
    public void removeMouseListener(IMouseListener listener) {

    }

    @Override
    public void removeAllMouseListeners() {

    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        return EDTManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return true;
    }

}
