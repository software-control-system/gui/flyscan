package fr.soleil.flyscan.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.prefs.Preferences;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.action.AbstractActionExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.datastorage.helper.Parameters;
import fr.soleil.comete.bean.datastoragebean.DataStorageBean;
import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.AbstractCometeBox.LoggingMode;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.action.ChangeDeviceTimeOutAction;
import fr.soleil.comete.box.listener.DeviceControlPanelLauncher;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.target.ADeviceLogAdapter;
import fr.soleil.comete.box.target.DevicesColorTarget;
import fr.soleil.comete.box.util.DeviceLogViewer;
import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.MatrixPanel;
import fr.soleil.comete.swing.ProgressBar;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoCommandHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GroupRefreshingStrategy;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.gui.data.ScanType;
import fr.soleil.flyscan.gui.data.target.FlyScanInfoTarget;
import fr.soleil.flyscan.gui.data.target.FlyScanTarget;
import fr.soleil.flyscan.gui.data.target.PluginsTarget;
import fr.soleil.flyscan.gui.listener.ButtonDecorationListener;
import fr.soleil.flyscan.gui.listener.ConfigSelectionListener;
import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.util.IConfigNameChecker;
import fr.soleil.flyscan.gui.util.IFlyScanConstants;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.component.ConfigurationTree;
import fr.soleil.flyscan.gui.view.component.ConfigurationsPanel;
import fr.soleil.flyscan.gui.view.component.DynamicForegroundTabLabel;
import fr.soleil.flyscan.gui.view.component.FSInternalStateViewer;
import fr.soleil.flyscan.gui.view.component.FSLabel;
import fr.soleil.flyscan.gui.view.component.FSProgressBar;
import fr.soleil.flyscan.gui.view.component.TabLabel;
import fr.soleil.flyscan.gui.view.component.TextTabComponent;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;
import fr.soleil.flyscan.gui.view.dialog.ConfigCreator;
import fr.soleil.flyscan.gui.view.menu.FlyScanMenu;
import fr.soleil.flyscan.gui.view.scrollpane.FSScrollPane;
import fr.soleil.flyscan.gui.view.tabbedpane.BoldTitleTabbedPane;
import fr.soleil.flyscan.gui.view.tabbedpane.CloseSaveButtonTabbedPane;
import fr.soleil.flyscan.gui.view.tabbedpane.CloseSaveButtonTabbedPane.CloseSaveButtonTab;
import fr.soleil.flyscan.gui.view.text.FSTextArea;
import fr.soleil.flyscan.gui.view.text.TextMatrixArea;
import fr.soleil.flyscan.lib.model.parsing.configuration.ConfigParser;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.FSObject;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.ContextMonitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Hook;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Monitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Sensor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.TimeBase;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.AcquisitionSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.InfoSection;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.plugin.PluginParser;
import fr.soleil.flyscan.lib.model.parsing.plugin.PluginType;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.logging.LogAppender;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.awt.FontUtils;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;
import fr.soleil.lib.project.swing.text.JSmoothLabel;
import fr.soleil.lib.project.swing.text.RotatedLabel;
import fr.soleil.lib.project.swing.text.RotatedLabel.Direction;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;
import fr.soleil.tango.clientapi.InsertExtractUtils;

/**
 * Fly Scan Tango box
 * 
 * @author guerre-giordano
 */
public class FlyScanTangoBox extends AbstractTangoBox implements IFlyScanConstants {

    private static final long serialVersionUID = -687786777635070267L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(FlyScanTangoBox.class.getName());

    protected static final String CONFIG_LIST = "configList";
    protected static final String CONFIGURATION = "configuration";
    protected static final String CONFIGURATION_ATTRIBUTE = "/configuration";
    protected static final String MONITORED_DEVICE_NAMES = "monitoredDeviceNames";
    protected static final String MONITORED_DEVICE_STATES = "monitoredDeviceStates";
    protected static final String INFO = "info";
    protected static final String PLUGINS_INFO = "pluginsInfo";
    protected static final String SCAN_PROGRESS = "scanProgress";
    protected static final String SCAN_POSITION = "scanPosition";
    protected static final String SCAN_START_DATE = "scanStartDate";
    protected static final String SCAN_END_DATE = "scanEndDate";
    protected static final String SCAN_REMAINING_TIME = "scanRemainingTime";
    protected static final String SCAN_ELAPSED = "scanElapsedTime";
    protected static final String SCAN_ESTIMATED_TIME = "scanEstimatedTime";
    protected static final String ACTORS_EXEC_TIMES = "actorsExecTimes";
    protected static final String INTERNAL_STATE = "internalState";
    protected static final String STATUS = "Status";
    protected static final String LOG = "log";
    protected static final String CLEAR_LOG = "ClearLog";
    protected static final String FLYSCAN_START = "flyscan.start";
    protected static final String FLYSCAN_RESUME = "flyscan.resume";
    protected static final String FLYSCAN_STOP = "flyscan.stop";
    protected static final String FLYSCAN_PAUSE = "flyscan.pause";
    protected static final String FLYSCAN_LOAD = "flyscan.load.config";
    protected static final String START = "start";
    protected static final String RESUME = "resume";
    protected static final String PAUSE = "pause";
    protected static final String STATE = "state";
    protected static final String READ_CONFIG = "ReadConfig";
    protected static final String CHECK_CONFIG = "CheckConfig";
    protected static final String EVALUATE_CONFIG = "EvaluateConfig";
    protected static final String WRITE_CONFIG = "WriteConfig";
    protected static final String LOAD_CONFIG = "LoadConfig";
    protected static final String DELETE_CONFIG = "DeleteConfig";
    protected static final String CFG_EXTENSION = ".cfg";
    protected static final String TXT_EXTENSION = ".txt";
    protected static final String DUAL_ARROW = "\u2194";
    protected static final String SINGLE_ARROW = "\u2192";
    protected static final String N_A = "N/A";

    protected static final String CHANGE_TIMEOUT_CONFIGURATION = Utilities
            .getMessage("flyscan.configuration.timeout.change");
    protected static final String TIMEOUT_CONFIGURATION = Utilities.getMessage("flyscan.configuration.timeout");
    protected static final String FSS = "FSS";
    protected static final String FSS_TITLE = "FSS:";
    protected static final String STARTING_SELECTED_CONFIGURATION = Utilities
            .getMessage("flyscan.configuration.selected.starting");
    protected static final String LOADING_SELECTED_CONFIGURATION = Utilities
            .getMessage("flyscan.configuration.selected.loading");
    protected static final String STATUS_TITLE = Utilities.getMessage("flyscan.status.title");
    protected static final String DEVICE_LOG_TITLE = Utilities.getMessage("flyscan.log.device");
    protected static final String ACTORS_EXEC_TIME_TITLE = Utilities.getMessage("flyscan.actor.exec.time");
    protected static final String APPLICATION_LOG_TITLE = Utilities.getMessage("flyscan.log.application");
    protected static final String START_SELECTED_CONFIG = Utilities.getMessage("flyscan.configuration.selected.start");
    protected static final String NO_START_FOR_CONFIG = Utilities.getMessage("flyscan.configuration.selected.no");
    protected static final String NO_START_FOR_STATE = Utilities.getMessage("flyscan.scan.running.already");
    protected static final String CURRENT_CONFIG_ERROR = Utilities
            .getMessage("flyscan.error.configuration.current.read");
    protected static final String MONITORED_DEVICES = Utilities.getMessage("flyscan.device.monitored");
    protected static final String LOG_COPY = Utilities.getMessage("flyscan.log.copy");
    protected static final String INTERNAL_STATE_TITLE = Utilities.getMessage("flyscan.state.internal");
    protected static final String NO_CONFIGURATION = Utilities.getMessage("flyscan.configuration.no.decorated");
    protected static final String WRITE_ATTRIBUTE_START_FORMAT = Utilities.getMessage("flyscan.attribute.write.start");
    protected static final String WRITE_ATTRIBUTE_VALUE_FORMAT = Utilities.getMessage("flyscan.attribute.write.value");
    protected static final String MS = "ms)";
    protected static final String TITLE_SEPARATOR = ":\n";

    protected static final PolledRefreshingStrategy DEFAULT_STRATEGY = new PolledRefreshingStrategy(1000);
    // SCAN-835: Use a group strategy for monitored devices
    protected static final GroupRefreshingStrategy MONITORED_DEVICES_STARTEGY = new GroupRefreshingStrategy(
            MONITORED_DEVICES, 1033);

    protected static final Dimension ERROR_SIZE = new Dimension(0, 60);

    protected static final int DEFAULT_FSS_TIMEOUT = 10000;

    protected static final int GAP = 30;
    protected static final int LABEL_MARGIN;
    public static final int SCROLL_SPEED;
    static {
        String prop = System.getProperty("LabelMargin");
        int value = 10;
        if (prop != null) {
            try {
                value = Math.abs(Integer.parseInt(prop));
            } catch (Exception e) {
                value = 10;
            }
        }
        LABEL_MARGIN = value;
        value = 1;
        prop = System.getProperty("ScrollSpeed");
        if (prop != null) {
            try {
                value = Math.abs(Integer.parseInt(prop));
            } catch (Exception e) {
                value = 1;
            }
        }
        SCROLL_SPEED = value;
        // SCAN-810: deactivate "Data Connection Error" popup
        AbstractCometeBox.setLoggingMode(LoggingMode.CONSOLE);
    }

    protected final boolean isExpert;

    protected final Map<String, ImageIcon> labelIconsMap;

    protected final ChangeFlyScanTimeOutAction timeOutAction;
    protected final SetEasyConfigOnlyAction easyConfigOnlyAction;
    protected final JPanel configDevicePanel;
    protected final PluginsTarget pluginTarget;
    protected final FlyScanInfoTarget infoTarget;
    protected final FlyScanTarget currentConfigTarget;
    protected final ConfigNameChecker configNameChecker;
    protected final CloseSaveButtonTabbedPane configTabbedPane;
    protected final StringMatrixBox stringMatrixBox;
    protected DeviceLogViewer logPanel;
    protected TextMatrixArea actorsExecTimesTextArea;
    protected JButton actorsExecTimesCopyButton;
    protected LogPauseButton actorsExecTimesPauseButton;
    protected final LogViewer applicationLogPanel;
    protected final LogAppender applicationLogAppender;
    protected final LogAppender cometeLogAppender;
    protected ADeviceLogAdapter<?> logAdapter;

    protected final ProgressBar scanProgressBar;

    protected final ScanPositionLabel scanPositionLabel;
    protected final FSLabel currentConfigNameLabel;
    protected final JPanel scanPositionPanel;
    protected final JPanel scanTimePanel;
    protected final Label scanStartDateLabel;
    protected final Label scanEndDateLabel;
    protected final Label scanRemainingTimeLabel;
    protected final Label scanElapsedLabel;
    protected final Label scanEstimatedTimeLabel;

    protected final MonitoredDevicesPanel monitoredDevices;
    protected final DeviceControlPanelLauncher monitoredDevicesListener;
    protected final DeviceControlPanelLauncher modelListener;
    protected DevicesColorTarget monitoredDeviceStatesTarget;

    protected final JPanel currentConfigNamePanel;
    protected final JPanel inernalStatePanel;
    protected final JPanel scanStartDatePanel;
    protected final JPanel scanEndDatePanel;
    protected final JPanel scanRemainingTimePanel;
    protected final JPanel scanElapsedPanel;
    protected final JPanel scanEstimatedTimePanel;

    protected final JPanel buttonPanel;
    protected final JPanel startPanel;
    protected final CardLayout startLayout;
    protected final StringButton startButton;
    protected final StringButton loadButton;
    protected final StartStateTarget startStateTarget;
    protected final StringButton stopButton;
    protected final StringButton pauseButton;
    protected final StringButton resumeButton;

    protected FSInternalStateViewer internalStateViewer;
    protected final DynamicForegroundLabel deviceLabel1;
    protected final DynamicForegroundLabel statusTitle;
    protected final FSTextArea statusText;

    protected ConfigurationsPanel configurationsPanel;
    protected JSplitPane configurationsSplitPane;

    protected ProgressDialog progressDialog;

    protected JMenu mockMenu;

    protected final BoldTitleTabbedPane bottomTabbedPane;
    protected final JSplitPane configsAndMessagesSplitPane;
    protected final JPanel mainPanel;

    protected volatile boolean startAllowed;

    protected volatile boolean easyConfigOnly;
    protected volatile int fssTimeout;

    protected final DataStorageBean dataStorageBean;

    /**
     * Constructor
     */
    public FlyScanTangoBox() {
        this(false, false);
    }

    /**
     * Constructor.
     * 
     * @param expert Whether to use expert mode.
     * @param useMock Whether mock configurations are allowed.
     */
    public FlyScanTangoBox(boolean expert, boolean useMock) {
        super();
        // Initialize default profile for DataStorageBean (SCAN-968)
        Parameters.getInstance().setProfile(Optional.ofNullable(ParsingUtil.DEFAULT_VALUE));
        dataStorageBean = DataStorageBean.instance();
        this.isExpert = expert;
        // Force default timeout to 10s (SCAN-488)
        fssTimeout = DEFAULT_FSS_TIMEOUT;
        labelIconsMap = new ConcurrentHashMap<>();
        startAllowed = true;
        timeOutAction = new ChangeFlyScanTimeOutAction();
        timeOutAction.putValue(ChangeDeviceTimeOutAction.NAME, String.format(CHANGE_TIMEOUT_CONFIGURATION, FSS));
        timeOutAction.setDialogTitle(String.format(CHANGE_TIMEOUT_CONFIGURATION, FSS));
        timeOutAction.setTextFieldTooltip(String.format(CHANGE_TIMEOUT_CONFIGURATION, FSS));
        timeOutAction.setTextFieldTitle(String.format(TIMEOUT_CONFIGURATION, FSS));
        easyConfigOnly = false;
        easyConfigOnlyAction = new SetEasyConfigOnlyAction();
        stringMatrixBox = new StringMatrixBox();

        internalStateViewer = new FSInternalStateViewer();

        deviceLabel1 = new DynamicForegroundLabel(NO_DEVICE_STRING);
        deviceLabel1.setFont(Utilities.BASIC_FONT);
        deviceLabel1.setOpaque(true);

        statusTitle = new DynamicForegroundTabLabel(STATUS_TITLE) {

            private static final long serialVersionUID = -4013426863040261483L;

            @Override
            public void setBackground(Color bg) {
                super.setBackground(bg);
                if (isOpaque()) {
                    deviceLabel1.setBackground(bg);
                }
            }
        };
        // Hack to ensure tab selection. Otherwise, it does not work :(
        statusTitle.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                bottomTabbedPane.setSelectedComponent(statusText);
            }
        });
        statusTitle.setOpaque(true);
        statusText = new FSTextArea(statusTitle) {

            private static final long serialVersionUID = 4932460799461130216L;

            @Override
            protected void applyTooltip(String text) {
                String tooltip = computeTooltip(text);
                setToolTipText(null);
                if (bottomTabbedPane != null) {
                    int index = bottomTabbedPane.indexOfComponent(statusText);
                    if (index > -1) {
                        bottomTabbedPane.setToolTipTextAt(index, tooltip);
                    }
                }
                statusTitle.setToolTipText(tooltip);
            }

        };
        statusText.setEditable(false);
        statusText.setRows(4);
        statusText.setMinimumSize(statusText.getPreferredSize());

        scanProgressBar = new FSProgressBar();
        scanProgressBar.setMinimum(0);
        scanProgressBar.setMaximum(100);
        scanPositionLabel = new ScanPositionLabel();
        scanPositionLabel.setFont(Utilities.BASIC_FONT);

        currentConfigNameLabel = new FSLabel();
        currentConfigNameLabel.setFont(Utilities.BASIC_FONT);
        currentConfigNameLabel.setForeground(Utilities.CURRENT_CONFIG_COLOR);

        scanStartDateLabel = new Label();
        scanStartDateLabel.setFont(Utilities.BASIC_FONT);

        scanEndDateLabel = new Label();
        scanEndDateLabel.setFont(Utilities.BASIC_FONT);

        scanRemainingTimeLabel = new Label();
        scanRemainingTimeLabel.setFont(Utilities.BASIC_FONT);

        scanElapsedLabel = new Label();
        scanElapsedLabel.setFont(Utilities.BASIC_FONT);

        scanEstimatedTimeLabel = new Label();
        scanEstimatedTimeLabel.setFont(Utilities.BASIC_FONT);

        monitoredDevicesListener = new DeviceControlPanelLauncher(null);
        monitoredDevices = new MonitoredDevicesPanel();
        // SCAN-835: Use a group strategy for monitored devices
        monitoredDevices.setListenedGroup(MONITORED_DEVICES);
        monitoredDevices.setTitle(MONITORED_DEVICES);
        monitoredDevices.addMouseListener(monitoredDevicesListener);
        monitoredDevices.addMouseMotionListener(monitoredDevicesListener);
        modelListener = new DeviceControlPanelLauncher(deviceLabel1.getFont());
        modelListener.setTextToAvoid(FSS_TITLE);
        getStatusPanel().addMouseListener(modelListener);
        getStatusPanel().addMouseMotionListener(modelListener);

        configDevicePanel = new JPanel(new GridBagLayout());
        configDevicePanel.setBorder(new TitledBorder(Utilities.getMessage("flyscan.scan.control")));

        final JPanel deviceAndCurrentConfigPanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.CENTER, 10, 5));
        deviceAndCurrentConfigPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                deviceAndCurrentConfigPanel.revalidate();
                Container parent = deviceAndCurrentConfigPanel.getParent();
                if (parent instanceof JComponent) {
                    JComponent parentComponent = (JComponent) parent;
                    parentComponent.revalidate();
                    parentComponent.repaint();
                }
            }
        });
        deviceAndCurrentConfigPanel.add(getStatusPanel());
        currentConfigNamePanel = createPanel(Utilities.CURRENT_CONFIG + Utilities.DOUBLE_POINT, currentConfigNameLabel);
        deviceAndCurrentConfigPanel.add(currentConfigNamePanel);
        inernalStatePanel = createPanel(INTERNAL_STATE_TITLE + Utilities.DOUBLE_POINT, internalStateViewer);
        deviceAndCurrentConfigPanel.add(inernalStatePanel);

        startStateTarget = new StartStateTarget();
        buttonPanel = new JPanel(new GridBagLayout());
        startLayout = new CardLayout();
        startPanel = new JPanel(startLayout);
        startButton = createCommandButton(FLYSCAN_START);
        startButton.addButtonListener(new IButtonListener() {
            @Override
            public void actionPerformed(EventObject event) {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(startButton));
                }
                progressDialog.setTitle(STARTING_SELECTED_CONFIGURATION);
                progressDialog.setAlwaysOnTop(true);
                StartWorker worker = new StartWorker();
                progressDialog.setMaxProgress(3);
                progressDialog.setProgressIndeterminate(false);
                progressDialog.setProgress(0, STARTING_SELECTED_CONFIGURATION,
                        Utilities.OPEN_PARENTHESIS
                                + String.format(Utilities.getMessage("flyscan.configuration.timeout"), getModel())
                                + Utilities.DOUBLE_POINT + TangoDeviceHelper.getTimeOutInMilliseconds(getModel()) + MS);
                progressDialog.pack();
                progressDialog.setLocationRelativeTo(startButton);
                progressDialog.setCancelable((ICancelable) worker);
                progressDialog.setVisible(true);
                worker.execute();
            }
        });
        resumeButton = createCommandButton(FLYSCAN_RESUME);
        resumeButton.addButtonListener(new IButtonListener() {
            @Override
            public void actionPerformed(EventObject event) {
                LOGGER.trace(Utilities.getMessage("flyscan.command.resume.call"));
                new StateRefreshWorker(RESUME).execute();
            }
        });
        loadButton = createCommandButton(FLYSCAN_LOAD);

        startPanel.add(startButton, START);
        startPanel.add(resumeButton, RESUME);
        startLayout.show(startPanel, START);

        GridBagConstraints startPanelGridBagConstraints = new GridBagConstraints();
        startPanelGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        startPanelGridBagConstraints.gridx = 0;
        startPanelGridBagConstraints.gridy = 0;
        startPanelGridBagConstraints.weightx = 0.25;
        startPanelGridBagConstraints.weighty = 0;
        startPanelGridBagConstraints.ipadx = 0;
        startPanelGridBagConstraints.anchor = GridBagConstraints.EAST;
        startPanelGridBagConstraints.insets = new Insets(5, 5, 5, 0);
        buttonPanel.add(startPanel, startPanelGridBagConstraints);

        stopButton = createCommandButton(FLYSCAN_STOP);
        stopButton.addButtonListener(new IButtonListener() {
            @Override
            public void actionPerformed(EventObject event) {
                LOGGER.trace(Utilities.getMessage("flyscan.command.abort.call"));
                new StateRefreshWorker(Utilities.ABORT).execute();
            }
        });

        GridBagConstraints stopButtonGridBagConstraints = new GridBagConstraints();
        stopButtonGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        stopButtonGridBagConstraints.gridx = 1;
        stopButtonGridBagConstraints.gridy = 0;
        stopButtonGridBagConstraints.weightx = 0;
        stopButtonGridBagConstraints.weighty = 0;
        stopButtonGridBagConstraints.ipadx = 0;
        stopButtonGridBagConstraints.anchor = GridBagConstraints.EAST;
        stopButtonGridBagConstraints.insets = new Insets(5, 5, 5, 0);
        buttonPanel.add(stopButton, stopButtonGridBagConstraints);

        pauseButton = createCommandButton(FLYSCAN_PAUSE);
        pauseButton.addButtonListener(new IButtonListener() {
            @Override
            public void actionPerformed(EventObject event) {
                LOGGER.trace(Utilities.getMessage("flyscan.command.abort.pause"));
                new StateRefreshWorker(PAUSE).execute();
            }
        });

        GridBagConstraints pauseButtonGridBagConstraints = new GridBagConstraints();
        pauseButtonGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        pauseButtonGridBagConstraints.gridx = 2;
        pauseButtonGridBagConstraints.gridy = 0;
        pauseButtonGridBagConstraints.weightx = 0;
        pauseButtonGridBagConstraints.weighty = 0;
        pauseButtonGridBagConstraints.ipadx = 0;
        pauseButtonGridBagConstraints.anchor = GridBagConstraints.EAST;
        pauseButtonGridBagConstraints.insets = new Insets(5, 5, 5, 0);
        buttonPanel.add(pauseButton, pauseButtonGridBagConstraints);

        scanTimePanel = new JPanel(new VerticalFlowLayout(VerticalFlowLayout.CENTER, 10, 5));
        scanTimePanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                scanTimePanel.revalidate();
                Container parent = scanTimePanel.getParent();
                if (parent instanceof JComponent) {
                    JComponent parentComponent = (JComponent) parent;
                    parentComponent.revalidate();
                    parentComponent.repaint();
                }
            }
        });
        scanStartDatePanel = createPanel(Utilities.getMessage("flyscan.scan.date.start"), scanStartDateLabel);
        scanEndDatePanel = createPanel(Utilities.getMessage("flyscan.scan.date.end"), scanEndDateLabel);
        scanRemainingTimePanel = createPanel(Utilities.getMessage("flyscan.scan.time.remaining"),
                scanRemainingTimeLabel);
        scanElapsedPanel = createPanel(Utilities.getMessage("flyscan.scan.time.elapsed"), scanElapsedLabel);
        scanEstimatedTimePanel = createPanel(Utilities.getMessage("flyscan.scan.time.estimated"),
                scanEstimatedTimeLabel);
        scanTimePanel.add(scanStartDatePanel);
        scanTimePanel.add(scanEndDatePanel);
        scanTimePanel.add(scanRemainingTimePanel);
        scanTimePanel.add(scanElapsedPanel);
        scanTimePanel.add(scanEstimatedTimePanel);

        scanPositionPanel = createPanel(Utilities.getMessage("flyscan.scan.position"), scanPositionLabel);

        int y = 0;

        final GridBagConstraints deviceAndCurrentConfigPanelConstraints = new GridBagConstraints();
        deviceAndCurrentConfigPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        deviceAndCurrentConfigPanelConstraints.gridx = 0;
        deviceAndCurrentConfigPanelConstraints.gridy = y++;
        deviceAndCurrentConfigPanelConstraints.weightx = 1;
        deviceAndCurrentConfigPanelConstraints.weighty = 0;
        deviceAndCurrentConfigPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        configDevicePanel.add(deviceAndCurrentConfigPanel, deviceAndCurrentConfigPanelConstraints);

        GridBagConstraints scanTimePanelGridBagConstraints = new GridBagConstraints();
        scanTimePanelGridBagConstraints.fill = GridBagConstraints.BOTH;
        scanTimePanelGridBagConstraints.gridx = 0;
        scanTimePanelGridBagConstraints.gridy = y++;
        scanTimePanelGridBagConstraints.weightx = 1;
        scanTimePanelGridBagConstraints.weighty = 0;
        scanTimePanelGridBagConstraints.insets = new Insets(0, 0, 0, 5);
        scanTimePanelGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        scanTimePanelGridBagConstraints.anchor = GridBagConstraints.WEST;
        configDevicePanel.add(scanTimePanel, scanTimePanelGridBagConstraints);

        GridBagConstraints buttonPanelGridBagConstraints = new GridBagConstraints();
        buttonPanelGridBagConstraints.fill = GridBagConstraints.BOTH;
        buttonPanelGridBagConstraints.gridx = 0;
        buttonPanelGridBagConstraints.gridy = y;
        buttonPanelGridBagConstraints.weightx = 0;
        buttonPanelGridBagConstraints.weighty = 0;
        buttonPanelGridBagConstraints.insets = new Insets(0, 5, 0, 5);
        buttonPanelGridBagConstraints.fill = GridBagConstraints.NONE;
        buttonPanelGridBagConstraints.anchor = GridBagConstraints.WEST;
        configDevicePanel.add(buttonPanel, buttonPanelGridBagConstraints);

        GridBagConstraints positionPanelGridBagConstraints = new GridBagConstraints();
        positionPanelGridBagConstraints.fill = GridBagConstraints.BOTH;
        positionPanelGridBagConstraints.gridx = 1;
        positionPanelGridBagConstraints.gridy = y;
        positionPanelGridBagConstraints.weightx = 0;
        positionPanelGridBagConstraints.weighty = 0;
        positionPanelGridBagConstraints.insets = new Insets(0, 10, 0, 5);
        positionPanelGridBagConstraints.fill = GridBagConstraints.NONE;
        positionPanelGridBagConstraints.anchor = GridBagConstraints.WEST;
        configDevicePanel.add(scanPositionPanel, positionPanelGridBagConstraints);

        GridBagConstraints progressPanelGridBagConstraints = new GridBagConstraints();
        progressPanelGridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        progressPanelGridBagConstraints.gridx = 2;
        progressPanelGridBagConstraints.gridy = y++;
        progressPanelGridBagConstraints.weightx = 1;
        progressPanelGridBagConstraints.weighty = 0;
        progressPanelGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        progressPanelGridBagConstraints.anchor = GridBagConstraints.WEST;
        progressPanelGridBagConstraints.insets = new Insets(5, 5, 5, 5);
        configDevicePanel.add(scanProgressBar, progressPanelGridBagConstraints);

        configTabbedPane = new CloseSaveButtonTabbedPane(expert);

        configTabbedPane.addChangeListener(e -> {
            updateStartButtonAvailability();
        });

        infoTarget = new FlyScanInfoTarget();
        pluginTarget = new PluginsTarget();
        configNameChecker = new ConfigNameChecker();
        ConfigSelectionListener configSelectionListener = e -> {
            if (e != null) {
                Component origin;
                if (e.getSource() instanceof Component) {
                    origin = (Component) e.getSource();
                } else {
                    origin = null;
                }
                loadAndDisplayConfig(isExpert, e.getConfigName(),
                        origin instanceof ConfigurationTree ? configurationsPanel : origin);
                if (origin != null) {
                    JMenu menu = Utilities.getParentMenu(origin);
                    if (menu != null) {
                        menu.getPopupMenu().setVisible(false);
                        menu.setSelected(false);
                    }
                }
            }
        };
        ActionListener newConfigListener;
        if (expert) {
            newConfigListener = e -> {
                SwingUtilities.invokeLater(() -> {
                    Configuration config = ConfigCreator.newConfig(configurationsPanel, infoTarget.getFlyScanVersion(),
                            configNameChecker);
                    if (config != null) {
                        // Parse plugins
                        Collection<Plugin> plugins = getPlugins();
                        Map<PluginType, Collection<Plugin>> categories = pluginTarget.getCategories();

                        // Display configuration for edition
                        ConfigurationGUIContainer guiContainer = createConfigurationGUI(config, plugins, categories,
                                isExpert, true);
                        addConfigurationGUI(guiContainer, isExpert, true, -1, null);
                        ConfigurationGUI gui = guiContainer == null ? null : guiContainer.getConfigurationGUI();
                        if (gui != null) {
                            if (!easyConfigOnly) {
                                gui.getTabbedPane().setSelectedTab(Utilities.GENERAL);
                            }
                            // Mark config as modified
                            UIDelegate.notifyEdition(true, gui);
                        }
                    }
                });
            };
        } else {
            newConfigListener = null;
        }

        ActionListener currentConfigMenuItemActionListener = e -> {
            AbstractButton flyScanMenuItem = (AbstractButton) e.getSource();
            loadAndDisplayCurrentConfig(isExpert, false, true);
            Container parent = flyScanMenuItem.getParent();
            if (parent instanceof JPopupMenu) {
                JPopupMenu popupMenu = (JPopupMenu) parent;
                popupMenu.setVisible(false);
            }
            startButton.setEnabled(false);
        };

        configurationsPanel = new ConfigurationsPanel(configSelectionListener, newConfigListener,
                currentConfigMenuItemActionListener);

        configurationsSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, configurationsPanel, configTabbedPane);
        configurationsSplitPane.setUI(new ColoredSplitPaneUI());
        configurationsSplitPane.setOneTouchExpandable(true);
        configurationsSplitPane.setResizeWeight(0);
        configurationsSplitPane.setDividerSize(12);

        if (useMock) {
            // Mock to load a config file without using the device
            mockMenu = new JMenu(Utilities.getMessage("flyscan.mock"));

            // Create a file chooser
            final JFileChooser fcConfig = new JFileChooser();

            JMenuItem mockConfigMenuItem = new JMenuItem(Utilities.getMessage("flyscan.load.file.config"));
            mockConfigMenuItem.setToolTipText(Utilities.getMessage("flyscan.load.file.plugin.first"));
            mockConfigMenuItem.addActionListener(e -> {
                // Handle open button action.
                int returnVal = fcConfig.showOpenDialog(FlyScanTangoBox.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fcConfig.getSelectedFile();
                    loadAndDisplayMockConfig(isExpert, file);
                }
            });
            mockMenu.add(mockConfigMenuItem);

            // Create a file chooser
            final JFileChooser fcPlugin = new JFileChooser();

            JMenuItem mockPluginMenuItem = new JMenuItem(Utilities.getMessage("flyscan.load.file.plugin"));
            mockPluginMenuItem.addActionListener(e -> {
                // Handle open button action.
                int returnVal = fcPlugin.showOpenDialog(FlyScanTangoBox.this);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fcPlugin.getSelectedFile();
                    pluginTarget.setMockPlugins(getMockPlugins(file));
                }
            });
            mockMenu.add(mockPluginMenuItem);
        }

        currentConfigTarget = new CurrentConfigTarget();

        logPanel = new DeviceLogViewer(FlyScanTangoBox.class.getName());
        logPanel.setDefaultClearLogsButtonVisible(false);

        actorsExecTimesCopyButton = new JButton(Utilities.LOG_COPY_ICON);
        actorsExecTimesCopyButton.setToolTipText(LOG_COPY);
        actorsExecTimesCopyButton.addActionListener(e -> {
            copyToClipBoard(actorsExecTimesTextArea.getText());
        });
        actorsExecTimesPauseButton = new LogPauseButton();
        actorsExecTimesTextArea = new TextMatrixArea();
        FSScrollPane actorsExecTimesScrollPane = new FSScrollPane(actorsExecTimesTextArea);
        JPanel actorsExecTimesPanel = new JPanel(new GridBagLayout());
        GridBagConstraints actorsExecTimesScrollPaneConstraints = new GridBagConstraints();
        actorsExecTimesScrollPaneConstraints.fill = GridBagConstraints.BOTH;
        actorsExecTimesScrollPaneConstraints.gridx = 0;
        actorsExecTimesScrollPaneConstraints.gridy = 0;
        actorsExecTimesScrollPaneConstraints.weightx = 1;
        actorsExecTimesScrollPaneConstraints.weighty = 1;
        actorsExecTimesScrollPaneConstraints.gridwidth = GridBagConstraints.REMAINDER;
        actorsExecTimesPanel.add(actorsExecTimesScrollPane, actorsExecTimesScrollPaneConstraints);
        GridBagConstraints actorsExecTimesCopyButtonConstraints = new GridBagConstraints();
        actorsExecTimesCopyButtonConstraints.fill = GridBagConstraints.BOTH;
        actorsExecTimesCopyButtonConstraints.gridx = 0;
        actorsExecTimesCopyButtonConstraints.gridy = 1;
        actorsExecTimesCopyButtonConstraints.weightx = 0;
        actorsExecTimesCopyButtonConstraints.weighty = 0;
        actorsExecTimesCopyButtonConstraints.insets = new Insets(0, 5, 0, 5);
        actorsExecTimesPanel.add(actorsExecTimesCopyButton, actorsExecTimesCopyButtonConstraints);
        GridBagConstraints actorsExecTimesPauseButtonConstraints = new GridBagConstraints();
        actorsExecTimesPauseButtonConstraints.fill = GridBagConstraints.BOTH;
        actorsExecTimesPauseButtonConstraints.gridx = 1;
        actorsExecTimesPauseButtonConstraints.gridy = 1;
        actorsExecTimesPauseButtonConstraints.weightx = 0;
        actorsExecTimesPauseButtonConstraints.weighty = 0;
        actorsExecTimesPanel.add(actorsExecTimesPauseButton, actorsExecTimesPauseButtonConstraints);
        GridBagConstraints actorsExecTimesGlueConstraints = new GridBagConstraints();
        actorsExecTimesGlueConstraints.fill = GridBagConstraints.BOTH;
        actorsExecTimesGlueConstraints.gridx = 2;
        actorsExecTimesGlueConstraints.gridy = 1;
        actorsExecTimesGlueConstraints.weightx = 1;
        actorsExecTimesGlueConstraints.weighty = 0;
        actorsExecTimesPanel.add(Box.createGlue(), actorsExecTimesGlueConstraints);

        bottomTabbedPane = new BoldTitleTabbedPane();
        bottomTabbedPane.addTab(DEVICE_LOG_TITLE, logPanel, new TabLabel(DEVICE_LOG_TITLE));
        bottomTabbedPane.addTab(STATUS_TITLE.replaceAll(Utilities.POINT, Utilities.SPACE), statusText, statusTitle);
        bottomTabbedPane.addTab(ACTORS_EXEC_TIME_TITLE, actorsExecTimesPanel, new TabLabel(ACTORS_EXEC_TIME_TITLE));

        applicationLogPanel = new LogViewer(FlyScanTangoBox.class.getName());
        applicationLogAppender = new LogAppender(applicationLogPanel, false);
        LogAppender.registerAppender(LOGGER, applicationLogAppender);
        cometeLogAppender = new LogAppender(applicationLogPanel);
        LogAppender.registerAppender(Mediator.LOGGER_ACCESS, cometeLogAppender);

        if (LOGGER instanceof ch.qos.logback.classic.Logger) {
            ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LOGGER;
            logger.setLevel(Level.ALL);
        }

        bottomTabbedPane.addTab(APPLICATION_LOG_TITLE, applicationLogPanel, new TabLabel(APPLICATION_LOG_TITLE));

        configsAndMessagesSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, configurationsSplitPane,
                bottomTabbedPane);
        configsAndMessagesSplitPane.setOneTouchExpandable(true);
        configsAndMessagesSplitPane.setDividerSize(8);
        configsAndMessagesSplitPane.setResizeWeight(0.9);

        mainPanel = new JPanel(new GridBagLayout());
        mainPanel.setMinimumSize(Utilities.ZERO_DIM);

        y = 0;

        final GridBagConstraints configDevicePanelConstraints = new GridBagConstraints();
        configDevicePanelConstraints.fill = GridBagConstraints.BOTH;
        configDevicePanelConstraints.gridx = 0;
        configDevicePanelConstraints.gridy = y++;
        configDevicePanelConstraints.weightx = 1;
        configDevicePanelConstraints.weighty = 0;
        configDevicePanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        configDevicePanelConstraints.anchor = GridBagConstraints.EAST;
        configDevicePanelConstraints.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(configDevicePanel, configDevicePanelConstraints);

        GridBagConstraints monitoredDevicesGridBagConstraints = new GridBagConstraints();
        monitoredDevicesGridBagConstraints.fill = GridBagConstraints.BOTH;
        monitoredDevicesGridBagConstraints.gridx = 0;
        monitoredDevicesGridBagConstraints.gridy = y++;
        monitoredDevicesGridBagConstraints.weightx = 1;
        monitoredDevicesGridBagConstraints.weighty = 0;
        monitoredDevicesGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
        monitoredDevicesGridBagConstraints.anchor = GridBagConstraints.CENTER;
        monitoredDevicesGridBagConstraints.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(monitoredDevices, monitoredDevicesGridBagConstraints);

        final GridBagConstraints splitPaneConstraints = new GridBagConstraints();
        splitPaneConstraints.fill = GridBagConstraints.BOTH;
        splitPaneConstraints.gridx = 0;
        splitPaneConstraints.gridy = y++;
        splitPaneConstraints.weightx = 1;
        splitPaneConstraints.weighty = 1;
        splitPaneConstraints.anchor = GridBagConstraints.NORTHWEST;
        splitPaneConstraints.gridwidth = GridBagConstraints.REMAINDER;
        mainPanel.add(configsAndMessagesSplitPane, splitPaneConstraints);

        Font tabFont = new Font(Font.DIALOG, Font.PLAIN, 14);
        Map<TextAttribute, Object> attributes = new HashMap<>(tabFont.getAttributes());
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        attributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
        Font selectedTabFont = tabFont.deriveFont(attributes);
        attributes.clear();

        BoldTitleTabbedPane scanRecordingTabbedPane = new BoldTitleTabbedPane(SwingConstants.LEFT);
        RotatedLabel scanLabel = new RotatedLabel("Scan", Utilities.FLYSCAN_ICON, SwingConstants.LEFT);
        scanLabel.setFont(selectedTabFont);
        scanLabel.setDirection(Direction.VERTICAL_UP);
        scanRecordingTabbedPane.addTab(scanLabel.getText(), mainPanel, scanLabel);
        RotatedLabel recordingLabel = new RotatedLabel("Recording", Utilities.RECORDING_ICON, SwingConstants.LEFT);
        recordingLabel.setFont(tabFont);
        recordingLabel.setDirection(Direction.VERTICAL_UP);
        scanRecordingTabbedPane.addTab(recordingLabel.getText(), dataStorageBean, recordingLabel);

        scanRecordingTabbedPane.addChangeListener(e -> {
            switch (scanRecordingTabbedPane.getSelectedIndex()) {
                case 0:
                    scanLabel.setFont(selectedTabFont);
                    recordingLabel.setFont(tabFont);
                    break;
                case 1:
                    scanLabel.setFont(tabFont);
                    recordingLabel.setFont(selectedTabFont);
                    break;
                default:
                    scanLabel.setFont(tabFont);
                    recordingLabel.setFont(tabFont);
                    break;
            }
        });

        setLayout(new BorderLayout());
        add(scanRecordingTabbedPane, BorderLayout.CENTER);
        updateStartButtonAvailability();
        progressDialog = null;
    }

    @Override
    public void start() {
        super.start();
        dataStorageBean.start();
    }

    @Override
    public void stop() {
        super.stop();
        dataStorageBean.stop();
    }

    public void setRecordingManager(String recordingManager) {
        dataStorageBean.setModel(recordingManager);
    }

    protected CommandButton createCommandButton(String iconIdentifier) {
        CommandButton button = new CommandButton();
        button.setIcon(Utilities.getIcon(iconIdentifier));
        button.setButtonLook(true);
        button.setBorder(Utilities.BIG_BUTTON_BORDER);
        button.addMouseListener(Utilities.createButtonBorderListener());
        return button;
    }

    protected JPanel createPanel(String text, JComponent viewer) {
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        JLabel title = new JSmoothLabel(text);
        title.setFont(Utilities.TITLE_FONT);
        Dimension size = title.getPreferredSize();
        size.width += 5;
        title.setPreferredSize(size);
        title.setMinimumSize(size);
        panel.add(title, BorderLayout.WEST);
        panel.add(viewer, BorderLayout.CENTER);
        if (viewer instanceof JLabel) {
            viewer.setFont(Utilities.BASIC_FONT);
        }
        return panel;
    }

    // Highlight active configuration (SCAN-491)
    protected void updateHighlightedConfiguration() {
        String configName = currentConfigNameLabel.getText();
        for (int i = 0; i < configTabbedPane.getTabCount(); i++) {
            try {
                TextTabComponent comp = configTabbedPane.getTextTabComponentAt(i);
                if (comp != null) {
                    if (ObjectUtils.sameObject(configName, comp.getTitle())) {
                        comp.setTitleForeground(Utilities.CURRENT_CONFIG_COLOR);
                    } else {
                        comp.setTitleForeground(null);
                    }
                }
            } catch (Exception e) {
                // Ignore
            }
        }

    }

    @Override
    public JPanel getStatusPanel() {
        if (statusPanel == null) {
            statusPanel = createPanel(FSS_TITLE, deviceLabel1);
        }
        return statusPanel;
    }

    protected void copyToClipBoard(String text) {
        if ((text != null) && (!text.trim().isEmpty())) {
            StringSelection stringSelection = new StringSelection(text);
            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
            clpbrd.setContents(stringSelection, null);
        }
    }

    protected Collection<Plugin> getMockPlugins(File file) {
        FileReader fileReader;
        String[] pluginsInfoContent = null;
        Collection<String> errors = new ArrayList<>();
        try {
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            Collection<String> lines = new ArrayList<>();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            bufferedReader.close();
            pluginsInfoContent = lines.toArray(new String[lines.size()]);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        Collection<Plugin> plugins = null;

        try {
            if (pluginsInfoContent != null && pluginsInfoContent.length > 0) {
                plugins = PluginParser.parse(pluginsInfoContent);
            }
            if (plugins == null) {
                throw new FSParsingException(Utilities.getMessage("flyscan.error.attribute.pluginsinfo"));
            }
        } catch (FSParsingException e) {
            errors.add(e.getMessage());
            LOGGER.error(e.getMessage(), e);
        }
        if (!errors.isEmpty()) {
            updateError(Utilities.getMessage("flyscan.load.file.plugin"), errors);
        }
        return plugins;
    }

    @Override
    protected void clearGUI() {
        timeOutAction.setDevice(null);
        timeOutAction.setDialogTitle(String.format(CHANGE_TIMEOUT_CONFIGURATION, FSS));
        timeOutAction.setTextFieldTooltip(String.format(CHANGE_TIMEOUT_CONFIGURATION, FSS));
        timeOutAction.setTextFieldTitle(String.format(TIMEOUT_CONFIGURATION, FSS));
        startButton.setEnabled(false);
        configTabbedPane.enableLoadButtons(false);
        cleanWidget(internalStateViewer);
        cleanWidget(configurationsPanel);
        // SCAN-835: Use a group strategy for monitored devices
        ((AbstractRefreshingManager<?>) DataSourceProducerProvider
                .getProducer(generateAttributeKey(MONITORED_DEVICE_NAMES)))
                        .removeRefreshingGroupListener(monitoredDevices);
        cleanWidget(monitoredDevices);
        cleanWidget(scanPositionLabel);
        cleanWidget(scanProgressBar);
        cleanWidget(infoTarget);
        cleanWidget(pluginTarget);
        cleanWidget(currentConfigTarget);
        cleanWidget(scanStartDateLabel);
        cleanWidget(scanEndDateLabel);
        cleanWidget(scanRemainingTimeLabel);
        cleanWidget(scanElapsedLabel);
        cleanWidget(scanEstimatedTimeLabel);

        disconnectAttributeActorsAxecTimes();
        disconnectCommandStart();
        disconnectCommandStop();
        disconnectCommandPause();
        disconnectCommandResume();
        disconnectAttributeLog();
        disconnectMonitoredDeviceStateAttribute();
        disconnectAttributeDeviceStatus();

        cleanStatusModel();
        startAllowed = true;
    }

    @Override
    protected void refreshGUI() {
        try {
            // Apply fss timeout
            TangoDeviceHelper.setTimeOutInMilliseconds(getModel().toLowerCase(), fssTimeout);
        } catch (Exception e) {
            LOGGER.error(String.format(Utilities.getMessage("flyscan.error.timeout.update"), getModel()), e);
        }
        timeOutAction.setDevice(getModel());
        timeOutAction.setDialogTitle(String.format(CHANGE_TIMEOUT_CONFIGURATION, getModel()));
        timeOutAction.setTextFieldTooltip(String.format(CHANGE_TIMEOUT_CONFIGURATION, getModel()));
        timeOutAction.setTextFieldTitle(String.format(TIMEOUT_CONFIGURATION, getModel()));
        startButton.setEnabled(false);
        configTabbedPane.enableLoadButtons(false);
        startAllowed = true;

        setStatusModel();

        connectInternalState();
        connectAttributeConfigList();
        connectAttributeMonitoredDevices();
        connectAttributeInfo();
        connectAttributePluginsInfo();
        connectAttributeCurrentConfig();
        connectAttributeScanProgress();
        connectAttributeScanPosition();
        connectAttributeScanStartDate();
        connectAttributeScanEndDate();
        connectAttributeScanRemainingTime();
        connectAttributeScanElapsed();
        connectAttributeScanEstimatedTime();
        connectAttributeActorsAxecTimes();

        connectCommandStart();
        connectCommandStop();
        connectCommandPause();
        connectCommandResume();
        connectAttributeLog();
        connectMonitoredDeviceStateAttribute();
        connectAttributeDeviceStatus();

        updateStartButtonAvailability();

        // Activate following code if you want to display current configuration at application start
//        loadAndDisplayCurrentConfig(isExpert, true);

        // Revalidate to ensure having all labels correctly displayed
        scanTimePanel.revalidate();
        scanPositionPanel.revalidate();
        buttonPanel.revalidate();
        configDevicePanel.revalidate();
        revalidate();
        repaint();

    }

    public void connectAttributeDeviceStatus() {
        if (statusText != null) {
            TangoKey key = generateAttributeKey(STATUS);
            setWidgetModel(statusText, stringBox, key);
        }

    }

    public void disconnectAttributeDeviceStatus() {
        cleanWidget(statusText);
    }

    @Override
    protected void setStatusModel() {
        super.setStatusModel();
        deviceLabel1.setText(getModel());

    }

    @Override
    protected void cleanStatusModel() {
        super.cleanStatusModel();
        deviceLabel1.setText(NO_DEVICE_STRING);
    }

    @Override
    protected void onConnectionError() {
        updateError(Utilities.getMessage("flyscan.application.start"),
                Utilities.getMessage("flyscan.error.connection"));
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // not managed
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // not managed
    }

    /**
     * Connection to internalState attribute
     */
    public void connectInternalState() {
        if (internalStateViewer != null) {
            TangoKey key = generateAttributeKey(INTERNAL_STATE);
            setWidgetModel(internalStateViewer, stringBox, key);
        }
    }

    /**
     * Connection to configList attribute
     */
    public void connectAttributeConfigList() {
        if (configurationsPanel != null) {
            TangoKey key = generateAttributeKey(CONFIG_LIST);
            setWidgetModel(configurationsPanel, stringMatrixBox, key);
        }
    }

    /**
     * Connection to configuration attribute
     */
    public void connectAttributeCurrentConfig() {
        if (currentConfigTarget != null) {
            TangoKey key = generateAttributeKey(CONFIGURATION);
            setWidgetModel(currentConfigTarget, stringMatrixBox, key);
        }

    }

    /**
     * Connection to monitoredDevices attribute
     */
    public void connectAttributeMonitoredDevices() {
        if (monitoredDevices != null) {
            TangoKey key = generateAttributeKey(MONITORED_DEVICE_NAMES);
            // SCAN-835: Transmit data outside of EDT
            stringMatrixBox.setSynchronTransmission(monitoredDevices, true);
            setWidgetModel(monitoredDevices, stringMatrixBox, key);
            // SCAN-835: Use a group strategy for monitored devices
            DataSourceProducerProvider.setRefreshingStrategy(key, MONITORED_DEVICES_STARTEGY);
            ((AbstractRefreshingManager<?>) DataSourceProducerProvider.getProducer(key))
                    .addRefreshingGroupListener(monitoredDevices);
        }
    }

    protected void connectMonitoredDeviceStateAttribute() {
        if (monitoredDeviceStatesTarget == null) {
            monitoredDeviceStatesTarget = new DevicesColorTarget(monitoredDevices,
                    LoggerFactory.getLogger(FlyScanTangoBox.class.getName()));
        }
        TangoKey key = generateAttributeKey(MONITORED_DEVICE_STATES);
        // SCAN-835: Transmit data outside of EDT
        stringMatrixBox.setSynchronTransmission(monitoredDeviceStatesTarget, true);
        setWidgetModel(monitoredDeviceStatesTarget, stringMatrixBox, key);
        // SCAN-835: Use a group strategy for monitored devices
        DataSourceProducerProvider.setRefreshingStrategy(key, MONITORED_DEVICES_STARTEGY);
    }

    protected void disconnectMonitoredDeviceStateAttribute() {
        cleanWidget(monitoredDeviceStatesTarget);
    }

    /**
     * Connection to info attribute
     */
    public void connectAttributeInfo() {
        if (infoTarget != null) {
            TangoKey key = generateAttributeKey(INFO);
            setWidgetModel(infoTarget, stringMatrixBox, key);
        }
    }

    /**
     * Connection to pluginsInfo attribute
     */
    public void connectAttributePluginsInfo() {
        if (pluginTarget != null) {
            TangoKey key = generateAttributeKey(PLUGINS_INFO);
            setWidgetModel(pluginTarget, stringMatrixBox, key);
        }
    }

    /**
     * Connection to scanProgress attribute
     */
    public void connectAttributeScanProgress() {
        if (scanProgressBar != null) {
            TangoKey key = generateAttributeKey(SCAN_PROGRESS);
            numberBox.setColorEnabled(scanProgressBar, false);
            setWidgetModel(scanProgressBar, numberBox, key);
        }
    }

    /**
     * Connection to scanPosition attribute
     */
    public void connectAttributeScanPosition() {
        if (scanPositionLabel != null) {
            TangoKey key = generateAttributeKey(SCAN_POSITION);
            stringBox.setColorEnabled(scanPositionLabel, false);
            setWidgetModel(scanPositionLabel, stringBox, key);
        }
    }

    /**
     * Connection to scanStartDate attribute
     */
    protected void connectAttributeScanStartDate() {
        if (scanStartDateLabel != null) {
            stringBox.setErrorText(scanStartDateLabel, N_A);
            TangoKey key = generateAttributeKey(SCAN_START_DATE);
            stringBox.setColorEnabled(scanStartDateLabel, false);
            setWidgetModel(scanStartDateLabel, stringBox, key);
        }
    }

    /**
     * Connection to scanEndDate attribute
     */
    protected void connectAttributeScanEndDate() {
        if (scanEndDateLabel != null) {
            stringBox.setErrorText(scanEndDateLabel, N_A);
            TangoKey key = generateAttributeKey(SCAN_END_DATE);
            stringBox.setColorEnabled(scanEndDateLabel, false);
            setWidgetModel(scanEndDateLabel, stringBox, key);
        }
    }

    /**
     * Connection to scanRemainingTime attribute
     */
    protected void connectAttributeScanRemainingTime() {
        if (scanRemainingTimeLabel != null) {
            stringBox.setErrorText(scanRemainingTimeLabel, N_A);
            TangoKey key = generateAttributeKey(SCAN_REMAINING_TIME);
            stringBox.setColorEnabled(scanRemainingTimeLabel, false);
            setWidgetModel(scanRemainingTimeLabel, stringBox, key);
        }
    }

    /**
     * Connection to scanElapsedTime attribute
     */
    protected void connectAttributeScanElapsed() {
        if (scanElapsedLabel != null) {
            stringBox.setErrorText(scanElapsedLabel, N_A);
            TangoKey key = generateAttributeKey(SCAN_ELAPSED);
            stringBox.setColorEnabled(scanElapsedLabel, false);
            setWidgetModel(scanElapsedLabel, stringBox, key);
        }
    }

    /**
     * Connection to scanEstimatedTime attribute
     */
    protected void connectAttributeScanEstimatedTime() {
        if (scanEstimatedTimeLabel != null) {
            stringBox.setErrorText(scanEstimatedTimeLabel, N_A);
            TangoKey key = generateAttributeKey(SCAN_ESTIMATED_TIME);
            stringBox.setColorEnabled(scanEstimatedTimeLabel, false);
            setWidgetModel(scanEstimatedTimeLabel, stringBox, key);
        }
    }

    /**
     * Connection to log attribute
     */
    protected void connectAttributeLog() {
        if (logPanel != null) {
            TangoKey key = generateAttributeKey(LOG);
            logAdapter = getAndConnectBestDeviceLogAdapter(logPanel, key, stringMatrixBox);
            connectCommandClearLogs();
        }
    }

    protected void disconnectAttributeLog() {
        stringMatrixBox.disconnectWidgetFromAll(logAdapter);
        disconnectCommandClearLogs();
    }

    /**
     * Connection to actorsExecTimes attribute
     */
    public void connectAttributeActorsAxecTimes() {
        if (actorsExecTimesTextArea != null) {
            TangoKey key = generateAttributeKey(ACTORS_EXEC_TIMES);
            setWidgetModel(actorsExecTimesTextArea, stringMatrixBox, key);
            actorsExecTimesPauseButton.setKey(key);
        }

    }

    /**
     * Disconnection to actorsExecTimes attribute
     */
    public void disconnectAttributeActorsAxecTimes() {
        if (actorsExecTimesTextArea != null) {
            cleanWidget(actorsExecTimesTextArea);
            actorsExecTimesPauseButton.setKey(null);
        }

    }

    /**
     * Read config
     * 
     * @param configName String
     * @return String[]
     * @throws DevFailed
     */
    public String[] readConfig(String configName) throws DevFailed {
        LOGGER.trace(String.format(Utilities.getMessage("flyscan.configuration.read"), configName));
        String[] result = TangoCommandHelper.executeCommand(TangoDeviceHelper.getDeviceProxy(getModel(), false),
                READ_CONFIG, String[].class, configName + CFG_EXTENSION);
        return result;
    }

    /**
     * Deletes a config
     * 
     * @param configName
     * @throws DevFailed
     */
    public void deleteConfig(String configName) throws DevFailed {
        LOGGER.trace(String.format(Utilities.getMessage("flyscan.configuration.delete.log"), configName));
        TangoCommandHelper.executeCommand(TangoDeviceHelper.getDeviceProxy(getModel(), false), DELETE_CONFIG,
                configName + CFG_EXTENSION);
    }

    /**
     * Read current config
     * 
     * @return String[]
     * @throws DevFailed
     */
    public String[] readCurrentConfig() throws DevFailed {
        String[] result = null;
        if (currentConfigTarget != null) {
            result = currentConfigTarget.getFlatStringMatrix();
        }
        return result;
    }

    /**
     * Check config before load, returning the errors recognized by the flyscan server.
     * 
     * @param configContent The config content, as a {@link String} array
     * @return A {@link String} array: the errors recognized by the flyscan server.
     * @throws DevFailed If a problem occurred (for example: a communication problem).
     */
    public String[] checkConfig(String... configContent) throws DevFailed {
        StringBuilder toLogBuilder = new StringBuilder(Utilities.getMessage("flyscan.command.check.config"));
        for (String s : configContent) {
            toLogBuilder.append(Utilities.NEW_LINE).append(s);
        }
        LOGGER.trace(toLogBuilder.toString());
        String[] result = TangoCommandHelper.executeCommand(TangoDeviceHelper.getDeviceProxy(getModel(), false),
                CHECK_CONFIG, String[].class, configContent);
        return result;
    }

    /**
     * Evaluate config, and gets its updated content according to easy config consequences
     * 
     * @param configContent The config content, as a {@link String} array
     * @return A {@link String} array: the new config content
     * @throws DevFailed If a problem occurred. For example, if config is invalid.
     */
    public String[] evaluateConfig(String... configContent) throws DevFailed {
        StringBuilder toLogBuilder = new StringBuilder(Utilities.getMessage("flyscan.command.evaluate.config"));
        for (String s : configContent) {
            toLogBuilder.append(Utilities.NEW_LINE).append(s);
        }
        LOGGER.trace(toLogBuilder.toString());
        String[] result = TangoCommandHelper.executeCommand(TangoDeviceHelper.getDeviceProxy(getModel(), false),
                EVALUATE_CONFIG, String[].class, configContent);
        return result;
    }

    /**
     * Connection to start command
     */
    protected void connectCommandStart() {
        if (startStateTarget != null) {
            TangoKey key = generateAttributeKey(STATE);
            stringBox.connectWidget(startStateTarget, key);
        }
    }

    protected void disconnectCommandStart() {
        stringBox.disconnectWidgetFromAll(startStateTarget);
    }

    /**
     * Connection to abort command
     */
    protected void connectCommandStop() {
        if (stopButton != null) {
            TangoKey key = generateCommandKey(Utilities.ABORT);
            stringBox.setConfirmation(stopButton, false);
            stringBox.setOutputInPopup(stopButton, false);
            stringBox.setCommandWidget(stopButton, true);
            stringBox.connectWidget(stopButton, key);
        }

    }

    protected void disconnectCommandStop() {
        stringBox.disconnectWidgetFromAll(stopButton);
    }

    /**
     * Connection to pause command
     */
    protected void connectCommandPause() {
        if (pauseButton != null) {
            TangoKey key = generateCommandKey(PAUSE);
            stringBox.setConfirmation(pauseButton, false);
            stringBox.setOutputInPopup(pauseButton, false);
            stringBox.setCommandWidget(pauseButton, true);
            stringBox.connectWidget(pauseButton, key);
        }
    }

    protected void disconnectCommandPause() {
        stringBox.disconnectWidgetFromAll(pauseButton);
    }

    /**
     * Connection to resume command
     */
    protected void connectCommandResume() {
        if (resumeButton != null) {
            TangoKey key = generateCommandKey(RESUME);
            stringBox.setConfirmation(resumeButton, false);
            stringBox.setOutputInPopup(resumeButton, false);
            stringBox.setCommandWidget(resumeButton, true);
            stringBox.connectWidget(resumeButton, key);
        }
    }

    protected void disconnectCommandResume() {
        stringBox.disconnectWidgetFromAll(resumeButton);
    }

    protected void connectCommandClearLogs() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, getModel(), CLEAR_LOG);
        connectClearDeviceLogsButton(logPanel, key, stringBox, DEFAULT_MESSAGE_MANAGER);
        // Deactivate confirmation message as asked in meeting
        // https://confluence.synchrotron-soleil.fr/pages/viewpage.action?pageId=102050365
        stringBox.setConfirmation(logPanel.getClearDeviceLogsButton(), false);
    }

    protected void disconnectCommandClearLogs() {
        stringBox.disconnectWidgetFromAll(logPanel.getClearDeviceLogsButton());
    }

    protected void prepareAndDisplayProgressDialog(String key, String configName, Component origin) {
        if (!progressDialog.isVisible()) {
            progressDialog.setTitle(String.format(Utilities.getMessage(key), configName));
            progressDialog.setMainMessage(progressDialog.getTitle());
            progressDialog.setProgressIndeterminate(true);
            progressDialog.pack();
            // Take some margin to fully display title
            int width = FontUtils.measureString(configName, progressDialog.getFont()).width;
            progressDialog.setSize(Math.max(progressDialog.getWidth() + 100, progressDialog.getWidth() + width - 50),
                    progressDialog.getHeight());
            progressDialog.setLocationRelativeTo(origin);
            progressDialog.setVisible(true);
        }
    }

    /**
     * Creates a {@link ConfigurationGUIContainer} for configuration display and edition.
     * 
     * @param configuration Configuration
     * @param plugins
     * @param categories
     * @param expert
     * @param enabled
     * @return
     */
    protected ConfigurationGUIContainer createConfigurationGUI(Configuration configuration, Collection<Plugin> plugins,
            Map<PluginType, Collection<Plugin>> categories, final boolean expert, boolean enabled) {
        final ConfigurationGUIContainer container;
        if (configuration == null) {
            container = null;
        } else {
            final JPanel headerPanel = createHeaderPanel();
            final JPanel errorPanel = createErrorPanel(headerPanel);
            ConfigurationGUI panel = new ConfigurationGUI(configuration, plugins, categories, errorPanel, expert,
                    enabled, isEasyConfigOnly());
            return new ConfigurationGUIContainer(panel, headerPanel, errorPanel);
        }
        return container;
    }

    protected void addConfigurationGUI(ConfigurationGUIContainer guiContainer, final boolean isExpert,
            boolean isEnabled, int tabIndex, String message) {
        ConfigurationGUI panel = guiContainer == null ? null : guiContainer.getConfigurationGUI();
        if (panel != null) {
            final JPanel headerPanel = guiContainer.getHeaderPanel();
            final JPanel errorPanel = guiContainer.getErrorPanel();
            Configuration configuration = panel.getConfiguration();
            if (isEnabled) {
                panel.addBeingEditedListener(new ConfigurationEditionStateListener() {

                    @Override
                    public void setConfigurationBeingEdited(boolean isBeingEdited) {
                        int selectedIndex = configTabbedPane.indexOfComponent(panel);
                        if (selectedIndex > -1) {
                            String title = configTabbedPane.getConfigNameAt(selectedIndex);
                            if (isBeingEdited) {
                                if (!title.endsWith(Utilities.ASTERISK)) {
                                    title = title + Utilities.SPACE + Utilities.ASTERISK;
                                }
                            } else {
                                title = title.replace(Utilities.ASTERISK, ObjectUtils.EMPTY_STRING).trim();
                            }
                            configTabbedPane.setTitleAt(selectedIndex, title);
                            configTabbedPane.enableSaveButton(selectedIndex, isBeingEdited && isExpert);
                        }

                    }
                });
            }

            final String title = configuration.getName();

            final FSScrollPane errorScrollPane = new FSScrollPane(errorPanel);
            errorScrollPane.setBorder(null);
            errorScrollPane.setPreferredSize(ERROR_SIZE);

            headerPanel.add(errorScrollPane, BorderLayout.CENTER);

            configTabbedPane.addTabWithHeader(title, panel, headerPanel, tabIndex, tabIndex < 0,
                    createReloadListener(isExpert, title),
                    Utilities.isStepByStep(configuration) ? createDeleteConfigListener(isExpert, title) : null);

            configTabbedPane.setSelectedComponent(panel);

            if (isEnabled) {
                int cnt = configTabbedPane.getTabCount();
                for (int index = 0; index < cnt; index++) {
                    Component component = configTabbedPane.getTabComponentAt(index);
                    final CloseSaveButtonTab closeSaveButtonTab = (CloseSaveButtonTab) component;
                    final ConfigurationGUI configurationGUI = closeSaveButtonTab.getConfigurationGUI();
                    if (configurationGUI != null) {
                        final JButton saveButton = closeSaveButtonTab.getSaveButton();
                        Collection<MouseListener> listeners = new ArrayList<>();
                        for (MouseListener listener : saveButton.getMouseListeners()) {
                            if (listener instanceof ButtonDecorationListener) {
                                listeners.add(listener);
                            }
                        }
                        for (MouseListener listener : listeners) {
                            saveButton.removeMouseListener(listener);
                        }
                        listeners.clear();

                        saveButton
                                .addMouseListener(new ButtonDecorationListener(Utilities.BUTTON_MOUSE_ENTERED_BORDER) {
                                    @Override
                                    public void mouseClicked(MouseEvent e) {
                                        saveButton.removeMouseListener(this);
                                        if (closeSaveButtonTab.getTitle().trim().endsWith(Utilities.ASTERISK)) {
                                            String configName = closeSaveButtonTab.getTitle()
                                                    .replace(Utilities.ASTERISK, ObjectUtils.EMPTY_STRING).trim();
                                            if (JOptionPane.showConfirmDialog(saveButton,
                                                    Utilities.getMessage("flyscan.configuration.write.continue"),
                                                    String.format(
                                                            Utilities.getMessage(
                                                                    "flyscan.configuration.write.confirmation"),
                                                            configName),
                                                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                                                Configuration configuration = closeSaveButtonTab.getConfigurationGUI()
                                                        .getConfiguration();
                                                StringBuilder deparsedConfigBuilder;
                                                int index = configuration.getName().lastIndexOf('/');
                                                if (index > 0) {
                                                    // SCAN-949: Set config parent path as 1st line
                                                    deparsedConfigBuilder = ConfigParser.deparse(configuration,
                                                            getPlugins(),
                                                            new StringBuilder(
                                                                    configuration.getName().substring(0, index))
                                                                            .append(ParsingUtil.CRLF));
                                                } else {
                                                    deparsedConfigBuilder = ConfigParser.deparse(configuration,
                                                            getPlugins(), null);
                                                }
                                                String deparsedConfig = deparsedConfigBuilder.toString();
                                                deparsedConfigBuilder.delete(0, deparsedConfigBuilder.length());
                                                String[] arguments = deparsedConfig.split(ParsingUtil.CRLF);
                                                try {
                                                    LOGGER.trace(Utilities.getMessage("flyscan.command.write.config")
                                                            + deparsedConfig);
                                                    TangoCommandHelper.executeCommand(
                                                            TangoDeviceHelper.getDeviceProxy(getModel(), false),
                                                            WRITE_CONFIG, String[].class, arguments);
                                                    saveButton.setEnabled(false);
                                                    closeSaveButtonTab.setTitle(configName);
                                                } catch (DevFailed e1) {
                                                    JOptionPane.showMessageDialog(saveButton, String.format(
                                                            Utilities.getMessage("flyscan.error.configuration.save"),
                                                            configName), Utilities.getMessage("flyscan.error.save"),
                                                            JOptionPane.ERROR_MESSAGE);
                                                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e1), e);
                                                }
                                            }
                                        }
                                        e.consume();
                                        boolean add = true;
                                        for (MouseListener listener : saveButton.getMouseListeners()) {
                                            if (listener == this) {
                                                add = false;
                                                break;
                                            }
                                        }
                                        if (add) {
                                            saveButton.addMouseListener(this);
                                        }
                                    }
                                });
                        final JButton loadButton = closeSaveButtonTab.getLoadButton();
                        loadButton.addActionListener(e -> {
                            if (progressDialog == null) {
                                progressDialog = new ProgressDialog(
                                        WindowSwingUtils.getWindowForComponent(startButton));
                            }
                            progressDialog.setTitle(LOADING_SELECTED_CONFIGURATION);
                            progressDialog.setAlwaysOnTop(true);
                            progressDialog.setProgressIndeterminate(true);
                            progressDialog.setMainMessage(progressDialog.getTitle());
                            LoadConfigInFSSWorker worker = new LoadConfigInFSSWorker(closeSaveButtonTab);
                            progressDialog.pack();
                            progressDialog.setLocationRelativeTo(loadButton);
                            progressDialog.setCancelable((ICancelable) worker);
                            progressDialog.setVisible(true);
                            worker.execute();
                        });

                    }

                }

            }
            if ((message != null) && (panel != null)) {
                removeErrorMessage(panel);
                displayErrorMessage(
                        String.format(Utilities.getMessage("flyscan.configuration.gui.build"), configuration.getName()),
                        message);
            }
        }
        updateHighlightedConfiguration();
    }

    protected ConfigurationGUI searchConfigurationGUI(Component comp) {
        ConfigurationGUI gui = null;
        if (comp instanceof ConfigurationGUI) {
            gui = (ConfigurationGUI) comp;
        } else if (comp instanceof Container) {
            Container cont = (Container) comp;
            for (Component child : cont.getComponents()) {
                ConfigurationGUI tmp = searchConfigurationGUI(child);
                if (tmp != null) {
                    gui = tmp;
                    break;
                }
            }
        }
        return gui;
    }

    protected ActionListener createReloadListener(final boolean isExpert, final String configName) {
        return e -> {
            Component source = null;
            if ((e != null) && (e.getSource() instanceof Component)) {
                source = (Component) e.getSource();
            }
            if (JOptionPane.showConfirmDialog(source,
                    String.format(Utilities.getMessage("flyscan.configuration.reload.confirmation"), configName),
                    Utilities.CONFIRMATION, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(this));
                }
                ConfigReloader reloader = new ConfigReloader(isExpert, configName);
                progressDialog.setCancelable(reloader);
                prepareAndDisplayProgressDialog("flyscan.configuration.reloading", configName, source);
                SwingUtilities.invokeLater(() -> {
                    reloader.run();
                });
            }
        };
    }

    protected ActionListener createDeleteConfigListener(final boolean isExpert, final String configName) {
        return e -> {
            Component source = null;
            if ((e != null) && (e.getSource() instanceof Component)) {
                source = (Component) e.getSource();
            }
            if (JOptionPane.showConfirmDialog(source,
                    String.format(Utilities.getMessage("flyscan.configuration.delete.confirmation"), configName),
                    Utilities.CONFIRMATION, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                int index = getConfigurationTabIndex(configName);
                String title = configTabbedPane.getTabTitleAt(index);
                configTabbedPane.remove(index);
                String[] configs = configurationsPanel == null ? null : configurationsPanel.getFlatStringMatrix();
                boolean found = false;
                if (configs != null) {
                    for (String config : configs) {
                        if (title.equals(config)) {
                            found = true;
                            break;
                        }
                    }
                }
                if (found) {
                    try {
                        deleteConfig(title);
                    } catch (DevFailed df) {
                        String message = TangoExceptionHelper.getErrorMessage(df);
                        LOGGER.error(message, df);
                        displayErrorMessage(
                                DELETE_CONFIG + TITLE_SEPARATOR + String
                                        .format(Utilities.getMessage("flyscan.configuration.delete.error"), configName),
                                message);
                    }
                }
            }
        };
    }

    /**
     * 
     * @return type and dimensions of the currently running configuration
     */
    protected Map<ScanType, int[]> getTypeAndScanDimensions(Configuration configuration) {
        Map<ScanType, int[]> typeAndDimension = new HashMap<>();

        ScanType scanType = null;
        int[] dimensions = null;

        if (configuration != null) {
            for (Section section : configuration.getSections()) {
                if (section instanceof AcquisitionSection) {
                    final AcquisitionSection acquisitionSection = (AcquisitionSection) section;
                    boolean isContinuous = acquisitionSection.isContinuousScan();
                    boolean isZigzag = acquisitionSection.isZigZag();
                    dimensions = acquisitionSection.getScanDimensions();

                    if (!isContinuous) {
                        scanType = ScanType.STEP_BY_STEP;
                    } else if (isZigzag) {
                        scanType = ScanType.ZIGZAG;
                    } else {
                        scanType = ScanType.CONTINUOUS;
                    }
                    break;
                }
            }
        }
        if ((scanType != null) && (dimensions != null)) {
            typeAndDimension.put(scanType, dimensions);
        }

        return typeAndDimension;
    }

    protected Configuration getCurrentConfiguration() {
        Configuration configuration = null;
        if (currentConfigTarget != null) {
            String[] currentConfig = currentConfigTarget.getFlatStringMatrix();
            try {
                if (currentConfig != null && currentConfig.length > 0) {
                    configuration = ConfigParser.parse(Utilities.CURRENT_CONFIG, currentConfig);
                }
                if (configuration == null) {
                    throw new FSParsingException(CURRENT_CONFIG_ERROR);
                }
            } catch (FSParsingException e1) {
                LOGGER.error(e1.getMessage());
                updateError(String.format(Utilities.getMessage("flyscan.configuration.read"), Utilities.CURRENT_CONFIG),
                        e1.getMessage());
            }
        }
        return configuration;
    }

    protected String getConfigurationName(Configuration configuration) {
        String currentConfigName = ObjectUtils.EMPTY_STRING;
        for (Section section : configuration.getSections()) {
            if (section instanceof InfoSection) {
                fr.soleil.flyscan.lib.model.parsing.configuration.Entry entry = section.getEntry(ParsingUtil.NAME);
                if (entry != null) {
                    currentConfigName = entry.getValue().trim();
                }
            }
        }
        return currentConfigName;
    }

    protected ConfigurationGUI getConfigurationGUIAt(int index) {
        return configTabbedPane.getConfigurationGUI(index);
    }

    protected Collection<Plugin> getPlugins() {
        Collection<Plugin> plugins = null;
        if (pluginTarget != null) {
            plugins = pluginTarget.getPlugins();
            if (plugins.isEmpty()) {
                String errorMessage = pluginTarget.getLastErrorMessage();
                if (errorMessage != null) {
                    LOGGER.error(errorMessage);
                    updateError(Utilities.getMessage("flyscan.plugins.parse"), errorMessage);
                }
            }
        }
        return plugins;
    }

    protected void updateError(String event, Collection<String> texts) {
        if ((event != null) && (texts != null) && !texts.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            boolean add = false;
            for (String text : texts) {
                if ((text != null) && !text.isEmpty()) {
                    if (add) {
                        builder.append('\n');
                    } else {
                        add = true;
                    }
                    builder.append(text);
                }
            }
            texts.clear();
            if (builder.length() > 0) {
                updateError(event, builder.toString());
                builder.delete(0, builder.length());
            }
        }
    }

    protected void updateError(String event, String text) {
        if ((event != null) && (text != null)) {
            JOptionPane.showMessageDialog(this,
                    String.format(Utilities.getMessage("flyscan.error.default"), event, text, DEVICE_LOG_TITLE,
                            STATUS_TITLE, APPLICATION_LOG_TITLE),
                    Utilities.getMessage("flyscan.error"), JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Load and display a configuration identified by its name
     * 
     * @param isExpert
     * @param configName String
     */
    protected void loadAndDisplayConfig(final boolean isExpert, String configName, Component origin) {
        removeErrorMessage(getConfigurationGUI(configName));

        // If configuration is already displayed, just select its tab
        int index = getConfigurationTabIndex(configName);

        if (index == -1) {
            readCheckAndParseConfig(isExpert, configName, -1, origin, true);
        } else {
            configTabbedPane.setSelectedIndex(index);
            if (configTabbedPane.getConfigNameAt(index).trim().endsWith(Utilities.ASTERISK)) {
                Object[] options = { Utilities.getMessage("flyscan.reload"),
                        Utilities.getMessage("flyscan.reload.no") };
                int n = JOptionPane.showOptionDialog(this,
                        Utilities.getMessage("flyscan.configuration.edited.reload.confirmation"),
                        Utilities.getMessage("flyscan.configuration.edited"), JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (n == 0) {
                    configTabbedPane.remove(index);
                    readCheckAndParseConfig(isExpert, configName, -1, origin, true);
                }
            }

        }
    }

    protected boolean isConfigurationContent(String... configContent) {
        return (configContent != null) && (configContent.length > 0) && (configContent[0] != null)
                && (!configContent[0].startsWith(NO_CONFIGURATION));
    }

    protected boolean isNoConfigurationContent(String... configContent) {
        return (configContent != null) && (configContent.length > 0) && (configContent[0] != null)
                && (configContent[0].startsWith(NO_CONFIGURATION));
    }

    /**
     * Load and display current configuration
     * 
     * @param isExpert
     */
    protected void loadAndDisplayCurrentConfig(final boolean isExpert, final boolean removeFormerTab,
            final boolean withProgressDialog) {
        // If configuration is already displayed, just select its tab
        final int index = getConfigurationTabIndex(Utilities.CURRENT_CONFIG);
        if (index == -1) {
            readAndParseCurrentConfig(isExpert, index, withProgressDialog);
        } else if (removeFormerTab) {
            JPanel panelLoading = new JPanel();
            panelLoading.setLayout(new BorderLayout());
            JLabel labelLoading = new JLabel(Utilities.LOADING_ICON);
            panelLoading.add(labelLoading, BorderLayout.CENTER);

            configTabbedPane.setComponentAt(index, panelLoading);

            // Deactivate option allowing to close tabs while loading current config
            for (int i = 0; i < configTabbedPane.getTabCount(); i++) {
                configTabbedPane.enableCloseButton(i, false);
            }

            SwingWorker<Map<ConfigurationGUI, JPanel>, Void> worker = new SwingWorker<Map<ConfigurationGUI, JPanel>, Void>() {

                @Override
                protected Map<ConfigurationGUI, JPanel> doInBackground() throws Exception {
                    Map<ConfigurationGUI, JPanel> map = new HashMap<>();
                    String[] configContent = readCurrentConfig();
                    if (isConfigurationContent(configContent)) {
                        // Parse config
                        Configuration configuration = ConfigParser.parse(Utilities.CURRENT_CONFIG, configContent);

                        // Parse plugins
                        Collection<Plugin> plugins = getPlugins();
                        Map<PluginType, Collection<Plugin>> categories = pluginTarget.getCategories();

                        if ((configuration != null) && (plugins != null)) {
                            final JPanel headerPanel = createHeaderPanel();
                            final JPanel errorPanel = createErrorPanel(headerPanel);
                            final FSScrollPane errorScrollPane = new FSScrollPane(errorPanel);
                            errorScrollPane.setBorder(null);
                            errorScrollPane.setPreferredSize(ERROR_SIZE);
                            headerPanel.add(errorScrollPane, BorderLayout.CENTER);
                            final ConfigurationGUI panel = new ConfigurationGUI(configuration, plugins, categories,
                                    errorPanel, isExpert, false, isEasyConfigOnly());
                            map.put(panel, headerPanel);
                        }
                    }
                    return map;
                }

                @Override
                protected void done() {
                    // Display configuration for edition
                    Map<ConfigurationGUI, JPanel> map;
                    try {
                        map = get();
                        ConfigurationGUI panel = null;
                        JPanel headerPanel = null;
                        for (Entry<ConfigurationGUI, JPanel> entry : map.entrySet()) {
                            panel = entry.getKey();
                            headerPanel = entry.getValue();
                        }
                        if ((panel != null) && (headerPanel != null)) {
                            String title = panel.getConfiguration().getName();
                            // Delete config not allowed for mock configurations
                            configTabbedPane.addTabWithHeader(title, panel, headerPanel, index, true,
                                    createReloadListener(isExpert, title), null);
                            configTabbedPane.setSelectedComponent(panel);
                        } else {
                            configTabbedPane.addEmptyConfigTab(Utilities.CURRENT_CONFIG, index);
                        }
                    } catch (InterruptedException e) {
                        LOGGER.warn(String.format(Utilities.getMessage("flyscan.configuration.update.cancelled"),
                                Utilities.CURRENT_CONFIG));
                    } catch (ExecutionException e) {
                        LOGGER.error(Utilities.getMessage("flyscan.error.configuration.update.current"), e.getCause());
                    } finally {
                        // Reactivate option allowing to close tabs after loading current config
                        for (int i = 0; i < configTabbedPane.getTabCount(); i++) {
                            configTabbedPane.enableCloseButton(i, true);
                        }
                    }

                }
            };
            worker.execute();

        } else {
            configTabbedPane.setSelectedIndex(index);
        }
    }

    protected int getConfigurationTabIndex(String tabName) {
        int index = -1;
        int nb = configTabbedPane.getTabCount();
        for (int i = 0; i < nb; i++) {
            String title = configTabbedPane.getConfigNameAt(i);
            if ((title != null) && (!title.isEmpty())
                    && (title.replace(Utilities.ASTERISK, ObjectUtils.EMPTY_STRING).trim().equalsIgnoreCase(tabName))) {
                index = i;
                break;
            }
        }
        return index;
    }

    protected ConfigurationGUI getConfigurationGUI(String tabName) {
        return getConfigurationGUIAt(getConfigurationTabIndex(tabName));
    }

    /**
     * Load and display a configuration stored in a file
     * 
     * @param isExpert
     * @param config File
     */
    protected void loadAndDisplayMockConfig(final boolean isExpert, File config) {
        // If configuration is already displayed, just select its tab
        String configName = config.getName().replace(TXT_EXTENSION, ObjectUtils.EMPTY_STRING).trim();
        int index = getConfigurationTabIndex(configName);
        if (index == -1) {
            readMockConfig(isExpert, config);
        } else {
            configTabbedPane.setSelectedIndex(index);
            if (configTabbedPane.getConfigNameAt(index).trim().endsWith(Utilities.ASTERISK)) {
                Object[] options = { Utilities.getMessage("flyscan.reload"),
                        Utilities.getMessage("flyscan.reload.no") };
                int n = JOptionPane.showOptionDialog(this,
                        Utilities.getMessage("flyscan.configuration.edited.reload.confirmation"),
                        Utilities.getMessage("flyscan.configuration.edited"), JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (n == 0) {
                    configTabbedPane.remove(index);
                    readMockConfig(isExpert, config);
                }
            }

        }
    }

    protected void readAndParseCurrentConfig(final boolean isExpert, int tabIndex, final boolean withProgressDialog) {
        readCheckAndParseConfig(isExpert, Utilities.CURRENT_CONFIG, tabIndex, configurationsPanel, withProgressDialog);
    }

    protected void readCheckAndParseConfig(final boolean isExpert, String configName, int index, final Component origin,
            final boolean withProgressDialog) {
        if (withProgressDialog) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(this));
            }
        }
        ConfigGUIBuilder builder = new ConfigGUIBuilder(configName, index, isExpert, withProgressDialog);
        if (withProgressDialog) {
            progressDialog.setCancelable((ICancelable) builder);
            prepareAndDisplayProgressDialog("flyscan.configuration.reading", configName, origin);
        }
        builder.execute();
    }

    protected void readMockConfig(final boolean isExpert, File config) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(this));
        }
        MockConfigGUIBuilder builder = new MockConfigGUIBuilder(config, isExpert);
        progressDialog.setCancelable((ICancelable) builder);
        prepareAndDisplayProgressDialog("flyscan.configuration.reading", config.getName(), mockMenu);
        builder.execute();
    }

    public ChangeDeviceTimeOutAction getTimeOutAction() {
        return timeOutAction;
    }

    public AbstractActionExt getEasyConfigOnlyAction() {
        return easyConfigOnlyAction;
    }

    protected FlyScanMenu searchFlyScanMenu(Component comp) {
        FlyScanMenu menu;
        if (comp == null) {
            menu = null;
        } else if (comp instanceof FlyScanMenu) {
            menu = (FlyScanMenu) comp;
        } else if (comp instanceof JPopupMenu) {
            menu = searchFlyScanMenu(((JPopupMenu) comp).getInvoker());
        } else {
            menu = searchFlyScanMenu(comp.getParent());
        }
        return menu;
    }

    public JMenu getMockMenu() {
        return mockMenu;
    }

    protected void removeErrorMessage(final ConfigurationGUI configurationGUI) {
        if (configurationGUI != null) {
            configurationGUI.removeAllErrorTextComponents();
        }
    }

    protected void displayErrorMessage(final String event, String message) {
        updateError(event, message);
    }

    public CloseSaveButtonTabbedPane getTabbedPane() {
        return configTabbedPane;
    }

    protected JPanel createErrorPanel(final JPanel headerPanel) {
        final JPanel errorPanel = new JPanel();
        errorPanel.setLayout(new BoxLayout(errorPanel, BoxLayout.Y_AXIS));
        errorPanel.setBackground(Color.WHITE);
        errorPanel.setOpaque(true);

        errorPanel.addContainerListener(new ContainerListener() {
            @Override
            public void componentRemoved(ContainerEvent e) {
                if (errorPanel.getComponentCount() < 1) {
                    headerPanel.setVisible(false);
                    Container headerContainer = headerPanel.getParent();
                    if (headerContainer instanceof JSplitPane) {
                        Utilities.updateSplitPane((JSplitPane) headerContainer, headerPanel);
                    }
                    configTabbedPane.repaint();
                    configTabbedPane.revalidate();
                }
            }

            @Override
            public void componentAdded(ContainerEvent e) {
                if (!headerPanel.isVisible()) {
                    headerPanel.setVisible(true);
                }
                configTabbedPane.repaint();
                configTabbedPane.revalidate();
            }
        });
        return errorPanel;
    }

    protected JPanel createHeaderPanel() {
        final JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BorderLayout());
        headerPanel.setVisible(false);
        headerPanel.setBackground(Color.WHITE);
        headerPanel.setOpaque(true);

        JLabel iconLabel = new JLabel(Utilities.ERROR_ICON);
        iconLabel.setOpaque(true);
        iconLabel.setBackground(Color.WHITE);
        headerPanel.add(iconLabel, BorderLayout.WEST);

        headerPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                Container headerContainer = headerPanel.getParent();
                if (headerContainer instanceof JSplitPane) {
                    Utilities.updateSplitPane((JSplitPane) headerContainer, headerPanel);
                }
            }
        });
        return headerPanel;
    }

    protected void updateStartButtonAvailability() {
        if (startAllowed) {
            final int selectedIndex = configTabbedPane.getSelectedIndex();
            boolean atLeastOneTabisDisplayed = (selectedIndex > -1);
            boolean currentConfigPanelisSelected = false;
            // If current config tab is selected, disable start button
            if (selectedIndex > -1) {
                String title = configTabbedPane.getConfigNameAt(selectedIndex);
                if ((title != null) && (title.equalsIgnoreCase(Utilities.CURRENT_CONFIG))) {
                    currentConfigPanelisSelected = true;
                }
            }
            if (atLeastOneTabisDisplayed && !(currentConfigPanelisSelected)) {
                startButton.setEnabled(true);
                startButton.setToolTipText(START_SELECTED_CONFIG);
            } else {
                startButton.setEnabled(false);
                startButton.setToolTipText(NO_START_FOR_CONFIG);
            }
        } else {
            startButton.setEnabled(false);
            startButton.setToolTipText(NO_START_FOR_STATE);
        }
        configTabbedPane.enableLoadButtons(startButton.isEnabled());
    }

    protected String[] deparseConfig(Configuration config) {
        String[] configContent;
        if (config == null) {
            configContent = null;
        } else {
            String deparsedConfig = ConfigParser.deparse(config, getPlugins());
            configContent = deparsedConfig.split(ParsingUtil.CRLF);
        }
        return configContent;
    }

    public boolean isEasyConfigOnly() {
        return easyConfigOnly;
    }

    public void setEasyConfigOnly(boolean easyConfigOnly) {
        if (this.easyConfigOnly != easyConfigOnly) {
            this.easyConfigOnly = easyConfigOnly;
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                easyConfigOnlyAction.setSelected(easyConfigOnly);
                for (int i = 0; i < configTabbedPane.getTabCount(); i++) {
                    ConfigurationGUI gui = getConfigurationGUIAt(i);
                    if (gui != null) {
                        gui.displayEasyConfigOnly(easyConfigOnly);
                    }
                }
            });
        }
    }

    public int getFssTimeout() {
        return fssTimeout;
    }

    public void setFssTimeout(int fssTimeout) {
        // Force default timeout to 10s (SCAN-488), and don't allow timeout under 3s.
        int timeout = fssTimeout < 3000 ? DEFAULT_FSS_TIMEOUT : fssTimeout;
        if (this.fssTimeout != timeout) {
            this.fssTimeout = timeout;
            timeOutAction.applyFSSTimeout();
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link FlyScanTarget} for current configuration.
     * 
     * @author guerre-giordano, Rapha&euml;l GIRARDOT
     */
    protected class CurrentConfigTarget extends FlyScanTarget {

        public CurrentConfigTarget() {
            super();
        }

        protected void refreshCurrentConfigTab() {
            int index = getConfigurationTabIndex(Utilities.CURRENT_CONFIG);

            if (index > -1) {
                loadAndDisplayCurrentConfig(isExpert, true, false);
            }
        }

        @Override
        protected void updateFromStringArray(String... data) {
            // When current config changes, reload current config tab if it is opened
            refreshCurrentConfigTab();
            Configuration configuration = getCurrentConfiguration();
            labelIconsMap.clear();
            if (configuration != null) {
                String configurationName = getConfigurationName(configuration);
                currentConfigNameLabel.setText(configurationName);
                Collection<Section> sections = configuration.getSections();
                if (sections != null) {
                    for (Section section : sections) {
                        if (section != null) {
                            if (ParsingUtil.ACTORS.equalsIgnoreCase(section.getName())) {
                                Collection<FSObject> objects = section.getObjects();
                                for (FSObject object : objects) {
                                    if (object instanceof Actor) {
                                        ImageIcon icon = null;
                                        Actor actor = (Actor) object;
                                        if (actor instanceof Actuator) {
                                            icon = Utilities.ACTUATOR_ICON;
                                        } else if ((actor instanceof ContextMonitor) || (actor instanceof Monitor)) {
                                            icon = Utilities.MONITOR_ICON;
                                        } else if (actor instanceof Hook) {
                                            icon = Utilities.HOOK_ICON;
                                        } else if (actor instanceof Sensor) {
                                            icon = Utilities.SENSOR_ICON;
                                        } else if (actor instanceof TimeBase) {
                                            icon = Utilities.TIMEBASE_ICON;
                                        }
                                        if (icon != null) {
                                            fr.soleil.flyscan.lib.model.parsing.configuration.Entry entry = actor
                                                    .getEntry(Utilities.TANGO_DEVICE);
                                            if ((entry != null) && (entry.getValue() != null)) {
                                                labelIconsMap.put(entry.getValue().toLowerCase(), icon);
                                            }
                                        }
                                    } // end if (object instanceof Actor)
                                } // end for (FSObject object : objects)
                            } // end if (ParsingUtil.ACTORS.equalsIgnoreCase(section.getName()))
                        } // end if (section != null)
                    } // end for (Section section : sections)
                } // end if (sections != null)
            } // end if (configuration != null)
            if (monitoredDevices != null) {
                monitoredDevices.refreshLabels();
            }
            updateHighlightedConfiguration();
        }

    }

    /**
     * A {@link SwingWorker} dedicated to {@link Configuration} management and that may be canceled.
     * 
     * @author Rapha&euml;l GIRARDOT
     *
     * @param <T> The result type returned by this {@code SwingWorker's} {@code doInBackground} and {@code get} methods.
     * @param <V> The type used for carrying out intermediate results by this {@code SwingWorker's} {@code publish} and
     *            {@code process} methods.
     */
    protected abstract class ConfigurationWorker<T, V> extends SwingWorker<T, V> implements ICancelable {

        protected volatile boolean canceled;

        public ConfigurationWorker() {
            super();
            canceled = false;
        }

        @Override
        public final boolean isCanceled() {
            return canceled;
        }

        @Override
        public final void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        /**
         * Writes given unparsed configuration in configuration attribute.
         * 
         * @param toRunBeforeWriting A {@link Runnable} that may be run before writing the attribute.
         * @param unparsedConfig The unparsed configuration.
         * @return A <code>boolean</code>. <code>true</code> if configuration attribute was effectively written,
         *         <code>false</code> otherwise.
         * @throws DevFailed If a tango problem occurred.
         */
        protected boolean writeUnparsedConfig(Runnable toRunBeforeWriting, String... unparsedConfig) throws DevFailed {
            boolean written = false;
            if (!isCanceled()) {
                // SCAN-894: don't use TangoAttribue as it will reset expected timeout
                DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(getModel());
                DeviceAttribute attribute = proxy.read_attribute(CONFIGURATION);
                if ((!isCanceled()) && (unparsedConfig != null)) {
                    if (toRunBeforeWriting != null) {
                        toRunBeforeWriting.run();
                    }
                    LOGGER.trace(String.format(WRITE_ATTRIBUTE_VALUE_FORMAT, getModel() + CONFIGURATION_ATTRIBUTE,
                            Utilities.toString(unparsedConfig)));
                    InsertExtractUtils.insert(attribute, unparsedConfig);
                    proxy.write_attribute(attribute);
                    written = true;
                }
            }
            return written;
        }

        /**
         * Writes given unparsed configuration in configuration attribute.
         * 
         * @param unparsedConfig
         * @param unparsedConfig The unparsed configuration.
         * @return A <code>boolean</code>. <code>true</code> if configuration attribute was effectively written,
         *         <code>false</code> otherwise.
         * @throws DevFailed If a tango problem occurred.
         */
        protected boolean writeUnparsedConfig(String... unparsedConfig) throws DevFailed {
            return writeUnparsedConfig(null, unparsedConfig);
        }

        /**
         * Writes given {@link Configuration} in configuration attribute.
         * 
         * @param config The {@link Configuration} to write.
         * @param toRunAfterDeparseAndBeforewrite A {@link Runnable} that may be run before writing the attribute, once
         *            {@link Configuration} is unparsed.
         * @return A <code>boolean</code>. <code>true</code> if configuration attribute was effectively written,
         *         <code>false</code> otherwise.
         * @throws DevFailed If a tango problem occurred.
         */
        protected boolean writeConfig(Configuration config, Runnable toRunAfterDeparseAndBeforewrite) throws DevFailed {
            boolean written = false;
            if (!isCanceled()) {
                written = writeUnparsedConfig(toRunAfterDeparseAndBeforewrite, deparseConfig(config));
            }
            return written;
        }

        /**
         * Writes given {@link Configuration} in configuration attribute.
         * 
         * @param config The {@link Configuration} to write.
         * @return A <code>boolean</code>. <code>true</code> if configuration attribute was effectively written,
         *         <code>false</code> otherwise.
         * @throws DevFailed If a tango problem occurred.
         */
        protected boolean writeConfig(Configuration config) throws DevFailed {
            return writeConfig(config, null);
        }
    }

    /**
     * A {@link SwingWorker} dedicated to a {@link ConfigurationGUI} and that may be canceled.
     * 
     * @author Rapha&euml;l GIRARDOT
     *
     * @param <T> The result type returned by this {@code SwingWorker's} {@code doInBackground} and {@code get} methods.
     * @param <V> The type used for carrying out intermediate results by this {@code SwingWorker's} {@code publish} and
     *            {@code process} methods.
     */
    protected abstract class ConfigurationGUIWorker<T, V> extends ConfigurationWorker<T, V> {

        protected boolean closeDialog;

        public ConfigurationGUIWorker() {
            super();
            closeDialog = true;
        }

        /**
         * Returns the {@link ConfigurationGUI} known by this {@link ConfigurationGuiWorker}.
         * 
         * @return A {@link ConfigurationGUI}.
         */
        protected abstract ConfigurationGUI getConfigurationGUI();

        /**
         * Do what has to be done in {@link #done()} once data obtained through {@link #get()}.
         * 
         * @param data The obtained data.
         */
        protected abstract void treadDataOnceDone(T data);

        /**
         * Do what has to be done in {@link #done()} once data is treated and progress dialog closed.
         */
        protected abstract void clean();

        /**
         * Returns the warning message to send to logger in case of interruption.
         * 
         * @return
         */
        protected abstract String getInterruptionWarningMessage();

        /**
         * Returns the event name to use with
         * {@link FlyScanTangoBox#displayErrorMessage(ConfigurationGUI, String, String)} method.
         * 
         * @return The event name to use.
         */
        protected abstract String getErrorMessageEvent();

        @Override
        protected final void done() {
            ConfigurationGUI configurationGUI = getConfigurationGUI();
            boolean hasError = false;
            try {
                T data = get();
                treadDataOnceDone(data);
            } catch (InterruptedException e) {
                String message = getInterruptionWarningMessage();
                if (message != null) {
                    LOGGER.warn(message);
                }
            } catch (ExecutionException e) {
                hasError = true;
                Throwable ex = e.getCause();
                String message = TangoExceptionHelper.getErrorMessage(ex);
                LOGGER.error(message, ex);
                displayErrorMessage(getErrorMessageEvent(), message);
            } finally {
                if (!hasError) {
                    removeErrorMessage(configurationGUI);
                }
                if (progressDialog != null && closeDialog) {
                    progressDialog.setProgress(progressDialog.getMaxProgress(), ObjectUtils.EMPTY_STRING);
                    progressDialog.setVisible(false);
                }
                clean();
            }

        }
    }

    /**
     * A {@link Label} that can replace first position of a scan by an arrow indicating the type of scan
     * 
     * @author GUERRE-GIORDANO
     */
    protected class ScanPositionLabel extends Label {

        private static final long serialVersionUID = -1385312221472109156L;

        public ScanPositionLabel() {
            super();
            setFont(Utilities.BASIC_FONT);
        }

        @Override
        public void setText(String text) {
            Configuration configuration = getCurrentConfiguration();

            Map<ScanType, int[]> typesAndDimensionsMap = getTypeAndScanDimensions(configuration);
            ScanType scanType = null;
            int[] dimensions = null;
            if (typesAndDimensionsMap.isEmpty()) {
                super.setText(text);
            } else {
                Iterator<Entry<ScanType, int[]>> iterator = typesAndDimensionsMap.entrySet().iterator();
                // No while loop needed, since there is only one entry within the map
                Entry<ScanType, int[]> typesAndDimensions = iterator.next();
                scanType = typesAndDimensions.getKey();
                dimensions = typesAndDimensions.getValue();

                StringBuilder sb = new StringBuilder();
                // dynamic scan position is obsolete (SCAN-594)
                sb.append(text);
                if (dimensions != null) {
                    sb.append(Utilities.SPACE).append(Utilities.SLASH).append(Utilities.SPACE)
                            .append(Utilities.INTERVAL_START);
                    int i = 1;
                    for (int dim : dimensions) {
                        sb.append(dim);
                        if (i < dimensions.length) {
                            sb.append(Utilities.COMMA).append(Utilities.SPACE);
                        }
                        i++;
                    }
                    // add an empty space at end to ensure bracket display
                    sb.append(Utilities.INTERVAL_END).append(Utilities.SPACE);
                }
                if (scanType != null) {
                    sb.append(Utilities.OPEN_PARENTHESIS);
                    switch (scanType) {
                        case ZIGZAG:
                            sb.append(DUAL_ARROW); // <->
                            break;
                        case CONTINUOUS:
                            sb.append(SINGLE_ARROW); // ->
                            break;
                        default:
                            sb.append(Utilities.AND_SO_ON);
                            break;
                    }
                    // add an empty space at end to ensure parenthesis display
                    sb.append(Utilities.CLOSE_PARENTHESIS).append(Utilities.SPACE);
                }
                super.setText(sb.toString());
            }
        }
    }

    protected class CommandButton extends StringButton {

        private static final long serialVersionUID = 4144704422178977095L;

        protected volatile boolean duringChange;

        public CommandButton() {
            super();
            duringChange = false;
            setBackground(Color.LIGHT_GRAY);
        }

        @Override
        protected void paintComponent(Graphics g) {
            // hack to ensure gradient background
            boolean opaque = isOpaque();
            duringChange = true;
            setOpaque(false);
            Graphics2D g2d = (Graphics2D) g;
            Color color1 = getBackground();
            Color color2 = color1.brighter();
            int w = getWidth();
            int h = getHeight();
            if (isEnabled()) {
                GradientPaint gp = new GradientPaint(0, 0, color1, 0, h, color2);
                g2d.setPaint(gp);
            } else {
                g2d.setColor(color2);
            }
            g2d.fillRect(0, 0, w, h);
            super.paintComponent(g);
            duringChange = true;
            setOpaque(opaque);
            duringChange = false;
        }

        @Override
        public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
            // hack to ensure gradient background
            if (!duringChange) {
                super.firePropertyChange(propertyName, oldValue, newValue);
            }
        }

        @Override
        public void setText(String text) {
            if ((text != null) && (!text.trim().isEmpty())) {
                LOGGER.error(text);
                JOptionPane.showMessageDialog(this, text, Utilities.getMessage("flyscan.error"),
                        JOptionPane.ERROR_MESSAGE);
            } else {
                super.setText(text);
            }
        }

    }

    protected class StartStateTarget implements ITextTarget {

        private String lastValue;

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public String getText() {
            return lastValue;
        }

        @Override
        public synchronized void setText(String value) {
            String stateValue = value == null ? DevState.UNKNOWN.toString() : value;
            lastValue = stateValue;
            startAllowed = (!DevState.RUNNING.toString().equalsIgnoreCase(stateValue));
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                updateStartButtonAvailability();
                if (DevState.STANDBY.toString().equalsIgnoreCase(stateValue)) {
                    startLayout.show(startPanel, RESUME);
                } else {
                    startLayout.show(startPanel, START);
                }
            });
        }

    }

    protected class Progress {

        protected final int progress;
        protected final String info;

        public Progress(int progress, String info) {
            super();
            this.progress = progress;
            this.info = (info == null ? ObjectUtils.EMPTY_STRING : info);
        }

        public int getProgress() {
            return progress;
        }

        public String getInfo() {
            return info;
        }

    }

    /**
     * A {@link SwingWorker} dedicated to listening to FSS state in order to update start/stop buttons availability.
     * 
     * @author GIRARDOT
     */
    protected class StateRefreshWorker extends SwingWorker<String, Void> {
        protected final String command;

        public StateRefreshWorker(String command) {
            super();
            this.command = command;
        }

        @Override
        protected String doInBackground() throws Exception {
            return TangoDeviceHelper.getDeviceState(TangoDeviceHelper.recoverDeviceName(getModel()));
        }

        @Override
        protected void done() {
            try {
                String state = get();
                if (startStateTarget != null) {
                    startStateTarget.setText(state);
                }
            } catch (InterruptedException e) {
                LOGGER.warn(String.format(Utilities.getMessage("flyscan.interrupted"), command));
            } catch (ExecutionException e) {
                Throwable ex = e.getCause();
                String message = TangoExceptionHelper.getErrorMessage(ex);
                LOGGER.error(message, ex);
                updateError(Utilities.getMessage("flyscan.state.read"), message);
            }
        }
    }

    /**
     * A {@link SwingWorker} that starts selected configuration.
     * 
     * @author GIRARDOT
     */
    protected class StartWorker extends ConfigurationGUIWorker<String, Progress> {

        protected ConfigurationGUI configurationGUI;
        protected final String toCheck = String.format(WRITE_ATTRIBUTE_START_FORMAT,
                getModel() + CONFIGURATION_ATTRIBUTE);

        @Override
        protected ConfigurationGUI getConfigurationGUI() {
            return configurationGUI;
        }

        @Override
        protected void treadDataOnceDone(String data) {
            if (startStateTarget != null) {
                startStateTarget.setText(data);
            }
        }

        @Override
        protected void clean() {
            new Thread(String.format(Utilities.getMessage("flyscan.state.refresh"), getModel())) {
                @Override
                public void run() {
                    try {
                        sleep(1000);
                        if (startStateTarget != null) {
                            startStateTarget.setText(
                                    TangoDeviceHelper.getDeviceState(TangoDeviceHelper.recoverDeviceName(getModel())));
                        }
                    } catch (InterruptedException e) {
                        // Ignore this exception;
                    }
                }
            }.start();
        }

        @Override
        protected String getInterruptionWarningMessage() {
            return String.format(Utilities.getMessage("flyscan.interrupted"), START);
        }

        @Override
        protected String getErrorMessageEvent() {
            return START_SELECTED_CONFIG;
        }

        @Override
        protected String doInBackground() throws Exception {
            int progress = 0;
            int index = configTabbedPane.getSelectedIndex();
            configurationGUI = getConfigurationGUIAt(index);
            if ((configurationGUI != null) && (!canceled)) {
                String configName = configTabbedPane.getConfigNameAt(index);
                configName = configName.replace(Utilities.ASTERISK, ObjectUtils.EMPTY_STRING).trim();
                if (!configName.isEmpty()) {
                    // Write config
                    String[] unparsedConfig = deparseConfig(configTabbedPane.getConfiguration(index));
                    int effectiveProgress = ++progress;
                    boolean writeOk = writeUnparsedConfig(() -> {
                        publish(new Progress(effectiveProgress, String.format(WRITE_ATTRIBUTE_VALUE_FORMAT,
                                getModel() + CONFIGURATION_ATTRIBUTE, Utilities.toString(unparsedConfig))));
                    }, unparsedConfig);
                    if (writeOk && !canceled) {
                        // Start scan
                        publish(new Progress(++progress, Utilities.getMessage("flyscan.command.start.call")));
                        TangoCommandHelper.executeCommand(TangoDeviceHelper.getDeviceProxy(getModel(), false), START,
                                null);
                    }
                }
            }
            return TangoDeviceHelper.getDeviceState(TangoDeviceHelper.recoverDeviceName(getModel()));
        }

        @Override
        protected void process(List<Progress> chunks) {
            ProgressDialog dialog = progressDialog;
            if (dialog != null) {
                for (Progress progress : chunks) {
                    if (!progress.getInfo().startsWith(toCheck)) {
                        LOGGER.trace(progress.getInfo());
                    }
                    int index = progress.getInfo().indexOf('\n');
                    if (index > -1) {
                        index = progress.getInfo().indexOf('\n', index + 1);
                    }
                    if (index > -1) {
                        dialog.setProgress(progress.getProgress(), progress.getInfo().substring(0, index) + " [...]");
                    } else {
                        dialog.setProgress(progress.getProgress(), progress.getInfo());
                    }
                    dialog.pack();
                }
            }
        }

    }

    /**
     * A {@link SwingWorker} that loads selected configuration in FSS.
     * 
     * @author GIRARDOT
     */
    protected class LoadConfigInFSSWorker extends ConfigurationGUIWorker<Void, Void> {

        private final WeakReference<CloseSaveButtonTab> closeSaveButtonTabRef;

        public LoadConfigInFSSWorker(CloseSaveButtonTab closeSaveButtonTab) {
            super();
            closeSaveButtonTabRef = (closeSaveButtonTab == null ? null : new WeakReference<>(closeSaveButtonTab));
        }

        @Override
        protected Void doInBackground() throws Exception {
            CloseSaveButtonTab closeSaveButtonTab = ObjectUtils.recoverObject(closeSaveButtonTabRef);
            if ((closeSaveButtonTab != null) && (!canceled)) {
                writeConfig(closeSaveButtonTab.getConfigurationGUI().getConfiguration());
            }
            return null;
        }

        @Override
        protected ConfigurationGUI getConfigurationGUI() {
            ConfigurationGUI configurationGUI = null;
            CloseSaveButtonTab closeSaveButtonTab = ObjectUtils.recoverObject(closeSaveButtonTabRef);
            if (closeSaveButtonTab != null) {
                configurationGUI = closeSaveButtonTab.getConfigurationGUI();
            }
            return configurationGUI;
        }

        @Override
        protected void treadDataOnceDone(Void data) {
            // nothing to do
        }

        @Override
        protected void clean() {
            // nothing to do
        }

        @Override
        protected String getInterruptionWarningMessage() {
            return String.format(Utilities.getMessage("flyscan.interrupted"), LOAD_CONFIG);
        }

        @Override
        protected String getErrorMessageEvent() {
            return Utilities.getMessage("flyscan.configuration.load");
        }

    }

    /**
     * A {@link JButton} used to pause/resume the listening to some logs.
     * 
     * @author GIRARDOT
     */
    protected class LogPauseButton extends JButton implements ActionListener {

        private static final long serialVersionUID = 8202196508437765207L;

        protected IKey key;

        public LogPauseButton() {
            super();
            setKey(null);
            setPause(true);
            addActionListener(this);
        }

        @Override
        public void setSelected(boolean selected) {
            if (selected != isSelected()) {
                super.setSelected(selected);
            }
        }

        public IKey getKey() {
            return key;
        }

        public void setKey(IKey key) {
            this.key = key;
            setEnabled(key != null);
        }

        protected void setPause(boolean pause) {
            if (pause) {
                setToolTipText(PAUSE);
                setIcon(Utilities.LOG_PAUSE_ICON);
            } else {
                setToolTipText(RESUME);
                setIcon(Utilities.LOG_PLAY_ICON);
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (PAUSE.equalsIgnoreCase(getToolTipText())) {
                DataSourceProducerProvider.setRefreshingStrategy(key, null);
            } else {
                DataSourceProducerProvider.setRefreshingStrategy(key, DEFAULT_STRATEGY);
            }
            setPause(DataSourceProducerProvider.getRefreshingStrategy(key) != null);
        }
    }

    /**
     * A {@link JSmoothLabel} that takes some more margin to compute its preferred size.
     * It is dedicated in monitored device display.
     * 
     * @author GIRARDOT
     */
    protected class DeviceLabel extends JSmoothLabel {

        private static final long serialVersionUID = -8859914567383151059L;

        public DeviceLabel() {
            super();
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension size = super.getPreferredSize();
            if ((size != null) && !isPreferredSizeSet()) {
                size.width += LABEL_MARGIN;
            }
            return size;
        }

    }

    /**
     * {@link MatrixPanel} that displays monitored devices.
     * 
     * @author GIRARDOT
     */
    protected class MonitoredDevicesPanel extends MatrixPanel {

        private static final long serialVersionUID = -5639453123956914510L;

        public MonitoredDevicesPanel() {
            super();
        }

        @Override
        protected JLabel createLabel() {
            JSmoothLabel label = new DeviceLabel();
            label.setHorizontalAlignment(halign);
            label.setFont(getFont());
            label.setForeground(getForeground());
            return label;
        }

        @Override
        protected void setLabelText(JLabel label, String value) {
            ImageIcon icon;
            if (value == null) {
                icon = null;
            } else {
                icon = labelIconsMap.get(value.toLowerCase());
                if (icon == null) {
                    icon = Utilities.DEVICE_ICON;
                }
            }
            label.setIcon(icon);
            super.setLabelText(label, value);
        }

        public void refreshLabels() {
            stringChanged = true;
            updateLabelsInEDT();
        }

    }

    protected class ChangeFlyScanTimeOutAction extends ChangeDeviceTimeOutAction {

        private static final long serialVersionUID = -3634645546338653365L;

        public ChangeFlyScanTimeOutAction() {
            super();
        }

        public ChangeFlyScanTimeOutAction(boolean refreshTimeOutOnDeviceChange) {
            super(refreshTimeOutOnDeviceChange);
        }

        @Override
        protected RefreshingPeriodPanel generateRefreshingPeriodPanel() {
            return new ChangeFlyScanTimePanel();
        }

        public void applyFSSTimeout() {
            Collection<RefreshingPeriodPanel> panels = getInstanciatedRefreshingPeriodPanels();
            if (panels != null) {
                int timeout = fssTimeout;
                for (RefreshingPeriodPanel panel : panels) {
                    panel.setPrecision(timeout);
                }
            }
        }

        protected class ChangeFlyScanTimePanel extends TimeOutPanel {

            private static final long serialVersionUID = -9175030642342526431L;

            public ChangeFlyScanTimePanel() {
                super();
            }

            @Override
            protected void precisionChanged() {
                fssTimeout = precision;
                super.precisionChanged();
            }

            @Override
            public void setPrecision(int precision) {
                int former = getPrecision();
                super.setPrecision(precision);
                if (getPrecision() != former) {
                    applyLastTimeOut();
                }
            }

        }
    }

    protected class SetEasyConfigOnlyAction extends AbstractActionExt {

        private static final long serialVersionUID = -2917503375173456837L;

        public SetEasyConfigOnlyAction() {
            super(Utilities.getMessage("flyscan.configuration.easy.only.text"));
            putValue(SHORT_DESCRIPTION, Utilities.getMessage("flyscan.configuration.easy.only.tooltip"));
            putValue(SMALL_ICON, Utilities.EASY_CONFIG_ICON);
            setSelected(easyConfigOnly);
            setActionCommand(Utilities.EASY_CONFIG);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setEasyConfigOnly(!easyConfigOnly);
        }

    }

    protected class ConfigNameChecker implements IConfigNameChecker {
        @Override
        public boolean isUniqueConfigName(String name) {
            boolean unique;
            if (name == null) {
                unique = true;
            } else {
                name = name.trim().toLowerCase();
                Collection<String> configList = new HashSet<>();
                configList.add(Utilities.CURRENT_CONFIG.trim().toLowerCase());
                if (configurationsPanel != null) {
                    String[] configurations = configurationsPanel.getFlatStringMatrix();
                    if (configurations != null) {
                        for (String configName : configurations) {
                            if (configName != null) {
                                configList.add(configName.trim().toLowerCase());
                            }
                        }
                    }
                }
                if (configTabbedPane != null) {
                    for (int i = 0; i < configTabbedPane.getTabCount(); i++) {
                        String title = configTabbedPane.getTabTitleAt(i);
                        if (title != null) {
                            String tmp = title.trim().toLowerCase();
                            configList.add(tmp);
                        }
                    }
                }
                unique = !configList.contains(name);
            }
            return unique;
        }

        @Override
        public String getExistingConfigName(String name) {
            String refName = null;
            if (name != null) {
                String toTest = name.trim();
                if (configurationsPanel != null) {
                    String[] configurations = configurationsPanel.getFlatStringMatrix();
                    if (configurations != null) {
                        for (String configName : configurations) {
                            if ((configName != null) && configName.trim().equalsIgnoreCase(toTest)) {
                                refName = configName;
                                break;
                            }
                        }
                    }
                    if ((refName == null) && (configTabbedPane != null)) {
                        for (int i = 0; i < configTabbedPane.getTabCount(); i++) {
                            String title = configTabbedPane.getTabTitleAt(i);
                            if (title.equalsIgnoreCase(toTest)) {
                                refName = title;
                                break;
                            }
                        }
                    }
                }
            }
            return refName;
        }

    }

    protected static class ConfigurationGUIContainer {
        private ConfigurationGUI configurationGUI;
        private JPanel headerPanel, errorPanel;

        public ConfigurationGUIContainer(ConfigurationGUI configurationGUI, JPanel headerPanel, JPanel errorPanel) {
            super();
            this.configurationGUI = configurationGUI;
            this.headerPanel = headerPanel;
            this.errorPanel = errorPanel;
        }

        public ConfigurationGUI getConfigurationGUI() {
            return configurationGUI;
        }

        public JPanel getHeaderPanel() {
            return headerPanel;
        }

        public JPanel getErrorPanel() {
            return errorPanel;
        }

        @Override
        protected void finalize() {
            configurationGUI = null;
            headerPanel = null;
            errorPanel = null;
        }
    }

    protected abstract class AConfigGUIBuilder extends SwingWorker<ConfigurationGUIContainer, Void>
            implements ICancelable {

        protected volatile boolean canceled;
        protected final String configName;
        protected int index;
        protected String message;
        protected boolean isExpert, closeDialog;
        protected ConfigurationGUIContainer guiContainer;

        public AConfigGUIBuilder(String configName, int index, boolean isExpert, boolean closeDialog) {
            super();
            this.configName = configName;
            this.index = index;
            this.isExpert = isExpert;
            this.closeDialog = closeDialog;
            message = null;
            guiContainer = null;
        }

        protected abstract String[] getConfigContent() throws DevFailed;

        protected abstract boolean shouldCheckConfig();

        protected void treatNullConfig() {
            // nothing to do by default
        }

        protected void treatExecutionError(Throwable error) {
            String message = TangoExceptionHelper.getErrorMessage(error);
            if (message == null) {
                message = ObjectUtils.EMPTY_STRING;
            }
            if (error == null) {
                LOGGER.error(message);
            } else {
                LOGGER.error(message, error);
            }
            displayErrorMessage(String.format(Utilities.getMessage("flyscan.configuration.read"), configName), message);
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        protected ConfigurationGUIContainer doInBackground() throws Exception {
            ConfigurationGUIContainer guiContainer = null;
            // Read config
            String[] configContent;
            if (canceled) {
                configContent = null;
            } else {
                configContent = getConfigContent();
                // Before loading a configuration, apply a checkConfig command on it
                try {
                    if ((!canceled) && shouldCheckConfig()) {
                        String[] checkResult = checkConfig(configContent);
                        if ((!canceled) && (checkResult != null)) {
                            StringBuilder sb = new StringBuilder();
                            for (String s : checkResult) {
                                sb.append(s);
                                sb.append(ParsingUtil.CRLF);
                            }
                            message = sb.toString();
                            sb.delete(0, sb.length() - (ParsingUtil.CRLF.length() - 1));
                        }
                    }
                } catch (DevFailed e) {
                    if (!canceled) {
                        message = TangoExceptionHelper.getErrorMessage(e);
                        LOGGER.error(message, e);
                    }
                }
                if (!canceled) {
                    // Parse config
                    Configuration configuration = ConfigParser.parse(configName, configContent);
                    if (!canceled) {
                        // Parse plugins
                        Collection<Plugin> plugins = getPlugins();
                        Map<PluginType, Collection<Plugin>> categories = pluginTarget.getCategories();

                        // Create configuration GUI for edition
                        if (!canceled) {
                            guiContainer = createConfigurationGUI(configuration, plugins, categories, isExpert,
                                    shouldCheckConfig());
                            this.guiContainer = guiContainer;
                        }
                    }
                }
            }
            return guiContainer;
        }

        @Override
        public void done() {
            ConfigurationGUIContainer guiContainer;
            try {
                if (canceled) {
                    guiContainer = null;
                } else {
                    guiContainer = get();
                }
            } catch (InterruptedException e) {
                guiContainer = null;
                canceled = true;
            } catch (ExecutionException e) {
                guiContainer = null;
                if (!canceled) {
                    treatExecutionError(e.getCause());
                }
            }
            if (guiContainer != null && !canceled) {
                ConfigurationGUI configurationGUI = guiContainer.getConfigurationGUI();
                if (configurationGUI == null) {
                    treatNullConfig();
                } else {
                    addConfigurationGUI(guiContainer, isExpert, true, index, message);
                }
            }
            if (closeDialog && (progressDialog != null) && progressDialog.isVisible()) {
                progressDialog.setVisible(false);
            }
        }

        @Override
        protected void finalize() {
            guiContainer = null;
        }
    }

    protected class ConfigGUIBuilder extends AConfigGUIBuilder {
        protected boolean currentConfig;

        public ConfigGUIBuilder(String configName, int index, boolean isExpert, boolean closeDialog) {
            super(configName, index, isExpert, closeDialog);
            currentConfig = Utilities.CURRENT_CONFIG.equalsIgnoreCase(configName);
        }

        @Override
        protected String[] getConfigContent() throws DevFailed {
            return currentConfig ? readCurrentConfig() : readConfig(configName);
        }

        @Override
        protected boolean shouldCheckConfig() {
            return !currentConfig;
        }

        @Override
        protected void treatNullConfig() {
            if (currentConfig) {
                configTabbedPane.addEmptyConfigTab(Utilities.CURRENT_CONFIG, index);
            }
        }

    }

    protected class MockConfigGUIBuilder extends AConfigGUIBuilder {
        protected File configFile;
        protected Collection<String> errors;

        public MockConfigGUIBuilder(File configFile, boolean isExert) {
            super(configFile.getName().replace(TXT_EXTENSION, ObjectUtils.EMPTY_STRING).trim(), -1, isExert, true);
            this.configFile = configFile;
            errors = new ArrayList<>();
        }

        @Override
        protected String[] getConfigContent() throws DevFailed {
            String[] configContent = null;
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(configFile));) {
                Collection<String> lines = new ArrayList<>();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    lines.add(line);
                }
                configContent = lines.toArray(new String[lines.size()]);
                if (pluginTarget.getMockPlugins().isEmpty()) {
                    errors.add(Utilities.getMessage("flyscan.load.file.plugin.before.config"));
                    configContent = null;
                }
                lines.clear();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
                errors.add(e.getMessage());
            }
            return configContent;
        }

        @Override
        protected boolean shouldCheckConfig() {
            return false;
        }

        @Override
        protected void treatExecutionError(Throwable error) {
            LOGGER.error(error.getMessage());
            errors.add(error.getMessage());
        }

        @Override
        public void done() {
            super.done();
            if (!errors.isEmpty()) {
                updateError(String.format(Utilities.getMessage("flyscan.configuration.read"), configName), errors);
            }
        }

        @Override
        protected void finalize() {
            super.finalize();
            configFile = null;
            errors.clear();
        }

    }

    protected class ConfigGUIBuilderWithMemory extends ConfigGUIBuilder {

        protected String title, configTitle;
        protected int hPos, vPos;
        protected ConfigurationGUI gui;

        public ConfigGUIBuilderWithMemory(String configName, int index, boolean isExpert, String title,
                String configTitle, int hPos, int vPos, ConfigurationGUI gui) {
            super(configName, index, isExpert, false);
            this.title = title;
            this.configTitle = configTitle;
            this.hPos = hPos;
            this.vPos = vPos;
            this.gui = gui;
        }

        @Override
        public void done() {
            super.done();
            if (title != null) {
                if ((guiContainer == null) || (guiContainer.getConfigurationGUI() == null)) {
                    if (gui != null) {
                        int tmpIndex = getConfigurationTabIndex(configName);
                        if (tmpIndex < 0) {
                            if (index < 0) {
                                configTabbedPane.addTabWithHeader(configTitle, gui, null, index, false,
                                        createReloadListener(isExpert, title),
                                        Utilities.isStepByStep(gui.getConfiguration())
                                                ? createDeleteConfigListener(isExpert, title)
                                                : null);
                            }
                        }
                    }
                } else {
                    ConfigurationGUI configurationGUI = guiContainer.getConfigurationGUI();
                    index = configurationGUI.getTabbedPane().getTabIndexForTitle(title);
                    if (index > -1) {
                        configurationGUI.getTabbedPane().setSelectedIndex(index);
                        if ((vPos > -1) && (hPos > -1)) {
                            Component comp = configurationGUI.getTabbedPane().getComponentAt(index);
                            int vValue = vPos, hValue = hPos;
                            SwingUtilities.invokeLater(() -> {
                                if (comp instanceof JScrollPane) {
                                    JScrollPane scrollPane = (JScrollPane) comp;
                                    scrollPane.getVerticalScrollBar().setValue(vValue);
                                    scrollPane.getHorizontalScrollBar().setValue(hValue);
                                }
                            });
                        }
                    }
                }
            }
            if ((progressDialog != null) && progressDialog.isVisible()) {
                progressDialog.setVisible(false);
            }
        }
    }

    protected class ConfigReloader implements Runnable, ICancelable {
        protected volatile boolean canceled;
        protected final boolean isExpert;
        protected final String configName;
        protected volatile ConfigGUIBuilderWithMemory builder;
        protected int index;

        public ConfigReloader(final boolean isExpert, final String configName) {
            super();
            this.isExpert = isExpert;
            this.configName = configName;
            index = -1;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
            ConfigGUIBuilderWithMemory builder = this.builder;
            if (builder != null) {
                builder.setCanceled(canceled);
            }
        }

        @Override
        public void run() {
            index = getConfigurationTabIndex(configName);
            String configTitle = configTabbedPane.getTabTitleAt(index);
            ConfigurationGUI gui = searchConfigurationGUI(configTabbedPane.getComponentAt(index));
            // SCAN-918: remember selected tab
            String title = null;
            int vPos = -1, hPos = -1;
            if (gui != null) {
                int selected = gui.getTabbedPane().getSelectedIndex();
                title = gui.getTabbedPane().getTabTitleAt(selected);
                if (selected > -1) {
                    Component comp = gui.getTabbedPane().getComponentAt(selected);
                    if (comp instanceof JScrollPane) {
                        JScrollPane scrollPane = (JScrollPane) comp;
                        vPos = scrollPane.getVerticalScrollBar().getValue();
                        hPos = scrollPane.getHorizontalScrollBar().getValue();
                    }
                }
            }
            if (!canceled) {
                configTabbedPane.remove(index);
                builder = new ConfigGUIBuilderWithMemory(configName, index, isExpert, title, configTitle, hPos, vPos,
                        gui);
                builder.setCanceled(canceled);
                builder.execute();
            }
        }

    }

}
