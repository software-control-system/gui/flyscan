package fr.soleil.flyscan.gui.view.trajectory;

import java.awt.Component;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.text.JTextComponent;

import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Interval;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.IntervalsTrajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;

public class IntervalsTrajectoryComponent extends AIntervalTrajectoryComponent<IntervalsTrajectory> {

    private static final long serialVersionUID = -259830428236814655L;

    public IntervalsTrajectoryComponent(JPanel errorPanel, Map<Component, JTextComponent> errorsMap,
            IntervalsTrajectory trajectory, String location, ConfigurationGUI configurationGUI, JLabel nameLabel,
            JRadioButton selector, String tabTitle, final String configName, final String actuatorName) {
        super(errorPanel, errorsMap, location, configurationGUI, nameLabel, selector, tabTitle, configName,
                actuatorName);
        this.trajectory = trajectory;
        List<Interval> intervals = trajectory.getIntervals();
        buildMainPanel(intervals);
    }

    @Override
    public IntervalsTrajectory getTrajectory() {
        trajectory.setIntervals(intervals);
        return trajectory;
    }

    @Override
    protected boolean isMyTrajectoryClass(Trajectory trajectory) {
        return trajectory instanceof IntervalsTrajectory;
    }

}
