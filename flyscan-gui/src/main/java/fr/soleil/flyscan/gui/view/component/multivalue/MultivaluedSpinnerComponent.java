package fr.soleil.flyscan.gui.view.component.multivalue;

import java.util.Collection;
import java.util.EventObject;

import fr.soleil.comete.definition.listener.ISpinnerListener;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingTool;

public class MultivaluedSpinnerComponent extends AbstractMultivaluedComponent<Spinner, Long>
        implements ISpinnerListener {

    private static final long serialVersionUID = 5556390022497185290L;

    private Long minimumValue;

    public MultivaluedSpinnerComponent(Collection<Long> values, int count, boolean mayThrowException)
            throws FSParsingException {
        super(Long.class, values, count, mayThrowException);
    }

    public MultivaluedSpinnerComponent(Collection<Long> values, int count, boolean mayThrowException, Long minimumValue)
            throws FSParsingException {
        super(Long.class, values, count, mayThrowException, minimumValue);
    }

    @Override
    protected void initBeforeValues(Object... otherData) {
        if ((otherData != null) && (otherData.length > 0)) {
            Object data = otherData[0];
            if (data instanceof Long) {
                minimumValue = (Long) data;
            }
        }
    }

    @Override
    protected void setValueToComponent(Spinner component, Long value, Integer rank) {
        if (value == null) {
            if ((minimumValue == null) || (minimumValue.longValue() < 0)) {
                value = Long.valueOf(0);
            } else {
                value = minimumValue;
            }
        }
        component.setNumberValue(value);
    }

    @Override
    protected void listenToComponent(Spinner component) {
        component.addSpinnerListener(this);
    }

    @Override
    protected void forgetComponent(Spinner component) {
        component.removeSpinnerListener(this);
    }

    @Override
    protected Spinner createComponent() {
        final Spinner ws = Utilities.createSpinner();
        if (minimumValue != null) {
            if (minimumValue.longValue() > 0) {
                ws.setValue(minimumValue);
            }
            ws.setMinimum(minimumValue);
        }
        return ws;
    }

    @Override
    protected Long getValue(Spinner component) {
        return (Long) component.getNumberValue();
    }

    @Override
    protected String toString(Long value) {
        return ParsingTool.toString(value);
    }

    @Override
    public void valueChanged(EventObject event) {
        fireEditionStateChanged(true);
    }

    @Override
    protected Long parse(String value) {
        return Long.valueOf(ParsingTool.parseLong(value));
    }

}
