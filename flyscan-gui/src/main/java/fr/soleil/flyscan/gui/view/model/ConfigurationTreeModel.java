package fr.soleil.flyscan.gui.view.model;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * TreeModel for ConfigurationTree.
 * 
 * @author GIRARDOT
 */
public class ConfigurationTreeModel extends DefaultTreeModel {

    private static final long serialVersionUID = 6162698448425080277L;

    private volatile String filter;
    private volatile String[] configList, filteredConfigList;
    private volatile boolean hasFolders;

    public ConfigurationTreeModel() {
        super(new DefaultMutableTreeNode());
        configList = Utilities.EMPTY_STRING_ARRAY;
    }

    @Override
    public DefaultMutableTreeNode getRoot() {
        return (DefaultMutableTreeNode) super.getRoot();
    }

    protected void cleanNode(DefaultMutableTreeNode node) {
        if (node != null) {
            while (node.getChildCount() > 0) {
                TreeNode child = node.getChildAt(0);
                if (child instanceof DefaultMutableTreeNode) {
                    cleanNode((DefaultMutableTreeNode) child);
                } else {
                    break;
                }
            }
            if (!node.isRoot()) {
                removeNodeFromParent(node);
            }
        }
    }

    protected void updateTreeStructure() {
        boolean hasFolders = false;
        String[] configList = filteredConfigList;
        cleanNode(getRoot());
        Map<String, PathNode> nodeMap = new HashMap<>();
        try {
            for (String config : configList) {
                DefaultMutableTreeNode parent = getRoot();
                int index = config.indexOf('/'), previousIndex = -1;
                String path, fullPath;
                while (index > -1) {
                    hasFolders = true;
                    fullPath = config.substring(0, index);
                    PathNode node = nodeMap.get(fullPath);
                    if (node == null) {
                        if (previousIndex < 0) {
                            path = fullPath;
                        } else {
                            path = config.substring(previousIndex, index);
                        }
                        node = new PathNode(path, fullPath);
                        nodeMap.put(fullPath, node);
                        insertNodeInto(node, parent, parent.getChildCount());
                    }
                    parent = node;
                    previousIndex = index + 1;
                    index = config.indexOf('/', previousIndex);
                }
                if (previousIndex < 0) {
                    path = config;
                } else {
                    path = config.substring(previousIndex);
                }
                insertNodeInto(new PathNode(path, config), parent, parent.getChildCount());
            }
        } finally {
            nodeMap.clear();
        }
        this.hasFolders = hasFolders;
    }

    protected void updateFilteredList() {
        String filter = this.filter;
        String[] configList = this.configList;
        if ((configList == null) || (configList.length == 0)) {
            filteredConfigList = Utilities.EMPTY_STRING_ARRAY;
        } else if ((filter == null) || filter.isEmpty()) {
            filteredConfigList = configList;
        } else {
            Collection<String> filteredList = new ArrayList<>();
            for (String config : configList) {
                if (config.toLowerCase().contains(filter)) {
                    filteredList.add(config);
                }
            }
            filteredConfigList = filteredList.toArray(new String[filteredList.size()]);
            filteredList.clear();
        }
        SwingUtilities.invokeLater(() -> {
            updateTreeStructure();
        });
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        // First, trim and lower case received filter.
        String filterToApply = filter == null ? null : filter.trim().toLowerCase();
        if (filterToApply.isEmpty()) {
            filterToApply = null;
        }
        // Then, apply obtained filter only if it differs from previous one.
        if (!ObjectUtils.sameObject(this.filter, filterToApply)) {
            this.filter = filterToApply;
            updateFilteredList();
        }
    }

    public String[] getConfigList() {
        return configList;
    }

    protected Collection<String> getSortedConfigsAndClear(Collection<String> configList) {
        Collection<String> sortedConfigs;
        if (configList == null) {
            sortedConfigs = new ArrayList<>();
        } else if (configList.isEmpty()) {
            sortedConfigs = configList;
        } else {
            Map<String, Collection<String>[]> confMap = new TreeMap<>(Collator.getInstance());
            for (String config : configList) {
                if (config != null) {
                    int index = config.indexOf('/');
                    if (index > -1) {
                        String folder = config.substring(0, index);
                        Collection<String>[] lists = confMap.get(folder);
                        if (lists == null) {
                            @SuppressWarnings("unchecked")
                            Collection<String>[] tmp = new Collection[2];
                            tmp[0] = new TreeSet<>(Collator.getInstance());
                            tmp[1] = new TreeSet<>(Collator.getInstance());
                            confMap.put(folder, tmp);
                            lists = tmp;
                        }
                        Collection<String> folders = lists[0], noFolder = lists[1];
                        String name = config.substring(index + 1);
                        if (name.indexOf('/') > -1) {
                            folders.add(name);
                        } else {
                            noFolder.add(name);
                        }
                    }
                }
            }
            try {
                configList.clear();
            } catch (Exception e) {
                // Ignore Exception
            }
            sortedConfigs = new ArrayList<>(configList.size());
            for (Entry<String, Collection<String>[]> entry : confMap.entrySet()) {
                String folder = entry.getKey();
                Collection<String> folders = getSortedConfigsAndClear(entry.getValue()[0]);
                Collection<String> noFolder = entry.getValue()[1];
                for (String config : folders) {
                    sortedConfigs.add(folder + '/' + config);
                }
                folders.clear();
                for (String config : noFolder) {
                    sortedConfigs.add(folder + '/' + config);
                }
                noFolder.clear();
            }
            confMap.clear();
        }
        return sortedConfigs;
    }

    public void setConfigList(String... configList) {
        String[] configs = configList;
        if ((configList != null) && (configList.length > 0)) {
            Collection<String> folders = new TreeSet<>(Collator.getInstance());
            Collection<String> noFolder = new TreeSet<>(Collator.getInstance());
            for (String config : configList) {
                if (config != null) {
                    if (config.indexOf('/') > -1) {
                        folders.add(config);
                    } else {
                        noFolder.add(config);
                    }
                }
            }
            folders = getSortedConfigsAndClear(folders);
            configs = new String[folders.size() + noFolder.size()];
            int index = 0;
            for (String config : folders) {
                configs[index++] = config;
            }
            folders.clear();
            for (String config : noFolder) {
                configs[index++] = config;
            }
            noFolder.clear();
        }
        if (!ArrayUtils.equals(configs, this.configList)) {
            this.configList = configs;
            updateFilteredList();
        }
    }

    public boolean hasFolders() {
        return hasFolders;
    }

}
