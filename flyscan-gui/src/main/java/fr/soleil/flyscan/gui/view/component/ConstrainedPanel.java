package fr.soleil.flyscan.gui.view.component;

import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.JPanel;

/**
 * A {@link JPanel} that limits its preferred size to its maximum size.
 * 
 * @author GIRARDOT
 */
public class ConstrainedPanel extends JPanel {

    private static final long serialVersionUID = -5737464609706060865L;

    public ConstrainedPanel() {
        super();
    }

    public ConstrainedPanel(LayoutManager layout) {
        super(layout);
    }

    public ConstrainedPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
    }

    public ConstrainedPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension size = super.getPreferredSize();
        if ((size != null) && !isPreferredSizeSet()) {
            Dimension maxSize = getMaximumSize();
            if (maxSize != null) {
                size = new Dimension(Math.min(size.width, maxSize.width), Math.min(size.height, maxSize.height));
            }
        }
        return size;
    }

}
