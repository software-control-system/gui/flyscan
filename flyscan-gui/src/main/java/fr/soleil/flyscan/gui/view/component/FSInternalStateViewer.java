package fr.soleil.flyscan.gui.view.component;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.gui.util.InternalState;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

/**
 * A viewer dedicated to FlyScanServer's internalState attribute
 * 
 * @author GIRARDOT
 */
public class FSInternalStateViewer extends DynamicForegroundLabel implements ITextTarget {

    private static final long serialVersionUID = 3183627424924391173L;

    public FSInternalStateViewer() {
        super();
        setOpaque(true);
    }

    protected void applyInteralState(InternalState state) {
        if (state == null) {
            state = InternalState.UNKNOWN;
        }
        super.setText(state.getText());
        super.setToolTipText(state.getHelp());
        super.setBackground(state.getColor());
    }

    @Override
    public void setText(String text) {
        applyInteralState(InternalState.parseInternalState(text));
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

}
