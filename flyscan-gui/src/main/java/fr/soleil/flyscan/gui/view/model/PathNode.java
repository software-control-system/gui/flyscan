package fr.soleil.flyscan.gui.view.model;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * A {@link DefaultMutableTreeNode} that contains a simple path and a full path.
 * 
 * @author GIRARDOT
 */
public class PathNode extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 4149121919180795013L;

    private String fullPath;

    public PathNode(String path, String fullPath) {
        super();
        super.setUserObject(path);
        this.fullPath = fullPath;
    }

    @Override
    public void setUserObject(Object userObject) {
        // not allowed
    }

    public String getFullPath() {
        return fullPath;
    }

}
