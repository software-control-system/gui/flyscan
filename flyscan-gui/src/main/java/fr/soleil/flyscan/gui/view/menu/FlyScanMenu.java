package fr.soleil.flyscan.gui.view.menu;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.flyscan.gui.FlyScanTangoBox;
import fr.soleil.flyscan.gui.listener.ConfigItemDynamicListener;
import fr.soleil.flyscan.gui.listener.ConfigSelectionListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.ConfigurationTree;
import fr.soleil.flyscan.gui.view.renderer.ConfigurationTreeRenderer;
import fr.soleil.flyscan.gui.view.scrollpane.FSScrollPane;
import fr.soleil.flyscan.gui.view.ui.ConfigMenuItemUI;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * Tango menu handling matrix target with custom action listener
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public class FlyScanMenu extends JMenu implements ITextMatrixTarget, IErrorNotifiableTarget {

    private static final long serialVersionUID = -4029935725806516616L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(FlyScanTangoBox.class.getName());

    private volatile String[] configurations;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private final ConfigSelectionListener configListener;
    private final TextFieldMenuItem searchField;
    private final ConfigurationTree configTree;
    private final FSScrollPane configTreeScrollPane;

    private final JMenuItem newConfigMenuItem, currentConfigMenuItem;

    /**
     * Constructor
     * 
     * @param configSelectionListener action listener to be applied to each menu item
     * @param currentConfigMenuItemActionListener ActionListener
     * @param filter Filter to apply to original list of item
     */
    public FlyScanMenu(ConfigSelectionListener configSelectionListener, ActionListener newConfigActionListener,
            ActionListener currentConfigMenuItemActionListener) {
        super();
        this.configListener = configSelectionListener;

        ConfigItemDynamicListener configItemListener = new ConfigItemDynamicListener();
        if (newConfigActionListener == null) {
            newConfigMenuItem = null;
        } else {
            newConfigMenuItem = createMenuItem(Utilities.NEW_CONFIG, Utilities.NEW_CONFIG_ICON, configItemListener);
            newConfigMenuItem.addActionListener(newConfigActionListener);
        }
        currentConfigMenuItem = createMenuItem(Utilities.CURRENT_CONFIG, Utilities.SCAN_CONFIGURATION_ICON,
                configItemListener);
        currentConfigMenuItem.addActionListener(currentConfigMenuItemActionListener);
        getPopupMenu().addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                // nothing to do
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                SwingUtilities.invokeLater(() -> {
                    currentConfigMenuItem.setBorder(Utilities.EMPTY_BORDER);
                    if (newConfigMenuItem != null) {
                        newConfigMenuItem.setBorder(Utilities.EMPTY_BORDER);
                    }
                    ((ConfigurationTreeRenderer) configTree.getCellRenderer()).cleanLabels();
                });
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
                // nothing to do
            }
        });

        configurations = Utilities.EMPTY_STRING_ARRAY;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        configTree = new ConfigurationTree(configListener);
        // hack to align tree nodes with "New Configuration" and "Current Configuration" items.
        configTree.setBorder(new EmptyBorder(0, 3, 0, 0));
        configTreeScrollPane = new FSScrollPane(configTree) {

            private static final long serialVersionUID = 4409563691527068015L;

            @Override
            public Dimension getPreferredSize() {
                // Hack preferred size to avoid the need of horizontal scrolling at filter change
                Dimension size = super.getPreferredSize();
                if (!isPreferredSizeSet()) {
                    Dimension treeSize = configTree.getPreferredSize();
                    if (size.width <= treeSize.width + 4 && size.height > treeSize.height + 4) {
                        size.width = treeSize.width + getVerticalScrollBar().getPreferredSize().width + 4;
                    }
                }
                return size;
            }
        };
        configTreeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        configTree.setBackground(getBackground());
        TreeModelListener contentListener = new TreeModelListener() {
            protected void contentChanged() {
                updateConfigTreeLater();
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                contentChanged();
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                contentChanged();
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                contentChanged();
            }

            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                contentChanged();
            }

        };
        configTree.getModel().addTreeModelListener(contentListener);
        searchField = new TextFieldMenuItem();

        searchField.getTextFieldComponent().getDocument().addDocumentListener(new DocumentListener() {

            protected void sendFilter() {
                configTree.getModel().setFilter(searchField.getTextFieldComponent().getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                sendFilter();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                sendFilter();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }
        });

        for (KeyListener listener : getKeyListeners()) {
            removeKeyListener(listener);
        }
        for (KeyListener listener : getPopupMenu().getKeyListeners()) {
            getPopupMenu().removeKeyListener(listener);
        }
        getPopupMenu().addPopupMenuListener(new PopupMenuListener() {

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        searchField.getTextFieldComponent().grabFocus();
                        searchField.getTextFieldComponent().requestFocusInWindow();
                    }
                });
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        add(searchField);
        if (newConfigMenuItem != null) {
            add(newConfigMenuItem);
        }
        add(currentConfigMenuItem);
        add(configTreeScrollPane);
        updateConfigTreeLater();
    }

    protected void updateConfigTreeLater() {
        // Double SwingUtilities.invokeLater to avoid the need of horizontal scrolling.
        // If everything is done in a single SwingUtilities.invokeLater,
        // there are sometimes some cases where the node text is not fully displayed and needs some scrolling.
        SwingUtilities.invokeLater(() -> {
            configTree.expandRoot();
            configTree.revalidate();
            SwingUtilities.invokeLater(() -> {
                configTreeScrollPane.revalidate();
                revalidate();
                getPopupMenu().pack();
                if (getPopupMenu().isVisible()) {
                    boolean focus = searchField.getTextFieldComponent().hasFocus();
                    getPopupMenu().repaint();
                    if (focus) {
                        SwingUtilities.invokeLater(() -> {
                            searchField.getTextFieldComponent().grabFocus();
                        });
                    }
                }
            });
        });
    }

    protected JMenuItem createMenuItem(String text, Icon icon, ConfigItemDynamicListener currentConfigListener) {
        JMenuItem configMenuItem = new JMenuItem(text);
        configMenuItem.setBorder(Utilities.EMPTY_BORDER);
        configMenuItem.setIcon(icon);
        configMenuItem.setUI(new ConfigMenuItemUI());
        configMenuItem.addMouseListener(currentConfigListener);
        configMenuItem.addMouseMotionListener(currentConfigListener);
        configMenuItem.addActionListener(currentConfigListener);
        return configMenuItem;
    }

    @Override
    public String[][] getStringMatrix() {
        String[] configurations = this.configurations;
        return configurations == null ? null : new String[][] { configurations.clone() };
    }

    @Override
    public void setStringMatrix(String[][] array) {
        // width and height are not used
        setFlatStringMatrix((String[]) ArrayUtils.convertArrayDimensionFromNTo1(array), 0, 0);
    }

    @Override
    public String[] getFlatStringMatrix() {
        return configurations;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        try {
            configurations = value;
            configTree.getModel().setConfigList(value);
            updateConfigTreeLater();
        } catch (Exception e) {
            LOGGER.error(Utilities.getMessage("flyscan.error.configurations.extract.failed"), e);
        }
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (String.class.equals(concernedDataClass)) {
            String[] configurations = this.configurations;
            height = configurations == null || configurations.length == 0 ? 0 : 1;
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (String.class.equals(concernedDataClass)) {
            String[] configurations = this.configurations;
            width = configurations == null ? 0 : configurations.length;
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

}
