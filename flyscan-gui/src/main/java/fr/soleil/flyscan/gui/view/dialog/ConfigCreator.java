package fr.soleil.flyscan.gui.view.dialog;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;
import java.lang.ref.WeakReference;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;

import fr.soleil.comete.swing.Spinner;
import fr.soleil.flyscan.gui.util.IConfigNameChecker;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class ConfigCreator extends ANamedDataChooser {

    private static final long serialVersionUID = -9183163866409286540L;

    protected String flyScanVersion;
    protected WeakReference<IConfigNameChecker> checkerRef;
    protected final ConstrainedCheckBox continuousBox;
    protected JLabel dimensionsLabel;
    protected Spinner dimensionsEditor;

    public ConfigCreator(Window owner, String flyScanVersion, IConfigNameChecker checker) {
        super(owner, Utilities.NEW_CONFIG);
        setIconImage(Utilities.NEW_CONFIG_ICON.getImage());
        this.flyScanVersion = flyScanVersion == null ? Utilities.DEFAULT_VERSION : flyScanVersion;
        this.checkerRef = checker == null ? null : new WeakReference<>(checker);
        continuousBox = new ConstrainedCheckBox(ParsingUtil.CONTINUOUS);
        dimensionsLabel = new JLabel(Utilities.DIMENSIONS, SwingConstants.RIGHT);
        dimensionsLabel.setFont(Utilities.TITLE_FONT);
        dimensionsEditor = new Spinner();
        dimensionsEditor.setNumberValue(Long.valueOf(1));
        dimensionsEditor.setMinimum(Long.valueOf(1));
        Dimension size = nameField.getPreferredSize();
        int height = dimensionsEditor.getPreferredSize().height;
        if (size.height < height) {
            size.height = height;
            nameField.setPreferredSize(size);
        }
        JPanel mainPanel = (JPanel) getContentPane();
        GridBagConstraints nameConstraints = addNameComponents(mainPanel);
        int y = nameConstraints.gridy;
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = nameConstraints.gridx + 1;
        constraints.gridy = y++;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = new Insets(DEFAULT_MARGIN, GAP, 0, DEFAULT_MARGIN);
        mainPanel.add(continuousBox, constraints);
        y = addErrorLabel(mainPanel);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = new Insets(DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN, TITLE_GAP);
        mainPanel.add(dimensionsLabel, constraints);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = y++;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.insets = new Insets(DEFAULT_MARGIN, 0, DEFAULT_MARGIN, DEFAULT_MARGIN);
        mainPanel.add(dimensionsEditor, constraints);
        addButtons(mainPanel);
        continuousBox.setVisible(false);
    }

    @Override
    protected void checkName(DocumentEvent e) {
        String name = nameField.getText();
        if (name == null) {
            okButton.setEnabled(false);
            errorLabel.setText(ObjectUtils.EMPTY_STRING);
        } else {
            name = name.trim();
            boolean invalid, cfg = false;
            if (name.isEmpty()) {
                invalid = true;
            } else {
                String[] split = name.split(Utilities.SLASH);
                for (String val : split) {
                    if (val.trim().toLowerCase().endsWith(CFG_EXT)) {
                        cfg = true;
                        break;
                    }
                }
                if (name.lastIndexOf('/') == name.length() - 1) {
                    invalid = false;
                } else {
                    invalid = cfg;
                }
            }
            String existingName = invalid ? null : getExisting(name);
            okButton.setEnabled((!invalid) && (existingName == null) && !cfg);
            if (cfg) {
                errorLabel.setText(CFG_ERROR_MESSAGE);
            } else if (invalid) {
                errorLabel.setText(ObjectUtils.EMPTY_STRING);
            } else if (existingName == null) {
                errorLabel.setText(ObjectUtils.EMPTY_STRING);
            } else {
                errorLabel.setText(String.format(ERROR_MESSAGE_FORMAT, existingName));
            }
        }
    }

    @Override
    protected String getExisting(String name) {
        String existing = null;
        IConfigNameChecker checker = ObjectUtils.recoverObject(checkerRef);
        if (checker == null) {
            existing = null;
        } else {
            existing = checker.getExistingConfigName(name);
        }
        return existing;
    }

    public static Configuration newConfig(Component parent, String flyScanVersion, IConfigNameChecker checker) {
        Configuration config = null;
        ConfigCreator creator = new ConfigCreator(WindowSwingUtils.getWindowForComponent(parent), flyScanVersion,
                checker);
        creator.pack();
        creator.errorLabel.setText(ObjectUtils.EMPTY_STRING);
        creator.setLocationRelativeTo(parent.isShowing() ? parent : creator.getOwner());
        creator.setVisible(true);
        if (creator.validated) {
            config = Utilities.createNewConfiguration(creator.nameField.getText().trim(), flyScanVersion,
                    creator.dimensionsEditor.getNumberValue().intValue(), creator.continuousBox.isSelected());
        }
        return config;
    }

}
