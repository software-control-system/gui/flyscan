package fr.soleil.flyscan.gui.data.target;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.plugin.PluginParser;
import fr.soleil.flyscan.lib.model.parsing.plugin.PluginType;

public class PluginsTarget extends FlyScanTarget {

    private volatile Collection<Plugin> storedPlugins, mockPlugins, plugins;
    private volatile Map<PluginType, Collection<Plugin>> storedCategories, mockCategories, categories;
    private volatile String lastErrorMessage;

    public PluginsTarget() {
        super();
        lastErrorMessage = null;
        storedPlugins = new ArrayList<>();
        plugins = Collections.unmodifiableCollection(storedPlugins);
        mockPlugins = new ArrayList<>();
        storedCategories = new HashMap<>();
        categories = Collections.unmodifiableMap(storedCategories);
        mockCategories = new HashMap<>();
    }

    protected Map<PluginType, Collection<Plugin>> splitByCategory(Collection<Plugin> plugins) {
        Map<PluginType, Collection<Plugin>> categories = new HashMap<>();
        if (plugins != null) {
            for (Plugin plugin : plugins) {
                if ((plugin != null) && (plugin.getType() != null)) {
                    Collection<Plugin> list = categories.get(plugin.getType());
                    if (list == null) {
                        list = new ArrayList<>();
                        categories.put(plugin.getType(), list);
                    }
                    list.add(plugin);
                }
            }
        }
        return categories;
    }

    @Override
    protected void updateFromStringArray(String... pluginsInfoContent) {
        String lastErrorMessage = null;
        Collection<Plugin> plugins = null;
        try {
            if (pluginsInfoContent != null && pluginsInfoContent.length > 0) {
                plugins = PluginParser.parse(pluginsInfoContent);
            }
            if (plugins == null) {
                throw new FSParsingException(Utilities.getMessage("flyscan.error.attribute.pluginsinfo"));
            }
        } catch (FSParsingException e1) {
            lastErrorMessage = e1.getMessage();
        }
        if (plugins == null) {
            plugins = new ArrayList<>();
        }
        Map<PluginType, Collection<Plugin>> categories = splitByCategory(plugins);
        this.storedPlugins = plugins;
        this.plugins = Collections.unmodifiableCollection(plugins);
        this.storedCategories = categories;
        this.categories = Collections.unmodifiableMap(categories);
        this.lastErrorMessage = lastErrorMessage;
    }

    public Collection<Plugin> getPlugins() {
        return plugins;
    }

    public Collection<Plugin> getMockPlugins() {
        return mockPlugins;
    }

    public Map<PluginType, Collection<Plugin>> getCategories() {
        return categories;
    }

    public Map<PluginType, Collection<Plugin>> getMockCategories() {
        return mockCategories;
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }

    public void setMockPlugins(Collection<Plugin> mockPlugins) {
        Collection<Plugin> plugins = mockPlugins == null ? new ArrayList<>() : mockPlugins;
        Map<PluginType, Collection<Plugin>> categories = splitByCategory(plugins);
        this.mockPlugins = plugins;
        this.mockCategories = categories;
    }

    @Override
    protected void finalize() throws Throwable {
        storedPlugins.clear();
        mockPlugins.clear();
        storedCategories.clear();
        mockCategories.clear();
    }

}
