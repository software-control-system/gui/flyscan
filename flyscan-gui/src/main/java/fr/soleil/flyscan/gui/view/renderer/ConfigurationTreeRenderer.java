package fr.soleil.flyscan.gui.view.renderer;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.ClickableLabel;
import fr.soleil.flyscan.gui.view.model.ConfigurationTreeModel;
import fr.soleil.flyscan.gui.view.model.PathNode;
import fr.soleil.lib.project.map.ConcurrentWeakHashMap;

/**
 * TreeCellRenderer for ConfigurationTree.
 * 
 * @author GIRARDOT
 */
public class ConfigurationTreeRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 3269786081392274174L;

    private final Map<TreeNode, ClickableLabel> labelMap;
    private static final String FORMAT_START = "<html><body>";
    private static final String FORMAT_HIGHLIGHT_TEXT = "<b><span style=\"color:#0080ff\">%s</span></b>";
    private static final String FORMAT_HIGHLIGHT_TOOLTIP = "<b><span style=\"color:#ff0080\">%s</span></b>";
    private static final String FORMAT_STD = "%s";
    private static final String FORMAT_END = "</body></html>";

    public ConfigurationTreeRenderer() {
        super();
        labelMap = new ConcurrentWeakHashMap<>();
    }

    protected String getHighlightedText(String text, String filter, boolean tooltip) {
        String refText = text;
        if ((filter != null) && (!filter.isEmpty()) && (text != null) && (!text.isEmpty())) {
            refText = text.toLowerCase();
            int index = refText.indexOf(filter);
            if (index > -1) {
                // Text matches filter: highlight all occurrences
                int subStart = 0;
                StringBuilder formatBuilder = new StringBuilder(FORMAT_START);
                List<String> values = new ArrayList<>();
                while (index > -1) {
                    formatBuilder.append(FORMAT_STD);
                    values.add(text.substring(subStart, index));
                    formatBuilder.append(tooltip ? FORMAT_HIGHLIGHT_TOOLTIP : FORMAT_HIGHLIGHT_TEXT);
                    subStart = index + filter.length();
                    values.add(text.substring(index, subStart));
                    index = refText.indexOf(filter, subStart);
                }
                if (subStart > 0) {
                    formatBuilder.append(FORMAT_STD);
                    values.add(text.substring(subStart));
                }
                formatBuilder.append(FORMAT_END);
                refText = String.format(formatBuilder.toString(), values.toArray());
                // Clean StringBuilder and list to reduce memory consumption
                values.clear();
                formatBuilder.delete(0, formatBuilder.length());
            }
        }
        return refText;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        JLabel comp = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        String filter = null;
        TreeModel model = tree.getModel();
        if (model instanceof ConfigurationTreeModel) {
            ConfigurationTreeModel cfgModel = (ConfigurationTreeModel) model;
            // If there is a filter, try to highlight matching values.
            // No need to trim or lower case filter, as it is already done in ConfigurationTreeModel.
            filter = cfgModel.getFilter();
        }
        if (value instanceof PathNode) {
            PathNode node = (PathNode) value;
            comp.setToolTipText(getHighlightedText(node.getFullPath(), filter, true));
            if (leaf || node.isLeaf()) {
                if (!node.isRoot()) {
                    ClickableLabel label = labelMap.get(node);
                    if (label == null) {
                        label = new ClickableLabel();
                        labelMap.put(node, label);
                    }
                    label.setFont(comp.getFont());
                    label.setForeground(comp.getForeground());
                    label.setText(comp.getText());
                    label.setToolTipText(comp.getToolTipText());
                    label.setIcon(Utilities.SCAN_CONFIGURATION_ICON);
                    Color bg = comp.getBackground();
                    Border border = comp.getBorder();
                    if (label.isPressed()) {
                        bg = Utilities.PRESSED_COLOR;
                        border = Utilities.PRESSED_BORDER;
                    } else if (label.isHovered()) {
                        bg = Utilities.HOVERED_COLOR;
                        border = Utilities.HOVERED_BORDER;
                    } else {
                        border = Utilities.EMPTY_BORDER;
                    }
                    label.setBackground(bg);
                    label.setOpaque(bg != null);
                    label.setBorder(border);
                    comp = label;
                }
            } else if (expanded) {
                comp.setIcon(Utilities.FOLDER_OPEN_ICON);
            } else {
                comp.setIcon(Utilities.FOLDER_ICON);
            }
        } else {
            comp.setToolTipText(null);
        }
        String text = comp.getText(), highlightedText = getHighlightedText(text, filter, false);
        if (text != highlightedText) {
            comp.setText(highlightedText);
        }
        if (Utilities.DEFAULT_LABEL_COLOR.equals(comp.getBackground())) {
            comp.setBackground(tree.getBackground());
            if (!comp.isOpaque()) {
                comp.setOpaque(true);
            }
        }
        return comp;
    }

    public void cleanLabels() {
        for (ClickableLabel label : labelMap.values()) {
            if (label != null) {
                label.setHovered(false);
                label.setPressed(false);
            }
        }
    }

}
