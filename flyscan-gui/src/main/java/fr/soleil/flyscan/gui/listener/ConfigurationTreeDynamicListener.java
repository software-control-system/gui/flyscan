package fr.soleil.flyscan.gui.listener;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;

import javax.swing.JLabel;
import javax.swing.tree.TreePath;

import fr.soleil.flyscan.gui.event.ConfigSelectionEvent;
import fr.soleil.flyscan.gui.view.component.ClickableLabel;
import fr.soleil.flyscan.gui.view.component.ConfigurationTree;
import fr.soleil.flyscan.gui.view.model.PathNode;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link MouseAdapter} that will update the display of a {@link ConfigurationTree}, to dynamically show its
 * configuration nodes as hovered or pressed.
 * 
 * @author GIRARDOT
 */
public class ConfigurationTreeDynamicListener extends MouseAdapter {

    private final WeakReference<ConfigSelectionListener> listenerRef;
    private WeakReference<ClickableLabel> lastLabelRef;
    private boolean pressed;
    private long lastPress;

    public ConfigurationTreeDynamicListener(ConfigSelectionListener listener) {
        super();
        listenerRef = listener == null ? null : new WeakReference<>(listener);
        lastLabelRef = null;
        pressed = false;
        lastPress = 0;
    }

    protected void cleanLabel(ClickableLabel label) {
        if (label != null) {
            label.setPressed(false);
            label.setHovered(false);
        }
    }

    protected ConfigurationTree getTree(MouseEvent e) {
        ConfigurationTree tree = null;
        if ((e != null) && (e.getComponent() instanceof ConfigurationTree)) {
            tree = (ConfigurationTree) e.getComponent();
        }
        return tree;
    }

    protected TreePath getPath(ConfigurationTree tree, MouseEvent e) {
        TreePath path = null;
        if ((tree != null) && (e != null)) {
            path = tree.getPathForLocation(e.getX(), e.getY());
        }
        return path;
    }

    protected PathNode getNode(TreePath path) {
        PathNode node = null;
        if (path != null) {
            Object obj = path.getLastPathComponent();
            if (obj instanceof PathNode) {
                node = (PathNode) obj;
            }
        }
        return node;
    }

    protected Component getRenderedComponent(ConfigurationTree tree, TreePath path, PathNode node) {
        Component comp = null;
        if ((tree != null) && (path != null) && (node != null)) {
            comp = tree.getCellRenderer().getTreeCellRendererComponent(tree, node, false, false, false,
                    tree.getRowForPath(path), false);
        }
        return comp;
    }

    protected ClickableLabel getLabel(Component comp, PathNode node) {
        ClickableLabel label = null;
        if ((comp instanceof ClickableLabel) && (node != null) && (node.isLeaf())) {
            label = (ClickableLabel) comp;
        }
        return label;
    }

    protected ClickableLabel getLabel(ConfigurationTree tree, TreePath path, PathNode node) {
        return getLabel(getRenderedComponent(tree, path, node), node);
    }

    protected void treetMouseMove(MouseEvent e) {
        // When mouse is over a tree node:
        // - Display full path in tooltip text
        // - Display that node as hovered if it is a configuration node
        // - Display previous configuration node as not hovered
        boolean changed = false;
        ConfigurationTree tree = getTree(e);
        TreePath path = getPath(tree, e);
        PathNode node = getNode(path);
        Component comp = getRenderedComponent(tree, path, node);
        // Update tooltip text
        if (tree != null) {
            if (comp instanceof JLabel) {
                tree.setToolTipText(((JLabel) comp).getToolTipText());
            } else {
                tree.setToolTipText(null);
            }
        }
        ClickableLabel label = getLabel(comp, node);
        ClickableLabel lastLabel = ObjectUtils.recoverObject(lastLabelRef);
        // Display previous configuration node as not hovered
        if ((label == null) || (label != lastLabel)) {
            cleanLabel(lastLabel);
            lastLabelRef = null;
            changed = true;
        }
        // Display configuration node as hovered
        if (label != null) {
            changed |= !label.isHovered();
            label.setHovered(true);
            lastLabelRef = new WeakReference<>(label);
        }
        // Repaint tree to force the dynamic display
        if ((changed && (tree != null))) {
            tree.repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        treetMouseMove(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        treetMouseMove(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // Mouse exited tree: display previous configuration node as not hovered
        ClickableLabel lastLabel = ObjectUtils.recoverObject(lastLabelRef);
        cleanLabel(lastLabel);
        ConfigurationTree tree = getTree(e);
        // Repaint tree to force the dynamic display
        if (tree != null) {
            tree.setToolTipText(null);
            tree.repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        treetMouseMove(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // Mouse pressed: display node under cursor as pressed if it is a configuration node
        boolean changed = false;
        ConfigurationTree tree = getTree(e);
        TreePath path = getPath(tree, e);
        PathNode node = getNode(path);
        ClickableLabel label = getLabel(tree, path, node);
        if (label != null) {
            changed = !label.isPressed();
            label.setPressed(true);
        }
        // Repaint tree to force the dynamic display
        if ((changed && (tree != null))) {
            tree.repaint();
        }
        // Remember press start time
        if (!pressed) {
            pressed = true;
            lastPress = System.currentTimeMillis();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // Mouse released: display node under cursor as not pressed if it is a configuration node
        boolean changed = false;
        ConfigurationTree tree = getTree(e);
        TreePath path = getPath(tree, e);
        PathNode node = getNode(path);
        ClickableLabel label = getLabel(tree, path, node);
        if (label != null) {
            changed = label.isPressed();
            label.setPressed(false);
        }
        // Repaint tree to force the dynamic display
        if ((changed && (tree != null))) {
            tree.repaint();
        }
        pressed = false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Mouse clicked:
        // - If click duration took less than 400ms and node under cursor is a configuration node,
        // send configuration selection event and display node as not hovered and not pressed (because menu will close).
        // - Otherwise, ignore that click.
        pressed = false;
        ConfigSelectionListener listener = ObjectUtils.recoverObject(listenerRef);
        if ((listener != null) && (System.currentTimeMillis() - lastPress < 400)) {
            ConfigurationTree tree = getTree(e);
            TreePath path = getPath(tree, e);
            PathNode node = getNode(path);
            ClickableLabel label = getLabel(tree, path, node);
            if ((tree != null) && (node != null) && (label != null)) {
                listener.configSelectionChanged(new ConfigSelectionEvent(tree, node.getFullPath()));
                label.setPressed(false);
                label.setHovered(false);
            }
        }
    }

}
