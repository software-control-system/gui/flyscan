package fr.soleil.flyscan.gui.view.trajectory;

import java.awt.Color;
import java.awt.Component;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.event.EventListenerList;
import javax.swing.text.JTextComponent;

import fr.soleil.flyscan.gui.listener.ConfigurationEditionStateListener;
import fr.soleil.flyscan.gui.listener.LabelErrorMouseAdapter;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TrajectoryParser;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.lib.project.ObjectUtils;

/**
 * Abstract component used to display a trajectory editor
 * 
 * @author guerre-giordano
 */
public abstract class ATrajectoryComponent<E extends Trajectory> extends JPanel implements IEntryComponent {

    private static final long serialVersionUID = 6321269179160780202L;

    protected E trajectory;
    private final JPanel errorPanel;
    private final EventListenerList listeners = new EventListenerList();
    private final Map<Component, JTextComponent> errorsMap;

    protected final String location, tabTitle;

    private final WeakReference<ConfigurationGUI> configurationGUIRef;
    private final WeakReference<JLabel> nameLabelRef;
    private final WeakReference<JRadioButton> selectorRef;
    protected final String configName, actuatorName;

    public ATrajectoryComponent(JPanel errorPanel, Map<Component, JTextComponent> errorsMap, String location,
            ConfigurationGUI configurationGUI, JLabel nameLabel, JRadioButton selector, String tabTitle,
            final String configName, final String actuatorName) {
        this.errorPanel = errorPanel;
        this.errorsMap = errorsMap == null ? new HashMap<>() : errorsMap;
        this.location = location;
        this.configurationGUIRef = configurationGUI == null ? null : new WeakReference<>(configurationGUI);
        this.nameLabelRef = nameLabel == null ? null : new WeakReference<>(nameLabel);
        this.selectorRef = selector == null ? null : new WeakReference<>(selector);
        this.tabTitle = tabTitle;
        this.configName = configName;
        this.actuatorName = actuatorName;
    }

    public abstract E getTrajectory();

    public void setTrajectory(E trajectory) {
        this.trajectory = trajectory;
    }

    public void addBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.add(ConfigurationEditionStateListener.class, listener);
    }

    public void removeBeingEditedListener(ConfigurationEditionStateListener listener) {
        listeners.remove(ConfigurationEditionStateListener.class, listener);
    }

    public ConfigurationEditionStateListener[] getBeingEditedListeners() {
        return listeners.getListeners(ConfigurationEditionStateListener.class);
    }

    protected void fireEditionStateChanged(boolean isBeingEdited) {
        if (isBeingEdited) {
            setBorder(null);
        }
        for (ConfigurationEditionStateListener listener : getBeingEditedListeners()) {
            listener.setConfigurationBeingEdited(isBeingEdited);
        }
    }

    protected void displayError(String textError) {
        JTextArea labelError = Utilities.getTextAreaError(location, textError);
        labelError.addMouseListener(new LabelErrorMouseAdapter(ObjectUtils.recoverObject(configurationGUIRef), this,
                ObjectUtils.recoverObject(nameLabelRef), ObjectUtils.recoverObject(selectorRef), tabTitle));
        errorPanel.add(labelError);
        errorsMap.put(this, labelError);
        setBorder(BorderFactory.createLineBorder(Color.RED));
    }

    protected abstract boolean isMyTrajectoryClass(Trajectory trajectory);

    @Override
    public void resetToDefault(String defaultValue) throws FSParsingException {
        if (defaultValue != null) {
            Trajectory trajectory = TrajectoryParser.parse(configName, defaultValue, actuatorName);
            if (isMyTrajectoryClass(trajectory)) {
                @SuppressWarnings("unchecked")
                E toApply = (E) trajectory;
                setTrajectory(toApply);
                fireEditionStateChanged(true);
            }
        }
    }

}
