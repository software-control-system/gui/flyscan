package fr.soleil.flyscan.gui.view.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.Icon;

import fr.soleil.lib.project.awt.SizeUtils;
import fr.soleil.lib.project.swing.text.DynamicForegroundLabel;

public class DynamicForegroundTabLabel extends DynamicForegroundLabel implements TextTabComponent {

    private static final long serialVersionUID = 777723104995108154L;

    public DynamicForegroundTabLabel() {
        super();
    }

    public DynamicForegroundTabLabel(String text) {
        super(text);
    }

    public DynamicForegroundTabLabel(Icon image) {
        super(image);
    }

    public DynamicForegroundTabLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
    }

    public DynamicForegroundTabLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
    }

    public DynamicForegroundTabLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
    }

    @Override
    public void setTitle(String title) {
        setText(title);
    }

    @Override
    public String getTitle() {
        return getText();
    }

    @Override
    public Font getTitleFont() {
        return getFont();
    }

    @Override
    public void setTitleFont(Font font) {
        setFont(font);
    }

    @Override
    public Color getTitleForeground() {
        return getForeground();
    }

    @Override
    public void setTitleForeground(Color fg) {
        setForeground(fg);
    }

    @Override
    public Dimension getPreferredSize() {
        return SizeUtils.getAdaptedPreferredSize(this, super.getPreferredSize());
    }

}
