package fr.soleil.flyscan.gui.view.component.multivalue;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.ListCellRenderer;

import org.slf4j.LoggerFactory;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.lib.project.map.ConcurrentWeakHashMap;
import fr.soleil.lib.project.swing.combo.DynamicRenderingComboBox;

/**
 * Component allowing to edit a multivalued list
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public abstract class AMultivaluedListComponent<T> extends AbstractMultivaluedComponent<JComboBox<T>, T> {

    private static final long serialVersionUID = 7047641669682162428L;

    private Collection<T> possibleValues;
    private Map<Integer, T> usedValues;
    private boolean areCBExclusive;
    private Class<? extends ListCellRenderer<? super T>> rendererClass;
    private Map<JComboBox<T>, EventListener> comboBoxListeners;

    public AMultivaluedListComponent(Class<T> dataClass, Collection<T> possibleValues, Collection<T> values, int count,
            boolean areCBExclusive, Class<? extends ListCellRenderer<? super T>> rendererClass,
            boolean mayThrowException, Object... otherInitValues) throws FSParsingException {
        super(dataClass, values, count, mayThrowException,
                getInitValues(possibleValues, areCBExclusive, rendererClass, otherInitValues));
    }

    protected static final <T> Object[] getInitValues(Collection<T> possibleValues, boolean areCBExclusive,
            Class<? extends ListCellRenderer<? super T>> rendererClass, Object... otherInitValues) {
        Object[] initValues;
        if ((otherInitValues == null) || (otherInitValues.length == 0)) {
            initValues = new Object[] { rendererClass, possibleValues, Boolean.valueOf(areCBExclusive) };
        } else {
            initValues = new Object[otherInitValues.length + 3];
            initValues[0] = rendererClass;
            initValues[1] = possibleValues;
            initValues[2] = Boolean.valueOf(areCBExclusive);
            System.arraycopy(otherInitValues, 0, initValues, 3, otherInitValues.length);
        }
        return initValues;
    }

    protected boolean enableAddButton() {
        return (!areCBExclusive) || (components.size() < possibleValues.size());
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void initBeforeValues(Object... otherData) {
        this.rendererClass = (Class<? extends ListCellRenderer<? super T>>) otherData[0];
        this.possibleValues = (Collection<T>) otherData[1];
        this.areCBExclusive = ((Boolean) otherData[2]).booleanValue();
        this.usedValues = new HashMap<>();
    }

    @Override
    protected void updateButtons() {
        refreshCombos(null);
        addButton.setEnabled(enableAddButton());
        removeButton.setEnabled(isThereAnElementToRemove());
    }

    @Override
    protected void remove(boolean updateButtons, boolean warn) {
        int lastIndex = components.size() - 1;
        if (lastIndex > -1) {
            JComboBox<T> toRemove = components.get(lastIndex);
            componentsPanel.remove(toRemove);
            components.remove(lastIndex);
            AMultivaluedListComponent.this.usedValues.remove(lastIndex);

            if (updateButtons) {
                updateButtons();
            }
            if (warn) {
                fireEditionStateChanged(true);
            }

            AMultivaluedListComponent.this.validate();
            AMultivaluedListComponent.this.updateUI();
        }
    }

    @Override
    protected void addComponent(Integer rank, T value, boolean mayThrowException) throws FSParsingException {
        FSParsingException exception = null;
        try {
            super.addComponent(rank, value, mayThrowException);
        } catch (FSParsingException e) {
            exception = e;
        }
        addButton.setEnabled(enableAddButton());
        if (exception != null) {
            throw exception;
        }
    }

    protected abstract boolean sameValue(T value1, T value2);

    protected Collection<T> getUnusedValues() {
        Collection<T> unusedValues = new ArrayList<>();
        unusedValues.addAll(possibleValues);

        if (areCBExclusive) {
            Collection<T> toRemove = new ArrayList<>();

            // Double for loop to ensure that word with different case are processed as they are same
            for (T s : usedValues.values()) {
                for (T str : unusedValues) {
                    if (sameValue(s, str)) {
                        toRemove.add(str);
                    }
                }
            }
            unusedValues.removeAll(toRemove);
        }

        return unusedValues;
    }

    /**
     * Refresh all combo boxes except the one passed as parameter
     * 
     * @param combo
     */
    @SuppressWarnings("unchecked")
    protected void refreshCombos(final JComboBox<T> combo) {
        Collection<T> unusedValues = getUnusedValues();
        for (JComboBox<T> box : components.values()) {
            if (box != combo) {
                T sel = (T) box.getSelectedItem();
                List<T> tmp = new ArrayList<>();
                tmp.addAll(unusedValues);
                if (tmp.size() > 0) {
                    T previousFirst = tmp.get(0);
                    tmp.set(0, sel);
                    tmp.add(previousFirst);
                } else {
                    tmp.add(sel);
                }
                box.setModel(new DefaultComboBoxModel<T>(tmp.toArray((T[]) Array.newInstance(valueClass, tmp.size()))));
            }
        }
        componentsPanel.revalidate();
        componentsPanel.repaint();
    }

    protected void refreshUsedValuesListAndCombos(final JComboBox<T> combo) {
        fireEditionStateChanged(true);
        @SuppressWarnings("unchecked")
        T selected = (T) combo.getSelectedItem();
        for (Entry<Integer, JComboBox<T>> entry : components.entrySet()) {
            Integer i = entry.getKey();
            JComboBox<T> cb = entry.getValue();
            if (cb == combo) {
                usedValues.put(i, selected);
            }
        }
        refreshCombos(combo);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            enabled = (count < 0);
        }
        addButton.setEnabled(enabled && enableAddButton());
    }

    @Override
    protected void setValueToComponent(JComboBox<T> component, T value, Integer rank) throws FSParsingException {
        if (value == null) {
            @SuppressWarnings("unchecked")
            T str = (T) component.getSelectedItem();
            usedValues.put(rank, str);
        } else {
            int index = -1;
            for (int i = 0; i < component.getItemCount(); i++) {
                if (sameValue(value, component.getItemAt(i))) {
                    usedValues.put(rank, value);
                    index = i;
                    break;
                }
            }
            component.setSelectedIndex(index);
            if (index < 0) {
                throw new FSParsingException(
                        String.format(Utilities.getMessage("flyscan.error.value.invalid.list"), value, possibleValues));
            }
        }
    }

    @Override
    protected final void listenToComponent(final JComboBox<T> component) {
        if (comboBoxListeners == null) {
            comboBoxListeners = new ConcurrentWeakHashMap<>();
        }
        EventListener listener = prepareListener(component);
        comboBoxListeners.put(component, listener);
    }

    protected EventListener prepareListener(final JComboBox<T> component) {
        ItemListener tmp = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                fireEditionStateChanged(true);
                refreshUsedValuesListAndCombos(component);
            }
        };
        component.addItemListener(tmp);
        return tmp;
    }

    @Override
    protected final void forgetComponent(JComboBox<T> component) {
        EventListener listener = comboBoxListeners.remove(component);
        if (listener != null) {
            stopListening(listener, component);
        }
    }

    protected void stopListening(EventListener listener, JComboBox<T> component) {
        component.removeItemListener((ItemListener) listener);
    }

    @Override
    protected JComboBox<T> createComponent() {
        final JComboBox<T> combo = new DynamicRenderingComboBox<>();
//        Utilities.prepareComboBox(combo);
        combo.setEditable(false);
        if (rendererClass != null) {
            try {
                Constructor<? extends ListCellRenderer<? super T>> constructor = rendererClass
                        .getConstructor(JComponent.class);
                combo.setRenderer(constructor.newInstance(combo));
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {
                LoggerFactory.getLogger(getClass()).error(getClass().getName() + ": failed to set combobox renderer",
                        e);
            }
        }
        Collection<T> unusedValues = getUnusedValues();
        for (T s : unusedValues) {
            combo.addItem(s);
        }
        return combo;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected T getValue(JComboBox<T> component) {
        return (T) component.getSelectedItem();
    }

    @Override
    protected void finalize() {
        if (comboBoxListeners != null) {
            try {
                comboBoxListeners.clear();
            } catch (Exception e) {
                // Ignore if clear failed
            }
        }
        if (possibleValues != null) {
            try {
                possibleValues.clear();
            } catch (Exception e) {
                // Ignore if clear failed
            }
        }
        if (usedValues != null) {
            try {
                usedValues.clear();
            } catch (Exception e) {
                // Ignore if clear failed
            }
        }
    }
}
