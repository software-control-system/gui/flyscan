package fr.soleil.flyscan.gui.view.renderer;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JList;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.flyscan.gui.util.Utilities;

public class DeviceRenderer extends ColoredSelectionListCellRenderer {

    private static final long serialVersionUID = 3538292751052257823L;

    protected static final String NOT_EXPORTED = Utilities.getMessage("flyscan.device.unexported");
    public static final Font UNEXPORTED_FONT;

    static {
        Map<TextAttribute, ?> tmpAttributes = Utilities.BASIC_FONT.getAttributes();
        Map<TextAttribute, Object> attributes = new HashMap<>();
        attributes.putAll(tmpAttributes);
        attributes.put(TextAttribute.STRIKETHROUGH, Boolean.TRUE);
        UNEXPORTED_FONT = new Font(attributes);
    }

    public DeviceRenderer(JComponent comp) {
        super(comp);
        useDefaultFont = false;
    }

    @Override
    protected void applyToComponent(JComponent comp, Font font, Color foreground, String tooltipText,
            JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus,
            boolean derive) {
        font = Utilities.BASIC_FONT;
        if ((tooltipText != null) && NOT_EXPORTED.equalsIgnoreCase(tooltipText)) {
            tooltipText = null;
        }
        if (value instanceof String) {
            String str = (String) value;
            if (!str.trim().isEmpty()) {
                try {
                    if (!TangoDeviceHelper.isDeviceRunning(str)) {
                        font = UNEXPORTED_FONT;
                        tooltipText = NOT_EXPORTED;
                    }
                } catch (Exception e) {
                    // nothing to do, we don't mind failure in this particular case
                }
            }
        }
        if (list != null) {
            Container parent = list.getParent();
            if (parent != null) {
                parent.setFont(comp.getFont());
            }
        }
        super.applyToComponent(comp, font, foreground, tooltipText, list, value, index, isSelected, cellHasFocus,
                derive);
    }
}
