package fr.soleil.flyscan.gui.view.menu.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * List filtered
 * 
 * @author guerre-giordano
 * 
 * @param <E>
 */
public class FilterList<E> {

    public <T> List<T> filterList(List<T> originalList, Filter<T, E> filter, E text) {
        List<T> filterList = new ArrayList<>();
        for (T object : originalList) {
            if (filter.isMatched(object, text)) {
                filterList.add(object);
            }
        }
        return filterList;
    }
}
