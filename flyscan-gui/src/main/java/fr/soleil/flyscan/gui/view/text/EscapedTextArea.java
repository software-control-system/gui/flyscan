package fr.soleil.flyscan.gui.view.text;

import javax.swing.event.DocumentEvent;

import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.util.DynamicSizeTextArea;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link TextArea} used to edit multilines values
 * 
 * @author GIRARDOT
 *
 */
public class EscapedTextArea extends DynamicSizeTextArea {

    private static final long serialVersionUID = 3325749756328612508L;

    public EscapedTextArea() {
        super();
    }

    public EscapedTextArea(String text) {
        super();
        setText(text);
    }

    protected String getEscapedText(String text) {
        String txt = text;
        if (txt == null) {
            txt = ObjectUtils.EMPTY_STRING;
        } else {
            txt = txt.trim().replace(ParsingUtil.ESCAPED_LF, Utilities.NEW_LINE);
        }
        return txt;
    }

    protected String getReverseEscapedText(String text) {
        String txt = text;
        if (txt == null) {
            txt = ObjectUtils.EMPTY_STRING;
        } else {
            txt = txt.trim().replace(Utilities.NEW_LINE, ParsingUtil.ESCAPED_LF);
        }
        return txt;
    }

    @Override
    protected void applyChange(DocumentEvent e) {
        isEditingData = false;
        super.applyChange(e);
    }

    public String getAdaptedText() {
        return getReverseEscapedText(super.getText());
    }

    @Override
    public void setText(String text) {
        String txt = getEscapedText(text);
        super.setText(txt);
    }

}
