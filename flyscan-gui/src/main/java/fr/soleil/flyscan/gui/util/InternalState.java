package fr.soleil.flyscan.gui.util;

import java.awt.Color;

/**
 * An {@link Enum} used to represent the possible values of FlyScanServer's internalState attribute
 * 
 * @author GIRARDOT
 */
public enum InternalState {

    /**
     * Represents "INVALID CONFIG" value
     */
    INVALID_CONFIG("INVALID CONFIG", Utilities.getMessage("flyscan.state.internal.config.invalid"), Color.RED),
    /**
     * Represents "INIT CONTEXT" value
     */
    INIT_CONTEXT("INIT CONTEXT", Utilities.getMessage("flyscan.state.internal.context.init"), IColorConstant.YELLOW_GRAY),
    /**
     * Represents "READY" value
     */
    READY("READY", Utilities.getMessage("flyscan.state.internal.ready"), Color.GREEN),
    /**
     * Represents "INIT SCAN" value
     */
    INIT_SCAN("INIT SCAN", Utilities.getMessage("flyscan.state.internal.scan.init"), IColorConstant.YELLOW_GRAY),
    /**
     * Represents "PREPARE SCAN" value
     */
    PREPARE_SCAN("PREPARE SCAN", Utilities.getMessage("flyscan.state.internal.scan.prepare"), IColorConstant.YELLOW_GRAY),
    /**
     * Represents "PREPARE ACTORS" value
     */
    PREPARE_ACTORS("PREPARE ACTORS", Utilities.getMessage("flyscan.state.internal.actors.prepare"), IColorConstant.YELLOW_GRAY),
    /**
     * Represents "START SCAN" value
     */
    START_SCAN("START SCAN", Utilities.getMessage("flyscan.state.internal.scan.start"), IColorConstant.DARK_GREEN),
    /**
     * Represents "RUNNING" value
     */
    RUNNING("RUNNING", Utilities.getMessage("flyscan.state.internal.running"), IColorConstant.DARK_GREEN),
    /**
     * Represents "INIT STEP" value
     */
    INIT_STEP("INIT STEP", Utilities.getMessage("flyscan.state.internal.step.init"), IColorConstant.DARK_GREEN),
    /**
     * Represents "STEP PROGRESS" value
     */
    STEP_PROGRESS("STEP PROGRESS", Utilities.getMessage("flyscan.state.internal.step.progress"), IColorConstant.DARK_GREEN),
    /**
     * Represents "END STEP" value
     */
    END_STEP("END STEP", Utilities.getMessage("flyscan.state.internal.step.end"), IColorConstant.DARK_GREEN),
    /**
     * Represents "END SCAN" value
     */
    END_SCAN("END SCAN", Utilities.getMessage("flyscan.state.internal.scan.end"), IColorConstant.DARK_GREEN),
    /**
     * Represents "SCAN DONE" value
     */
    SCAN_DONE("SCAN DONE", Utilities.getMessage("flyscan.state.internal.scan.done"), Color.GREEN),
    /**
     * Represents "PAUSED BY USER" value
     */
    PAUSED_BY_USER("PAUSED BY USER", Utilities.getMessage("flyscan.state.internal.paused.user"), Color.YELLOW),
    /**
     * Represents "PAUSED ON CONTEXT" value
     */
    PAUSED_ON_CONTEXT("PAUSED ON CONTEXT", Utilities.getMessage("flyscan.state.internal.paused.context"), Color.YELLOW),
    /**
     * Represents "PAUSED ON ERROR" value
     */
    PAUSED_ON_ERROR("PAUSED ON ERROR", Utilities.getMessage("flyscan.state.internal.paused.error"), IColorConstant.ORANGE),
    /**
     * Represents "ABORTING ON CONTEXT" value
     */
    ABORTING_ON_CONTEXT("ABORTING ON CONTEXT", Utilities.getMessage("flyscan.state.internal.aborting.context"), Color.RED),
    /**
     * Represents "ABORTING ON ERROR" value
     */
    ABORTING_ON_ERROR("ABORTING ON ERROR", Utilities.getMessage("flyscan.state.internal.aborting.error"), Color.RED),
    /**
     * Represents "ABORTED ON CONTEXT" value
     */
    ABORTED_ON_CONTEXT("ABORTED ON CONTEXT", Utilities.getMessage("flyscan.state.internal.aborted.context"), Color.RED),
    /**
     * Represents "ABORTED ON ERROR" value
     */
    ABORTED_ON_ERROR("ABORTED ON ERROR", Utilities.getMessage("flyscan.state.internal.aborted.error"), Color.RED),
    /**
     * Represents "ABORTING BY USER" value
     */
    ABORTING_BY_USER("ABORTING BY USER", Utilities.getMessage("flyscan.state.internal.aborting.user"), IColorConstant.ORANGE),
    /**
     * Represents "ABORTED BY USER" value
     */
    ABORTED_BY_USER("ABORTED BY USER", Utilities.getMessage("flyscan.state.internal.aborted.user"), IColorConstant.ORANGE),
    /**
     * Any other internal state value is considered as unknown
     */
    UNKNOWN("----", Utilities.getMessage("flyscan.state.internal.unknown"), IColorConstant.GRAY);

    private final String text, help;
    private final Color color;

    private InternalState(String text, String help, Color color) {
        this.text = text;
        this.help = help;
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public String getHelp() {
        return help;
    }

    public Color getColor() {
        return color;
    }

    public static InternalState parseInternalState(String text) {
        InternalState result = UNKNOWN;
        if (text != null) {
            text = text.trim();
            for (InternalState state : values()) {
                if (state.getText().equalsIgnoreCase(text)) {
                    result = state;
                    break;
                }
            }
        }
        return result;
    }
}