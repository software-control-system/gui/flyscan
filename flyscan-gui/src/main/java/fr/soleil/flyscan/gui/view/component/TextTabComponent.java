package fr.soleil.flyscan.gui.view.component;

import java.awt.Color;
import java.awt.Font;

public interface TextTabComponent {

    public void setTitle(String title);

    public String getTitle();

    public Font getTitleFont();

    public void setTitleFont(Font font);

    public Color getTitleForeground();

    public void setTitleForeground(Color fg);

}
