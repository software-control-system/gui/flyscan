package fr.soleil.flyscan.gui.view.combo;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.component.IEntryComponent;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;
import fr.soleil.flyscan.gui.view.renderer.DeviceRenderer;

/**
 * A {@link JPanel} that displays a device selection {@link ComboBox} and a {@link JButton} to launch device panel on
 * selected device.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class DeviceComboBox extends JPanel implements IEntryComponent {

    private static final long serialVersionUID = 197659206080340390L;

    private final ComboBox deviceBox;
    private final JButton deviceButton;

    public DeviceComboBox() {
        super(new BorderLayout());
        deviceButton = new JButton(Utilities.DEVICE_PANEL_ICON);
        deviceButton.setToolTipText(Utilities.LAUNCH_DEVICE_PANEL);
        deviceButton.setMargin(CometeUtils.getzInset());
        deviceButton.setEnabled(false);
        deviceBox = new ComboBox();
        deviceBox.setDynamicTooltipText(true);
        deviceBox.setRenderer(new DeviceRenderer(deviceBox));
        deviceButton.addActionListener(e -> {
            Utilities.createBatchExecutor((String) deviceBox.getSelectedValue()).execute();
        });
        deviceBox.addItemListener(e -> {
            deviceButton.setEnabled(deviceBox.getSelectedValue() != null);
        });
        add(deviceBox, BorderLayout.CENTER);
        add(deviceButton, BorderLayout.WEST);
    }

    public ComboBox getDeviceBox() {
        return deviceBox;
    }

    public JButton getDeviceButton() {
        return deviceButton;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (deviceBox != null) {
            deviceBox.setEnabled(enabled);
        }
    }

    @Override
    public void resetToDefault(String defaultDevice) {
        if (defaultDevice != null) {
            UIDelegate.applySelectedValue(getDeviceBox(), defaultDevice);
        }
    }
}
