package fr.soleil.flyscan.gui.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;

import javax.swing.JComponent;
import javax.swing.JRadioButton;

import fr.soleil.lib.project.ObjectUtils;

public class FSMouseSelector extends MouseAdapter {

    private WeakReference<JRadioButton> selectorRef;
    private WeakReference<JComponent> glueRef;

    public FSMouseSelector(JRadioButton selector, JComponent glue) {
        super();
        selectorRef = selector == null ? null : new WeakReference<>(selector);
        glueRef = glue == null ? null : new WeakReference<>(glue);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        JRadioButton selector = ObjectUtils.recoverObject(selectorRef);
        if (selector != null) {
            Object source = e.getSource();
            selector.setSelected(true);
            if (source instanceof JComponent) {
                JComponent comp = (JComponent) source;
                if (comp.isEnabled()) {
                    comp.grabFocus();
                } else {
                    JComponent glue = ObjectUtils.recoverObject(glueRef);
                    if (glue != null) {
                        glue.grabFocus();
                    }
                }
            }
        }
    }

}
