package fr.soleil.flyscan.gui.view.component.multivalue;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Collection;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;

/**
 * Component allowing to edit a multivalued list of strings
 * 
 * @author guerre-giordano
 * 
 */
public class MultivaluedTextFieldComponent extends AbstractMultivaluedComponent<JTextField, String>
        implements DocumentListener {

    private static final long serialVersionUID = -5694695356590595767L;

    public MultivaluedTextFieldComponent(Collection<String> values, int count, boolean mayThrowException)
            throws FSParsingException {
        super(String.class, values, count, mayThrowException);
    }

    @Override
    protected void setValueToComponent(JTextField component, String value, Integer rank) {
        if (value != null) {
            component.setText(value);
        }
    }

    @Override
    protected void listenToComponent(JTextField component) {
        component.getDocument().addDocumentListener(this);
    }

    @Override
    protected void forgetComponent(JTextField component) {
        component.getDocument().removeDocumentListener(this);
    }

    @Override
    protected JTextField createComponent() {
        final JTextField tf = Utilities.createTextField(null, 20);
        tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                e.consume();// mandatory to avoid duplicate paste (SCAN-492). Don't ask why.
            }
        });
        return tf;
    }

    @Override
    protected String getValue(JTextField component) {
        return component.getText();
    }

    @Override
    protected String toString(String value) {
        return value;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        fireEditionStateChanged(true);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        fireEditionStateChanged(true);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        fireEditionStateChanged(true);
    }

    @Override
    protected String parse(String value) {
        return value;
    }

}
