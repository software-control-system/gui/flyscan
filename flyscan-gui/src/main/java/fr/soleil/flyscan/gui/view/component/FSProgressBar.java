package fr.soleil.flyscan.gui.view.component;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.border.EmptyBorder;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.swing.ProgressBar;
import fr.soleil.comete.tango.data.adapter.StateAdapter;
import fr.soleil.lib.project.swing.ui.TubeProgressBarUI;

/**
 * A {@link ProgressBar} with 2 digits of precision.
 * 
 * @author GIRARDOT
 */
public class FSProgressBar extends ProgressBar {

    private static final long serialVersionUID = 2492797038928977539L;

    public FSProgressBar() {
        super();
        setBorder(new EmptyBorder(0, 0, 0, 0));
        TubeProgressBarUI ui = new TubeProgressBarUI();
        ui.setProgressColor(ColorTool.getColor(StateAdapter.RUNNING));
        setUI(ui);
    }

    @Override
    public Number getNumberValue() {
        return Integer.valueOf(super.getValue() / 100);
    }

    @Override
    public void setNumberValue(Number value) {
        setValue(value == null ? 0 : Math.round(value.floatValue() * 100));
    }

    @Override
    public void setMaximum(int n) {
        super.setMaximum(n * 100);
    }

    @Override
    public void paint(Graphics g) {
        // anti aliasing
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        }
        super.paint(g);
    }

}
