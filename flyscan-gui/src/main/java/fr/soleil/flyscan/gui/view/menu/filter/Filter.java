package fr.soleil.flyscan.gui.view.menu.filter;

/**
 * Generic behavior for a list filter
 * 
 * @author guerre-giordano
 * 
 * @param <T>
 * @param <E>
 */
public interface Filter<T, E> {
    boolean isMatched(T object, E text);
}
