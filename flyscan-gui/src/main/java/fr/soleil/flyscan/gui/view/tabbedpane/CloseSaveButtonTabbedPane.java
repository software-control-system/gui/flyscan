package fr.soleil.flyscan.gui.view.tabbedpane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.metal.MetalButtonUI;

import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.flyscan.gui.listener.ButtonDecorationListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.component.TextTabComponent;
import fr.soleil.flyscan.lib.model.parsing.configuration.Configuration;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * Cf http://halilkarakose.blogspot.fr/2009/01/easiest-tab-implementation-with-close.html
 * 
 * @author Halil KARAKOSE, Rapha&euml;l GIRARDOT
 */
public final class CloseSaveButtonTabbedPane extends BoldTitleTabbedPane {

    private static final long serialVersionUID = -7614677131399836356L;

    private final boolean isExpert;
    private final Object lock = new Object();
    private volatile boolean canLoad;

    public CloseSaveButtonTabbedPane(boolean isExpert) {
        super();
        canLoad = false;
        this.isExpert = isExpert;
    }

    @Override
    public void addTab(String title, Icon icon, Component component, String tip) {
        synchronized (lock) {
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.add(component, BorderLayout.CENTER);
            super.addTab(title, icon, panel, tip);
            int count = this.getTabCount() - 1;
            if (component instanceof ConfigurationGUI) {
                ConfigurationGUI configGui = ((ConfigurationGUI) component);
                boolean isEnabled = configGui.isEnabled();
                setTabComponentAt(count, new CloseSaveButtonTab(title, icon, (ConfigurationGUI) component, null, null,
                        isExpert, isEnabled));
            }
        }
    }

    @Override
    public void addTab(String title, Icon icon, Component component) {
        addTab(title, icon, component, null);
    }

    @Override
    public void addTab(String title, Component component) {
        addTab(title, (Icon) null, component);
    }

    public void addEmptyConfigTab(String title, int tabIndex) {
        synchronized (lock) {
            int count = -1;
            final JLabel label = new JSmoothLabel(Utilities.getMessage("flyscan.configuration.no"));
            if (tabIndex == -1) {
                super.addTab(title, null, label, null);
                count = this.getTabCount() - 1;
            } else {
                count = tabIndex;
                setComponentAt(count, label);
            }
            setTabComponentAt(count, new CloseSaveButtonTab(title, null, null, null, null, isExpert, true));
        }
    }

    /**
     * Add tab with header
     * 
     * @param title
     * @param component
     * @param errorComponent
     * @param tabIndex (default value -1)
     */
    public void addTabWithHeader(String title, Component component, JComponent errorComponent, int tabIndex,
            boolean replace, ActionListener reloadListener, ActionListener removeListener) {
        synchronized (lock) {
            final JSplitPane splitPane = Utilities.createSplitPane(component, errorComponent);

            int count = -1;
            if (tabIndex < 0) {
                count = getTabCount();
                super.addTab(title, null, splitPane, null);
            } else {
                count = tabIndex;
                if (replace) {
                    setComponentAt(count, component);
                } else {
                    insertTab(title, null, component, null, tabIndex);
                }
            }

            if (component instanceof ConfigurationGUI) {
                ConfigurationGUI configGui = ((ConfigurationGUI) component);
                boolean isEnabled = configGui.isEnabled();
                setTabComponentAt(count, new CloseSaveButtonTab(title, null, (ConfigurationGUI) component,
                        reloadListener, removeListener, isExpert, isEnabled));
            }
        }

    }

    @Override
    public void setComponentAt(int index, Component component) {
        Component comp = getComponentAt(index);
        if (comp instanceof JSplitPane) {
            JSplitPane splitPane = (JSplitPane) comp;
            if (splitPane != null) {
                splitPane.setRightComponent(component);
            }
        } else {
            super.setComponentAt(index, component);
        }

    }

    public String getConfigNameAt(int index) {
        String title;
        TextTabComponent tab = getTextTabComponentAt(index);
        if (tab == null) {
            title = ObjectUtils.EMPTY_STRING;
        } else {
            title = tab.getTitle();
        }
        return title;
    }

    @Override
    public int indexOfComponent(Component component) {
        int index = -1;
        for (Component comp : getComponents()) {
            if (comp instanceof JSplitPane
                    || comp instanceof JLabel/* && ((JPanel) comp).getLayout() instanceof BorderLayout*/) {
                index++;
                if (comp instanceof JSplitPane) {
                    for (Component c : ((JSplitPane) comp).getComponents()) {
                        if (component.equals(c)) {
                            return index;
                        }
                    }
                }

            }
        }
        return super.indexOfComponent(component);
    }

    public void enableSaveButton(int index, boolean enable) {
        Component component = getTabComponentAt(index);
        if (component instanceof CloseSaveButtonTab) {
            CloseSaveButtonTab closeSaveButtonTab = (CloseSaveButtonTab) component;
            closeSaveButtonTab.getSaveButton().setEnabled(enable);
        }
    }

    public void enableLoadButtons(boolean enable) {
        canLoad = enable;
        int count = getTabCount();
        for (int index = 0; index < count; index++) {
            Component component = getTabComponentAt(index);
            if (component instanceof CloseSaveButtonTab) {
                CloseSaveButtonTab closeSaveButtonTab = (CloseSaveButtonTab) component;
                closeSaveButtonTab.getLoadButton().setEnabled(canLoad && closeSaveButtonTab.isEnabled);
            }
        }
    }

    public synchronized void enableCloseButton(int index, boolean enable) {
        Component component = getTabComponentAt(index);
        if (component instanceof CloseSaveButtonTab) {
            CloseSaveButtonTab closeSaveButtonTab = (CloseSaveButtonTab) component;
            closeSaveButtonTab.getCloseButton().setEnabled(enable);
        }
    }

    /**
     * Returns the {@link ConfigurationGUI} at given index
     * 
     * @param index The index
     * @return A {@link ConfigurationGUI}
     */
    public ConfigurationGUI getConfigurationGUI(int index) {
        ConfigurationGUI configurationGUI = null;
        if (index > -1) {
            Component component = getTabComponentAt(index);
            CloseSaveButtonTab closeSaveButtonTab = (CloseSaveButtonTab) component;
            if (closeSaveButtonTab != null) {
                configurationGUI = closeSaveButtonTab.getConfigurationGUI();
            }
        }
        return configurationGUI;
    }

    /**
     * Return configuration
     * 
     * @param index int
     * @return Configuration
     */
    public Configuration getConfiguration(int index) {
        ConfigurationGUI configurationGUI = getConfigurationGUI(index);
        return configurationGUI == null ? null : configurationGUI.getConfiguration();

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public class CloseSaveButtonTab extends JPanel implements TextTabComponent {

        private static final long serialVersionUID = -3426146783690853890L;

        private String title;
        private final JLabel label;
        private final JButton saveButton, loadButton, reloadButton, deleteButton, closeButton;
        private final ConfigurationGUI configurationGUI;
        private final boolean isExpert, isEnabled;

        public CloseSaveButtonTab(final String title, Icon icon, ConfigurationGUI configurationGUI,
                ActionListener reloadListener, ActionListener deleteListener, boolean isExpert, boolean isEnabled) {
            super();
            this.title = title;
            this.configurationGUI = configurationGUI;
            this.isExpert = isExpert;
            this.isEnabled = isEnabled;
            setBorder(null);

            setOpaque(false);
            FlowLayout flowLayout = new FlowLayout(FlowLayout.LEADING, 5, 0);
            setLayout(flowLayout);
            setVisible(true);

            label = new JSmoothLabel(this.title);
            label.setIcon(icon);
            updateLabelPreferredSize();
            add(label);

            saveButton = new JButton(Utilities.SAVE_ICON);
            saveButton.setEnabled(false);
            saveButton.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
            if (this.isEnabled && this.isExpert) {
                add(saveButton);
            }

            loadButton = new JButton(Utilities.LOAD_ICON);
            loadButton.setEnabled(canLoad && this.isEnabled);
            loadButton.setBorder(new LineBorder(Color.GRAY, 1));
            loadButton.setToolTipText(Utilities.getMessage("flyscan.configuration.load"));
            loadButton.addMouseListener(new ButtonDecorationListener(Utilities.BUTTON_MOUSE_ENTERED_BORDER));
            if (this.isEnabled) {
                add(loadButton);
            }

            reloadButton = new JButton(Utilities.RELOAD_ICON);
            reloadButton.setBorder(new LineBorder(Color.GRAY, 1));
            reloadButton.setToolTipText(Utilities.getMessage("flyscan.configuration.reload"));
            reloadButton.addMouseListener(new ButtonDecorationListener(Utilities.BUTTON_MOUSE_ENTERED_BORDER));
            if (reloadListener != null) {
                reloadButton.addActionListener(reloadListener);
                if (this.isEnabled) {
                    add(reloadButton);
                }
            }

            deleteButton = new JButton(Utilities.DELETE_CONFIG_ICON);
            deleteButton.setBorder(new LineBorder(Color.GRAY, 1));
            deleteButton.setToolTipText(Utilities.getMessage("flyscan.configuration.delete"));
            deleteButton.addMouseListener(new ButtonDecorationListener(Utilities.BUTTON_MOUSE_ENTERED_BORDER));
            if (deleteListener != null) {
                deleteButton.addActionListener(deleteListener);
                if (this.isEnabled && this.isExpert) {
                    add(deleteButton);
                }
            }

            closeButton = new DarkPressButton(Utilities.getIcon("flyscan.close"));
            closeButton.setToolTipText(Utilities.getMessage("flyscan.tab.close"));
            closeButton.setOpaque(false);
            closeButton.setBackground(Color.WHITE);
            closeButton.setMargin(CometeUtils.getzInset());
            closeButton.setBorder(new LineBorder(Color.GRAY, 1));
            closeButton.addMouseListener(new ButtonDecorationListener(Utilities.BUTTON_MOUSE_ENTERED_BORDER) {
                @Override
                public void mouseEntered(MouseEvent e) {
                    super.mouseEntered(e);
                    closeButton.setOpaque(true);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    super.mouseExited(e);
                    closeButton.setOpaque(false);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (getTitle().trim().endsWith(Utilities.ASTERISK)) {
                        Object[] options = { Utilities.getMessage("flyscan.abort"),
                                Utilities.getMessage("flyscan.tab.close") };
                        BoldTitleTabbedPane tabbedPane = CloseSaveButtonTabbedPane.this;
                        int index = tabbedPane.indexOfTabComponent(CloseSaveButtonTab.this);
                        Component tab = index > -1 ? tabbedPane.getComponent(index) : null;
                        int n = JOptionPane.showOptionDialog(tab,
                                Utilities.getMessage("flyscan.tab.close.confirm.changes"),
                                Utilities.getMessage("flyscan.changes.unsaved"), JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                        if (n == 1) {
                            removeTab();
                        }
                    } else {
                        removeTab();
                    }

                }

                private void removeTab() {
                    BoldTitleTabbedPane tabbedPane = CloseSaveButtonTabbedPane.this;
                    int index = tabbedPane.indexOfTabComponent(CloseSaveButtonTab.this);
                    if (index > -1) {
                        tabbedPane.remove(index);
                    }
                }

            });
            add(closeButton);
        }

        @Override
        public void setTitle(String title) {
            this.title = title;
            label.setText(title);
            updateLabelPreferredSize();
        }

        @Override
        public String getTitle() {
            if (title == null) {
                title = label.getText();
            }
            return title;
        }

        public JButton getSaveButton() {
            return saveButton;
        }

        public JButton getLoadButton() {
            return loadButton;
        }

        public ConfigurationGUI getConfigurationGUI() {
            return configurationGUI;
        }

        public JButton getCloseButton() {
            return closeButton;
        }

        @Override
        public Font getTitleFont() {
            return label.getFont();
        }

        @Override
        public void setTitleFont(Font font) {
            label.setFont(font);
            updateLabelPreferredSize();
        }

        @Override
        public Color getTitleForeground() {
            return label.getForeground();
        }

        @Override
        public void setTitleForeground(Color fg) {
            label.setForeground(fg);
        }

        protected void updateLabelPreferredSize() {
            label.setPreferredSize(null);
            Dimension size = label.getPreferredSize();
            size.width += 5;
            label.setPreferredSize(size);
            if (label.getParent() instanceof JComponent) {
                label.revalidate();
                ((JComponent) label.getParent()).revalidate();
            }
        }
    }

    protected static class DarkPressMetalButtonUI extends MetalButtonUI {

        protected static final Color SELECT_COLOR;
        static {
            double factor = 0.3;
            Color tmp = UIManager.getColor("TabbedPane.selected");
            tmp = tmp instanceof UIResource ? new Color(tmp.getRed(), tmp.getGreen(), tmp.getBlue()) : tmp;
            SELECT_COLOR = ColorUtils.darker(tmp, factor);
        }

        protected WeakReference<AbstractButton> buttonRef;

        public DarkPressMetalButtonUI() {
            super();
        }

        @Override
        protected Color getSelectColor() {
            return SELECT_COLOR;
        }

        @Override
        public void installDefaults(AbstractButton b) {
            super.installDefaults(b);
            buttonRef = b == null ? null : new WeakReference<>(b);
        }
    }

    protected static class DarkPressButton extends JButton {

        private static final long serialVersionUID = 8133798403768573929L;

        public DarkPressButton() {
            super();
        }

        public DarkPressButton(Action a) {
            super(a);
        }

        public DarkPressButton(Icon icon) {
            super(icon);
        }

        public DarkPressButton(String text, Icon icon) {
            super(text, icon);
        }

        public DarkPressButton(String text) {
            super(text);
        }

        @Override
        public void updateUI() {
            setUI(new DarkPressMetalButtonUI());
        }
    }
}