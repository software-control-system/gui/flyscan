package fr.soleil.flyscan.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.flyscan.gui.util.DeviceManager;
import fr.soleil.flyscan.gui.util.IFlyScanConstants;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.tabbedpane.CloseSaveButtonTabbedPane;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.lib.project.swing.panel.ImagePanel;
import fr.soleil.lib.project.swing.text.JSmoothLabel;

/**
 * Fly scan main class
 * 
 * @author guerre-giordano, Rapha&euml;l GIRARDOT
 */
public class FlyScan extends Application implements IFlyScanConstants {

    private static final long serialVersionUID = -3217434037772954846L;

    public static long startDate = 0;

    protected static final String COPYRIGHT = "SOLEIL Synchrotron";
    protected static final String TITLE = "FlyScan GUI";
    protected static final String MOCK_ARGUMENT = "mockEnabled=";
    protected static final String MODE_ARGUMENT = "mode=";
    protected static final String FSS_TIMEOUT = "FlyScanTimeout";
    protected static final String EASY_CONFIG_ONLY = "EasyConfigOnly";

    private final FlyScanTangoBox flyScanTangoBox;
    private final JCheckBoxMenuItem easyConfigItem;

    private final boolean expert;
    private final String title;

    /**
     * Constructor
     * 
     * @param expert boolean
     */
    public FlyScan(String model, String recordingManager, final boolean expert, final boolean useMock) {
        this(null, model, recordingManager, expert, useMock);
    }

    /**
     * Constructor
     * 
     * @param expert boolean
     */
    public FlyScan(Splash splash, String model, String recordingManager, final boolean expert, final boolean useMock) {
        super(null, splash, false);
        title = String.format(Utilities.getMessage(expert ? "flyscan.level.expert" : "flyscan.level.user"),
                Utilities.getApplicationNameAndVersion(TITLE));
        setTitle(title);
        getSplash().setTitle(getTitle());
        this.expert = expert;
        // Use US Locale as default
        Locale.setDefault(Locale.US);

        flyScanTangoBox = new FlyScanTangoBox(expert, useMock);
        flyScanTangoBox.setModel(model);
        flyScanTangoBox.setRecordingManager(recordingManager);
        flyScanTangoBox.start();

        easyConfigItem = new JCheckBoxMenuItem(flyScanTangoBox.getEasyConfigOnlyAction());
        easyConfigItem.setSelected(flyScanTangoBox.isEasyConfigOnly());

        initialize();

        // Remove help menu to put it at the end
        getJMenuBar().remove(getHelpMenu());
        getEditMenu().remove(getPreferenceMenuItem());
        getEditMenu().add(flyScanTangoBox.getTimeOutAction());
        getEditMenu().add(easyConfigItem);
        if (useMock) {
            getJMenuBar().add(flyScanTangoBox.getMockMenu());
        }
        // Save action is useless: remove it
        getFileMenu().remove(getSaveMenuItem());
        // Add help menu to put it at the end
        getJMenuBar().add(getHelpMenu());

        setContentPane(flyScanTangoBox);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    protected static String getTitle(boolean expert) {
        return String.format(Utilities.getMessage(expert ? "flyscan.level.expert" : "flyscan.level.user"),
                Utilities.getApplicationNameAndVersion(TITLE));
    }

    @Override
    protected void loadPreferences() {
        super.loadPreferences();
        int timeout = prefs.getInt(FSS_TIMEOUT, flyScanTangoBox.getFssTimeout());
        flyScanTangoBox.setFssTimeout(timeout);
        boolean easyConfigOnly = prefs.getBoolean(EASY_CONFIG_ONLY, flyScanTangoBox.isEasyConfigOnly());
        flyScanTangoBox.setEasyConfigOnly(easyConfigOnly);
        easyConfigItem.setSelected(easyConfigOnly);
    }

    @Override
    protected void savePreferences() {
        super.savePreferences();
        prefs.putInt(FSS_TIMEOUT, flyScanTangoBox.getFssTimeout());
        prefs.putBoolean(EASY_CONFIG_ONLY, flyScanTangoBox.isEasyConfigOnly());
    }

    @Override
    public boolean quit() {
        boolean canQuit;
        if (expert) {
            CloseSaveButtonTabbedPane tabbedPane = flyScanTangoBox.getTabbedPane();
            int nbConfigUnsaved = 0;
            for (int i = 0; i < tabbedPane.getTabCount(); i++) {
                if (tabbedPane.getConfigNameAt(i).trim().endsWith(Utilities.ASTERISK)) {
                    nbConfigUnsaved++;
                }
            }
            if (nbConfigUnsaved > 0) {
                Object[] options = { Utilities.CANCEL, Utilities.getMessage("flyscan.changes.ignore.quit") };
                int n = JOptionPane.showOptionDialog(tabbedPane,
                        String.format(Utilities.getMessage("flyscan.configuration.unsaved.confirm.quit"),
                                nbConfigUnsaved),
                        Utilities.getMessage("flyscan.changes.saved.not"), JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                canQuit = (n == 1);

                if (canQuit) {
                    doQuit();
                }

            } else {
                canQuit = true;
                doQuit();
            }
        } else {
            canQuit = true;
            doQuit();
        }
        return canQuit;
    }

    @Override
    protected JDialog generateAboutDialog() {
        ResourceBundle bundle = ResourceBundle.getBundle(FlyScan.class.getPackage().getName() + ".application");
        JDialog aboutDialog = new JDialog(this, String.format(Utilities.getMessage("flyscan.about.title"), TITLE),
                true);
        ImagePanel panel = new ImagePanel(new BorderLayout());
        panel.setPreferredSize(
                new Dimension(Utilities.SPLASH_ICON.getIconWidth(), Utilities.SPLASH_ICON.getIconHeight()));
        panel.setBackgroundImage(Utilities.SPLASH_ICON);
        panel.setImageAlpha(1);
        JLabel label = new JSmoothLabel(
                String.format(Utilities.getMessage("flyscan.about.description"), bundle.getString("project.version"),
                        DateUtil.milliToString(FlyScan.getStartDate(), IDateConstants.US_DATE_FORMAT)),
                SwingConstants.LEFT);
        label.setFont(Utilities.BASIC_FONT.deriveFont(14f));
        panel.add(label, BorderLayout.SOUTH);
        aboutDialog.setContentPane(panel);
        aboutDialog.pack();
        aboutDialog.setResizable(false);
        aboutDialog.setLocationRelativeTo(getHelpMenu());
        return aboutDialog;
    }

    @Override
    public void paint(Graphics g) {
        // force text anti aliasing
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        }
        super.paint(g);
    }

    @Override
    protected ImageIcon getSplashImage() {
        return Utilities.SPLASH_ICON;
    }

    @Override
    protected Image getDefaultIconImage() {
        return Utilities.FLYSCAN_ICON.getImage();
    }

    @Override
    protected String getApplicationTitle() {
        return title;
    }

    @Override
    protected String getCopyright() {
        return COPYRIGHT;
    }

    public static long getStartDate() {
        if (startDate == 0) {
            startDate = System.currentTimeMillis();
        }
        return startDate;
    }

    public static void main(String[] args) {
        System.out.println("Starting flyscangui...");
        long date = System.currentTimeMillis();
        Splash splash = new Splash(Utilities.SPLASH_ICON, false);
        splash.setTitle(getTitle(false));
        splash.initProgress();
        splash.setMaxProgress(5);
        splash.setCopyright(COPYRIGHT);
        FlyScan.startDate = date;
        splash.progress(0);
        splash.setMessage(
                FlyScan.getMessageManager(FlyScan.class.getClassLoader()).getMessage("Application.progressMessage0"));
        splash.setVisible(true);
        boolean isExpert = false, useMock = false;
        String fss = ObjectUtils.EMPTY_STRING, recordingManager = ObjectUtils.EMPTY_STRING;
        Collection<String> devices = new ArrayList<>();
        for (String arg : args) {
            if (arg.startsWith(MOCK_ARGUMENT)) {
                useMock = Boolean.parseBoolean(arg.substring(MOCK_ARGUMENT.length()));
            } else if (arg.startsWith(MODE_ARGUMENT)) {
                String level = arg.substring(MODE_ARGUMENT.length());
                if (level.equalsIgnoreCase(ParsingUtil.EXPERT)) {
                    isExpert = true;
                }
            } else {
                devices.add(arg);
            }
        } // end for (String arg : args)
        splash.setTitle(getTitle(false));
        for (String device : devices) {
            String devClass = TangoDeviceHelper.getDeviceClass(device);
            if (devClass != null && !devClass.isEmpty()) {
                if (FSS.equalsIgnoreCase(devClass)) {
                    if (fss.isEmpty()) {
                        fss = device;
                    }
                } else if (RECORDING_MANAGER.equalsIgnoreCase(devClass)) {
                    // SCAN-925: get RecordingManager from arguments
                    if (recordingManager.isEmpty()) {
                        recordingManager = device;
                    }
                }
            }
        }
        devices.clear();
        fss = Utilities.getNotNullDeviceName(fss);
        recordingManager = Utilities.getNotNullDeviceName(recordingManager);
        String[] selectedDevices = DeviceManager.getDevices(splash, fss, recordingManager);
        fss = selectedDevices[0];
        recordingManager = selectedDevices[1];
        if ((!fss.isEmpty()) || useMock) {
            if (recordingManager.isEmpty()) {
                System.err.println("No RecordingManager device selected");
                System.exit(1);
            }
            System.out.println("Launching flyscangui...");
            if (!splash.isVisible()) {
                splash.setVisible(true);
            }
            FlyScan flyScan = new FlyScan(splash, fss, recordingManager, isExpert, useMock);
            int state = flyScan.getExtendedState();
            Rectangle bounds = flyScan.getBounds();
            flyScan.pack();
            Dimension size = flyScan.getSize();
            int refWidth = 650, refHeight = 550;
            int w = Math.max(bounds.width, refWidth), h = Math.max(bounds.height, refHeight);
            if (size.width < w) {
                size.width = w;
            }
            if (size.height < h) {
                size.height = h;
            }
            flyScan.setSize(size);
            size.width = refWidth;
            size.height = refHeight;
            flyScan.setMinimumSize(size);
            flyScan.setExtendedState(state);
            flyScan.setVisible(true);
            splash.setVisible(false);
        } else { // end if ((!fss.isEmpty()) || useMock)
            LOGGER.error(Utilities.getMessage("flyscan.error.device.no"));
        }
    }

}
