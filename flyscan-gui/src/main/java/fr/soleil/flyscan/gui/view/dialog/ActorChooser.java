package fr.soleil.flyscan.gui.view.dialog;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;
import java.lang.ref.WeakReference;
import java.util.Collection;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.delegate.UIDelegate;
import fr.soleil.flyscan.lib.model.parsing.configuration.Entry;
import fr.soleil.flyscan.lib.model.parsing.configuration.PredefinedEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.TrajectoryEntry;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Actuator;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Hook;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Monitor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.Sensor;
import fr.soleil.flyscan.lib.model.parsing.configuration.fsobject.actor.TimeBase;
import fr.soleil.flyscan.lib.model.parsing.configuration.section.Section;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.Trajectory;
import fr.soleil.flyscan.lib.model.parsing.configuration.trajectory.TrajectoryParser;
import fr.soleil.flyscan.lib.model.parsing.exception.FSParsingException;
import fr.soleil.flyscan.lib.model.parsing.plugin.EntryType;
import fr.soleil.flyscan.lib.model.parsing.plugin.Plugin;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class ActorChooser extends ANamedDataChooser {

    private static final long serialVersionUID = -2568926510994680879L;

    protected static final String TITLE_FORMAT = Utilities.getMessage("flyscan.actor.choose");

    protected final JLabel typeLabel;
    protected final JComboBox<Plugin> typeCombo;
    protected final WeakReference<Section> sectionRef;

    protected ActorChooser(Window owner, String title, Section section, Plugin... types) {
        super(owner, title);
        sectionRef = section == null ? null : new WeakReference<>(section);
        typeLabel = new JLabel(Utilities.TYPE);
        typeLabel.setFont(Utilities.TITLE_FONT);
        errorLabel.setFont(ERROR_FONT);
        typeCombo = new JComboBox<>(types);
        typeCombo.setRenderer(new PluginRenderer());

        JPanel mainPanel = (JPanel) getContentPane();
        int y = 0;
        GridBagConstraints constraints = addNameComponents(mainPanel);
        y = constraints.gridy + 1;
        y = addErrorLabel(mainPanel);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = y;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = new Insets(0, DEFAULT_MARGIN, DEFAULT_MARGIN, TITLE_GAP);
        mainPanel.add(typeLabel, constraints);
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = y++;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.insets = new Insets(0, 0, DEFAULT_MARGIN, DEFAULT_MARGIN);
        mainPanel.add(typeCombo, constraints);
        addButtons(mainPanel);
    }

    public static Actor chooseActor(Component parent, String actorTypeName, String configName, Section section,
            Collection<Plugin> plugins) {
        Actor actor;
        ActorChooser chooser = new ActorChooser(WindowSwingUtils.getWindowForComponent(parent),
                String.format(TITLE_FORMAT, actorTypeName), section, plugins.toArray(new Plugin[plugins.size()]));
        chooser.pack();
        chooser.errorLabel.setText(ObjectUtils.EMPTY_STRING);
        chooser.setLocationRelativeTo(parent);
        chooser.setVisible(true);
        if (chooser.validated) {
            String name = chooser.nameField.getText();
            Plugin plugin = (Plugin) chooser.typeCombo.getSelectedItem();
            if (plugin == null) {
                actor = null;
            } else {
                switch (plugin.getType()) {
                    case ACTUATOR:
                        actor = new Actuator(plugin.getTypeName(), section);
                        PredefinedEntry dimensionEntry = new PredefinedEntry(ParsingUtil.DIMENSION, Utilities.ONE,
                                EntryType.INT);
                        dimensionEntry.setComment(Utilities.ACTUATOR_DIMENSION_DESCRIPTION);
                        actor.addEntry(dimensionEntry);
                        TrajectoryEntry trajectoryEntry = new TrajectoryEntry(ParsingUtil.TRAJECTORY,
                                Utilities.DEFAULT_TRAJECTORY);
                        Trajectory trajectory;
                        try {
                            trajectory = TrajectoryParser.parse(configName, trajectoryEntry.getValue(),
                                    actor.getClass().getSimpleName() + Utilities.SPACE + actor.getName());
                        } catch (FSParsingException e) {
                            // should not happen
                            trajectory = null;
                            Utilities.LOGGER.error(configName + ": Failed to create default trajectory for "
                                    + actor.getName() + " actuator", e);
                        }
                        trajectoryEntry.setTrajectory(trajectory);
                        actor.addEntry(trajectoryEntry);
                        break;
                    case SENSOR:
                        actor = new Sensor(section);
                        break;
                    case TIMEBASE:
                        actor = new TimeBase(section);
                        break;
                    case HOOK:
                        actor = new Hook(section);
                        break;
                    case MONITOR:
                        actor = new Monitor(section);
                        break;
                    default:
                        // EASY_CONFIG_PROP: not managed
                        actor = null;
                        break;
                }
            }
            if (actor != null) {
                // Actor is validated: register the 3 minimum predefined entries (name, type and enable),
                // and mark them as present in configuration.
                Entry entry = new PredefinedEntry(ParsingUtil.NAME, name, EntryType.STRING);
                entry.setPresentInConfig(true);
                actor.addEntry(entry);
                entry = new PredefinedEntry(ParsingUtil.TYPE, plugin.getName(), EntryType.STRING);
                entry.setPresentInConfig(true);
                actor.addEntry(entry);
                entry = new PredefinedEntry(ParsingUtil.ENABLE, Utilities.FALSE, EntryType.BOOL);
                entry.setPresentInConfig(true);
                actor.addEntry(entry);
                // Add "order" parameter (SCAN-962)
                entry = new PredefinedEntry(ParsingUtil.ORDER, Utilities.ONE, EntryType.INT);
                entry.setPresentInConfig(true);
                actor.addEntry(entry);
                // Add plugin parameters
                UIDelegate.processParams(actor, plugin, configName);
            }
        } else {
            actor = null;
        }
        return actor;
    }

    @Override
    protected String getExisting(String name) {
        Actor actor = Utilities.getActorNamedLike(name, ObjectUtils.recoverObject(sectionRef), null);
        return actor == null ? null : actor.getName();
    }

    @Override
    protected void finalize() {
        super.finalize();
        typeCombo.removeAllItems();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class PluginRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = -5494278761942331254L;

        public PluginRenderer() {
            super();
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                boolean cellHasFocus) {
            JLabel comp = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof Plugin) {
                comp.setText(((Plugin) value).getName());
            }
            return comp;
        }
    }

}
