package fr.soleil.flyscan.gui.util;

public interface IConfigNameChecker {

    public boolean isUniqueConfigName(String name);

    public String getExistingConfigName(String name);
}
