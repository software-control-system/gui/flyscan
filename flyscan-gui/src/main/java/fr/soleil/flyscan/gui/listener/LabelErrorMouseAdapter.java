package fr.soleil.flyscan.gui.listener;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import fr.soleil.flyscan.gui.view.ConfigurationGUI;
import fr.soleil.flyscan.gui.view.component.FSLabel;
import fr.soleil.lib.project.ObjectUtils;

public final class LabelErrorMouseAdapter extends MouseAdapter {

    protected static final Font SMALL_FONT = new FSLabel().getFont();
    protected static final Font BIG_FONT = SMALL_FONT.deriveFont(SMALL_FONT.getSize() + 6f);

    private final WeakReference<ConfigurationGUI> configurationGUIRef;
    private final WeakReference<JComponent> componentRef;
    private final WeakReference<JLabel> nameLabelRef;
    private final String tabTitle;
    private Font originalErrorFont;
    private final Timer timer;
    private final WeakReference<JRadioButton> buttonRef;

    public LabelErrorMouseAdapter(ConfigurationGUI configurationGUI, JComponent component, JLabel nameLabel,
            JRadioButton selector, String tabTitle) {
        this.configurationGUIRef = configurationGUI == null ? null : new WeakReference<>(configurationGUI);
        this.componentRef = component == null ? null : new WeakReference<>(component);
        this.nameLabelRef = nameLabel == null ? null : new WeakReference<>(nameLabel);
        this.tabTitle = tabTitle;
        this.buttonRef = selector == null ? null : new WeakReference<>(selector);
        timer = new Timer();
    }

    protected JScrollPane findScrollPane(Component comp) {
        JScrollPane scrollPane;
        if (comp == null) {
            scrollPane = null;
        } else if (comp instanceof JScrollPane) {
            scrollPane = (JScrollPane) comp;
        } else {
            Container parent = comp.getParent();
            if (parent instanceof Component) {
                scrollPane = findScrollPane(parent);
            } else {
                scrollPane = null;
            }
        }
        return scrollPane;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // Open enclosing tab
        ConfigurationGUI configurationGUI = ObjectUtils.recoverObject(configurationGUIRef);
        if (configurationGUI != null) {
            configurationGUI.getTabbedPane().setSelectedTab(tabTitle);
            // Select radio button and make selection visible by scrolling in parent scrollpane.
            JRadioButton selector = ObjectUtils.recoverObject(buttonRef);
            if (selector != null) {
                selector.setSelected(true);
                Container parent = selector.getParent();
                if (parent instanceof JComponent) {
                    JComponent parentComp = (JComponent) parent;
                    Component[] comps = parent.getComponents();
                    if (comps != null) {
                        int index = -1;
                        for (Component comp : comps) {
                            index++;
                            if (comp == selector) {
                                break;
                            }
                        }
                        if (index > -1 && index < comps.length - 1) {
                            Component comp = comps[index + 1];
                            if (comp != null) {
                                // Scroll later to ensure scrolling to to right position
                                SwingUtilities.invokeLater(() -> {
                                    parentComp.scrollRectToVisible(comp.getBounds());
                                });
                            }
                        }
                    }
                }
            }
            // Do the rest later to ensure letting enough time for the tab to be displayed.
            SwingUtilities.invokeLater(() -> {
                // Scroll to enclosing section/object
                JComponent component = ObjectUtils.recoverObject(componentRef);
                if (component != null) {
                    JScrollPane scrollPane = findScrollPane(component);
                    if (scrollPane != null) {
                        // reset position to 0. Otherwise, the scrollpane won't be able to scroll up (don't ask why)
                        scrollPane.getVerticalScrollBar().setValue(0);
                        // scroll to component position
                        Container parent = component.getParent();
                        if (parent instanceof JComponent) {
                            ((JComponent) parent).scrollRectToVisible(component.getBounds());
                        }
                    }

                    // Grab focus
                    component.grabFocus();
                }
                JLabel nameLabel = ObjectUtils.recoverObject(nameLabelRef);
                if (nameLabel != null) {
                    nameLabel.setFont(BIG_FONT);
                    final TimerTask bigFontTask = new TimerTask() {
                        @Override
                        public void run() {
                            nameLabel.setFont(BIG_FONT);
                        }
                    };
                    final TimerTask smallFontTask = new TimerTask() {
                        @Override
                        public void run() {
                            nameLabel.setFont(SMALL_FONT);
                        }
                    };
                    final TimerTask smallFontTask2 = new TimerTask() {
                        @Override
                        public void run() {
                            smallFontTask.run();
                        }
                    };
                    int mult = 500;
                    timer.schedule(smallFontTask, mult);
                    timer.schedule(bigFontTask, mult * 2);
                    timer.schedule(smallFontTask2, mult * 3);
                }
            });
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        originalErrorFont = e.getComponent().getFont();
        Map<TextAttribute, Object> attributes = new HashMap<>(originalErrorFont.getAttributes());
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        e.getComponent().setFont(originalErrorFont.deriveFont(attributes));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        e.getComponent().setFont(originalErrorFont);
    }
}