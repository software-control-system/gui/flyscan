package fr.soleil.flyscan.gui.view.component;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.IErrorNotifiableTarget;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.flyscan.gui.FlyScanTangoBox;
import fr.soleil.flyscan.gui.listener.ConfigItemDynamicListener;
import fr.soleil.flyscan.gui.listener.ConfigSelectionListener;
import fr.soleil.flyscan.gui.util.Utilities;
import fr.soleil.flyscan.gui.view.renderer.ConfigurationTreeRenderer;
import fr.soleil.flyscan.gui.view.scrollpane.FSScrollPane;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * {@link JPanel} to select the configuration to display from known configurations.
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class ConfigurationsPanel extends JPanel implements ITextMatrixTarget, IErrorNotifiableTarget, MouseListener {

    private static final long serialVersionUID = -2638481280578435981L;

    protected static final Logger LOGGER = LoggerFactory.getLogger(FlyScanTangoBox.class.getName());

    private volatile String[] configurations;
    private final TargetDelegate delegate;
    private final ErrorNotificationDelegate errorNotificationDelegate;
    private final ConfigSelectionListener configListener;
    private final JLabel searchLabel;
    private final JTextField searchField;
    private final ConfigurationTree configTree;
    private final FSScrollPane configTreeScrollPane;
    private final Color background;

    private final AbstractButton newConfigButton, currentConfigButton;

    public ConfigurationsPanel(ConfigSelectionListener configSelectionListener, ActionListener newConfigActionListener,
            ActionListener currentConfigMenuItemActionListener) {
        super(new GridBagLayout());
        int y = 0;
        Color bg = getBackground();
        background = new Color(bg.getRed(), bg.getGreen(), bg.getBlue());
        this.configListener = configSelectionListener;

        configTree = new ConfigurationTree(configListener);
        configTreeScrollPane = new FSScrollPane(configTree) {

            private static final long serialVersionUID = -7305884256560204030L;

            @Override
            public Dimension getPreferredSize() {
                // Hack preferred size to avoid the need of horizontal scrolling at filter change
                Dimension size = super.getPreferredSize();
                if (!isPreferredSizeSet()) {
                    Dimension treeSize = configTree.getPreferredSize();
                    if (size.width <= treeSize.width + 4 && size.height > treeSize.height + 4) {
                        size.width = treeSize.width + getVerticalScrollBar().getPreferredSize().width + 4;
                    }
                }
                return size;
            }

        };
        configTree.setBackground(getBackground());
        TreeModelListener contentListener = new TreeModelListener() {
            protected void contentChanged() {
                updateConfigTreeLater();
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                contentChanged();
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                contentChanged();
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                contentChanged();
            }

            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                contentChanged();
            }

        };
        configTree.getModel().addTreeModelListener(contentListener);

        ConfigItemDynamicListener configItemListener = new ConfigItemDynamicListener();
        if (newConfigActionListener == null) {
            newConfigButton = null;
        } else {
            newConfigButton = createButton(Utilities.NEW_CONFIG, Utilities.NEW_CONFIG_ICON, configItemListener);
            newConfigButton.addActionListener(newConfigActionListener);
        }
        currentConfigButton = createButton(Utilities.CURRENT_CONFIG, Utilities.SCAN_CONFIGURATION_ICON,
                configItemListener);
        currentConfigButton.addActionListener(currentConfigMenuItemActionListener);

        configurations = Utilities.EMPTY_STRING_ARRAY;
        delegate = new TargetDelegate();
        errorNotificationDelegate = new ErrorNotificationDelegate(this);
        searchLabel = new JLabel(Utilities.SEARCH_ICON);
        searchField = new JTextField();
        searchField.getDocument().addDocumentListener(new DocumentListener() {
            protected void sendFilter() {
                configTree.getModel().setFilter(searchField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                sendFilter();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                sendFilter();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }
        });
        searchField.grabFocus();

        addHierarchyListener(e -> {
            if (isShowing()) {
                SwingUtilities.invokeLater(() -> {
                    if (currentConfigButton != null) {
                        currentConfigButton.setBackground(background);
                        currentConfigButton.setBorder(Utilities.EMPTY_BORDER);
                    }
                    if (newConfigButton != null) {
                        newConfigButton.setBackground(background);
                        newConfigButton.setBorder(Utilities.EMPTY_BORDER);
                    }
                    if (configTree != null) {
                        ((ConfigurationTreeRenderer) configTree.getCellRenderer()).cleanLabels();
                    }
                });
            }
        });
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = y;
        constraints.weightx = 1;
        constraints.weighty = 0;
        constraints.insets = new Insets(1, 0, 4, 1);
        add(searchField, constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = 1;
        constraints.gridy = y++;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = new Insets(1, 1, 4, 0);
        add(searchLabel, constraints);
        // hack to align buttons with tree nodes.
        Insets insets = new Insets(1, 2, 4, 1);
        if (newConfigButton != null) {
            constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.NONE;
            constraints.gridx = 0;
            constraints.gridy = y++;
            constraints.weightx = 0;
            constraints.weighty = 0;
            constraints.insets = insets;
            constraints.anchor = GridBagConstraints.WEST;
            add(newConfigButton, constraints);
        }
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.gridx = 0;
        constraints.gridy = y++;
        constraints.weightx = 0;
        constraints.weighty = 0;
        constraints.insets = insets;
        constraints.anchor = GridBagConstraints.WEST;
        add(currentConfigButton, constraints);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = y++;
        constraints.weightx = 1;
        constraints.weighty = y;
        constraints.gridwidth = 2;
        constraints.insets = new Insets(1, 1, 1, 1);
        add(configTreeScrollPane, constraints);
        updateConfigTreeLater();
    }

    public void updateConfigTreeLater() {
        // Double SwingUtilities.invokeLater to avoid the need of horizontal scrolling.
        // If everything is done in a single SwingUtilities.invokeLater,
        // there are sometimes some cases where the node text is not fully displayed and needs some scrolling.
        SwingUtilities.invokeLater(() -> {
            configTree.expandRoot();
            configTree.revalidate();
            SwingUtilities.invokeLater(() -> {
                configTreeScrollPane.revalidate();
                revalidate();
                Container parent = getParent();
                if (parent != null) {
                    parent.revalidate();
                }
                SwingUtilities.invokeLater(() -> {
                    searchField.grabFocus();
                    // TODO margin according to configurationsSplitPane divider size
                    int width = getPreferredSize().width + 2;
                    if (parent instanceof JSplitPane) {
                        JSplitPane splitPane = (JSplitPane) parent;
                        int location = splitPane.getDividerLocation();
                        if (location > 0 && location != width) {
                            splitPane.setDividerLocation(width);
                        }
                    }
                });
            });
        });
    }

    protected AbstractButton createButton(String text, Icon icon, ConfigItemDynamicListener currentConfigListener) {
        AbstractButton configButton = new JButton(text);
        configButton.setHorizontalAlignment(SwingConstants.LEFT);
        configButton.setBorder(Utilities.EMPTY_BORDER);
        configButton.setIcon(icon);
        configButton.setBackground(background);
        configButton.addMouseListener(currentConfigListener);
        configButton.addMouseListener(this);
        configButton.addMouseMotionListener(currentConfigListener);
        configButton.addActionListener(currentConfigListener);
        configButton.setFont(configTree.getFont());
        configButton.setFocusPainted(false);
        return configButton;
    }

    @Override
    public String[][] getStringMatrix() {
        String[] configurations = this.configurations;
        return configurations == null ? null : new String[][] { configurations.clone() };
    }

    @Override
    public void setStringMatrix(String[][] array) {
        // width and height are not used
        setFlatStringMatrix((String[]) ArrayUtils.convertArrayDimensionFromNTo1(array), 0, 0);
    }

    @Override
    public String[] getFlatStringMatrix() {
        return configurations;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        try {
            configurations = value;
            configTree.getModel().setConfigList(value);
            updateConfigTreeLater();
        } catch (Exception e) {
            LOGGER.error(Utilities.getMessage("flyscan.error.configurations.extract.failed"), e);
        }
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (String.class.equals(concernedDataClass)) {
            String[] configurations = this.configurations;
            height = configurations == null || configurations.length == 0 ? 0 : 1;
        }
        return height;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (String.class.equals(concernedDataClass)) {
            String[] configurations = this.configurations;
            width = configurations == null ? 0 : configurations.length;
        }
        return width;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return true;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        errorNotificationDelegate.notifyForError(message, error);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // not managed
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // not managed
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof AbstractButton)) {
            ((AbstractButton) e.getSource()).setBackground(Utilities.HOVERED_COLOR);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if ((e != null) && (e.getSource() instanceof AbstractButton)) {
            ((AbstractButton) e.getSource()).setBackground(background);
        }
    }

}
