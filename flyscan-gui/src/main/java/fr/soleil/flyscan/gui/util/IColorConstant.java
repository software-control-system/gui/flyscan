package fr.soleil.flyscan.gui.util;

import java.awt.Color;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.tango.data.adapter.StateAdapter;

public interface IColorConstant {
    public static final Color GRAY = ColorTool.getColor(StateAdapter.UNKNOWN);
    public static final Color DARK_GREEN = ColorTool.getColor(StateAdapter.RUNNING);
    public static final Color ORANGE = ColorTool.getColor(StateAdapter.ALARM);
    public static final Color BLUE = ColorTool.getColor(StateAdapter.MOVING);
    public static final Color YELLOW_GRAY = ColorTool.getColor(StateAdapter.INIT);
}
