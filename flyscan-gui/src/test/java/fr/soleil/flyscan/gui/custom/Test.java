package fr.soleil.flyscan.gui.custom;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;

public class Test {

    public static void main(String[] args) {
//        System.setProperty("TANGO_HOST", "172.16.4.17:20001");
        System.setProperty("TANGO_HOST", "172.16.4.17:20001");
        try {
//            DeviceProxy dev = new DeviceProxy("flyscan/gui/flyscanserver");
            DeviceProxy dev = new DeviceProxy("sys/tg_test/1");
            System.out.println("new OK");
            System.out.println(dev.state());
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
        }

    }
}
