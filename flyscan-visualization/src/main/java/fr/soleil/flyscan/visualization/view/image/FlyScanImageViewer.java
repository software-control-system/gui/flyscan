package fr.soleil.flyscan.visualization.view.image;

import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.SwingUtilities;

import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.flyscan.visualization.event.MotorPositionEvent;
import fr.soleil.flyscan.visualization.listener.FlyScanImageMotorPositionListener;
import fr.soleil.flyscan.visualization.util.FSVUtilities;
import fr.soleil.lib.project.ObjectUtils;
import ij.gui.Roi;

public class FlyScanImageViewer extends ImageViewer implements IFlyScanMotorPositionManager {

    private static final long serialVersionUID = 7671987758664803404L;

    // dark purple
    protected static final CometeColor IMAGE_VIEWER_MINIMUM_COLOR = new CometeColor(40, 0, 50);
    // default minimum color in classic Gradient
    protected static final CometeColor COLOR_TO_AVOID = new CometeColor(200, 0, 250);

    // key to recover tool tip text in default MessageManager
    protected static final String TOOLTIP_KEY = "flyscan.motor.position.set.tooltip";

    protected final Collection<FlyScanImageMotorPositionListener> motorPositionListeners;
    protected volatile double stableSpeedStart = Double.NaN, stableSpeedEnd = Double.NaN;

    public FlyScanImageViewer() {
        super();
        setBeamPointEnabled(true);
        setAllowBeamPositionByClick(true);
        setDrawBeamPosition(true);
        setAlwaysFitMaxSize(true);
        updateTooltip();
        motorPositionListeners = Collections.newSetFromMap(new ConcurrentHashMap<>());
    }

    public void addMotorPositionListener(FlyScanImageMotorPositionListener listener) {
        if (listener != null) {
            motorPositionListeners.add(listener);
        }
    }

    public void removeMotorPositionListener(FlyScanImageMotorPositionListener listener) {
        if (listener != null) {
            motorPositionListeners.remove(listener);
        }
    }

    protected void warnMotorPositionListeners() {
        MotorPositionEvent event = new MotorPositionEvent(this);
        for (FlyScanImageMotorPositionListener listener : motorPositionListeners) {
            if (listener != null) {
                listener.motorPositionChanged(event);
            }
        }
    }

    @Override
    protected void updateTooltip() {
        if (imagePanel != null) {
            imagePanel.setToolTipText(FSVUtilities.getMessage(TOOLTIP_KEY));
        }
    }

    @Override
    public void setBeamPoint(double... theBeamPoint) {
        double[] previous = this.beamPoint;
        super.setBeamPoint(theBeamPoint);
        if (!ObjectUtils.sameObject(theBeamPoint, previous)) {
            warnMotorPositionListeners();
        }
        repaint();
    }

    @Override
    protected void setBeamPointFromClick(double ox, double oy, double... theBeamPoint) {
        double[] previous = this.beamPoint;
        super.setBeamPointFromClick(ox, oy, theBeamPoint);
        if (this.beamPoint == theBeamPoint && !ObjectUtils.sameObject(theBeamPoint, previous)) {
            warnMotorPositionListeners();
        }
    }

    @Override
    protected IJCanvas generateImageCanvas(IJRoiManager imp) {
        FlyScanIJCanvas canvas = new FlyScanIJCanvas(imp, this);
        canvas.setStableSpeedBounds(stableSpeedStart, stableSpeedEnd);
        return canvas;
    }

    @Override
    protected void setupNewCanvasFromOldCanvas(IJCanvas canvas, IJCanvas newCanvas) {
        super.setupNewCanvasFromOldCanvas(canvas, newCanvas);
        newCanvas.setBeamColor(canvas.getBeamColor());
        newCanvas.setBeamCrossType(canvas.getBeamCrossType());
        newCanvas.setBeamPoint(canvas.getBeamPoint());
        newCanvas.setDrawBeamPosition(canvas.isDrawBeamPosition());
        if (newCanvas instanceof FlyScanIJCanvas) {
            FlyScanIJCanvas newFSCanvas = (FlyScanIJCanvas) newCanvas;
            if (canvas instanceof FlyScanIJCanvas) {
                FlyScanIJCanvas fsCanvas = (FlyScanIJCanvas) canvas;
                newFSCanvas.setStableSpeedColor(fsCanvas.getStableSpeedColor());
                newFSCanvas.setStableSpeedStroke(fsCanvas.getStableSpeedStroke());
            }
        }
    }

    @Override
    public FlyScanIJCanvas getImageCanvas() {
        return (FlyScanIJCanvas) super.getImageCanvas();
    }

    @Override
    public void setGradient(Gradient theGradient) throws ApplicationIdException {
        if ((theGradient != null) && (theGradient.getEntryNumber() == 5)
                && COLOR_TO_AVOID.equals(theGradient.getColorAt(0))) {
            // set minimum color
            theGradient.setColorAt(0, IMAGE_VIEWER_MINIMUM_COLOR);
            theGradient.setAsDefaultRainbowGradient();
        }
        super.setGradient(theGradient);
    }

    @Override
    protected void initColorStuff() {
        super.initColorStuff();
        gradient.buildRainbowGradient();
        gradient.setColorAt(0, IMAGE_VIEWER_MINIMUM_COLOR);
        gradientViewer.setGradient(gradient);
        gColormap = gradient.buildColorMap(65536);
    }

    @Override
    protected boolean isBeamPointSetAtMousePressed() {
        return false;
    }

    @Override
    protected boolean isBeamPointSetAtMouseReleased() {
        return false;
    }

    @Override
    protected boolean isBeamPointSetAtMouseClicked() {
        return true;
    }

    @Override
    protected boolean isBeamPointClick(MouseEvent e, MouseEvent te) {
        boolean beamPointClick;
        if (isAllowBeamPositionByClick() && isBeamPointEnabled() && SwingUtilities.isLeftMouseButton(te)) {
            if (e.isAltDown() || e.isControlDown() || e.isShiftDown()) {
                beamPointClick = true;
            } else if (imageCanvas instanceof FlyScanIJCanvas) {
                FlyScanIJCanvas canvas = (FlyScanIJCanvas) imageCanvas;
                Roi roi = canvas.getRoiAtPoint(te.getX(), te.getY());
                beamPointClick = roi == null;
            } else {
                beamPointClick = false;
            }
        } else {
            beamPointClick = false;
        }
        return beamPointClick;
    }

    @Override
    public double getStableSpeedStart() {
        return stableSpeedStart;
    }

    @Override
    public void setStableSpeedStart(double start) {
        this.stableSpeedStart = start;
        getImageCanvas().setStableSpeedStart(start);
    }

    @Override
    public double getStableSpeedEnd() {
        return stableSpeedEnd;
    }

    @Override
    public void setStableSpeedEnd(double end) {
        this.stableSpeedEnd = end;
        getImageCanvas().setStableSpeedEnd(end);
    }

    @Override
    public void setStableSpeedBounds(double start, double end) {
        this.stableSpeedStart = start;
        this.stableSpeedEnd = end;
        getImageCanvas().setStableSpeedBounds(start, end);
    }

    @Override
    public boolean isStableSpeedOnY() {
        return getImageCanvas().isStableSpeedOnY();
    }

    @Override
    public void setStableSpeedOnY(boolean stableSpeedOnY) {
        getImageCanvas().setStableSpeedOnY(stableSpeedOnY);
    }
}
