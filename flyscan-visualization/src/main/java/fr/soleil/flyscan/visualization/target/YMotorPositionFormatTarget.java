package fr.soleil.flyscan.visualization.target;

import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.lib.project.ObjectUtils;

/**
 * The {@link ITextTarget} that listens to y motor position format.
 * 
 * @author GIRARDOT
 */
public class YMotorPositionFormatTarget extends ABeanContainerTarget implements ITextTarget {

    public YMotorPositionFormatTarget(FlyScanVisualizationTangoBox bean) {
        super(bean);
    }

    @Override
    public String getText() {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        return bean == null ? null : bean.getYMotorPositionFormat();
    }

    @Override
    public void setText(String text) {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        if (bean != null) {
            bean.setYMotorPositionFormat(text);
        }
    }

}
