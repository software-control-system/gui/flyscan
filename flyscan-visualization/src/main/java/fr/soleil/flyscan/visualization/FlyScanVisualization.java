package fr.soleil.flyscan.visualization;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import fr.soleil.flyscan.visualization.util.FSVUtilities;
import fr.soleil.lib.project.application.Application;

/**
 * Main class for flyscanvisualization
 * 
 * @author GIRARDOT
 */
public class FlyScanVisualization extends Application {

    private static final long serialVersionUID = 6413324373174565317L;

    private static final String IMAGES_MENU = "flyscan.cartography.images.menu";
    private static final String TRANSFORMATIONS_MENU = "flyscan.cartography.images.menu.transformations";
    protected static final String PROPERTIES_ARGUMENT = "propFile=";

    private final FlyScanVisualizationTangoBox flyScanVisualizationTangoBox;
    protected JMenu imageMenu, tranformationMenu;

    public FlyScanVisualization() {
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        flyScanVisualizationTangoBox = new FlyScanVisualizationTangoBox();
        imageMenu = new JMenu(FSVUtilities.getMessage(IMAGES_MENU));
        tranformationMenu = new JMenu(FSVUtilities.getMessage(TRANSFORMATIONS_MENU));
        imageMenu.add(tranformationMenu);
        JPanel transformationPanel = flyScanVisualizationTangoBox.getTransformationPanel();
        Dimension size = transformationPanel.getPreferredSize();
        size.width += 150; // take a 150 pixels margin to ensure displaying all transformation information
        transformationPanel.setPreferredSize(size);
        tranformationMenu.add(transformationPanel);
        getJMenuBar().remove(getHelpMenu());
        getJMenuBar().add(imageMenu);
        getJMenuBar().add(getHelpMenu());
        setContentPane(flyScanVisualizationTangoBox);
        getFileMenu().remove(getSaveMenuItem());
        getEditMenu().remove(getPreferenceMenuItem());
        getEditMenu().add(flyScanVisualizationTangoBox.getOpenPropertiesEditorAction());
    }

    @Override
    protected ImageIcon getSplashImage() {
        return FSVUtilities.SPLASH_ICON;
    }

    @Override
    protected Image getDefaultIconImage() {
        return FSVUtilities.FLYSCAN_ICON.getImage();
    }

    @Override
    protected String getApplicationTitle() {
        return FSVUtilities.APPLICATION_NAME_WITH_VERSION;
    }

    @Override
    protected String getCopyright() {
        return FSVUtilities.COPYRIGHT;
    }

    public void setPropertiesFile(String path) {
        flyScanVisualizationTangoBox.setPropertiesFile(path);
    }

    public static void main(String[] args) {
        String file = null;
        if ((args != null) && (args.length == 1)) {
            file = args[0].substring(PROPERTIES_ARGUMENT.length());
        }
        if ((file != null) && (!file.isEmpty())) {
            final FlyScanVisualization appli = new FlyScanVisualization();
            appli.pack();
            Dimension size = appli.getSize();
            if (size.width < 850) {
                size.width = 850;
            }
            if (size.height < 550) {
                size.height = 550;
            }
            appli.setSize(size);
            appli.setVisible(true);
            appli.setPropertiesFile(file);
            SwingUtilities.invokeLater(() -> {
                appli.flyScanVisualizationTangoBox.setBottomDividertoBestLocation();
            });
            appli.flyScanVisualizationTangoBox.start();
        } else {
            System.err.println("Invalid arguments!");
            System.err.println("Valid call is");
            System.err.println("flyscanvisu propFile=/path/to/flyscan/visualization/properties/file");
        }
    }

}
