package fr.soleil.flyscan.visualization.event;

import java.util.EventObject;

import fr.soleil.flyscan.visualization.view.image.IFlyScanMotorPositionManager;

public class MotorPositionEvent extends EventObject {

    private static final long serialVersionUID = 3478353330317005735L;

    private IFlyScanMotorPositionManager source;

    public MotorPositionEvent(IFlyScanMotorPositionManager source) {
        super(source);
        this.source = source;
    }

    @Override
    public IFlyScanMotorPositionManager getSource() {
        return source;
    }
}
