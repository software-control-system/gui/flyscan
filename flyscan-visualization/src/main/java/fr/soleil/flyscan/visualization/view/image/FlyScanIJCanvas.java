package fr.soleil.flyscan.visualization.view.image;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ijviewer.IJCanvas;
import fr.soleil.comete.swing.image.ijviewer.IJRoiManager;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.flyscan.visualization.target.IStableSpeedTarget;
import ij.gui.Roi;

public class FlyScanIJCanvas extends IJCanvas implements IStableSpeedTarget {

    private static final long serialVersionUID = -6967193052126517339L;

    public static final Color DEFAULT_STABILIZED_SPEED_COLOR = Color.WHITE;

    protected volatile double stableSpeedStart, stableSpeedEnd;
    protected volatile boolean stableSpeedOnY;

    protected Color stableSpeedColor;
    protected Stroke stableSpeedStroke;

    public FlyScanIJCanvas(IJRoiManager imgp, ImageViewer imageViewer) {
        super(imgp, imageViewer);
        stableSpeedStart = Double.NaN;
        stableSpeedEnd = Double.NaN;
        stableSpeedStroke = new BasicStroke(3.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f,
                new float[] { 0.0f, 6.0f }, 3.0f);
        stableSpeedColor = DEFAULT_STABILIZED_SPEED_COLOR;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public double getStableSpeedStart() {
        return stableSpeedStart;
    }

    @Override
    public void setStableSpeedStart(double stableSpeedStart) {
        this.stableSpeedStart = stableSpeedStart;
    }

    @Override
    public double getStableSpeedEnd() {
        return stableSpeedEnd;
    }

    @Override
    public void setStableSpeedEnd(double stableSpeedEnd) {
        this.stableSpeedEnd = stableSpeedEnd;
    }

    @Override
    public void setStableSpeedBounds(double start, double end) {
        this.stableSpeedStart = start;
        this.stableSpeedEnd = end;
    }

    public boolean isStableSpeedOnY() {
        return stableSpeedOnY;
    }

    public void setStableSpeedOnY(boolean stableSpeedOnY) {
        this.stableSpeedOnY = stableSpeedOnY;
    }

    public Color getStableSpeedColor() {
        return stableSpeedColor;
    }

    public void setStableSpeedColor(Color stableSpeedColor) {
        if (stableSpeedColor != null) {
            this.stableSpeedColor = stableSpeedColor;
        }
    }

    public Stroke getStableSpeedStroke() {
        return stableSpeedStroke;
    }

    public void setStableSpeedStroke(Stroke stableSpeedStroke) {
        if (stableSpeedStroke != null) {
            this.stableSpeedStroke = stableSpeedStroke;
        }
    }

    protected void drawStableSpeed(Graphics2D g2d) {
        if ((!Double.isNaN(stableSpeedStart)) && (!Double.isInfinite(stableSpeedStart))
                && (!Double.isNaN(stableSpeedEnd)) & (!Double.isInfinite(stableSpeedEnd))) {
            int start = screenXD(stableSpeedStart), end = screenXD(stableSpeedEnd);
            if (start < end) {
                g2d.setStroke(stableSpeedStroke);
                g2d.setColor(stableSpeedColor);
                if (isStableSpeedOnY()) {
                    if (start >= 0 && start <= getHeight()) {
                        g2d.drawLine(0, start, (int) (srcRect.width * magnification), start);
                    }
                    if (end >= 0 && end <= getHeight()) {
                        g2d.drawLine(0, end, (int) (srcRect.width * magnification), end);
                    }
                } else {
                    if (start >= 0 && start <= getWidth()) {
                        g2d.drawLine(start, 0, start, (int) (srcRect.height * magnification));
                    }
                    if (end >= 0 && end <= getWidth()) {
                        g2d.drawLine(end, 0, end, (int) (srcRect.height * magnification));
                    }
                }
            }
        }
    }

    @Override
    protected void drawOverRois(Graphics2D g2d) {
        super.drawOverRois(g2d);
        drawStableSpeed(g2d);
    }

    /**
     * Returns the {@link Roi} at given point in screen coordinates.
     * 
     * @param sx The point screen x coordinate.
     * @param sy The point screen y coordinate.
     * @return A {@link Roi}. Can be <code>null</code>.
     */
    public Roi getRoiAtPoint(int sx, int sy) {
        return getRoiAtPoint(sx, sy, offScreenX(sx), offScreenY(sy));
    }

}
