package fr.soleil.flyscan.visualization.view.image;

import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.flyscan.visualization.target.IStableSpeedTarget;

public interface IFlyScanMotorPositionManager extends IImageViewer, IStableSpeedTarget {

    public boolean isStableSpeedOnY();

    public void setStableSpeedOnY(boolean stableSpeedOnY);

}
