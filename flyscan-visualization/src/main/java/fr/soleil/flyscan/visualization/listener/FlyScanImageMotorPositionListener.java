package fr.soleil.flyscan.visualization.listener;

import java.util.EventListener;

import fr.soleil.flyscan.visualization.event.MotorPositionEvent;

public interface FlyScanImageMotorPositionListener extends EventListener {

    public void motorPositionChanged(MotorPositionEvent event);

}
