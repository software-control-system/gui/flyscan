package fr.soleil.flyscan.visualization.view.image;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.ImageTransformationPanel;
import fr.soleil.comete.swing.image.util.ImageTransformationInfo;

public class FlyScanImageTransformationPanel extends ImageTransformationPanel {

    private static final long serialVersionUID = 1441291309118207554L;

    protected final JPanel transformationInfoPanel;
    protected final JLabel transformationLabel2;

    public FlyScanImageTransformationPanel() {
        super();
        JLabel transformationTitle = new JLabel(TRANSFORMATION);
        transformationLabel2 = new JLabel();
        transformationLabel2.setFont(transformationLabel.getFont());
        transformationLabel2.setText(transformationLabel.getText());
        transformationLabel2.setToolTipText(transformationLabel.getToolTipText());
        transformationInfoPanel = new JPanel(new BorderLayout(5, 0));
        transformationInfoPanel.add(transformationTitle, BorderLayout.WEST);
        transformationInfoPanel.add(transformationLabel2, BorderLayout.CENTER);
    }

    @Override
    protected boolean applyTransformations() {
        boolean switchDim = super.applyTransformations();
        if (transformationLabel2 != null) {
            transformationLabel2.setText(transformationLabel.getText());
            transformationLabel2.setToolTipText(transformationLabel.getToolTipText());
        }
        return switchDim;
    }

    @Override
    protected boolean applyTransformations(ImageViewer imageViewer, ImageTransformationInfo transformationInfo) {
        boolean switchDim = super.applyTransformations(imageViewer, transformationInfo);
        if (imageViewer instanceof IFlyScanMotorPositionManager) {
            ((IFlyScanMotorPositionManager) imageViewer).setStableSpeedOnY(switchDim);
        }
        return switchDim;
    }

    public JPanel getTransformationInfoPanel() {
        return transformationInfoPanel;
    }

}
