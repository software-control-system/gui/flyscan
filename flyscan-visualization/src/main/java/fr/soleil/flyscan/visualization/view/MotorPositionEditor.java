package fr.soleil.flyscan.visualization.view;

import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.flyscan.visualization.util.FSVUtilities;

public class MotorPositionEditor extends TextFieldButton {

    private static final long serialVersionUID = -2120965759387062462L;

    // Motor textfields columns
    protected static final int MOTOR_FIELDS_COLUMNS = 20;

    public MotorPositionEditor() {
        super(true);
        textField.setFont(FSVUtilities.DEFAULT_FONT);
        sendButton.setFont(FSVUtilities.DEFAULT_FONT_BOLD);
        setTextFieldColumns(MOTOR_FIELDS_COLUMNS);
        setAlwaysUpdateTextField(true);
    }

}
