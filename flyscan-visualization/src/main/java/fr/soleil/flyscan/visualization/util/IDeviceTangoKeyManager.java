package fr.soleil.flyscan.visualization.util;

import java.util.List;

import fr.soleil.comete.tango.data.service.AttributePropertyType;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.service.IKey;

/**
 * An interface for something that knows a device and can manage {@link TangoKey}s around it.
 * 
 * @author GIRARDOT
 *
 */
public interface IDeviceTangoKeyManager {

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateAttributeKey(String attributeShortName);

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device, and for which you
     * expect the data source to be read only.
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateReadOnlyAttributeKey(String attributeShortName);

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateWriteAttributeKey(String attributeShortName);

    /**
     * Generates the {@link TangoKey} that corresponds to a list of attributes
     * 
     * @param completeNames The attributes complete names (example: "tango/tangotest/1/short_scalar_ro")
     * @return The expected {@link TangoKey}
     */
    public List<IKey> generateMultiAttributeKey(String... completeNames);

    /**
     * Generates the {@link TangoKey} that corresponds to attributes couples.
     * This means that the generated {@link TangoKey} represents a list of {attribute VS attribute}. This is typically
     * used in a chart, with one attribute in X and one in Y coordinates for each curve.
     * 
     * @param xCompleteNames The complete names of the attributes in X
     * @param yCompleteNames The complete names of the attributes in Y
     * @return The expected {@link TangoKey}
     */
    public List<IKey> generateDualAttributeKey(String[] xCompleteNames, String[] yCompleteNames);

    /**
     * Generates the {@link TangoKey} that corresponds to an attribute property in the associated device
     * 
     * @param attributeShortName the attribute short name (without device name)
     * @param propertyType the property
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateAttributePropertyKey(String attributeShortName, AttributePropertyType propertyType);

    /**
     * Generates the {@link TangoKey} that corresponds to a command in the associated device
     * 
     * @param commandShortName the command short name (without device name)
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateCommandKey(String commandShortName);

    /**
     * Generates the {@link TangoKey} that corresponds to a property in the associated device
     * 
     * @param propertyName the name of the device property
     * @return The expected {@link TangoKey}
     */
    public TangoKey generatePropertyKey(String propertyName);

    /**
     * Generates the {@link TangoKey} that corresponds to the associated device
     * 
     * @return The expected {@link TangoKey}
     */
    public TangoKey generateDeviceKey();

}
