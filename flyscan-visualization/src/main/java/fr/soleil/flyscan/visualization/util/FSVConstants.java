package fr.soleil.flyscan.visualization.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public interface FSVConstants {

    // Preferences/properties file keys
    public static final String FLYSCAN_VIEWER_DEVICE_KEY = "flyscan.viewer.device";
    public static final String FLYSCAN_VIEWER_DEVICE_COMMENT = "flyscanviewer device complete name. Example: flyscan/viewer/1";
    public static final String CARTOGRAPHY_IMAGES_PROP_KEY = "flyscan.cartography.images";
    public static final String CARTOGRAPHY_IMAGES_PROP_COMMENT = "simple names (i.e without device name) of the cartography image attributes, separated by comas. Example: roi1,roi2,roi3";
    public static final String CARTOGRAPHY_X_PROP_KEY = "flyscan.cartography.x";
    public static final String CARTOGRAPHY_X_PROP_COMMENT = "simple name (i.e without device name) of the attribute that gives the first axis (generally X) scale of cartorgraphy images. Example: map_m1";
    public static final String CARTOGRAPHY_Y_PROP_KEY = "flyscan.cartography.y";
    public static final String CARTOGRAPHY_Y_PROP_COMMENT = "simple name (i.e without device name) of the attribute that gives the second axis (generally Y) scale of cartorgraphy images. Example: map_m2";
    public static final String MOTOR_X_POSITION_PROP_KEY = "flyscan.motor.x.position";
    public static final String MOTOR_X_POSITION_PROP_COMMENT = "complete name (i.e. with device name) of the first dimension motor position attribute (the one that moves the most, generally X). Example: flyscan/actuator/m1/position";
    public static final String MOTOR_Y_POSITION_PROP_KEY = "flyscan.motor.y.position";
    public static final String MOTOR_Y_POSITION_PROP_COMMENT = "complete name (i.e. with device name) of the second dimension motor position attribute (the one that moves the less, generally Y). Example: flyscan/actuator/m2/position";
    public static final String OK_KEY = "flyscan.ok";
    public static final String CANCEL_KEY = "flyscan.cancel";
    public static final String ERROR_KEY = "flyscan.error";
    public static final String PROPERTIES_FILE_WRITE_ERROR_KEY = "flyscan.properties.file.write.error";

    // Flyscanviewer Device property keys
    public static final String CONFIG_PROPERTY = "__FlyscanVisuParameters";
    public static final String CARTOGRAPHY_IMAGES_CONFIG_PROP_KEY = "map_image_attributes";
    public static final String CARTOGRAPHY_X_CONFIG_PROP_KEY = "first_dim_axis_map_values_attribute";
    public static final String CARTOGRAPHY_Y_CONFIG_PROP_KEY = "second_dim_axis_map_values_attribute";
    public static final String MOTOR_X_POSITION_CONFIG_PROP_KEY = "first_dim_axis_attribute";
    public static final String MOTOR_Y_POSITION_CONFIG_PROP_KEY = "second_dim_axis_attribute";

    // UI messages
    public static final String FLYSCAN_VIEWER_DEVICE_TITLE = "flyscan.viewer.device.title";
    public static final String MOTOR_X_POSITION_TITLE = "flyscan.motor.position.x.attribute.title";
    public static final String MOTOR_Y_POSITION_TITLE = "flyscan.motor.position.y.attribute.title";
    public static final String CARTOGRAPHY_IMAGES_TITLE = "flyscan.cartography.images.attribute.title";
    public static final String CARTOGRAPHY_X_TITLE = "flyscan.cartography.x.attribute.title";
    public static final String CARTOGRAPHY_Y_TITLE = "flyscan.cartography.y.attribute.title";

    // Common constants
    public static final String EQUALS = "=";
    public static final String NULL = "null";

    // States
    public static final String RUNNING = "RUNNING";

    public static final Collection<String> ALL_PROPERTIES = Collections.unmodifiableCollection(
            Arrays.asList(FLYSCAN_VIEWER_DEVICE_KEY, CARTOGRAPHY_IMAGES_PROP_KEY, CARTOGRAPHY_X_PROP_KEY,
                    CARTOGRAPHY_Y_PROP_KEY, MOTOR_X_POSITION_PROP_KEY, MOTOR_Y_POSITION_PROP_KEY));

    public static final Map<String, String> PROPERTIES_DESCRIPTIONS = buildPropertiesDescriptionMap();
    public static final Map<String, String> PROPERTIES_UI_TITLE_KEYS = buildPropertiesUITitleKeysMap();

    public static Map<String, String> buildPropertiesDescriptionMap() {
        Map<String, String> map = new HashMap<>();
        map.put(FLYSCAN_VIEWER_DEVICE_KEY, FLYSCAN_VIEWER_DEVICE_COMMENT);
        map.put(CARTOGRAPHY_IMAGES_PROP_KEY, CARTOGRAPHY_IMAGES_PROP_COMMENT);
        map.put(CARTOGRAPHY_X_PROP_KEY, CARTOGRAPHY_X_PROP_COMMENT);
        map.put(CARTOGRAPHY_Y_PROP_KEY, CARTOGRAPHY_Y_PROP_COMMENT);
        map.put(MOTOR_X_POSITION_PROP_KEY, MOTOR_X_POSITION_PROP_COMMENT);
        map.put(MOTOR_Y_POSITION_PROP_KEY, MOTOR_Y_POSITION_PROP_COMMENT);
        return Collections.unmodifiableMap(map);
    }

    public static Map<String, String> buildPropertiesUITitleKeysMap() {
        Map<String, String> map = new HashMap<>();
        map.put(FLYSCAN_VIEWER_DEVICE_KEY, FLYSCAN_VIEWER_DEVICE_TITLE);
        map.put(CARTOGRAPHY_IMAGES_PROP_KEY, CARTOGRAPHY_IMAGES_TITLE);
        map.put(CARTOGRAPHY_X_PROP_KEY, CARTOGRAPHY_X_TITLE);
        map.put(CARTOGRAPHY_Y_PROP_KEY, CARTOGRAPHY_Y_TITLE);
        map.put(MOTOR_X_POSITION_PROP_KEY, MOTOR_X_POSITION_TITLE);
        map.put(MOTOR_Y_POSITION_PROP_KEY, MOTOR_Y_POSITION_TITLE);
        return Collections.unmodifiableMap(map);
    }

}
