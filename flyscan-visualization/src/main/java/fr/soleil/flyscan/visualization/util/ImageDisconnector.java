package fr.soleil.flyscan.visualization.util;

import java.lang.ref.WeakReference;

import javax.swing.SwingWorker;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageTransformationPanel;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageViewer;
import fr.soleil.lib.project.ObjectUtils;

public class ImageDisconnector extends SwingWorker<Void, Void> {

    protected final FlyScanImageViewer[] viewers;
    protected final IImageViewer[] targets;
    protected final WeakReference<NumberMatrixBox> numberMatrixBoxRef;
    protected final WeakReference<FlyScanImageTransformationPanel> transformationPanelRef;
    protected final int startIndex;

    public ImageDisconnector(FlyScanImageViewer[] viewers, IImageViewer[] targets, int startIndex,
            NumberMatrixBox numberMatrixBox, FlyScanImageTransformationPanel transformationPanel) {
        super();
        this.viewers = viewers;
        this.targets = targets;
        this.startIndex = startIndex;
        this.numberMatrixBoxRef = numberMatrixBox == null ? null : new WeakReference<>(numberMatrixBox);
        this.transformationPanelRef = transformationPanel == null ? null : new WeakReference<>(transformationPanel);
    }

    @Override
    protected Void doInBackground() {
        NumberMatrixBox numberMatrixBox = ObjectUtils.recoverObject(numberMatrixBoxRef);
        FlyScanImageTransformationPanel transformationPanel = ObjectUtils.recoverObject(transformationPanelRef);
        if ((viewers != null) && (targets != null) && (numberMatrixBox != null) && (transformationPanel != null)) {
            for (int i = startIndex; i < viewers.length; i++) {
                transformationPanel.unregisterImageViewer(viewers[i]);
                numberMatrixBox.disconnectWidgetFromAll(targets[i]);
                viewers[i] = null;
                targets[i] = null;
            }
        }
        return null;
    }

}
