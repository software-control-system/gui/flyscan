package fr.soleil.flyscan.visualization.target;

import java.lang.ref.WeakReference;

import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.service.thread.NoDrawingThreadManager;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.flyscan.visualization.util.FSVUtilities;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageTransformationPanel;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link INumberMatrixTarget} dedicated in {@link ImageViewer}'s scales.
 * 
 * @author GIRARDOT
 */
public abstract class CartographyScaleTarget extends ABeanContainerTarget implements INumberMatrixTarget, IComponent {

    protected final WeakReference<ArrayPositionConvertor> convertorReference;
    protected final WeakReference<FlyScanImageTransformationPanel> transformationPanelRef;
    protected final String connectionErrorKey;
    protected volatile boolean enabled;

    public CartographyScaleTarget(ArrayPositionConvertor convertor, FlyScanImageTransformationPanel transformationPanel,
            FlyScanVisualizationTangoBox box, String connectionErrorKey) {
        super(box);
        this.convertorReference = convertor == null ? null : new WeakReference<>(convertor);
        this.transformationPanelRef = transformationPanel == null ? null : new WeakReference<>(transformationPanel);
        this.connectionErrorKey = connectionErrorKey;
        this.enabled = true;
    }

    protected abstract String getAxisSource();

    protected abstract void updateAxisTitle(String title, FlyScanImageTransformationPanel transformationPanel);

    protected abstract Object extractArrayFromMatrix(Object[] matrix);

    protected abstract Object extractArrayFromFlatMatrix(Object flatMatrix, int width, int height);

    protected void applyArrayValue(ArrayPositionConvertor convertor, Object array) {
        convertor.setNumberArray(array);
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return ObjectUtils.isNumberClass(concernedDataClass);
    }

    @Override
    public Object[] getNumberMatrix() {
        // not managed
        return null;
    }

    @Override
    public Object getFlatNumberMatrix() {
        // not managed
        return null;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        // not managed
        return 0;
    }

    @Override
    public void setNumberMatrix(Object[] matrix) {
        ArrayPositionConvertor convertor = ObjectUtils.recoverObject(convertorReference);
        if (convertor != null) {
            applyArrayValue(convertor, extractArrayFromMatrix(matrix));
        }
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        ArrayPositionConvertor convertor = ObjectUtils.recoverObject(convertorReference);
        if (convertor != null) {
            applyArrayValue(convertor, extractArrayFromFlatMatrix(value, width, height));
        }
    }

    @Override
    public void notifyForError(String message, Throwable error) {
        FlyScanImageTransformationPanel transformationPanel = ObjectUtils.recoverObject(transformationPanelRef);
        if (transformationPanel != null) {
            updateAxisTitle(message, transformationPanel);
        }
    }

    @Override
    public boolean hasFocus() {
        // not managed
        return false;
    }

    @Override
    public void setOpaque(boolean opaque) {
        // not managed
    }

    @Override
    public boolean isOpaque() {
        // not managed
        return false;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        // not managed
    }

    @Override
    public CometeColor getCometeBackground() {
        // not managed
        return null;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        FlyScanImageTransformationPanel transformationPanel = ObjectUtils.recoverObject(transformationPanelRef);
        FlyScanVisualizationTangoBox box = ObjectUtils.recoverObject(beanRef);
        if ((box != null) && (transformationPanel != null)) {
            if (enabled) {
                updateAxisTitle(null, transformationPanel);
            } else if (!box.isStopped()) {
                updateAxisTitle(String.format(FSVUtilities.getMessage(connectionErrorKey), getAxisSource()),
                        transformationPanel);
            }
            box.repaint();
        }
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        // not managed
    }

    @Override
    public CometeColor getCometeForeground() {
        // not managed
        return null;
    }

    @Override
    public void setCometeFont(CometeFont font) {
        // not managed
    }

    @Override
    public CometeFont getCometeFont() {
        // not managed
        return null;
    }

    @Override
    public void setToolTipText(String text) {
        // not managed
    }

    @Override
    public String getToolTipText() {
        // not managed
        return null;
    }

    @Override
    public void setVisible(boolean visible) {
        // not managed
    }

    @Override
    public boolean isVisible() {
        // not managed
        return false;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // not managed
    }

    @Override
    public int getHorizontalAlignment() {
        // not managed
        return 0;
    }

    @Override
    public void setSize(int width, int height) {
        // not managed
    }

    @Override
    public void setPreferredSize(int width, int height) {
        // not managed
    }

    @Override
    public int getWidth() {
        // not managed
        return 0;
    }

    @Override
    public int getHeight() {
        // not managed
        return 0;
    }

    @Override
    public void setLocation(int x, int y) {
        // not managed
    }

    @Override
    public int getX() {
        // not managed
        return 0;
    }

    @Override
    public int getY() {
        // not managed
        return 0;
    }

    @Override
    public boolean isEditingData() {
        // not managed
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        // not managed
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        // not managed
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        // not managed
    }

    @Override
    public void removeAllMouseListeners() {
        // not managed
    }

    @Override
    public IDrawingThreadManager getDrawingThreadManager() {
        // not managed
        return NoDrawingThreadManager.INSTANCE;
    }

    @Override
    public boolean shouldReceiveDataInDrawingThread() {
        return false;
    }
}
