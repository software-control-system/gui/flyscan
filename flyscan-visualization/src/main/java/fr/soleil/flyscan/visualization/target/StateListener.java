package fr.soleil.flyscan.visualization.target;

import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link ITextTarget}, expected to be connected to flyscanviewer state, and that will force reconnection to config
 * property when state changes to RUNNING, see SCAN-875.
 * 
 * @author GIRARDOT
 */
public class StateListener extends ABeanContainerTarget implements ITextTarget {

    protected volatile String state;

    public StateListener(FlyScanVisualizationTangoBox bean) {
        super(bean);
        this.state = null;
    }

    @Override
    public String getText() {
        return state;
    }

    @Override
    public void setText(String text) {
        if (!ObjectUtils.sameObject(text, state)) {
            state = text;
            if ((text != null) && text.equalsIgnoreCase(RUNNING)) {
                FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
                if (bean != null) {
                    bean.disconnectConfigProperty();
                    bean.connectConfigProperty();
                }
            }
        }
    }

    @Override
    protected void finalize() {
        state = null;
        super.finalize();
    }
}
