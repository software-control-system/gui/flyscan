package fr.soleil.flyscan.visualization.target;

import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.lib.project.ObjectUtils;

/**
 * The {@link ITextTarget} that listens to x motor position format.
 * 
 * @author GIRARDOT
 */
public class XMotorPositionFormatTarget extends ABeanContainerTarget implements ITextTarget {

    public XMotorPositionFormatTarget(FlyScanVisualizationTangoBox bean) {
        super(bean);
    }

    @Override
    public String getText() {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        return bean == null ? null : bean.getXMotorPositionFormat();
    }

    @Override
    public void setText(String text) {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        if (bean != null) {
            bean.setXMotorPositionFormat(text);
        }
    }

}
