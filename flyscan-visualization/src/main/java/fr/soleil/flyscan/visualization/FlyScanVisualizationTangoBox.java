package fr.soleil.flyscan.visualization;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.listener.PresetDeviceControlPanelLauncher;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.comete.swing.image.ImageJManager;
import fr.soleil.comete.tango.data.service.AttributePropertyType;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.lib.model.parsing.util.ParsingUtil;
import fr.soleil.flyscan.visualization.event.MotorPositionEvent;
import fr.soleil.flyscan.visualization.listener.FlyScanImageMotorPositionListener;
import fr.soleil.flyscan.visualization.target.CartographyScaleTarget;
import fr.soleil.flyscan.visualization.target.CartographyXTarget;
import fr.soleil.flyscan.visualization.target.CartographyYTarget;
import fr.soleil.flyscan.visualization.target.ConfigPropertyTarget;
import fr.soleil.flyscan.visualization.target.StateListener;
import fr.soleil.flyscan.visualization.target.XMotorPositionFormatTarget;
import fr.soleil.flyscan.visualization.target.YMotorPositionFormatTarget;
import fr.soleil.flyscan.visualization.util.FSVConstants;
import fr.soleil.flyscan.visualization.util.FSVUtilities;
import fr.soleil.flyscan.visualization.util.IDeviceTangoKeyManager;
import fr.soleil.flyscan.visualization.util.ImageConnector;
import fr.soleil.flyscan.visualization.util.ImageDisconnector;
import fr.soleil.flyscan.visualization.util.SavePropertiesWorker;
import fr.soleil.flyscan.visualization.view.AdaptedSizeLabel;
import fr.soleil.flyscan.visualization.view.FSVPropertiesEditor;
import fr.soleil.flyscan.visualization.view.MotorPositionEditor;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageTransformationPanel;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageViewer;
import fr.soleil.flyscan.visualization.view.image.IFlyScanMotorPositionManager;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.SizeUtils;
import fr.soleil.lib.project.awt.layout.LayoutUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;

/**
 * {@link AbstractTangoBox} used by flyscanvisualization.
 * 
 * @author GIRARDOT
 */
public class FlyScanVisualizationTangoBox extends AbstractTangoBox
        implements FSVConstants, IDeviceTangoKeyManager, FlyScanImageMotorPositionListener, PropertyChangeListener {

    private static final long serialVersionUID = -118072461408353784L;

    // UI messages keys
    protected static final String FLYSCAN_DEVICE_ERROR_KEY = "flyscan.viewer.device.error";
    protected static final String CARTOGRAPHY_IMAGE_KEY = CARTOGRAPHY_IMAGES_PROP_KEY;
    protected static final String MOTOR_POSITIONS_TITLE_KEY = "flyscan.motor.positions";
    protected static final String X_POSITION_TITLE_KEY = "flyscan.motor.position.x";
    protected static final String Y_POSITION_TITLE_KEY = "flyscan.motor.position.y";
    protected static final String WARNING_KEY = "flyscan.warning";
    protected static final String MOTOR_DEVICE_ERROR_KEY = "flyscan.motor.device.error";
    protected static final String PROPERTIES_DIRECTORY_ERROR_KEY = "flyscan.properties.directory.error";
    protected static final String PROPERTIES_FILE_INIT_ERROR_KEY = "flyscan.properties.file.init.error";
    protected static final String PROPERTIES_FILE_INCOMPLETE_WARNING_KEY = "flyscan.properties.file.incomplete.warning";
    protected static final String PROPERTIES_FILE_CREATED_WARNING = "flyscan.properties.file.created.warning";
    protected static final String PROPERTIES_FILE_READ_ERROR_KEY = "flyscan.properties.file.read.error";
    protected static final String FLYSCAN_PROPERTIES_EDIT_KEY = "flyscan.properties.edit";
    protected static final String CONNECTION_ERROR_KEY = "flyscan.connection.error";
    protected static final String BULLET_START = "<li><b>";
    protected static final String DESCRIPTION_SEPARATOR = "</b> <i>";
    protected static final String BULLET_END = "</i></li>";

    // property
    private static final String ENABLED = "enabled";

    // File encoding norm
    protected static final String FILE_ENCODING = "8859_1";

    protected static final DateTimeFormatter FILE_DATE_FORMATTER = DateTimeFormatter
            .ofPattern("'Last modification date:' EEEE yyyy-MM-dd HH:mm:ss");

    protected final NumberMatrixBox numberMatrixBox;

    protected String propertiesFile;
    protected String xMotor, yMotor, xPosition, yPosition, cartographyImage, cartographyX, cartographyY;
    protected String xMotorPositionFormat, yMotorPositionFormat;

    protected final String applicationId;
    protected final JPanel imageDisplayPanel;
    protected final JPanel imagePanel;
    protected final FlyScanImageTransformationPanel transformationPanel;
    protected FlyScanImageViewer[] imageViewers;
    protected IImageViewer[] imageTargets;
    protected final ITextTarget stateTarget, configTarget;
    protected final ArrayPositionConvertor xScale, yScale;
    protected final CartographyScaleTarget xTarget, yTarget;
    protected final JLabel xPositionTitle, yPositionTitle;
    protected final MotorPositionEditor xPositionEditor, yPositionEditor;
    protected final Label xLabel, yLabel, xMotorState, yMotorState;
    protected final ITextTarget xFormatTarget, yFormatTarget;
    protected final JPanel deviceNamePanel, motorPositionsPanel;
    protected final JScrollPane motorPositionsScrollPane, fsvStatusScrollPane;
    protected final JSplitPane mainSplitPane, bottomSplitPane;
    protected final PresetDeviceControlPanelLauncher modelListener, statusListener;
    protected final PresetDeviceControlPanelLauncher xMotorListener, xMotorStateListener;
    protected final PresetDeviceControlPanelLauncher yMotorListener, yMotorStateListener;
    protected FSVPropertiesEditor propertiesEditor;
    protected final Action openPropertiesEditorAction;

    protected boolean propertiesInitialized;
    protected final Object propertiesEditorLock, propertiesFileLock;
    protected volatile double stableSpeedStart, stableSpeedEnd;

    protected volatile ImageConnector connector;
    protected boolean firstConnection;

    public FlyScanVisualizationTangoBox() {
        super();
        applicationId = ImageJManager.generateApplicationId(FSVUtilities.APPLICATION_NAME);
        modelListener = new PresetDeviceControlPanelLauncher(FSVUtilities.SMALL_FONT_BOLD);
        modelListener.setTextToAvoid(NO_DEVICE_STRING);
        deviceLabel.setFont(FSVUtilities.SMALL_FONT_BOLD);
        deviceLabel.addMouseListener(modelListener);
        deviceLabel.addMouseMotionListener(modelListener);
        statusListener = new PresetDeviceControlPanelLauncher(FSVUtilities.SMALL_FONT);
        statusLabel.setFont(FSVUtilities.SMALL_FONT);
        statusLabel.addMouseListener(statusListener);
        statusLabel.addMouseMotionListener(statusListener);
        firstConnection = true;
        propertiesInitialized = false;
        propertiesEditorLock = new Object();
        propertiesFileLock = new Object();
        numberMatrixBox = new NumberMatrixBox();
        xScale = new ArrayPositionConvertor();
        yScale = new ArrayPositionConvertor();
        xFormatTarget = new XMotorPositionFormatTarget(this);
        yFormatTarget = new YMotorPositionFormatTarget(this);

        stateTarget = new StateListener(this);
        configTarget = new ConfigPropertyTarget(this);

        xMotorListener = new PresetDeviceControlPanelLauncher(FSVUtilities.DEFAULT_FONT_BOLD);
        xPositionTitle = new JLabel(FSVUtilities.getMessage(X_POSITION_TITLE_KEY), JLabel.RIGHT);
        xPositionTitle.setFont(FSVUtilities.DEFAULT_FONT_BOLD);
        xPositionTitle.addMouseListener(xMotorListener);
        xPositionTitle.addMouseMotionListener(xMotorListener);

        yMotorListener = new PresetDeviceControlPanelLauncher(FSVUtilities.DEFAULT_FONT_BOLD);
        yPositionTitle = new JLabel(FSVUtilities.getMessage(Y_POSITION_TITLE_KEY), JLabel.RIGHT);
        yPositionTitle.setFont(FSVUtilities.DEFAULT_FONT_BOLD);
        yPositionTitle.addMouseListener(yMotorListener);
        yPositionTitle.addMouseMotionListener(yMotorListener);

        xPositionEditor = new MotorPositionEditor();
        yPositionEditor = new MotorPositionEditor();

        xLabel = new AdaptedSizeLabel();
        yLabel = new AdaptedSizeLabel();

        xMotorState = new AdaptedSizeLabel();
        xMotorStateListener = new PresetDeviceControlPanelLauncher(xMotorState.getFont());
        xMotorState.addMouseListener(xMotorStateListener);
        xMotorState.addMouseMotionListener(xMotorStateListener);

        yMotorState = new AdaptedSizeLabel();
        yMotorStateListener = new PresetDeviceControlPanelLauncher(yMotorState.getFont());
        yMotorState.addMouseListener(yMotorStateListener);
        yMotorState.addMouseMotionListener(yMotorStateListener);

        motorPositionsPanel = new JPanel(new GridBagLayout());
        int y = 0;
        y = addMotorComponents(y, xPositionTitle, xLabel, xPositionEditor, xMotorState);
        y = addMotorComponents(y, yPositionTitle, yLabel, yPositionEditor, yMotorState);
        motorPositionsScrollPane = new JScrollPane(motorPositionsPanel);
        motorPositionsScrollPane.setBorder(
                new TitledBorder(new EmptyBorder(0, 0, 0, 0), FSVUtilities.getMessage(MOTOR_POSITIONS_TITLE_KEY),
                        TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, FSVUtilities.SMALL_FONT_BOLD));

        stableSpeedStart = stableSpeedEnd = Double.NaN;
        fsvStatusScrollPane = new JScrollPane(getStatusPanel());
        getStatusPanel().remove(deviceLabel);
        deviceNamePanel = new JPanel(new BorderLayout(5, 5));
        deviceNamePanel.setBorder(new EmptyBorder(0, 5, 0, 5));
        deviceNamePanel.add(deviceLabel, BorderLayout.WEST);
        deviceNamePanel.add(Box.createGlue(), BorderLayout.CENTER);
        fsvStatusScrollPane.setColumnHeaderView(deviceNamePanel);

        imagePanel = new JPanel(new BorderLayout());
        imagePanel.setBorder(
                new TitledBorder(new LineBorder(Color.BLACK, 2), FSVUtilities.getMessage(CARTOGRAPHY_IMAGE_KEY)));
        transformationPanel = new FlyScanImageTransformationPanel();
        xTarget = new CartographyXTarget(xScale, transformationPanel, this, CONNECTION_ERROR_KEY);
        yTarget = new CartographyYTarget(yScale, transformationPanel, this, CONNECTION_ERROR_KEY);

        imagePanel.add(transformationPanel.getTransformationInfoPanel(), BorderLayout.NORTH);
        imageDisplayPanel = new JPanel();
        imagePanel.add(imageDisplayPanel, BorderLayout.CENTER);
        imageViewers = new FlyScanImageViewer[0];
        imageTargets = new IImageViewer[0];
        bottomSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        bottomSplitPane.setOneTouchExpandable(true);
        bottomSplitPane.setDividerSize(8);
        bottomSplitPane.setLeftComponent(fsvStatusScrollPane);
        bottomSplitPane.setRightComponent(motorPositionsScrollPane);
        bottomSplitPane.setDividerLocation(0.4);
        bottomSplitPane.setResizeWeight(0);

        mainSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        mainSplitPane.setOneTouchExpandable(true);
        mainSplitPane.setDividerSize(8);
        mainSplitPane.setTopComponent(imagePanel);
        mainSplitPane.setBottomComponent(bottomSplitPane);
        mainSplitPane.setDividerLocation(0.8);
        mainSplitPane.setResizeWeight(1);
        setLayout(new BorderLayout());
        add(mainSplitPane, BorderLayout.CENTER);

        openPropertiesEditorAction = generateOpenPropertiesEditorAction();
    }

    public boolean isStopped() {
        return stopped;
    }

    public String getCartographyX() {
        return cartographyX;
    }

    public String getCartographyY() {
        return cartographyY;
    }

    public String getCartographyImage() {
        return cartographyImage;
    }

    public String getXMotorPositionFormat() {
        return xMotorPositionFormat;
    }

    public void setXMotorPositionFormat(String xMotorPositionFormat) {
        this.xMotorPositionFormat = xMotorPositionFormat;
        if (transformationPanel != null) {
            transformationPanel.setXAxisFormat(true, xMotorPositionFormat);
            repaint();
        }
    }

    public String getYMotorPositionFormat() {
        return yMotorPositionFormat;
    }

    public void setYMotorPositionFormat(String yMotorPositionFormat) {
        this.yMotorPositionFormat = yMotorPositionFormat;
        if (transformationPanel != null) {
            transformationPanel.setYAxisFormat(true, yMotorPositionFormat);
            repaint();
        }
    }

    protected FlyScanImageViewer generateImageViewer() {
        FlyScanImageViewer imageViewer = new FlyScanImageViewer();
        updateImageViewer(imageViewer);
        return imageViewer;
    }

    protected void updateImageViewer(FlyScanImageViewer imageViewer) {
        imageViewer.setApplicationId(applicationId);
        imageViewer.setXAxisFormat(xMotorPositionFormat);
        imageViewer.setYAxisFormat(yMotorPositionFormat);
        imageViewer.setStableSpeedBounds(stableSpeedStart, stableSpeedEnd);
        imageViewer.getXAxis().getAttributes().setTitleAlignment(SwingConstants.LEFT);
        imageViewer.getYAxis().getAttributes().setTitleAlignment(SwingConstants.LEFT);
        imageViewer.setXAxisConvertor(xScale);
        imageViewer.setYAxisConvertor(yScale);
        imageViewer.addMotorPositionListener(this);
        imageViewer.addPropertyChangeListener(this);
    }

    /**
     * Parses the motor position from an attribute full name.
     * 
     * @param attrFullName The attribute full name.
     * @return A {@link String} array, never <code>null</code>, of length 2: <code>[device, attribute]</code>, where
     *         <code>device</code> and/or <code>attribute</code> might be <code>null</code>.
     */
    protected String[] parseMotorPosition(String attrFullName, String... defaultValue) {
        String[] result;
        if (attrFullName == null) {
            result = defaultValue;
        } else {
            result = new String[2];
            int index = attrFullName.lastIndexOf('/');
            if ((index > 0) && (index < attrFullName.length() - 1)) {
                result[0] = attrFullName.substring(0, index);
                result[1] = attrFullName.substring(index + 1);
            }
        }
        return result;
    }

    /**
     * Parses the motor position from a property.
     * 
     * @param properties The {@link Properties} that contains the desired property.
     * @param key The property key.
     * @param defaultValue The default value to return
     * @return A {@link String} array of length 2: <code>[device, attribute]</code>. <code>defaultValue</code> if the
     *         desired property wasn't found.
     */
    protected String[] parseMotorPosition(Properties properties, String key, String... defaultValue) {
        String[] result;
        if ((properties != null) && (key != null)) {
            String attrName = properties.getProperty(key);
            result = parseMotorPosition(attrName, defaultValue);
        } else {
            result = defaultValue;
        }
        return result;
    }

    protected int addMotorComponents(int y, JLabel titleLabel, Label valueLabel, TextFieldButton editor,
            Label stateLabel) {
        int x = 0;
        int topInsets = 5, bottomInsets = (y == 0 ? 0 : 5);
        GridBagConstraints titleLabelConstraints = new GridBagConstraints();
        titleLabelConstraints.fill = GridBagConstraints.BOTH;
        titleLabelConstraints.gridx = x++;
        titleLabelConstraints.gridy = y;
        titleLabelConstraints.weightx = 0;
        titleLabelConstraints.weighty = 0;
        titleLabelConstraints.insets = new Insets(topInsets, 5, bottomInsets, 5);
        motorPositionsPanel.add(titleLabel, titleLabelConstraints);
        GridBagConstraints editorConstraints = new GridBagConstraints();
        editorConstraints.fill = GridBagConstraints.HORIZONTAL;
        editorConstraints.gridx = x++;
        editorConstraints.gridy = y;
        editorConstraints.weightx = 0;
        editorConstraints.weighty = 0;
        editorConstraints.insets = new Insets(topInsets, 0, bottomInsets, 5);
        motorPositionsPanel.add(editor, editorConstraints);
        GridBagConstraints valueLabelConstraints = new GridBagConstraints();
        valueLabelConstraints.fill = GridBagConstraints.BOTH;
        valueLabelConstraints.gridx = x++;
        valueLabelConstraints.gridy = y;
        valueLabelConstraints.weightx = 0;
        valueLabelConstraints.weighty = 0;
        valueLabelConstraints.insets = new Insets(topInsets, 0, bottomInsets, 0);
        motorPositionsPanel.add(valueLabel, valueLabelConstraints);
        GridBagConstraints stateLabelConstraints = new GridBagConstraints();
        stateLabelConstraints.fill = GridBagConstraints.BOTH;
        stateLabelConstraints.gridx = x++;
        stateLabelConstraints.gridy = y++;
        stateLabelConstraints.weightx = 0;
        stateLabelConstraints.weighty = 0;
        stateLabelConstraints.insets = new Insets(topInsets, 30, bottomInsets, 5);
        motorPositionsPanel.add(stateLabel, stateLabelConstraints);
        return y;
    }

    protected Collection<String> getMissingKeys(Properties properties) {
        Collection<String> missing = new ArrayList<>();
        if (properties == null) {
            missing.addAll(ALL_PROPERTIES);
        } else {
            for (String key : ALL_PROPERTIES) {
                if (!properties.containsKey(key)) {
                    missing.add(key);
                }
            }
        }
        return missing;
    }

    protected void setXMotorProperty(String value) {
        String[] motor = parseMotorPosition(value);
        this.xMotor = motor[0];
        this.xPosition = motor[1];
    }

    protected void setYMotorProperty(String value) {
        String[] motor = parseMotorPosition(value);
        this.yMotor = motor[0];
        this.yPosition = motor[1];
    }

    public boolean applyProperties(Properties properties) {
        boolean changed = false;
        if (properties != null) {
            String[] motor = parseMotorPosition(properties, MOTOR_X_POSITION_PROP_KEY, xMotor, xPosition);
            if (!ObjectUtils.sameObject(this.xMotor, motor[0])) {
                this.xMotor = motor[0];
                changed = true;
            }
            if (!ObjectUtils.sameObject(this.xPosition, motor[1])) {
                this.xPosition = motor[1];
                changed = true;
            }
            motor = parseMotorPosition(properties, MOTOR_Y_POSITION_PROP_KEY, yMotor, yPosition);
            if (!ObjectUtils.sameObject(this.yMotor, motor[0])) {
                this.yMotor = motor[0];
                changed = true;
            }
            if (!ObjectUtils.sameObject(this.yPosition, motor[1])) {
                this.yPosition = motor[1];
                changed = true;
            }
            String prop = properties.getProperty(CARTOGRAPHY_IMAGES_PROP_KEY, cartographyImage);
            if (!ObjectUtils.sameObject(this.cartographyImage, prop)) {
                this.cartographyImage = prop;
                changed = true;
            }
            prop = properties.getProperty(CARTOGRAPHY_X_PROP_KEY, cartographyX);
            if (!ObjectUtils.sameObject(this.cartographyX, prop)) {
                this.cartographyX = prop;
                changed = true;
            }
            prop = properties.getProperty(CARTOGRAPHY_Y_PROP_KEY, cartographyY);
            if (!ObjectUtils.sameObject(this.cartographyY, prop)) {
                this.cartographyY = prop;
                changed = true;
            }
        } // end if (properties != null)
        return changed;
    }

    protected void loadProperties(Properties properties, boolean firstStart) {
        synchronized (runLock) {
            Collection<String> missing = getMissingKeys(properties);
            boolean deviceAutoSet = false;
            if (!missing.isEmpty()) {
                for (String prop : missing) {
                    if (FLYSCAN_VIEWER_DEVICE_KEY.equals(prop)) {
                        try {
                            String[] devices = ApiUtil.get_db_obj().get_device_exported_for_class("FlyScanViewer");
                            if ((devices != null) && (devices.length == 1) && (devices[0] != null)) {
                                properties.put(FLYSCAN_VIEWER_DEVICE_KEY, devices[0]);
                                deviceAutoSet = true;
                            }
                        } catch (DevFailed e) {
                            deviceAutoSet = false;
                        }
                    }
                    break;
                }
                if (deviceAutoSet) {
                    missing.remove(FLYSCAN_VIEWER_DEVICE_KEY);
                }
                if ((!missing.isEmpty()) && !firstStart) {
                    StringBuilder keysBuilder = new StringBuilder();
                    for (String key : missing) {
                        keysBuilder.append(BULLET_START)
                                .append(FSVUtilities.getMessage(PROPERTIES_UI_TITLE_KEYS.get(key)))
                                .append(DESCRIPTION_SEPARATOR).append(PROPERTIES_DESCRIPTIONS.get(key))
                                .append(BULLET_END);
                    }
                    JOptionPane.showMessageDialog(this,
                            String.format(FSVUtilities.getMessage(PROPERTIES_FILE_INCOMPLETE_WARNING_KEY), keysBuilder,
                                    openPropertiesEditorAction.getValue(Action.NAME)),
                            FSVUtilities.getMessage(ERROR_KEY), JOptionPane.WARNING_MESSAGE);
                    keysBuilder.delete(0, keysBuilder.length());
                }
            }
            missing.clear();
            applyProperties(properties);
            propertiesInitialized = true;
            String formerModel = getModel();
            String newModel = properties.getProperty(FLYSCAN_VIEWER_DEVICE_KEY, formerModel);
            boolean wasStopped = stopped;
            setModel(newModel);
            getPropertiesEditor().setProperties(properties);
            if (deviceAutoSet) {
                new SavePropertiesWorker(this).execute();
            }
            if (formerModel.equalsIgnoreCase(newModel) && !wasStopped) {
                stop();
                start();
            }
        }
    }

    public String getXMotorPositionProperty() {
        return (xMotor == null) || (xPosition == null) ? null : xMotor + '/' + xPosition;
    }

    public String getYMotorPositionProperty() {
        return (yMotor == null) || (yPosition == null) ? null : yMotor + '/' + yPosition;
    }

    public void saveProperties() throws IOException {
        String filePath = propertiesFile;
        if ((filePath != null) && (!filePath.isEmpty())) {
            synchronized (propertiesFileLock) {
                try (FileOutputStream stream = new FileOutputStream(filePath);
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream, FILE_ENCODING));
                        FileChannel channel = stream.getChannel();
                        // lock file for writing
                        FileLock lock = channel.lock();) {
                    writeComment(writer, FSVUtilities.APPLICATION_NAME_WITH_VERSION);
                    writeComment(writer, FILE_DATE_FORMATTER.format(Instant.ofEpochMilli(System.currentTimeMillis())
                            .atZone(ZoneId.systemDefault()).toLocalDateTime()));
                    writer.newLine();
                    writeProperty(writer, FLYSCAN_VIEWER_DEVICE_COMMENT, FLYSCAN_VIEWER_DEVICE_KEY, getModel());
                    writeProperty(writer, CARTOGRAPHY_IMAGES_PROP_COMMENT, CARTOGRAPHY_IMAGES_PROP_KEY,
                            cartographyImage);
                    writeProperty(writer, CARTOGRAPHY_X_PROP_COMMENT, CARTOGRAPHY_X_PROP_KEY, cartographyX);
                    writeProperty(writer, CARTOGRAPHY_Y_PROP_COMMENT, CARTOGRAPHY_Y_PROP_KEY, cartographyY);
                    writeProperty(writer, MOTOR_X_POSITION_PROP_COMMENT, MOTOR_X_POSITION_PROP_KEY,
                            getXMotorPositionProperty());
                    writeProperty(writer, MOTOR_Y_POSITION_PROP_COMMENT, MOTOR_Y_POSITION_PROP_KEY,
                            getYMotorPositionProperty());
                    writer.flush();
                }
            }
        }
    }

    protected void setPropertiesFile(String propertiesFile, boolean ignoreDuplicationTest) {
        if (ignoreDuplicationTest || !ObjectUtils.sameObject(this.propertiesFile, propertiesFile)) {
            this.propertiesFile = propertiesFile;
            File tmp = new File(propertiesFile);
            boolean fileOk = true;
            boolean firstStart = false;
            if (!tmp.exists()) {
                File parent = tmp.getParentFile();
                if (parent.exists() || parent.mkdirs()) {
                    try {
                        saveProperties();
                        firstStart = true;
                        JOptionPane.showMessageDialog(this,
                                String.format(FSVUtilities.getMessage(PROPERTIES_FILE_CREATED_WARNING),
                                        openPropertiesEditorAction.getValue(Action.NAME)),
                                FSVUtilities.getMessage(WARNING_KEY), JOptionPane.WARNING_MESSAGE);
                    } catch (IOException | IllegalStateException e) {
                        fileOk = false;
                        JOptionPane.showMessageDialog(this,
                                String.format(FSVUtilities.getMessage(PROPERTIES_FILE_INIT_ERROR_KEY),
                                        tmp.getAbsolutePath()),
                                FSVUtilities.getMessage(ERROR_KEY), JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    fileOk = false;
                    JOptionPane.showMessageDialog(this,
                            String.format(FSVUtilities.getMessage(PROPERTIES_DIRECTORY_ERROR_KEY),
                                    parent.getAbsolutePath()),
                            FSVUtilities.getMessage(ERROR_KEY), JOptionPane.ERROR_MESSAGE);
                }
            }
            if (fileOk) {
                Properties properties = new Properties();
                synchronized (propertiesFileLock) {
                    try (FileInputStream stream = new FileInputStream(tmp);
                            FileChannel channel = stream.getChannel();
                            // lock file for reading
                            FileLock lock = channel.lock(0L, Long.MAX_VALUE, true);) {
                        properties.load(stream);
                        this.propertiesFile = propertiesFile;
                    } catch (IOException | IllegalStateException e) {
                        fileOk = false;
                    }
                }
                if (fileOk) {
                    loadProperties(properties, firstStart);
                } else {
                    JOptionPane.showMessageDialog(this,
                            String.format(FSVUtilities.getMessage(PROPERTIES_FILE_READ_ERROR_KEY),
                                    tmp.getAbsolutePath()),
                            FSVUtilities.getMessage(ERROR_KEY), JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    public String getPropertiesFile() {
        return propertiesFile;
    }

    public void setPropertiesFile(String propertiesFile) {
        setPropertiesFile(propertiesFile, false);
    }

    public void reloadProperties() {
        setPropertiesFile(propertiesFile, true);
    }

    protected Action generateOpenPropertiesEditorAction() {
        AbstractAction action = new AbstractAction(
                String.format(FSVUtilities.getMessage(FLYSCAN_PROPERTIES_EDIT_KEY), FSVUtilities.APPLICATION_NAME)) {

            private static final long serialVersionUID = 5842980024091779187L;

            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = getPropertiesEditor();
                if (!dialog.isVisible()) {
                    dialog.setLocationRelativeTo(dialog.getOwner());
                    dialog.setVisible(true);
                }
                dialog.toFront();
            }
        };
        return action;
    }

    public Action getOpenPropertiesEditorAction() {
        return openPropertiesEditorAction;
    }

    public FSVPropertiesEditor getPropertiesEditor() {
        if (propertiesEditor == null) {
            synchronized (propertiesEditorLock) {
                if (propertiesEditor == null) {
                    propertiesEditor = new FSVPropertiesEditor(WindowSwingUtils.getWindowForComponent(this),
                            (String) openPropertiesEditorAction.getValue(Action.NAME));
                    propertiesEditor.pack();
                    propertiesEditor.setSize(Math.max(propertiesEditor.getWidth(), 500), propertiesEditor.getHeight());
                    propertiesEditor.addPropertyChangeListener(this);
                }
            }
        }
        return propertiesEditor;
    }

    public FlyScanImageTransformationPanel getTransformationPanel() {
        return transformationPanel;
    }

    protected boolean isStableDelta(double delta, double previousDelta) {
        // delta is stable if :
        // - it is neither NaN nor infinite,
        // - it is different from 0 (motor did not yet accelerate or it already stopped),
        // - delta evolution is less than 0.1%.
        return (Double.isFinite(delta) && Double.isFinite(previousDelta) && (delta != 0) && (previousDelta != 0)
                && Math.abs(delta - previousDelta) <= Math.abs(delta / 1000));
    }

    protected double getDelta(double[] xPositions, int firstIndex, int secondIndex) {
        return Math.abs(xPositions[firstIndex] - xPositions[secondIndex]);
    }

    public void updateSpeedStability(double... xPositions) {
        double start = Double.NaN, end = Double.NaN;
        if (xPositions != null) {
            // The stable speed bounds follow that logic: start is included, end is excluded.
            // This way, we are sure that, even in zoom mode, pixels between left and right dashes are stable,
            // because 0,0 reference is top-left pixel in drawings.
            boolean startFound = false, endFound = false;
            double lastDeltaStart = Double.POSITIVE_INFINITY, lastDeltaEnd = Double.POSITIVE_INFINITY;
            for (int i = 1; i < xPositions.length && ((!startFound) || (!endFound)); i++) {
                double delta;
                if (!startFound) {
                    // Stable speed start algorithm:
                    // For the start, we search for the first stable pixel (first pixel at stable speed).
                    // This pixel indicates where x motor ended acceleration.
                    // - current delta is between i and i-1
                    // - previous delta is between i-1 and i-2
                    // - if current delta and previous delta are almost the same,
                    // it means stable speed is obtained at i-2
                    delta = getDelta(xPositions, i - 1, i);
                    if (isStableDelta(delta, lastDeltaStart)) {
                        startFound = true;
                        start = i - 2;
                    }
                    lastDeltaStart = delta;
                }
                if (!endFound) {
                    // Stable speed end algorithm:
                    // For the end, we search for the first unstable pixel (first pixel at unstable speed).
                    // This pixel indicates where x motor starts deceleration.
                    // - current delta is between length-(i+1) and length-i
                    // - previous delta is between length-i and length-(i-1)
                    // - if current delta and previous delta are almost the same,
                    // it means last stable speed pixel is at length-(i-1),
                    // which means first unstable speed pixel is at length-(i-2).
                    delta = getDelta(xPositions, xPositions.length - i, xPositions.length - (i + 1));
                    if (isStableDelta(delta, lastDeltaEnd)) {
                        endFound = true;
                        end = xPositions.length - (i - 2);
                    }
                    lastDeltaEnd = delta;
                }
            } // end for (int i = 1; i < xPositions.length && ((!startFound) || (!endFound)); i++)
        } // end if (xPositions != null)
        stableSpeedStart = start;
        stableSpeedEnd = end;
        FlyScanImageViewer[] viewers = imageViewers;
        if (viewers != null) {
            for (FlyScanImageViewer viewer : viewers) {
                if (viewer != null) {
                    viewer.setStableSpeedBounds(start, end);
                    viewer.repaint();
                }
            }
        }
    }

    public void disconnectXMotor() {
        xPositionTitle.setToolTipText(null);
        xPositionTitle.setText(FSVUtilities.getMessage(X_POSITION_TITLE_KEY));
        xMotorListener.setDevice(null);
        xMotorStateListener.setDevice(null);
        stringBox.setErrorText(xLabel, null);
        stringBox.disconnectWidgetFromAll(xFormatTarget);
        stringBox.disconnectWidgetFromAll(xLabel);
        stringBox.disconnectWidgetFromAll(xPositionEditor);
        stringBox.disconnectWidgetFromAll(xMotorState);
    }

    public void connectXMotor() {
        // Connect to x motor if it does exist
        if ((xMotor == null) || (xMotor.isEmpty())) {
            xPositionTitle.setForeground(Color.BLACK);
            xPositionTitle.setToolTipText(null);
            xPositionTitle.setText(FSVUtilities.getMessage(X_POSITION_TITLE_KEY));
        } else if (TangoDeviceHelper.deviceExists(xMotor)) {
            xPositionTitle.setForeground(Color.BLACK);
            xPositionTitle.setToolTipText(xMotor + '/' + xPosition);
            xPositionTitle.setText(xMotor);
            xMotorListener.setDevice(xMotor);
            xMotorStateListener.setDevice(xMotor);
            TangoKey key = new TangoKey();
            TangoKeyTool.registerAttribute(key, xMotor, xPosition);
            stringBox.setErrorText(xLabel,
                    String.format(FSVUtilities.getMessage(CONNECTION_ERROR_KEY), xPositionTitle.getToolTipText()));
            stringBox.connectWidget(xLabel, key);
            key = new TangoKey();
            TangoKeyTool.registerWriteAttribute(key, xMotor, xPosition);
            stringBox.setColorEnabled(xPositionEditor, false);
            stringBox.connectWidget(xPositionEditor, key);
            key = new TangoKey();
            TangoKeyTool.registerAttributeProperty(key, xMotor, xPosition, AttributePropertyType.FORMAT);
            stringBox.connectWidget(xFormatTarget, key);
            key = new TangoKey();
            TangoKeyTool.registerAttribute(key, xMotor, TangoAttributeHelper.STATE);
            stringBox.connectWidget(xMotorState, key);
        } else {
            xPositionTitle.setForeground(Color.RED);
            xPositionTitle.setToolTipText(String.format(FSVUtilities.getMessage(MOTOR_DEVICE_ERROR_KEY), xMotor));
            xPositionTitle.setText(xMotor);
        }
    }

    public void disconnectYMotor() {
        yPositionTitle.setToolTipText(null);
        yPositionTitle.setText(FSVUtilities.getMessage(Y_POSITION_TITLE_KEY));
        yMotorListener.setDevice(null);
        yMotorStateListener.setDevice(null);
        stringBox.setErrorText(yLabel, null);
        stringBox.disconnectWidgetFromAll(yLabel);
        stringBox.disconnectWidgetFromAll(yPositionEditor);
        stringBox.disconnectWidgetFromAll(yFormatTarget);
        stringBox.disconnectWidgetFromAll(yMotorState);
    }

    public void connectYMotor() {
        // Connect to y motor if it does exist
        if ((yMotor == null) || yMotor.isEmpty()) {
            yPositionTitle.setForeground(Color.BLACK);
            yPositionTitle.setToolTipText(null);
            yPositionTitle.setText(FSVUtilities.getMessage(Y_POSITION_TITLE_KEY));
        } else if (TangoDeviceHelper.deviceExists(yMotor)) {
            yPositionTitle.setForeground(Color.BLACK);
            yPositionTitle.setToolTipText(yMotor + '/' + yPosition);
            yPositionTitle.setText(yMotor);
            yMotorListener.setDevice(yMotor);
            yMotorStateListener.setDevice(yMotor);
            TangoKey key = new TangoKey();
            TangoKeyTool.registerAttribute(key, yMotor, yPosition);
            stringBox.setErrorText(yLabel,
                    String.format(FSVUtilities.getMessage(CONNECTION_ERROR_KEY), yPositionTitle.getToolTipText()));
            stringBox.connectWidget(yLabel, key);
            key = new TangoKey();
            TangoKeyTool.registerWriteAttribute(key, yMotor, yPosition);
            stringBox.setColorEnabled(yPositionEditor, false);
            stringBox.connectWidget(yPositionEditor, key);
            key = new TangoKey();
            TangoKeyTool.registerAttributeProperty(key, yMotor, yPosition, AttributePropertyType.FORMAT);
            stringBox.connectWidget(yFormatTarget, key);
            key = new TangoKey();
            TangoKeyTool.registerAttribute(key, yMotor, TangoAttributeHelper.STATE);
            stringBox.connectWidget(yMotorState, key);
        } else {
            yPositionTitle.setForeground(Color.RED);
            yPositionTitle.setToolTipText(String.format(FSVUtilities.getMessage(MOTOR_DEVICE_ERROR_KEY), yMotor));
            yPositionTitle.setText(yMotor);
        }
    }

    public void disconnectCartographyXScale() {
        numberMatrixBox.disconnectWidgetFromAll(xTarget);
        xTarget.setEnabled(true);
        stableSpeedStart = Double.NaN;
        stableSpeedEnd = Double.NaN;
    }

    public void connectCartographyXScale() {
        TangoKey key = generateAttributeKey(cartographyX);
        numberMatrixBox.connectWidget(xTarget, key);
    }

    public void disconnectCartographyYScale() {
        numberMatrixBox.disconnectWidgetFromAll(yTarget);
        yTarget.setEnabled(true);
    }

    public void connectCartographyYScale() {
        TangoKey key = generateAttributeKey(cartographyY);
        numberMatrixBox.connectWidget(yTarget, key);
    }

    protected TitledBorder generateImageBorder(String attribute) {
        return new TitledBorder(new LineBorder(Color.BLUE, 1), attribute, TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, null, Color.BLUE);
    }

    public void disconnectCartographyImages() {
        imageDisplayPanel.removeAll();
        ImageConnector connector = this.connector;
        if (connector != null) {
            connector.setCanceled(true);
        }
        FlyScanImageViewer[] viewers = this.imageViewers;
        IImageViewer[] targets = this.imageTargets;
        if (viewers != null) {
            int index = 0;
            for (FlyScanImageViewer viewer : viewers) {
                if (viewer != null) {
                    viewer.removePropertyChangeListener(this);
                }
                IImageViewer target = targets[index];
                targets[index] = null;
                viewers[index++] = null;
                numberMatrixBox.disconnectWidgetFromAll(target);
            }
        }
        imageDisplayPanel.setLayout(new FlowLayout());
    }

    public void connectCartographyImages() {
        String[] images = cartographyImage == null || cartographyImage.trim().isEmpty() ? new String[0]
                : cartographyImage.split(ParsingUtil.COMMA);
        FlyScanImageViewer[] viewers = this.imageViewers;
        IImageViewer[] targets = this.imageTargets;
        if (viewers.length != images.length) {
            ImageDisconnector disconnector = new ImageDisconnector(viewers, targets, images.length, numberMatrixBox,
                    transformationPanel);
            viewers = Arrays.copyOf(viewers, images.length);
            targets = Arrays.copyOf(targets, images.length);
            disconnector.execute();
        }
        this.imageViewers = viewers;
        this.imageTargets = targets;
        if (images.length > 0) {
            imageDisplayPanel.setLayout(LayoutUtils.generateBestGridLayout(images.length));
            for (int i = 0; i < images.length; i++) {
                String attribute = images[i];
                if (attribute == null) {
                    attribute = ObjectUtils.EMPTY_STRING;
                } else {
                    attribute = attribute.trim();
                }
                images[i] = attribute;
                FlyScanImageViewer viewer = viewers[i];
                IImageViewer target = targets[i];
                if (viewer == null) {
                    viewer = generateImageViewer();
                    viewers[i] = viewer;
                    target = transformationPanel.registerImageViewer(viewer);
                    targets[i] = target;
                } else {
                    target.setXAxisFormat(xMotorPositionFormat);
                    target.setYAxisFormat(yMotorPositionFormat);
                    viewer.setStableSpeedBounds(stableSpeedStart, stableSpeedEnd);
                }
                viewer.setBorder(generateImageBorder(attribute));
                imageDisplayPanel.add(viewer);
            }
            imageDisplayPanel.revalidate();
            imageDisplayPanel.repaint();
            ImageConnector connector = this.connector;
            if ((connector == null) || connector.isCanceled() || connector.isDone()) {
                connector = new ImageConnector(targets, images, numberMatrixBox, this);
                this.connector = connector;
                connector.execute();
            }
        }
    }

    protected TangoKey generateConfigPropertyKey() {
        TangoKey key = generatePropertyKey(CONFIG_PROPERTY);
        TangoKeyTool.registerSettable(key, Boolean.FALSE);
        return key;
    }

    public void disconnectConfigProperty() {
        cleanWidget(configTarget);
    }

    protected void connectConfigProperty(TangoKey configPropertyKey) {
        setWidgetModel(configTarget, stringBox, configPropertyKey);
    }

    public void connectConfigProperty() {
        connectConfigProperty(generateConfigPropertyKey());
    }

    protected void writeComment(BufferedWriter writer, String comment) throws IOException {
        writer.write('#');
        writer.write(comment);
        writer.newLine();
    }

    protected void writeProperty(BufferedWriter writer, String comment, String key, String value) throws IOException {
        writeComment(writer, comment);
        writer.write(key);
        writer.write('=');
        if (value != null) {
            writer.write(value);
        }
        writer.newLine();
    }

    @Override
    protected void cleanStateModel() {
        cleanWidget(stateTarget);
    }

    @Override
    protected void setStateModel() {
        TangoKey key = generateAttributeKey(STATE);
        setWidgetModel(stateTarget, stringBox, key);
    }

    @Override
    protected void cleanStatusModel() {
        modelListener.setDevice(null);
        statusListener.setDevice(null);
        super.cleanStatusModel();
    }

    @Override
    protected void setStatusModel() {
        modelListener.setDevice(getModel());
        statusListener.setDevice(getModel());
        super.setStatusModel();
    }

    public void setBottomDividertoBestLocation() {
        int motorWidth = motorPositionsScrollPane.getWidth(), statusWidth = fsvStatusScrollPane.getWidth();
        int bestMotorWidth = SizeUtils.getAdaptedPreferredSize(motorPositionsPanel,
                motorPositionsPanel.getPreferredSize()).width;
        Insets bounds = motorPositionsScrollPane.getBorder().getBorderInsets(motorPositionsPanel);
        bestMotorWidth += bounds.left + bounds.right;
        int bestStatusWidth = Math.max(
                SizeUtils.getAdaptedPreferredSize(statusPanel, statusPanel.getPreferredSize()).width,
                SizeUtils.getAdaptedPreferredSize(deviceLabel, deviceLabel.getPreferredSize()).width);
        bounds = fsvStatusScrollPane.getBorder().getBorderInsets(statusPanel);
        bestStatusWidth += bounds.left + bounds.right + 5;
        if ((motorWidth > bestMotorWidth) || (statusWidth < bestStatusWidth)) {
            bounds = bottomSplitPane.getBorder().getBorderInsets(bottomSplitPane);
            int refWidth = bottomSplitPane.getWidth() - (bottomSplitPane.getDividerSize() + bounds.left + bounds.right);
            int refLocation = refWidth - bestMotorWidth;
            if (bestStatusWidth > refLocation) {
                bottomSplitPane.setDividerLocation(refLocation);
            } else {
                bottomSplitPane.setDividerLocation(bestStatusWidth);
            }
        }
    }

    /**
     * Returns the status panel. The status panel is a {@link JPanel} that informs about the name and status of a Tango
     * device
     * 
     * @return a {@link JPanel}
     * @see #setStatusModel()
     * @see #cleanStatusModel()
     */
    @Override
    public JPanel getStatusPanel() {
        if (statusPanel == null) {
            statusPanel = new JPanel(new GridBagLayout());
            GridBagConstraints deviceConstraints = new GridBagConstraints();
            deviceConstraints.fill = GridBagConstraints.VERTICAL;
            deviceConstraints.gridx = 0;
            deviceConstraints.gridy = 0;
            deviceConstraints.weightx = 1;
            deviceConstraints.weighty = 0;
            deviceConstraints.insets = new Insets(0, 5, 0, 5);
            statusPanel.add(deviceLabel, deviceConstraints);
            GridBagConstraints statusConstraints = new GridBagConstraints();
            statusConstraints.fill = GridBagConstraints.BOTH;
            statusConstraints.gridx = 0;
            statusConstraints.gridy = 1;
            statusConstraints.weightx = 1;
            statusConstraints.weighty = 0;
            statusConstraints.insets = new Insets(0, 5, 0, 5);
            statusPanel.add(statusLabel, statusConstraints);
        }
        return statusPanel;
    }

    @Override
    protected void clearGUI() {
        cleanStateModel();
        disconnectConfigProperty();
        cleanStatusModel();
        disconnectXMotor();
        disconnectYMotor();
        disconnectCartographyXScale();
        disconnectCartographyYScale();
        disconnectCartographyImages();
    }

    /**
     * Might update bottom splitpane divider location, in case of first connection to flyscanviewer device.
     */
    public void upateUIIfFirstConnection() {
        if (firstConnection) {
            firstConnection = false;
            // At first connection, we wait for the components to finish connecting,
            // then we adapt bottom split pane divider location
            new Thread("update later bottom splitpane divider location") {
                @Override
                public void run() {
                    try {
                        // first: wait a little bit
                        sleep(1000);
                        // then adapt divider location in EDT
                        SwingUtilities.invokeLater(() -> {
                            setBottomDividertoBestLocation();
                        });
                    } catch (InterruptedException e) {
                        // Ignore exception because it is not that important
                    }
                }
            }.start();
        }
    }

    @Override
    protected void refreshGUI() {
        connectXMotor();
        connectYMotor();
        setStatusModel();
        connectCartographyImages();
        connectCartographyXScale();
        connectCartographyYScale();
        connectConfigProperty();
        setStateModel();
        upateUIIfFirstConnection();
    }

    @Override
    protected void onConnectionError() {
        JOptionPane.showMessageDialog(this,
                String.format(FSVUtilities.getMessage(FLYSCAN_DEVICE_ERROR_KEY), getModel()),
                FSVUtilities.getMessage(ERROR_KEY), JOptionPane.ERROR_MESSAGE);
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // not managed
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // not managed
    }

    @Override
    public void motorPositionChanged(MotorPositionEvent event) {
        if ((event != null) && (event.getSource() != null)) {
            IFlyScanMotorPositionManager source = event.getSource();
            FlyScanImageViewer[] viewers = imageViewers;
            if (viewers != null) {
                for (FlyScanImageViewer viewer : viewers) {
                    if ((viewer != null) && (viewer != source)) {
                        viewer.removeMotorPositionListener(this);
                        viewer.clearBeamPoint();
                        viewer.repaint();
                        viewer.addMotorPositionListener(this);
                    }
                }
            }
            double[] position = null;
            if (source instanceof ImageViewer) {
                IImageViewer adapter = transformationPanel.getImageAdapter((ImageViewer) event.getSource());
                if (adapter != null) {
                    position = adapter.getBeamPoint();
                }
            }
            if ((position == null) || (position.length != 2)) {
                xPositionEditor.setText(ObjectUtils.EMPTY_STRING);
                yPositionEditor.setText(ObjectUtils.EMPTY_STRING);
            } else {
                xPositionEditor.setText(Format.format(xMotorPositionFormat, position[0]));
                yPositionEditor.setText(Format.format(yMotorPositionFormat, position[1]));
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt != null) {
            Object source = evt.getSource();
            if (source == propertiesEditor) {
                boolean writePropertyFile;
                switch (evt.getPropertyName()) {
                    case FLYSCAN_VIEWER_DEVICE_KEY:
                        setModel((String) evt.getNewValue());
                        writePropertyFile = true;
                        break;
                    case CARTOGRAPHY_X_PROP_KEY:
                        this.cartographyX = (String) evt.getNewValue();
                        disconnectCartographyXScale();
                        connectCartographyXScale();
                        writePropertyFile = true;
                        break;
                    case CARTOGRAPHY_Y_PROP_KEY:
                        this.cartographyY = (String) evt.getNewValue();
                        disconnectCartographyYScale();
                        connectCartographyYScale();
                        writePropertyFile = true;
                        break;
                    case CARTOGRAPHY_IMAGES_PROP_KEY:
                        this.cartographyImage = (String) evt.getNewValue();
                        disconnectCartographyImages();
                        connectCartographyImages();
                        writePropertyFile = true;
                        break;
                    case MOTOR_X_POSITION_PROP_KEY:
                        setXMotorProperty((String) evt.getNewValue());
                        disconnectXMotor();
                        connectXMotor();
                        writePropertyFile = true;
                        break;
                    case MOTOR_Y_POSITION_PROP_KEY:
                        setYMotorProperty((String) evt.getNewValue());
                        disconnectYMotor();
                        connectYMotor();
                        writePropertyFile = true;
                        break;
                    case FSVPropertiesEditor.ALL_PROPERTIES:
                        loadProperties((Properties) evt.getNewValue(), false);
                        writePropertyFile = true;
                        break;
                    default:
                        writePropertyFile = false;
                        break;
                } // end switch (evt.getPropertyName())
                if (writePropertyFile) {
                    new SavePropertiesWorker(this).execute();
                }
            } else if (source instanceof FlyScanImageViewer) {
                FlyScanImageViewer viewer = (FlyScanImageViewer) source;
                Border border = viewer.getBorder();
                if (ENABLED.equals(evt.getPropertyName()) && (border instanceof TitledBorder)) {
                    String title = ((TitledBorder) border).getTitle();
                    Boolean value = (Boolean) evt.getNewValue();
                    if ((value != null) && (!value.equals(evt.getOldValue()))) {
                        viewer.setImageName(value.booleanValue() ? null
                                : String.format(FSVUtilities.getMessage(CONNECTION_ERROR_KEY), title));
                        viewer.revalidate();
                        viewer.repaint();
                    }
                }
            }
        } // end if ((evt != null) && (evt.getSource() == propertiesEditor))
    }

}
