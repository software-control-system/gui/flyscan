package fr.soleil.flyscan.visualization.view;

import fr.soleil.comete.swing.TextField;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link TextField} that will avoid sending useless document events.
 * 
 * @author GIRARDOT
 */
public class FSVTextField extends TextField {

    private static final long serialVersionUID = 5410457606531640506L;

    public FSVTextField() {
        super();
    }

    @Override
    protected void doSetText(String text) {
        if ((getDocument() == null) || (!ObjectUtils.sameObject(text, getText()))) {
            super.doSetText(text);
        }
    }
}
