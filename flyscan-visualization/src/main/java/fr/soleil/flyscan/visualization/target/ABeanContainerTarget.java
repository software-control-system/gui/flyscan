package fr.soleil.flyscan.visualization.target;

import java.lang.ref.WeakReference;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.ITarget;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.flyscan.visualization.util.FSVConstants;

public abstract class ABeanContainerTarget implements ITarget, FSVConstants {

    protected WeakReference<FlyScanVisualizationTangoBox> beanRef;

    public ABeanContainerTarget(FlyScanVisualizationTangoBox bean) {
        this.beanRef = bean == null ? null : new WeakReference<>(bean);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    protected void finalize() {
        beanRef = null;
    }
}
