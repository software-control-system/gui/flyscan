package fr.soleil.flyscan.visualization.util;

import java.lang.ref.WeakReference;

import javax.swing.SwingWorker;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;

public class ImageConnector extends SwingWorker<Void, Void> implements ICancelable {

    protected final IImageViewer[] viewers;
    protected final String[] attributes;
    protected final WeakReference<NumberMatrixBox> numberMatrixBoxRef;
    protected final WeakReference<IDeviceTangoKeyManager> managerRef;
    protected volatile boolean canceled;

    public ImageConnector(IImageViewer[] viewers, String[] attributes, NumberMatrixBox numberMatrixBox,
            IDeviceTangoKeyManager manager) {
        super();
        this.viewers = viewers;
        this.attributes = attributes;
        this.numberMatrixBoxRef = numberMatrixBox == null ? null : new WeakReference<>(numberMatrixBox);
        this.managerRef = manager == null ? null : new WeakReference<>(manager);
    }

    @Override
    protected Void doInBackground() {
        NumberMatrixBox numberMatrixBox = ObjectUtils.recoverObject(numberMatrixBoxRef);
        IDeviceTangoKeyManager manager = ObjectUtils.recoverObject(managerRef);
        if ((viewers != null) && (attributes != null) && (viewers.length == attributes.length)
                && (numberMatrixBox != null) && (manager != null)) {
            for (int i = 0; i < attributes.length && !canceled; i++) {
                String attribute = attributes[i];
                if (attribute == null) {
                    attribute = ObjectUtils.EMPTY_STRING;
                } else {
                    attribute = attribute.trim();
                }
                IImageViewer viewer = viewers[i];
                if ((viewer != null) && (!attribute.isEmpty()) && (!canceled)) {
                    // First, disconnect widget (SCAN-883)
                    numberMatrixBox.disconnectWidgetFromAll(viewer);
                    numberMatrixBox.connectWidget(viewer, manager.generateAttributeKey(attribute));
                }
            }
        }
        return null;
    }

    @Override
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Override
    public boolean isCanceled() {
        return canceled;
    }
}
