package fr.soleil.flyscan.visualization.util;

import java.awt.Font;

import javax.swing.ImageIcon;

import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.Icons;

public class FSVUtilities {

    private static final Icons ICONS = new Icons("fr.soleil.flyscan.visualization.icons");
    private static final MessageManager MESSAGE_MANAGER = new MessageManager("fr.soleil.flyscan.visualization");

    public static final String APPLICATION_NAME = MESSAGE_MANAGER.getAppMessage("project.name");
    public static final String APPLICATION_VERSION = MESSAGE_MANAGER.getAppMessage("project.version");
    public static final String APPLICATION_NAME_WITH_VERSION = new StringBuilder(APPLICATION_NAME).append(' ')
            .append(APPLICATION_VERSION).toString();
    public static final String COPYRIGHT = "SOLEIL Synchrotron";

    public static final ImageIcon SPLASH_ICON = ICONS.getIcon("flyscan.splash");
    public static final ImageIcon FLYSCAN_ICON = ICONS.getIcon("flyscan.icon");

    protected static final int DEFAULT_FONT_SIZE = 14;
    public static final Font DEFAULT_FONT = new Font(Font.DIALOG, Font.PLAIN, DEFAULT_FONT_SIZE);
    public static final Font DEFAULT_FONT_BOLD = new Font(Font.DIALOG, Font.BOLD, DEFAULT_FONT_SIZE);

    protected static final int SMALL_FONT_SIZE = 12;
    public static final Font SMALL_FONT = new Font(Font.DIALOG, Font.PLAIN, SMALL_FONT_SIZE);
    public static final Font SMALL_FONT_BOLD = new Font(Font.DIALOG, Font.BOLD, SMALL_FONT_SIZE);

    private FSVUtilities() {
        // nothing particular to do
    }

    public static String getMessage(String key) {
        return MESSAGE_MANAGER.getMessage(key);
    }

    public static ImageIcon getIcon(String key) {
        return ICONS.getIcon(key);
    }

}
