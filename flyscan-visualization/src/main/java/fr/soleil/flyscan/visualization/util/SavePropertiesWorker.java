package fr.soleil.flyscan.visualization.util;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.lib.project.ObjectUtils;

/**
 * A {@link SwingWorker} dedicated in properties file saving.
 * 
 * @author GIRARDOT
 */
public class SavePropertiesWorker extends SwingWorker<Void, Void> implements FSVConstants {

    private WeakReference<FlyScanVisualizationTangoBox> beanRef;

    public SavePropertiesWorker(FlyScanVisualizationTangoBox bean) {
        super();
        this.beanRef = bean == null ? null : new WeakReference<>(bean);
    }

    @Override
    protected Void doInBackground() throws Exception {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        if (bean != null) {
            bean.saveProperties();
        }
        return null;
    }

    @Override
    protected void done() {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        String path = bean == null ? null : bean.getPropertiesFile();
        if (path == null) {
            path = NULL;
        }
        try {
            get();
        } catch (InterruptedException e) {
            // Nothing to do in case of interruption
        } catch (ExecutionException e) {
            JOptionPane.showMessageDialog(bean,
                    String.format(FSVUtilities.getMessage(PROPERTIES_FILE_WRITE_ERROR_KEY), path),
                    FSVUtilities.getMessage(ERROR_KEY), JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void finalize() {
        beanRef = null;
    }

}
