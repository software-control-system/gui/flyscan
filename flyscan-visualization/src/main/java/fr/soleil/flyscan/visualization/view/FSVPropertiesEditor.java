package fr.soleil.flyscan.visualization.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.Properties;
import java.util.WeakHashMap;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.comete.bean.tangotree.TangoTree;
import fr.soleil.comete.bean.tangotree.data.AttributeFilter;
import fr.soleil.comete.bean.tangotree.data.DataDimension;
import fr.soleil.comete.bean.tangotree.data.DataLevel;
import fr.soleil.comete.bean.tangotree.data.DataType;
import fr.soleil.comete.swing.TextField;
import fr.soleil.data.controller.BasicTextTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.flyscan.visualization.util.FSVConstants;
import fr.soleil.flyscan.visualization.util.FSVUtilities;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An editor for flyscanvisualization properties file
 * 
 * @author GIRARDOT
 */
public class FSVPropertiesEditor extends JDialog implements FSVConstants, ActionListener {

    private static final long serialVersionUID = 8789641145686011756L;

    public static final String ALL_PROPERTIES = "All properties";

    protected static final String FLYSCAN_VIEWER_DEVICE_CHOOSE = "flyscan.viewer.device.choose";
    protected static final String FLYSCAN_VIEWER_DEVICE_CLASS = "FlyscanViewer";
    protected static final String X_MOTOR_POSITION_DESCRIPTION = "flyscan.motor.position.x.attribute.description";
    protected static final String X_MOTOR_POSITION_CHOOSE = "flyscan.motor.position.x.attribute.choose";
    protected static final String Y_MOTOR_POSITION_DESCRIPTION = "flyscan.motor.position.y.attribute.description";
    protected static final String Y_MOTOR_POSITION_CHOOSE = "flyscan.motor.position.y.attribute.choose";
    protected static final String FLYSCAN_DEVICE_DESCRIPTION = "flyscan.viewer.device.description";
    protected static final String CAROGRAPHY_IMAGES_DESCRIPTION = "flyscan.cartography.images.attribute.description";
    protected static final String CAROGRAPHY_X_DESCRIPTION = "flyscan.cartography.x.attribute.description";
    protected static final String CAROGRAPHY_Y_DESCRIPTION = "flyscan.cartography.y.attribute.description";

    protected static final String BROWSE_TEXT = "...";

    protected static final Insets BUTTON_INSETS = new Insets(0, 2, 0, 2);

    protected static final Font FIELD_FONT = new Font(Font.DIALOG, Font.PLAIN, 12);
    protected static final Font FIELD_EDIT_FONT = new Font(Font.DIALOG, Font.ITALIC, 12);

    protected Properties properties;

    protected final JPanel mainPanel;

    protected final JPanel fieldPanel;
    protected final JScrollPane fieldScrollPane;

    protected final JLabel flyscanviewerTitle;
    protected final TextField flyscanviewerField;
    protected final JButton flyscanviewerEditor;
    protected JDialog flyscanviewerDialog;
    protected final TangoTree flyscanviewerTree;

    protected final JLabel cartographyImageTitle;
    protected final JTextField cartographyImageField;

    protected final JLabel cartographyXTitle;
    protected final JTextField cartographyXField;

    protected final JLabel cartographyYTitle;
    protected final JTextField cartographyYField;

    protected final JLabel xMotorPositionTitle;
    protected final TextField xMotorPositionField;
    protected final JButton xMotorPositionEditor;
    protected JDialog xMotorPositionDialog;
    protected final TangoTree xMotorPositionTree;

    protected final JLabel yMotorPositionTitle;
    protected final TextField yMotorPositionField;
    protected final JButton yMotorPositionEditor;
    protected JDialog yMotorPositionDialog;
    protected final TangoTree yMotorPositionTree;

    protected final JPanel buttonPanel;
    protected final JButton okButton, cancelButton;

    protected final ActionListener editorConfirmation;
    protected final KeyListener editorCanceler;
    protected final BasicTextTargetController textController;
    protected final Map<TextField, TangoTree> treeMap;

    public FSVPropertiesEditor(Window owner, String title) {
        super(owner, title);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cancelAndClose(true);
            }
        });
        editorConfirmation = e -> {
            if ((e != null) && (e.getSource() instanceof JTextField)) {
                notifyProperty((JTextField) e.getSource());
            }
        };
        editorCanceler = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if ((e != null) && (e.getSource() instanceof JTextField) && (e.getKeyCode() == KeyEvent.VK_ESCAPE)) {
                    cancelEditor((JTextField) e.getSource());
                }
            }
        };
        textController = new BasicTextTargetController() {
            @Override
            protected void setDataToTarget(ITarget target, String data, AbstractDataSource<String> source) {
                super.setDataToTarget(target, data, source);
                if (target instanceof JTextField) {
                    notifyProperty((JTextField) target);
                }
            }
        };
        treeMap = new WeakHashMap<>();
        mainPanel = new JPanel(new BorderLayout());
        fieldPanel = new JPanel(new GridBagLayout());
        int y = 0;

        flyscanviewerTitle = buildTitleLabel(FLYSCAN_VIEWER_DEVICE_TITLE);
        flyscanviewerField = buildCometeEditor(FLYSCAN_VIEWER_DEVICE_KEY, FLYSCAN_DEVICE_DESCRIPTION);
        flyscanviewerEditor = buildEditorButton(FLYSCAN_VIEWER_DEVICE_CHOOSE);
        flyscanviewerTree = buildDeviceTree(flyscanviewerField);
        flyscanviewerTree.setFilteredDeviceClass(FLYSCAN_VIEWER_DEVICE_CLASS);
        y = addComponentsToFieldPanel(flyscanviewerTitle, flyscanviewerField, flyscanviewerEditor, y, false);

        cartographyImageTitle = buildTitleLabel(CARTOGRAPHY_IMAGES_TITLE);
        cartographyImageField = buildEditor(CARTOGRAPHY_IMAGES_PROP_KEY, CAROGRAPHY_IMAGES_DESCRIPTION);
        y = addComponentsToFieldPanel(cartographyImageTitle, cartographyImageField, null, y, false);

        cartographyXTitle = buildTitleLabel(CARTOGRAPHY_X_TITLE);
        cartographyXField = buildEditor(CARTOGRAPHY_X_PROP_KEY, CAROGRAPHY_X_DESCRIPTION);
        y = addComponentsToFieldPanel(cartographyXTitle, cartographyXField, null, y, false);

        cartographyYTitle = buildTitleLabel(CARTOGRAPHY_Y_TITLE);
        cartographyYField = buildEditor(CARTOGRAPHY_Y_PROP_KEY, CAROGRAPHY_Y_DESCRIPTION);
        y = addComponentsToFieldPanel(cartographyYTitle, cartographyYField, null, y, false);

        xMotorPositionTitle = buildTitleLabel(MOTOR_X_POSITION_TITLE);
        xMotorPositionField = buildCometeEditor(MOTOR_X_POSITION_PROP_KEY, X_MOTOR_POSITION_DESCRIPTION);
        xMotorPositionTree = buildAttributeTree(xMotorPositionField);
        xMotorPositionEditor = buildEditorButton(X_MOTOR_POSITION_CHOOSE);
        y = addComponentsToFieldPanel(xMotorPositionTitle, xMotorPositionField, xMotorPositionEditor, y, false);

        yMotorPositionTitle = buildTitleLabel(MOTOR_Y_POSITION_TITLE);
        yMotorPositionField = buildCometeEditor(MOTOR_Y_POSITION_PROP_KEY, Y_MOTOR_POSITION_DESCRIPTION);
        yMotorPositionTree = buildAttributeTree(yMotorPositionField);
        yMotorPositionEditor = buildEditorButton(Y_MOTOR_POSITION_CHOOSE);
        y = addComponentsToFieldPanel(yMotorPositionTitle, yMotorPositionField, yMotorPositionEditor, y, true);
        fieldScrollPane = new JScrollPane(fieldPanel);
        mainPanel.add(fieldScrollPane, BorderLayout.CENTER);

        buttonPanel = new JPanel(new GridBagLayout());
        int x = 0;
        okButton = buildButton(OK_KEY);
        x = addButtonToButtonPanel(okButton, x, 50);
        cancelButton = buildButton(CANCEL_KEY);
        x = addButtonToButtonPanel(cancelButton, x, 0);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        setContentPane(mainPanel);
    }

    protected void modifyEditor(JTextField editor, DocumentEvent e) {
        editor.setFont(FIELD_EDIT_FONT);
    }

    protected JLabel buildTitleLabel(String key) {
        return new JLabel(FSVUtilities.getMessage(key), SwingConstants.RIGHT);
    }

    protected void setupTextField(JTextField editor, String nameKey, String tooltipKey) {
        editor.setFont(FIELD_FONT);
        editor.setName(nameKey);
        editor.setToolTipText(FSVUtilities.getMessage(tooltipKey));
        editor.addActionListener(editorConfirmation);
        editor.addKeyListener(editorCanceler);
        editor.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                modifyEditor(editor, e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                modifyEditor(editor, e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                modifyEditor(editor, e);
            }
        });
    }

    protected JTextField buildEditor(String nameKey, String tooltipKey) {
        JTextField editor = new JTextField(20);
        setupTextField(editor, nameKey, tooltipKey);
        return editor;
    }

    protected TextField buildCometeEditor(String nameKey, String tooltipKey) {
        TextField editor = new FSVTextField();
        editor.setColumns(20);
        setupTextField(editor, nameKey, tooltipKey);
        return editor;
    }

    protected void prepareButton(JButton button) {
        button.setMargin(BUTTON_INSETS);
        button.addActionListener(this);
    }

    protected JButton buildEditorButton(String key) {
        JButton button = new JButton(BROWSE_TEXT);
        button.setToolTipText(FSVUtilities.getMessage(key));
        prepareButton(button);
        return button;
    }

    protected JButton buildButton(String key, String iconKey) {
        JButton button = new JButton(FSVUtilities.getMessage(key), FSVUtilities.getIcon(iconKey));
        prepareButton(button);
        return button;
    }

    protected JButton buildButton(String key) {
        return buildButton(key, key);
    }

    protected int addComponentsToFieldPanel(JLabel title, JTextField editor, JButton button, int y, boolean bottomGap) {
        int x = 0;
        int bottomMargin = bottomGap ? 5 : 0;
        GridBagConstraints titleConstraints = new GridBagConstraints();
        titleConstraints.fill = GridBagConstraints.BOTH;
        titleConstraints.gridx = x++;
        titleConstraints.gridy = y;
        titleConstraints.weightx = 0;
        titleConstraints.weighty = 0;
        titleConstraints.insets = new Insets(5, 5, bottomMargin, 5);
        fieldPanel.add(title, titleConstraints);
        GridBagConstraints editorConstraints = new GridBagConstraints();
        editorConstraints.fill = GridBagConstraints.BOTH;
        editorConstraints.gridx = x++;
        editorConstraints.gridy = y;
        editorConstraints.weightx = 1;
        editorConstraints.weighty = 0;
        editorConstraints.insets = new Insets(5, 0, bottomMargin, 0);
        fieldPanel.add(editor, editorConstraints);
        if (button != null) {
            GridBagConstraints buttonConstraints = new GridBagConstraints();
            buttonConstraints.fill = GridBagConstraints.BOTH;
            buttonConstraints.gridx = x++;
            buttonConstraints.gridy = y;
            buttonConstraints.weightx = 0;
            buttonConstraints.weighty = 0;
            buttonConstraints.insets = new Insets(5, 0, bottomMargin, 5);
            fieldPanel.add(button, buttonConstraints);
        }
        return ++y;
    }

    protected int addButtonToButtonPanel(JButton button, int x, int rightMargin) {
        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.NONE;
        buttonConstraints.gridx = x++;
        buttonConstraints.gridy = 0;
        buttonConstraints.insets = new Insets(0, 0, 0, rightMargin);
        buttonPanel.add(button, buttonConstraints);
        return x;
    }

    protected JDialog getFlyscanviewerDialog() {
        if (flyscanviewerDialog == null) {
            flyscanviewerDialog = new JDialog(this, flyscanviewerEditor.getToolTipText());
            flyscanviewerDialog.setContentPane(new JScrollPane(flyscanviewerTree));
        }
        return flyscanviewerDialog;
    }

    protected JDialog getXMotorPositionDialog() {
        if (xMotorPositionDialog == null) {
            xMotorPositionDialog = new JDialog(this, xMotorPositionEditor.getToolTipText());
            xMotorPositionDialog.setContentPane(new JScrollPane(xMotorPositionTree));
        }
        return xMotorPositionDialog;
    }

    protected JDialog getYMotorPositionDialog() {
        if (yMotorPositionDialog == null) {
            yMotorPositionDialog = new JDialog(this, yMotorPositionEditor.getToolTipText());
            yMotorPositionDialog.setContentPane(new JScrollPane(yMotorPositionTree));
        }
        return yMotorPositionDialog;
    }

    protected TangoTree buildDeviceTree(TextField target) {
        TangoTree tree = new TangoTree();
        tree.setSelectableDataLevel(DataLevel.DEVICE);
        treeMap.put(target, tree);
        textController.addLink(tree.getSelectedDataSource(), target);
        return tree;
    }

    protected TangoTree buildAttributeTree(TextField target) {
        TangoTree tree = new TangoTree();
        tree.setSelectableDataLevel(DataLevel.ATTRIBUTE);
        tree.setAttributeFilters(new AttributeFilter(DataDimension.SCALAR, DataType.NUMBER));
        treeMap.put(target, tree);
        textController.addLink(tree.getSelectedDataSource(), target);
        return tree;
    }

    protected void addProperty(Properties properties, JTextField field) {
        properties.put(field.getName(), field.getText());
    }

    protected Properties buildProperties() {
        Properties properties = new Properties();
        addProperty(properties, flyscanviewerField);
        addProperty(properties, cartographyImageField);
        addProperty(properties, cartographyXField);
        addProperty(properties, cartographyYField);
        addProperty(properties, xMotorPositionField);
        addProperty(properties, yMotorPositionField);
        return properties;
    }

    protected boolean propertyChanged(Properties properties, JTextField field) {
        return !ObjectUtils.sameObject(properties.getProperty(field.getName()), field.getText());
    }

    protected String getProperty(Properties properties, String propertyName) {
        return properties == null ? ObjectUtils.EMPTY_STRING
                : properties.getProperty(propertyName, ObjectUtils.EMPTY_STRING);
    }

    protected void cancelProperty(Properties properties, JTextField field, boolean looseFocus) {
        if (looseFocus) {
            mainPanel.grabFocus();
        }
        field.setText(getProperty(properties, field.getName()));
        if (field instanceof TextField) {
            TangoTree tree = treeMap.get(field);
            if (tree != null) {
                tree.selectData(field.getText());
            }
        }
        field.setFont(FIELD_FONT);
    }

    protected void cancelProperties() {
        Properties properties = this.properties;
        mainPanel.grabFocus();
        cancelProperty(properties, flyscanviewerField, false);
        cancelProperty(properties, cartographyImageField, false);
        cancelProperty(properties, cartographyXField, false);
        cancelProperty(properties, cartographyYField, false);
        cancelProperty(properties, xMotorPositionField, false);
        cancelProperty(properties, yMotorPositionField, false);
    }

    protected void cancelAndClose(boolean isClosing) {
        hideTreeDialogs();
        cancelProperties();
        if (!isClosing) {
            setVisible(false);
        }
    }

    protected void notifyProperties() {
        Properties properties = this.properties;
        if ((properties == null) || propertyChanged(properties, flyscanviewerField)
                || propertyChanged(properties, cartographyImageField) || propertyChanged(properties, cartographyXField)
                || propertyChanged(properties, cartographyYField) || propertyChanged(properties, xMotorPositionField)
                || propertyChanged(properties, yMotorPositionField)) {
            mainPanel.grabFocus();
            firePropertyChange(ALL_PROPERTIES, properties, buildProperties());
        }
    }

    protected void applyAndClose() {
        hideTreeDialogs();
        notifyProperties();
        setVisible(false);
    }

    protected String getNewProperty(JTextField field) {
        String newValue = field.getText();
        if (newValue == null) {
            newValue = ObjectUtils.EMPTY_STRING;
        } else {
            newValue = newValue.trim();
        }
        return newValue;
    }

    protected void notifyProperty(JTextField field) {
        Properties properties = this.properties;
        String oldValue = getProperty(properties, field.getName());
        String newValue = getNewProperty(field);
        boolean changed;
        if (oldValue.equalsIgnoreCase(newValue) || newValue.isEmpty()) {
            changed = false;
            newValue = oldValue;
        } else {
            changed = true;
        }
        mainPanel.grabFocus();
        if (changed) {
            if (properties != null) {
                properties.put(field.getName(), newValue);
                cancelProperty(properties, field, false);
            }
            firePropertyChange(field.getName(), oldValue, newValue);
        } else {
            field.setText(newValue);
        }
    }

    protected void cancelEditor(JTextField field) {
        cancelProperty(properties, field, true);
    }

    protected void hideTreeDialogs() {
        if ((xMotorPositionDialog != null) && xMotorPositionDialog.isVisible()) {
            xMotorPositionDialog.setVisible(false);
        }
        if ((yMotorPositionDialog != null) && yMotorPositionDialog.isVisible()) {
            yMotorPositionDialog.setVisible(false);
        }
    }

    protected void setupAndShow(JDialog dialog, JComponent editor) {
        if (!dialog.isVisible()) {
            dialog.setSize(500, 400);
            dialog.setLocationRelativeTo(editor);
            dialog.setVisible(true);
        }
        dialog.toFront();
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
        SwingUtilities.invokeLater(() -> {
            cancelProperties();
        });
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (!visible) {
            hideTreeDialogs();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e != null) {
            if (e.getSource() == flyscanviewerEditor) {
                setupAndShow(getFlyscanviewerDialog(), flyscanviewerEditor);
            } else if (e.getSource() == xMotorPositionEditor) {
                setupAndShow(getXMotorPositionDialog(), xMotorPositionEditor);
            } else if (e.getSource() == yMotorPositionEditor) {
                setupAndShow(getYMotorPositionDialog(), yMotorPositionEditor);
            } else if (e.getSource() == okButton) {
                applyAndClose();
            } else if (e.getSource() == cancelButton) {
                cancelAndClose(false);
            }
        }
    }

}
