package fr.soleil.flyscan.visualization.target;

import java.lang.reflect.Array;

import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageTransformationPanel;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * The {@link CartographyScaleTarget} dedicated in Y axis scale.
 * 
 * @author GIRARDOT
 */
public class CartographyYTarget extends CartographyScaleTarget {

    public CartographyYTarget(ArrayPositionConvertor convertor, FlyScanImageTransformationPanel transformationPanel,
            FlyScanVisualizationTangoBox box, String connectionErrorKey) {
        super(convertor, transformationPanel, box, connectionErrorKey);
    }

    @Override
    protected String getAxisSource() {
        FlyScanVisualizationTangoBox box = ObjectUtils.recoverObject(beanRef);
        return box == null ? null : box.getCartographyY();
    }

    @Override
    protected void updateAxisTitle(String title, FlyScanImageTransformationPanel transformationPanel) {
        if (transformationPanel != null) {
            transformationPanel.setYScale(true, title);
        }
    }

    @Override
    protected Object extractArrayFromMatrix(Object[] matrix) {
        Object array = null;
        int[] shape = ArrayUtils.recoverShape(matrix);
        if ((shape != null) && (shape.length == 2)) {
            Class<?> dataType = ArrayUtils.recoverDataType(matrix);
            if (ObjectUtils.isNumberClass(dataType)) {
                array = matrix[0];
                if (matrix instanceof byte[][]) {
                    byte[][] numberMatrix = (byte[][]) matrix;
                    byte[] numberArray = new byte[shape[0]];
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                } else if (matrix instanceof short[][]) {
                    short[][] numberMatrix = (short[][]) matrix;
                    short[] numberArray = new short[shape[0]];
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                } else if (matrix instanceof int[][]) {
                    int[][] numberMatrix = (int[][]) matrix;
                    int[] numberArray = new int[shape[0]];
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                } else if (matrix instanceof long[][]) {
                    long[][] numberMatrix = (long[][]) matrix;
                    long[] numberArray = new long[shape[0]];
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                } else if (matrix instanceof float[][]) {
                    float[][] numberMatrix = (float[][]) matrix;
                    float[] numberArray = new float[shape[0]];
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                } else if (matrix instanceof double[][]) {
                    double[][] numberMatrix = (double[][]) matrix;
                    double[] numberArray = new double[shape[0]];
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                } else if (matrix instanceof Number[][]) {
                    Number[][] numberMatrix = (Number[][]) matrix;
                    Number[] numberArray = (Number[]) Array.newInstance(dataType, shape[0]);
                    for (int y = 0; y < numberArray.length; y++) {
                        numberArray[y] = numberMatrix[y][0];
                    }
                    array = numberArray;
                }
            }
        }
        return array;
    }

    @Override
    protected Object extractArrayFromFlatMatrix(Object flatMatrix, int width, int height) {
        Object array = null;
        Class<?> dataType = ArrayUtils.recoverDataType(flatMatrix);
        if (ObjectUtils.isNumberClass(dataType) && (width > 0) && (height > 0)) {
            if (flatMatrix instanceof byte[]) {
                byte[] numberFlatMatrix = (byte[]) flatMatrix;
                byte[] numberArray = new byte[height];
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            } else if (flatMatrix instanceof short[]) {
                short[] numberFlatMatrix = (short[]) flatMatrix;
                short[] numberArray = new short[height];
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            } else if (flatMatrix instanceof int[]) {
                int[] numberFlatMatrix = (int[]) flatMatrix;
                int[] numberArray = new int[height];
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            } else if (flatMatrix instanceof long[]) {
                long[] numberFlatMatrix = (long[]) flatMatrix;
                long[] numberArray = new long[height];
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            } else if (flatMatrix instanceof float[]) {
                float[] numberFlatMatrix = (float[]) flatMatrix;
                float[] numberArray = new float[height];
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            } else if (flatMatrix instanceof double[]) {
                double[] numberFlatMatrix = (double[]) flatMatrix;
                double[] numberArray = new double[height];
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            } else if (flatMatrix instanceof Number[]) {
                Number[] numberFlatMatrix = (Number[]) flatMatrix;
                Number[] numberArray = (Number[]) Array.newInstance(dataType, height);
                for (int y = 0; y < height; y++) {
                    numberArray[y] = numberFlatMatrix[y * width];
                }
                array = numberArray;
            }
        }
        return array;
    }

}
