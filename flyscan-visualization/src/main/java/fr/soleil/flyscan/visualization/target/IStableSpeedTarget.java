package fr.soleil.flyscan.visualization.target;

import fr.soleil.data.target.ITarget;

public interface IStableSpeedTarget extends ITarget {

    public double getStableSpeedStart();

    public void setStableSpeedStart(double start);

    public double getStableSpeedEnd();

    public void setStableSpeedEnd(double end);

    public void setStableSpeedBounds(double start, double end);

}
