package fr.soleil.flyscan.visualization.target;

import java.util.Properties;

import javax.swing.SwingUtilities;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.flyscan.visualization.util.SavePropertiesWorker;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link ITextTarget} that receives a property and transmits its configuration equivalent to flyscanvisu, as
 * described in SCAN-875.
 * 
 * @author GIRARDOT
 */
public class ConfigPropertyTarget extends ABeanContainerTarget implements ITextTarget {

    public ConfigPropertyTarget(FlyScanVisualizationTangoBox bean) {
        super(bean);
    }

    @Override
    public String getText() {
        // not managed
        return null;
    }

    @Override
    public void setText(String text) {
        FlyScanVisualizationTangoBox bean = ObjectUtils.recoverObject(beanRef);
        if ((bean != null) && (text != null) && (!text.trim().isEmpty())
                && !StringScalarBox.DEFAULT_ERROR_STRING.equals(text)) {
            String[] lines = text.split(ObjectUtils.NEW_LINE);
            if (lines != null) {
                Properties properties = new Properties();
                for (String line : lines) {
                    String[] keyValue = line.split(EQUALS);
                    if ((keyValue != null) && (keyValue.length == 2) && (keyValue[0] != null)
                            && (keyValue[1] != null)) {
                        String key = keyValue[0].trim().toLowerCase();
                        String value = keyValue[1].trim();
                        switch (key) {
                            case CARTOGRAPHY_IMAGES_CONFIG_PROP_KEY:
                                properties.put(CARTOGRAPHY_IMAGES_PROP_KEY, value);
                                break;
                            case CARTOGRAPHY_X_CONFIG_PROP_KEY:
                                properties.put(CARTOGRAPHY_X_PROP_KEY, value);
                                break;
                            case CARTOGRAPHY_Y_CONFIG_PROP_KEY:
                                properties.put(CARTOGRAPHY_Y_PROP_KEY, value);
                                break;
                            case MOTOR_X_POSITION_CONFIG_PROP_KEY:
                                properties.put(MOTOR_X_POSITION_PROP_KEY, value);
                                break;
                            case MOTOR_Y_POSITION_CONFIG_PROP_KEY:
                                properties.put(MOTOR_Y_POSITION_PROP_KEY, value);
                                break;
                            default:
                                // nothing to do
                                break;
                        }
                    } // end if ((keyValue != null) && (keyValue.length == 2) && (keyValue[0] != null) &&
                      // (keyValue[1] != null))
                } // end for (String line : lines)
                if (bean.applyProperties(properties)) {
                    new SavePropertiesWorker(bean).execute();
                    properties.put(FLYSCAN_VIEWER_DEVICE_KEY, bean.getModel());
                    properties.put(CARTOGRAPHY_IMAGES_PROP_KEY, bean.getCartographyImage());
                    properties.put(CARTOGRAPHY_X_PROP_KEY, bean.getCartographyX());
                    properties.put(CARTOGRAPHY_Y_PROP_KEY, bean.getCartographyY());
                    properties.put(MOTOR_X_POSITION_PROP_KEY, bean.getXMotorPositionProperty());
                    properties.put(MOTOR_Y_POSITION_PROP_KEY, bean.getYMotorPositionProperty());
                    bean.getPropertiesEditor().setProperties(properties);
                    SwingUtilities.invokeLater(() -> {
                        bean.disconnectCartographyImages();
                        bean.disconnectCartographyXScale();
                        bean.disconnectCartographyYScale();
                        bean.disconnectXMotor();
                        bean.disconnectYMotor();
                        bean.connectCartographyImages();
                        bean.connectCartographyXScale();
                        bean.connectCartographyYScale();
                        bean.connectXMotor();
                        bean.connectYMotor();
                        bean.upateUIIfFirstConnection();
                    });
                }
            } // end if (lines != null)
        } // end if ((text != null) && (!text.trim().isEmpty()) &&
          // !StringScalarBox.DEFAULT_ERROR_STRING.equals(text))
    }

}
