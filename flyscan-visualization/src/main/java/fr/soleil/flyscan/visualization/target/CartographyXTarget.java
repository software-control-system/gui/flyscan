package fr.soleil.flyscan.visualization.target;

import java.lang.reflect.Array;

import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.flyscan.visualization.FlyScanVisualizationTangoBox;
import fr.soleil.flyscan.visualization.view.image.FlyScanImageTransformationPanel;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * The {@link CartographyScaleTarget} dedicated in X scale.
 * <p>
 * This one will update the speed stability limits too.
 * </p>
 * 
 * @author GIRARDOT
 */
public class CartographyXTarget extends CartographyScaleTarget {

    public CartographyXTarget(ArrayPositionConvertor convertor, FlyScanImageTransformationPanel transformationPanel,
            FlyScanVisualizationTangoBox box, String connectionErrorKey) {
        super(convertor, transformationPanel, box, connectionErrorKey);
    }

    @Override
    protected String getAxisSource() {
        FlyScanVisualizationTangoBox box = ObjectUtils.recoverObject(beanRef);
        return box == null ? null : box.getCartographyX();
    }

    @Override
    protected void updateAxisTitle(String title, FlyScanImageTransformationPanel transformationPanel) {
        if (transformationPanel != null) {
            transformationPanel.setXScale(true, title);
        }
    }

    @Override
    protected Object extractArrayFromMatrix(Object[] matrix) {
        Object array = null;
        int[] shape = ArrayUtils.recoverShape(matrix);
        if ((shape != null) && (shape.length == 2) && ObjectUtils.isNumberClass(ArrayUtils.recoverDataType(matrix))) {
            array = matrix[0];
        }
        return array;
    }

    @Override
    protected Object extractArrayFromFlatMatrix(Object flatMatrix, int width, int height) {
        Object array = null;
        Class<?> dataClass = ArrayUtils.recoverDataType(flatMatrix);
        if (ObjectUtils.isNumberClass(dataClass) && (width > -1) && (height > -1)) {
            array = Array.newInstance(dataClass, width);
            System.arraycopy(flatMatrix, 0, array, 0, width);
        }
        return array;
    }

    @Override
    protected void applyArrayValue(ArrayPositionConvertor convertor, Object array) {
        super.applyArrayValue(convertor, array);
        FlyScanVisualizationTangoBox box = ObjectUtils.recoverObject(beanRef);
        if (box != null) {
            box.updateSpeedStability(NumberArrayUtils.extractDoubleArray(array));
        }
    }

}
