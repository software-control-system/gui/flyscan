package fr.soleil.flyscan.visualization.view;

import java.awt.Dimension;

import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import fr.soleil.comete.swing.Label;
import fr.soleil.flyscan.visualization.util.FSVUtilities;
import fr.soleil.lib.project.awt.SizeUtils;

public class AdaptedSizeLabel extends Label {

    private static final long serialVersionUID = -2028315343811227197L;

    protected static final Border DEFAULT_BORDER = new EmptyBorder(0, 5, 0, 5);

    public AdaptedSizeLabel() {
        super();
        setBorder(DEFAULT_BORDER);
        setFont(FSVUtilities.DEFAULT_FONT_BOLD);
    }

    @Override
    public Dimension getPreferredSize() {
        return SizeUtils.getAdaptedPreferredSize(this, super.getPreferredSize());
    }
}
